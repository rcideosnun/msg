module.exports = {
    'env': {
        'suitescript/suitescript1': true,
        'suitescript/suitescript2': true,
        'node': true
    },
    'plugins': ['suitescript'],
    'extends': ['eslint:recommended', 'plugin:suitescript/all'],
    'parserOptions': {
        'ecmaVersion': 6
    },
    'rules': {
        "suitescript/log-args": "off",
        "suitescript/no-log-module": "warn",
        "suitescript/module-vars": "warn",
        "suitescript/script-type": "warn",
        "suitescript/no-module-extensions": "warn",
        "no-unused-vars": "warn",
        "no-undef": "warn",
        "no-redeclare": "warn",
        "no-prototype-builtins": "warn",
        "no-cond-assign": "warn",
        "no-constant-condition": "warn",
        "no-global-assign": "warn",
        "no-unreachable": "warn",
        "no-empty": "warn",
    },
    'ignorePatterns': [
        "moment-timezone-with-data.js",
        "moment.js"
    ]
};

