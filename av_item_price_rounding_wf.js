/**
 *@NApiVersion 2.1  
 *@NScriptType WorkflowActionScript
 */
define([], function() {

    function onAction(scriptContext) { 
        try {
            let listNames = ['price1','price2','price4','price5'];
            let context = scriptContext.newRecord;
            listNames.forEach(element => {
                roundValues(context, element);
            });   
        } 
        catch (error) {
            
        }

    }

    function roundValues(context, listName){
        let lineCount = context.getLineCount(listName);

        for (let i=0; i< lineCount; i++)
        {
            context.selectLine({
                sublistId: listName,
                line: i
            });

            let value = context.getCurrentMatrixSublistValue({
                sublistId: listName,
                fieldId: 'price',
                column: 0,
            });
            if (value && value > 0){
                let decimalPlaces = countDecimals(value);
                if (decimalPlaces > 2)
                {
                    let roundedValue =  Math.round((value + Number.EPSILON) * 100) / 100;
                    context.setCurrentMatrixSublistValue({
                        sublistId: listName,
                        fieldId: 'price',
                        column: 0,
                        value: roundedValue
                    });
                }
                context.commitLine({
                    sublistId: listName
                });

            }

            

        } 
        //     context.save();



    }
    function countDecimals (value) {
        if(Math.floor(value) === value) return 0;
        return value.toString().split('.')[1].length || 0;
    }

    return {
        onAction: onAction
    };
});
