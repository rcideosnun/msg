/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tehseen Ahmed
* Date: 19/7/2022
* RMA Process
* @NApiVersion 2.1
* @NScriptType WorkflowActionScript
*/

define(['N/record', 'N/runtime', 'N/search', 'N/redirect', 'SuiteScripts/RMA/av_rma_process_lib.js', '../pimCoreIntegration/av_int_log.js'],
  function (record, runtime, search, redirect, rmaHelper, logLib) {
    function onAction(context) {
      log.audit({ title: 'onAction', details: 'triggered' });
      var contextRec = context.newRecord;
      var returnReason = runtime.getCurrentScript().getParameter({
        name: 'custscript_av_rma_reason'
      })

      if (handleManualProcess(contextRec)) return;

      var fulfilledItems = getFulfilledItems(contextRec.getValue({
        fieldId: 'createdfrom'
      }));
      if (returnReason == '1' || returnReason == '2' || returnReason == '3') {
        var itemReceiptId = createItemReceipt(contextRec, returnReason, fulfilledItems);
        log.audit({ title: 'itemReceiptId', details: itemReceiptId });
      }

      // Disabled because not need at this point.
      // if (returnReason == '4') {
      //   if (vendor && vendor != null && vendor != '') {
      //     var itemReceiptId = createItemReceipt(contextRec, returnReason, fulfilledItems);
      //     log.audit({ title: 'itemReceiptId', details: itemReceiptId });
      //     var vendorReturnId = createVendorRMA(contextRec);
      //     log.audit({ title: 'vendorReturnId', details: vendorReturnId });
      //     updateCustomerReturn(contextRec.id, vendorReturnId);
      //   } else {
      //     log.error({ title: 'Please select a vendor', details: 'Please select a vendor in AV Return Vendor field.' });
      //   }
      // }
    }

    function handleManualProcess(contextRec) {
      var isManualIA = runtime.getCurrentScript().getParameter({
        name: 'custscript_av_rma_manual_ia'
      })

      var isManualTOrder = runtime.getCurrentScript().getParameter({
        name: 'custscript_av_rma_manual_torder'
      })

      var isManualSO = runtime.getCurrentScript().getParameter({
        name: 'custscript_av_rma_manual_sorder'
      })

      if (isManualIA) {
        var iaId = rmaHelper.handleInvAdj(contextRec);
        if (iaId) {
          redirectToRecord(record.Type.INVENTORY_ADJUSTMENT, iaId)
        }
        return true;
      }

      if (isManualTOrder) {
        var orderId = rmaHelper.handleTransferOrder(contextRec);
        if (orderId) {
          redirectToRecord(record.Type.TRANSFER_ORDER, orderId)
        }
        return true;
      }

      if (isManualSO) {
        var orderId = rmaHelper.createSalesOrder(contextRec);
        if (orderId) {
          redirectToRecord(record.Type.SALES_ORDER, orderId)
        }
        return true;
      }
      return false;
    }

    function redirectToRecord(type, id) {
      redirect.toRecord({
        type: type,
        id: id,
        isEditMode: true,
        parameters: {
          'isManual': true
        }
      });
    }

    function createItemReceipt(contextRec, returnReason, fulfilledItems) {
      try {
        var itemReceiptRecord = record.transform({
          fromType: record.Type.RETURN_AUTHORIZATION,
          fromId: contextRec.id,
          toType: record.Type.ITEM_RECEIPT,
          isDynamic: true,
        });

        itemReceiptRecord.setValue({
          fieldId: 'custbody_av_ir_return_reason',
          value: returnReason
        });

        var itemLineCount = itemReceiptRecord.getLineCount({
          sublistId: 'item'
        });

        for (var i = 0; i < itemLineCount; i++) {
          itemReceiptRecord.selectLine({
            sublistId: 'item',
            line: i
          });

          var itemReceiptItem = itemReceiptRecord.getCurrentSublistValue({
            sublistId: 'item',
            fieldId: 'item'
          });

          var itemSerialObject = fulfilledItems.find(function (fulfilledItem) {
            return fulfilledItem.item == itemReceiptItem;
          })

          if (!!itemSerialObject && itemSerialObject.serials.length > 1) {
            // redirect
            redirect.toRecordTransform({
              fromType: record.Type.RETURN_AUTHORIZATION,
              fromId: contextRec.id,
              toType: record.Type.ITEM_RECEIPT,
              parameters: {
                custbody_av_ir_return_reason: returnReason
              }
            });
          } else {
            itemReceiptRecord.setCurrentSublistValue({
              sublistId: 'item',
              fieldId: 'location',
              value: '6'
            });

            itemReceiptRecord.setCurrentSublistValue({
              sublistId: 'item',
              fieldId: 'quantity',
              value: itemReceiptRecord.getCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'quantityremaining'
              })
            });

            if (!!itemSerialObject && !!itemSerialObject.serials && itemSerialObject.serials.length > 0) {
              var invDetailRecord = itemReceiptRecord.getCurrentSublistSubrecord({
                sublistId: 'item',
                fieldId: 'inventorydetail'
              });

              invDetailRecord.selectNewLine({ sublistId: 'inventoryassignment' });

              invDetailRecord.setCurrentSublistValue({
                sublistId: 'inventoryassignment',
                fieldId: 'receiptinventorynumber',
                line: 1,
                value: itemSerialObject.serials[0]
              });

              invDetailRecord.setCurrentSublistValue({
                sublistId: 'inventoryassignment',
                fieldId: 'quantity',
                line: 1,
                value: itemSerialObject.quantity
              });

              invDetailRecord.commitLine({ sublistId: 'inventoryassignment' });
            }

            itemReceiptRecord.commitLine({
              sublistId: 'item'
            })
          }
        }

        return itemReceiptRecord.save({
          enableSourcing: true,
          ignoreMandatoryFields: true
        });

      } catch (ex) {
        log.error({ title: 'createItemReceipt : ex', details: ex });
        logLib.error_message({
          msg: 'RMA0001',
          transaction: contextRec.id,
          var1: ex.message ? ex.message : JSON.stringify(ex)
        });
      }
    }

    function getFulfilledItems(createdFrom) {
      var fulfilledItems = [];
      var fulfillmentId = getFulfillment(createdFrom);
      var fulfillmentRecord = record.load({
        type: record.Type.ITEM_FULFILLMENT,
        id: fulfillmentId
      });

      var itemLineCount = fulfillmentRecord.getLineCount({
        sublistId: 'item'
      });

      for (var i = 0; i < itemLineCount; i++) {
        var item = {
          item: fulfillmentRecord.getSublistValue({
            sublistId: 'item',
            fieldId: 'item',
            line: i
          }),
          quantity: fulfillmentRecord.getSublistValue({
            sublistId: 'item',
            fieldId: 'quantity',
            line: i
          }),
          serials: []
        };

        var objSubRecord = fulfillmentRecord.getSublistSubrecord({
          sublistId: 'item',
          fieldId: 'inventorydetail',
          line: i
        });

        var inventoryDetailCount = objSubRecord.getLineCount({
          sublistId: 'inventoryassignment'
        });

        if (inventoryDetailCount > 0) {
          for (var j = 0; j < item.quantity; j++) {
            item.serials.push(
              objSubRecord.getSublistText({
                sublistId: 'inventoryassignment',
                fieldId: 'issueinventorynumber',
                line: j
              })
            );
          }
        }

        fulfilledItems.push(item);
      }

      return fulfilledItems;
    }

    function getFulfillment(salesOrder) {
      var fulfillment = '';
      var fulfillmentSearch = search.create({
        type: search.Type.ITEM_FULFILLMENT,
        filters: [
          ['createdfrom', 'anyof', salesOrder]
        ],
        columns: []
      });

      var searchResults = fulfillmentSearch.run();
      var results = searchResults.getRange({
        start: 0,
        end: 1
      });

      for (var i in results) {
        if (!!results[i].id) {
          fulfillment = results[i].id;
          break;
        }
      };

      return fulfillment;
    }

    function createVendorRMA(contextRec) {
      try {
        var vendorReturnRecord = record.create({
          type: record.Type.VENDOR_RETURN_AUTHORIZATION,
          isDynamic: true,
        });

        vendorReturnRecord.setValue({
          fieldId: 'entity',
          value: contextRec.getValue('custbody_av_return_vendor')
        });

        vendorReturnRecord.setValue({
          fieldId: 'custbody_av_return_transaction',
          value: contextRec.id
        });

        var itemLineCount = contextRec.getLineCount({
          sublistId: 'item'
        });

        for (var i = 0; i < itemLineCount; i++) {
          vendorReturnRecord.selectNewLine({
            sublistId: 'item',
            line: i
          });

          vendorReturnRecord.setCurrentSublistValue({
            sublistId: 'item',
            fieldId: 'item',
            value: contextRec.getSublistValue({
              sublistId: 'item',
              fieldId: 'item',
              line: i
            })
          });

          vendorReturnRecord.setCurrentSublistValue({
            sublistId: 'item',
            fieldId: 'location',
            value: '5'
          });


          vendorReturnRecord.setCurrentSublistValue({
            sublistId: 'item',
            fieldId: 'quantity',
            value: contextRec.getSublistValue({
              sublistId: 'item',
              fieldId: 'quantity',
              line: i
            })
          });

          vendorReturnRecord.commitLine({
            sublistId: 'item'
          })
        }

        return vendorReturnRecord.save({
          enableSourcing: true,
          ignoreMandatoryFields: true
        });

      } catch (ex) {
        log.error({ title: 'createVendorRMA : ex', details: ex });
      }
    }

    function updateCustomerReturn(customerReturnId, vendorReturnId) {
      try {
        record.submitFields({
          type: record.Type.RETURN_AUTHORIZATION,
          id: customerReturnId,
          values: {
            'custbody_av_return_transaction': vendorReturnId
          }
        })
      } catch (ex) {
        log.error({ title: 'updateCustomerReturn : ex', details: ex });
      }
    }

    return {
      onAction: onAction
    }
  }
)