/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tehseen Ahmed
* Date: 17/8/2022
* RMA Process
* @NApiVersion 2.1
* @NScriptType UserEventScript
*/

define(['N/record', 'N/workflow', 'SuiteScripts/RMA/av_rma_process_lib.js'], function (record, workflow, rmaHelper) {
  function beforeLoad(context) {
    if (context.type === context.UserEventType.CREATE) {
      var contextRec = context.newRecord;
      var returnReason = contextRec.getValue({
        fieldId: 'custbody_av_ir_return_reason'
      });

      if (!returnReason || returnReason == '') {
        returnReason = !!context.request && !!context.request.parameters
          && context.request.parameters.custbody_av_ir_return_reason;

        if (!!returnReason) {
          contextRec.setValue({
            fieldId: 'custbody_av_ir_return_reason',
            value: returnReason
          });

          var itemLineCount = contextRec.getLineCount({
            sublistId: 'item'
          });

          for (var i = 0; i < itemLineCount; i++) {
            contextRec.setSublistValue({
              sublistId: 'item',
              fieldId: 'location',
              value: '6',
              line: i
            });
          }
        }
      }
    }
  }

  function afterSubmit(context) {
    try {
      if (context.type === context.UserEventType.CREATE) {
        var contextRec = record.load({
          type: record.Type.ITEM_RECEIPT,
          id: context.newRecord.id,
          isDynamic: true
        });

        var createdFrom = contextRec.getText({
          fieldId: 'createdfrom'
        });

        var returnReason = contextRec.getValue({
          fieldId: 'custbody_av_ir_return_reason'
        });

        var createdFromId = contextRec.getValue({
          fieldId: 'createdfrom'
        });
        if (!!createdFrom && createdFrom.indexOf('RMA') > -1) {
          var creditMemoId = null;
          var transferOrderId = '';
          log.audit({ title: 'returnReason', details: returnReason });
          if (returnReason == '1') {
            creditMemoId = rmaHelper.createCreditMemo(contextRec, returnReason);
            log.audit({ title: 'creditMemoId', details: creditMemoId });
            transferOrderId = rmaHelper.createTransferOrder(contextRec.id);
            log.audit({ title: 'transferOrderId', details: transferOrderId });

          } else if (returnReason == '2' || returnReason == '3') {
            creditMemoId = rmaHelper.createCreditMemo(contextRec, returnReason);
            log.audit({ title: 'creditMemoId', details: creditMemoId });

            var invAdjLines = rmaHelper.getItemsForIA(contextRec, returnReason);
            log.audit({ title: 'invAdjLines', details: invAdjLines });
            var invAdj = rmaHelper.createInvAdj(invAdjLines, contextRec, true);
            log.audit({ title: 'invAdj', details: invAdj });

            if (invAdj) {
              transferOrderId = rmaHelper.createTransferOrder(invAdj, true);
              log.audit({ title: 'transferOrderId', details: transferOrderId });
            }
          }

          // if (creditMemoId) {
          //   workflow.initiate({
          //     recordId: creditMemoId,
          //     recordType: record.Type.CREDIT_MEMO,
          //     workflowId: 'customworkflow57'
          //   });
          // }

          if (!!transferOrderId && (returnReason == '1' || returnReason == '2' || returnReason == '3')) {
            var fulfillmentId = rmaHelper.createItemFulfillment(transferOrderId, createdFromId);
            log.audit({ title: 'fulfillmentId', details: fulfillmentId });
          }
        }
      }
    } catch (ex) {
      log.error({ title: 'afterSubmit : ex', details: ex });
    }
  }

  return {
    beforeLoad: beforeLoad,
    afterSubmit: afterSubmit
  }
})