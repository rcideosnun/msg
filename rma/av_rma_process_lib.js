/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tehseen Ahmed
* Date: 1/9/2022
* RMA Process Library
*/

define(['N/log', 'N/record', 'N/search', '../pimCoreIntegration/av_int_log.js'], function (log, record, search, logLib) {

  function handleInvAdj(contextRec) {
    var itemReceiptId = getTransaction(contextRec.id, search.Type.ITEM_RECEIPT, 'createdfrom');
    if (!itemReceiptId || itemReceiptId == '') {
      logLib.error_message({
        msg: 'RMA0013',
        transaction: contextRec.id,
        var1: 'Item Receipt: ' + null
      });
      return;
    }
    var itemReceipt = record.load({
      type: record.Type.ITEM_RECEIPT,
      id: itemReceiptId,
      isDynamic: true
    })
    var invAdjLines = getItemsForIA(itemReceipt);
    log.audit({ title: 'invAdjLines', details: invAdjLines });
    return createInvAdj(invAdjLines, itemReceipt);
  }

  function getItemsForIA(contextRec, returnReason) {
    var lines = { oitems: [] };
    if (returnReason) {
      lines.bitems = [];
    }
    var itemLineCount = contextRec.getLineCount({
      sublistId: 'item'
    });

    for (var i = 0; i < itemLineCount; i++) {
      contextRec.selectLine({
        sublistId: 'item',
        line: i
      });
      var item = contextRec.getCurrentSublistValue({
        sublistId: 'item',
        fieldId: 'item',
        line: i
      });

      var hasSubrecord = contextRec.hasCurrentSublistSubrecord({
        sublistId: 'item',
        fieldId: 'inventorydetail'
      });

      var serials = [];
      if (hasSubrecord) {
        var objSubRecord = contextRec.getCurrentSublistSubrecord({
          sublistId: 'item',
          fieldId: 'inventorydetail',
          line: i
        });

        serials = getSerialNumbers(objSubRecord);
      }

      lines.oitems.push({
        item: item,
        quantity: contextRec.getCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'quantity',
          line: i
        }),
        location: '6',
        serials: serials
      });

      if (returnReason) {
        lines.bitems.push({
          item: getBItem(item, returnReason, contextRec.id),
          oitem: item,
          quantity: contextRec.getCurrentSublistValue({
            sublistId: 'item',
            fieldId: 'quantity',
            line: i
          }),
          location: '6',
          serials: serials
        });
      }
    }

    return lines;
  }

  function getBItem(itemId, returnReason, rmaId) {
    var bItem = '';
    var filters = [
      ['parent', 'anyof', itemId],
      'AND'
    ];
    switch (returnReason) {
      case '2':
        filters.push(['custitem_av_item_bware', 'is', true])
        break;
      case '3':
        filters.push(['custitem_av_item_cware', 'is', true])
        break;
    }

    var itemSearch = search.create({
      type: search.Type.ITEM,
      filters: filters,
      columns: []
    });

    var searchResults = itemSearch.run();
    var results = searchResults.getRange({
      start: 0,
      end: 1
    });

    for (var i in results) {
      if (!!results[i].id) {
        bItem = results[i].id;
        break;
      }
    };

    if (bItem == '') {
      bItem = createBItem(itemId, returnReason, rmaId);
    }

    return bItem;
  }

  function createBItem(itemId, returnReason, rmaId) {
    try {
      var recordType = getRecordType(itemId);
      var originItem = record.load({
        type: recordType,
        id: itemId
      })

      var itemRecord = record.copy({
        type: recordType,
        id: itemId,
        isDynamic: true
      })

      var string = '';
      switch (returnReason) {
        case '2':
          string = 'b';
          itemRecord.setValue({
            fieldId: 'custitem_av_item_bware',
            value: true
          });
          break;
        case '3':
          string = 'c';
          itemRecord.setValue({
            fieldId: 'custitem_av_item_cware',
            value: true
          });
          break;
      }

      itemRecord.setValue({
        fieldId: 'custitem_msg_product_type',
        value: '3'
      });

      itemRecord.setValue({
        fieldId: 'upccode',
        value: ''
      });

      itemRecord.setValue({
        fieldId: 'custitem_av_is_store_online',
        value: false
      });

      itemRecord.setValue({
        fieldId: 'itemid',
        value: string + originItem.getValue({
          fieldId: 'itemid'
        })
      });

      itemRecord.setValue({
        fieldId: 'displayname',
        value: originItem.getValue({
          fieldId: 'displayname'
        })
      });

      var parentItem = originItem.getValue({
        fieldId: 'parent'
      })

      itemRecord.setValue({
        fieldId: 'parent',
        value: parentItem ? parentItem : itemId
      });

      return itemRecord.save({
        enableSourcing: true,
        ignoreMandatoryFields: true
      });
    } catch (ex) {
      log.error({ title: 'createBItem : ex', details: ex });
      logLib.error_message({
        msg: 'RMA0008',
        transaction: rmaId,
        var1: ex.message ? ex.message : JSON.stringify(ex)
      });
    }
  }

  function getRecordType(itemId) {
    var recordType = '';
    var recordTypeSearch = search.create({
      type: search.Type.ITEM,
      filters: [
        ['internalid', 'anyof', itemId]
      ],
      columns: []
    });

    var searchResults = recordTypeSearch.run();
    var results = searchResults.getRange({
      start: 0,
      end: 1
    });

    for (var i in results) {
      if (!!results[i].recordType) {
        recordType = results[i].recordType;
        break;
      }
    };

    return recordType;
  }

  function getSerialNumbers(objSubRecord, isValue) {
    var serials = [];
    var inventoryDetailCount = objSubRecord.getLineCount({
      sublistId: 'inventoryassignment'
    });

    if (inventoryDetailCount > 0) {
      for (var j = 0; j < inventoryDetailCount; j++) {
        serials.push({
          serial: isValue ? objSubRecord.getSublistValue({
            sublistId: 'inventoryassignment',
            fieldId: 'receiptinventorynumber',
            line: j
          }) : objSubRecord.getSublistText({
            sublistId: 'inventoryassignment',
            fieldId: 'receiptinventorynumber',
            line: j
          }),
          quantity: objSubRecord.getSublistValue({
            sublistId: 'inventoryassignment',
            fieldId: 'quantity',
            line: j
          })
        });
      }
    }
    return serials;
  }

  function createInvAdj(lines, contextRec, includeBItems) {
    try {
      var IARecord = record.create({
        type: record.Type.INVENTORY_ADJUSTMENT,
        isDynamic: true
      });

      IARecord.setValue({
        fieldId: 'subsidiary',
        value: contextRec.getValue({
          fieldId: 'subsidiary'
        })
      });

      // Need to decide
      IARecord.setValue({
        fieldId: 'account',
        value: 214
      });

      IARecord.setValue({
        fieldId: 'memo',
        value: 'inventory adjustment for ' + contextRec.getText({
          fieldId: 'createdfrom'
        })
      });

      IARecord.setValue({
        fieldId: 'custbody_av_return_transaction',
        value: contextRec.getValue({
          fieldId: 'createdfrom'
        })
      });

      lines.oitems.forEach(function (item) {
        addLineItem(IARecord, item)
      })

      if (includeBItems) {
        lines.bitems.forEach(function (item) {
          addLineItem(IARecord, item, true)
        })
      }

      return IARecord.save({
        enableSourcing: true,
        ignoreMandatoryFields: true
      });
    } catch (ex) {
      log.error({ title: 'createInventoryAdjustment : ex', details: ex });
      logLib.error_message({
        msg: 'RMA0006',
        transaction: contextRec.id,
        var1: ex.message ? ex.message : JSON.stringify(ex)
      });
    }
  }

  function addLineItem(IARecord, item, isBItem) {
    IARecord.selectNewLine({
      sublistId: 'inventory'
    });

    IARecord.setCurrentSublistValue({
      sublistId: 'inventory',
      fieldId: 'item',
      value: item.item
    });

    IARecord.setCurrentSublistValue({
      sublistId: 'inventory',
      fieldId: 'location',
      value: item.location
    });

    IARecord.setCurrentSublistValue({
      sublistId: 'inventory',
      fieldId: 'adjustqtyby',
      value: isBItem ? item.quantity : -item.quantity
    });

    // The field has been changed to a source field from item parent. Mo need to set it manually.
    // if (!!item.oitem) {
    //   IARecord.setCurrentSublistValue({
    //     sublistId: 'inventory',
    //     fieldId: 'custcol_av_original_item',
    //     value: item.oitem
    //   });
    // }

    if (item.serials && item.serials.length > 0) {
      var invDetailRecord = IARecord.getCurrentSublistSubrecord({
        sublistId: 'inventory',
        fieldId: 'inventorydetail'
      });

      item.serials.forEach(function (serial) {
        invDetailRecord.selectNewLine({ sublistId: 'inventoryassignment' });

        invDetailRecord.setCurrentSublistValue({
          sublistId: 'inventoryassignment',
          fieldId: 'receiptinventorynumber',
          value: serial.serial
        });

        invDetailRecord.setCurrentSublistValue({
          sublistId: 'inventoryassignment',
          fieldId: 'quantity',
          value: isBItem ? serial.quantity : -serial.quantity
        });

        invDetailRecord.commitLine({ sublistId: 'inventoryassignment' });
      })
    }

    IARecord.commitLine({ sublistId: 'inventory' });
  }

  function handleTransferOrder(contextRec) {
    var isIA = false;
    var tranId = getTransaction(contextRec.id, search.Type.INVENTORY_ADJUSTMENT, 'custbody_av_return_transaction');
    if (tranId) {
      isIA = true;
    } else {
      tranId = getTransaction(contextRec.id, search.Type.ITEM_RECEIPT, 'createdfrom');
    }
    if (!tranId || tranId == '') {
      logLib.error_message({
        msg: 'RMA0014',
        transaction: contextRec.id,
        var1: 'Transaction: ' + null
      });
      return;
    }
    return createTransferOrder(tranId, isIA, contextRec.id);
  }

  function getTransaction(rmaId, type, field) {
    var transaction = '';
    var transactionSearch = search.create({
      type: type,
      filters: [
        [field, 'anyof', rmaId]
      ],
      columns: []
    });

    var searchResults = transactionSearch.run();
    var results = searchResults.getRange({
      start: 0,
      end: 1
    });

    for (var i in results) {
      if (!!results[i].id) {
        transaction = results[i].id;
        break;
      }
    };

    return transaction;
  }

  function createTransferOrder(tranId, isIA, rmaId) {
    try {
      var transactionRecord = record.load({
        type: isIA ? record.Type.INVENTORY_ADJUSTMENT : record.Type.ITEM_RECEIPT,
        id: tranId,
        isDynamic: true
      })

      var transferOrderRecord = record.create({
        type: record.Type.TRANSFER_ORDER,
        isDynamic: true
      });

      transferOrderRecord.setValue({
        fieldId: 'subsidiary',
        value: transactionRecord.getValue({
          fieldId: 'subsidiary'
        })
      });

      transferOrderRecord.setValue({
        fieldId: 'location',
        value: '6'
      });

      transferOrderRecord.setValue({
        fieldId: 'transferlocation',
        value: '3'
      });

      transferOrderRecord.setValue({
        fieldId: 'orderstatus',
        value: 'B'
      });

      transferOrderRecord.setValue({
        fieldId: 'custbody_av_return_transaction',
        value: isIA ? transactionRecord.getValue({
          fieldId: 'custbody_av_return_transaction'
        }) : transactionRecord.getValue({
          fieldId: 'createdfrom'
        })
      });

      var itemLineCount = transactionRecord.getLineCount({
        sublistId: isIA ? 'inventory' : 'item'
      });

      for (var i = 0; i < itemLineCount; i++) {
        if (isIA) {
          var originItem = transactionRecord.getSublistValue({
            sublistId: isIA ? 'inventory' : 'item',
            fieldId: 'custcol_av_original_item',
            line: i
          })
          if (!!originItem) {
            addItemToTransferOrder(transferOrderRecord, transactionRecord, isIA, i)
          }
        } else {
          addItemToTransferOrder(transferOrderRecord, transactionRecord, isIA, i)
        }
      }

      var toLineItemCount = transferOrderRecord.getLineCount({
        sublistId: 'item'
      });

      if (toLineItemCount) {
        return transferOrderRecord.save({
          enableSourcing: true,
          ignoreMandatoryFields: true
        });
      } else {
        logLib.error_message({
          msg: 'RMA0007',
          transaction: rmaId,
          var1: 'Inventory Adjustment: ' + transactionRecord.getValue('tranid')
        });
        return;
      }

    } catch (ex) {
      log.error({ title: 'createTransferOrder : ex', details: ex });
      logLib.error_message({
        msg: 'RMA0005',
        transaction: rmaId,
        var1: ex.message ? ex.message : JSON.stringify(ex)
      });
    }
  }

  function addItemToTransferOrder(transferOrderRecord, transactionRecord, isIA, i) {
    transferOrderRecord.selectNewLine({
      sublistId: 'item',
      line: i
    });

    transactionRecord.selectLine({
      sublistId: isIA ? 'inventory' : 'item',
      line: i
    });

    transferOrderRecord.setCurrentSublistValue({
      sublistId: 'item',
      fieldId: 'item',
      value: transactionRecord.getCurrentSublistValue({
        sublistId: isIA ? 'inventory' : 'item',
        fieldId: 'item',
        line: i
      })
    });

    transferOrderRecord.setCurrentSublistValue({
      sublistId: 'item',
      fieldId: 'quantity',
      value: transactionRecord.getCurrentSublistValue({
        sublistId: isIA ? 'inventory' : 'item',
        fieldId: 'quantity',
        line: i
      })
    });

    var hasSubrecord = transactionRecord.getCurrentSublistValue({
      sublistId: isIA ? 'inventory' : 'item',
      fieldId: 'inventorydetailreq',
      line: i
    });

    if (hasSubrecord == 'T') {
      var itemReceiptSubRecord = transactionRecord.getCurrentSublistSubrecord({
        sublistId: isIA ? 'inventory' : 'item',
        fieldId: 'inventorydetail',
        line: i
      });

      var serials = getSerialNumbers(itemReceiptSubRecord, true);
      log.audit({ title: 'serials', details: serials });
      if (serials.length > 0) {
        var transferOrderSubRecord = transferOrderRecord.getCurrentSublistSubrecord({
          sublistId: 'item',
          fieldId: 'inventorydetail',
          line: i
        });

        serials.forEach(function (serial) {
          transferOrderSubRecord.selectNewLine({ sublistId: 'inventoryassignment' });

          transferOrderSubRecord.setCurrentSublistValue({
            sublistId: 'inventoryassignment',
            fieldId: 'receiptinventorynumber',
            value: serial.serial
          });

          transferOrderSubRecord.setCurrentSublistValue({
            sublistId: 'inventoryassignment',
            fieldId: 'quantity',
            value: serial.quantity
          });

          transferOrderSubRecord.commitLine({ sublistId: 'inventoryassignment' });
        })
      }
    }

    transferOrderRecord.commitLine({
      sublistId: 'item'
    })
  }

  function createSalesOrder(contextRec) {
    try {
      var salesOrder = record.create({
        type: record.Type.SALES_ORDER,
        isDynamic: true
      });
  
      salesOrder.setValue({
        fieldId: 'entity',
        value: contextRec.getValue({
          fieldId: 'entity'
        })
      });
  
      salesOrder.setValue({
        fieldId: 'memo',
        value: 'Kostenvoranschlag basierend auf ' + contextRec.getValue({
          fieldId: 'tranid'
        })
      });
  
      salesOrder.setValue({
        fieldId: 'custbody_av_return_transaction',
        value: contextRec.id
      });
  
      salesOrder.setValue({
        fieldId: 'custbody_av_msg_payment_method',
        value: 13
      });
  
      salesOrder.selectNewLine({
        sublistId: 'item',
        line: 0
      });
  
      salesOrder.setCurrentSublistValue({
        sublistId: 'item',
        fieldId: 'item',
        value: 1736
      });
  
      salesOrder.setCurrentSublistValue({
        sublistId: 'item',
        fieldId: 'quantity',
        value: 1
      });
  
      salesOrder.setCurrentSublistValue({
        sublistId: 'item',
        fieldId: 'amount',
        value: 1
      });
  
      salesOrder.commitLine({
        sublistId: 'item'
      })
  
      return salesOrder.save({
        enableSourcing: true,
        ignoreMandatoryFields: true
      });
    } catch (ex) {
      log.error({ title: 'createSalesOrder : ex', details: ex });
      logLib.error_message({
        msg: 'RMA0003',
        transaction: contextRec.id,
        var1: ex.message ? ex.message : JSON.stringify(ex)
      });
    }
  }

  function createCreditMemo(contextRec, returnReason) {
    try {
      var creditDeduction = getCreditDeduction(returnReason);
      var creditMemoRecord = record.transform({
        fromType: record.Type.RETURN_AUTHORIZATION,
        fromId: contextRec.getValue({
          fieldId: 'createdfrom'
        }),
        toType: record.Type.CREDIT_MEMO,
        isDynamic: true,
      });

      var itemLineCount = creditMemoRecord.getLineCount({
        sublistId: 'item'
      });

      for (var i = 0; i < itemLineCount; i++) {
        creditMemoRecord.selectLine({
          sublistId: 'item',
          line: i
        });

        var rate = creditMemoRecord.getCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'grossamt'
        })

        creditMemoRecord.setCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'price',
          value: '-1'
        });

        creditDeduction = parseFloat(creditDeduction);

        creditMemoRecord.setCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'grossamt',
          value: (returnReason != '1' && creditDeduction > 0) ? rate - ((rate / 100) * creditDeduction) : rate
        });

        creditMemoRecord.commitLine({
          sublistId: 'item'
        })
      }

      return creditMemoRecord.save({
        enableSourcing: true,
        ignoreMandatoryFields: true
      });

    } catch (ex) {
      log.error({ title: 'createCreditMemo : ex', details: ex });
      logLib.error_message({
        msg: 'RMA0002',
        transaction: contextRec.id,
        var1: ex.message ? ex.message : JSON.stringify(ex)
      });
    }
  }

  function getCreditDeduction(returnReason) {
    var creditDeduction = '';
    var creditDeductionSearch = search.create({
      type: 'customrecord_av_return_reasons_credit',
      filters: [
        ['custrecord_av_ret_res_reason', 'anyof', returnReason]
      ],
      columns: ['custrecord_av_ret_res_credit']
    });

    var searchResults = creditDeductionSearch.run();
    var results = searchResults.getRange({
      start: 0,
      end: 1
    });

    for (var i in results) {
      if (!!results[i].id) {
        creditDeduction = results[i].getValue('custrecord_av_ret_res_credit');
        break;
      }
    };

    return creditDeduction;
  }

  function createItemFulfillment(transferOrderId, createdFromId) {
    try {
      var itemFulfillment = record.transform({
        fromType: record.Type.TRANSFER_ORDER,
        fromId: transferOrderId,
        toType: record.Type.ITEM_FULFILLMENT,
        isDynamic: true,
      });

      itemFulfillment.setValue({
        fieldId: 'shipstatus',
        value: 'C'
      });

      itemFulfillment.setValue({
        fieldId: 'custbody_av_return_transaction',
        value: createdFromId
      });

      var itemLineCount = itemFulfillment.getLineCount({
        sublistId: 'item'
      });

      for (var i = 0; i < itemLineCount; i++) {
        itemFulfillment.selectLine({
          sublistId: 'item',
          line: i
        });

        itemFulfillment.setCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'quantity',
          value: itemFulfillment.getCurrentSublistValue({
            sublistId: 'item',
            fieldId: 'quantityremaining'
          })
        });

        itemFulfillment.commitLine({
          sublistId: 'item'
        })
      }

      return itemFulfillment.save({
        enableSourcing: true,
        ignoreMandatoryFields: true
      });

    } catch (ex) {
      log.error({ title: 'createItemFulfillment : ex', details: ex });
      logLib.error_message({
        msg: 'RMA0004',
        transaction: createdFromId,
        var1: ex.message ? ex.message : JSON.stringify(ex)
      });
    }
  }

  return {
    handleInvAdj: handleInvAdj,
    handleTransferOrder: handleTransferOrder,
    createSalesOrder: createSalesOrder,
    getItemsForIA: getItemsForIA,
    createInvAdj: createInvAdj,
    createTransferOrder: createTransferOrder,
    createCreditMemo: createCreditMemo,
    createItemFulfillment: createItemFulfillment
  }
})