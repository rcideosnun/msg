/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tehseen Ahmed
* Date: 2/8/2022
* Validate PO Item Line
* @NApiVersion 2.1
* @NScriptType ClientScript
*/

define(['N/record'],
  function (record) {
    function validateLine(context) {
      var currentRecord = context.currentRecord;
      var sublistName = context.sublistId;
      log.debug({ title: 'sublistName', details: sublistName });

      if (sublistName === 'item') {
        var purchasePrice = currentRecord.getCurrentSublistValue({
          sublistId: sublistName,
          fieldId: 'custcol_av_item_purchase_price'
        });
        if (!purchasePrice || purchasePrice == '') {
          alert('Der Artikel hat keinen Einkaufspreis, bitte kontaktieren Sie die Einkaufsabteilung.')
          return false;
        }
      }

      return true;
    }
    return {
      validateLine: validateLine
    }
  })