/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tehseen Ahmed
* Date: 2/9/2022
* Display alert if coming from RMA
* @NApiVersion 2.1
* @NScriptType ClientScript
*/

define(['N/record'], function (record) {
  function pageInit(context) {
    var currentUrl = document.location.href;
    var url = new URL(currentUrl);
    var isManual = url.searchParams.get('isManual');

    if (isManual) {
      alert('Bitte überprüfen Sie den Kostenvoranschlag und ergänzen Sie den Betrag im Artikel');
    }

    return true;
  }
  return {
    pageInit: pageInit
  }
})