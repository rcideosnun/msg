/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.x
 ** @author: [Tuba Mohsin]
 ** @Description: 
 */
define(['N/record', 'N/search', 'N/task', 'N/format', '../pimCoreIntegration/av_int_log.js'], function (record, search, task, format, avLog) {
    /**
     *
     * @param {Object} context
     * @returns
     */
    const STATUS_MAP = {
        'picked': 'A',
        'packed': 'B',
        'shipped': 'C'
    }


    const copyTransferOrder = (oldTransferOrderList) => {
        const newTransferOrderList = [];
        var oldId = null;
        oldTransferOrderList.forEach(oldTO => {
            try {
                oldId = oldTO.id;

                const recordObj = record.load({
                    type: record.Type.TRANSFER_ORDER,
                    id: oldId,
                });
                log.audit('recordObj', recordObj);
                const transOrder = record.create({
                    type: record.Type.TRANSFER_ORDER,
                });

                // const transOrder = record.copy({
                //     type: record.Type.TRANSFER_ORDER,
                //     id: oldTO.id,
                //     isDynamic: true,
                // });

                // const tranDate = oldTO.trandate;
                transOrder.setValue('externalid', oldId);
                transOrder.setValue('trandate', format.parse({ value: recordObj.getValue('trandate'), type: format.Type.DATE }));
                transOrder.setValue('subsidiary', recordObj.getValue('subsidiary'));
                transOrder.setValue('incoterm', recordObj.getValue('incoterm'));
                transOrder.setValue('location', recordObj.getValue('location'));
                transOrder.setValue('transferlocation', recordObj.getValue('transferlocation'));
                transOrder.setValue('memo', recordObj.getValue('memo'));

                transOrder.setValue('shipdate', format.parse({ value: recordObj.getValue('shipdate'), type: format.Type.DATE }));
                transOrder.setValue('shipcarrier', recordObj.getValue('shipcarrier'));
                transOrder.setValue('shipmethod', recordObj.getValue('shipmethod'));
                transOrder.setValue('shippingcost', recordObj.getValue('shippingcost'));
                transOrder.setValue('orderstatus', 'B');

                const linkedRMA = recordObj.getValue('custbody_av_return_transaction');
                if (linkedRMA) {
                    transOrder.setValue('custbody_av_return_transaction', linkedRMA);
                    recordObj.setValue('custbody_av_return_transaction', '');
                }
                const lineCount = recordObj.getLineCount({ sublistId: 'item' });

                for (let x = 0; x < lineCount; x++) {
                    const itemId = recordObj.getSublistValue('item', 'item', x);
                    const qty = recordObj.getSublistValue('item', 'quantity', x);
                    const description = recordObj.getSublistValue('item', 'description', x);

                    transOrder.setSublistValue({
                        sublistId: "item",
                        fieldId: "item",
                        line: x,
                        value: itemId
                    });
                    transOrder.setSublistValue({
                        sublistId: "item",
                        fieldId: "quantity",
                        line: x,
                        value: qty
                    });
                    transOrder.setSublistValue({
                        sublistId: "item",
                        fieldId: "description",
                        line: x,
                        value: description
                    });
                    // transOrder.setSublistValue('item', 'quantity', qty, x);
                    // transOrder.setSublistValue('item', 'description', description, x);
                }

                log.audit('transOrder', transOrder);

                const transOrderId = transOrder.save();
                if (transOrderId) {
                    log.audit('Transfer Order created', transOrderId);
                    newTransferOrderList.push({
                        internalId: transOrderId,
                        externalId: oldTO.id
                    })

                    recordObj.setValue('externalid', transOrderId);
                    recordObj.setValue('custbody_av_corrected_to', transOrderId);
                    recordObj.save();
                }

            } catch (ex) {
                log.audit('error', ex);
                avLog.error_message({
                    transaction: oldId,
                    var1: `Error occurred while correcting TO: InternalID ${oldId}.`,
                    var2: JSON.stringify(ex)
                });
            }

        });

        if (newTransferOrderList.length > 0) {
            processRelatedRecords(newTransferOrderList);
        }
    }

    const processRelatedRecords = (transferOrderList) => {
        const externalTransferOrders = transferOrderList.map(i => i.externalId);
        const toFulfillmentsList = getFulfillments(externalTransferOrders);
        const toReceiptsList = getReceipts(externalTransferOrders);

        log.audit('toFulfillmentsList', toFulfillmentsList);
        log.audit('toReceiptsList', toReceiptsList);

        transferOrderList.forEach(transOrder => {
            try {

                if (toFulfillmentsList[transOrder.externalId]) {
                    createItemFulfillment({
                        internalId: transOrder.internalId,
                        externalId: transOrder.externalId,
                        toRelatedRecs: toFulfillmentsList[transOrder.externalId]
                    });
                }
                const IFCount = Object.keys(toFulfillmentsList[transOrder.externalId]).length;
                if (toReceiptsList[transOrder.externalId]) {
                    createItemReceipt({
                        internalId: transOrder.internalId,
                        externalId: transOrder.externalId,
                        toRelatedRecs: toReceiptsList[transOrder.externalId],
                        IFCount
                    });
                }
            } catch (ex) {
                log.audit('error', ex);
                avLog.error_message({
                    transaction: transOrder.internalId,
                    var1: `Error occurred while creating fulfilments/receipts for TO: InternalID ${transOrder.internalId}, ExternalID: ${transOrder.externalId}.`,
                    var2: JSON.stringify(ex)
                });
            }
        });

    }

    const createItemReceipt = (data) => {
        const IFCount = data.IFCount;
        let allRecLines = [];

        if (IFCount > 1) {
            Object.keys(data.toRelatedRecs).forEach(element => {
                allRecLines = [...allRecLines, ...data.toRelatedRecs[element].lines]
            })
        }

        log.audit('allRecLines', allRecLines);
        log.audit('data.toRelatedRecs', data.toRelatedRecs);

        Object.keys(data.toRelatedRecs).forEach(element => {
            const toReceipt = data.toRelatedRecs[element];
            const productLines = (IFCount > 1 && allRecLines.length > 0) ? allRecLines : toReceipt.lines;

            let itemReceipt = record.transform({
                fromType: record.Type.TRANSFER_ORDER,
                fromId: data.internalId,
                toType: record.Type.ITEM_RECEIPT,
                isDynamic: true
            });

            itemReceipt.setValue({ fieldId: 'trandate', value: format.parse({ value: toReceipt.receiptDate, type: format.Type.DATE }) });
            itemReceipt.setValue({ fieldId: 'postingperiod', value: toReceipt.receiptPeriod });
            itemReceipt.setValue({ fieldId: 'memo', value: toReceipt.receiptMemo });

            const lineItemCount = itemReceipt.getLineCount({
                sublistId: 'item'
            });

            if (lineItemCount == 0) {
                log.error(`No Valid Line item for TO: ${data.internalId}`, 'You must have at least one valid line item for this transaction.')
                return;
            }
            for (let i = 0; i < lineItemCount; i++) {
                itemReceipt.selectLine({
                    sublistId: 'item',
                    line: i
                });
                let itemId = itemReceipt.getCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'item'
                });
                let itemQty = itemReceipt.getCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'quantityremaining'
                });
                // let itemLine = productLines.find(line => (line.item == itemId && line.processed !== true && Number(line.qty) <= itemQty));
                // let itemIndex = productLines.findIndex(line => (line.item == itemId && line.processed !== true && Number(line.qty) <= itemQty));

                let { itemLine, itemIndex } = findItemLine(productLines, itemId, itemQty);
                if (itemLine && itemIndex >= 0) {
                    productLines[itemIndex].processed = true;
                    itemReceipt.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'quantity',
                        value: itemLine.qty
                    });
                    itemReceipt.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'itemreceive',
                        value: true
                    });
                } else {
                    itemReceipt.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'itemreceive',
                        value: false
                    });
                }
                itemReceipt.commitLine({
                    sublistId: 'item'
                });
            }
            log.audit('P productLines', productLines);
            const receiptId = itemReceipt.save({
                enableSourcing: true,
                ignoreMandatoryFields: true
            });
            log.audit('item Receipt created = ', receiptId);

            if (IFCount > 1) allRecLines = productLines;
        });
    }

    const createItemFulfillment = (data) => {
        Object.keys(data.toRelatedRecs).forEach(element => {
            const toFulfill = data.toRelatedRecs[element];
            const productLines = toFulfill.lines;

            let itemFulfill = record.transform({
                fromType: record.Type.TRANSFER_ORDER,
                fromId: data.internalId,
                toType: record.Type.ITEM_FULFILLMENT,
                isDynamic: true
            });


            if (toFulfill.pickedDate) itemFulfill.setValue({ fieldId: 'pickeddate', value: format.parse({ value: toFulfill.pickedDate, type: format.Type.DATE }) });
            if (toFulfill.packedDate) itemFulfill.setValue({ fieldId: 'packeddate', value: format.parse({ value: toFulfill.packedDate, type: format.Type.DATE }) });
            if (toFulfill.shippedDate) itemFulfill.setValue({ fieldId: 'shippeddate', value: format.parse({ value: toFulfill.shippedDate, type: format.Type.DATE }) });

            itemFulfill.setValue({ fieldId: 'shipstatus', value: STATUS_MAP[toFulfill.fulfillmentStatus] });
            itemFulfill.setValue({ fieldId: 'trandate', value: format.parse({ value: toFulfill.fulfillmentDate, type: format.Type.DATE }) });
            itemFulfill.setValue({ fieldId: 'postingperiod', value: toFulfill.fulfillmentPeriod });
            itemFulfill.setValue({ fieldId: 'memo', value: toFulfill.fulfillmentMemo });

            const lineItemCount = itemFulfill.getLineCount({
                sublistId: 'item'
            });

            if (lineItemCount == 0) {
                log.error(`No Valid Line item for TO: ${data.internalId}`, 'You must have at least one valid line item for this transaction.')
                return;
            }
            for (let i = 0; i < lineItemCount; i++) {
                itemFulfill.selectLine({
                    sublistId: 'item',
                    line: i
                });
                let itemId = itemFulfill.getCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'item'
                });
                let itemQty = itemFulfill.getCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'quantityremaining'
                });

                // let itemLine = productLines.find(line => (line.item == itemId && line.processed !== true && Number(line.qty) <= itemQty));
                // let itemIndex = productLines.findIndex(line => (line.item == itemId && line.processed !== true && Number(line.qty) <= itemQty));
                let { itemLine, itemIndex } = findItemLine(productLines, itemId, itemQty);

                if (itemLine && itemIndex >= 0) {
                    productLines[itemIndex].processed = true;
                    itemFulfill.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'quantity',
                        value: itemLine.qty
                    });
                } else {
                    itemFulfill.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'itemreceive',
                        value: false
                    });
                }
                itemFulfill.commitLine({
                    sublistId: 'item'
                });
            }

            const fulfillmentId = itemFulfill.save({
                enableSourcing: true,
                ignoreMandatoryFields: true
            });

            // if (fulfillmentId) {
            //     let transactionRec = record.load({
            //         id: fulfillmentId,
            //         type: record.Type.ITEM_FULFILLMENT,
            //     });
            //     log.audit('loading,', toFulfill.shippedDate);
            //     transactionRec.setText({ fieldId: 'shippeddate', value: '30.6.2023' });
            //     transactionRec.setValue({ fieldId: 'pickeddate', value: format.parse({ value: toFulfill.pickedDate, type: format.Type.DATE }) });
            //     transactionRec.setValue({ fieldId: 'memo', value: 'updated again' });
            //     transactionRec.save();
            // }
            log.audit('item fulfillment created = ', fulfillmentId);
        });
    }

    const findItemLine = (productLines, itemId, itemQty) => {
        let itemIndex = -1, itemLine = null;
        for (let x = 0; x < productLines.length; x++) {
            if (productLines[x].processed) continue;
            if (productLines[x].item !== itemId) continue;
            if (Number(productLines[x].qty) == itemQty) {
                itemIndex = x;
                itemLine = productLines[x];
                break;
            }
            if (Number(productLines[x].qty) < itemQty) {
                itemIndex = x;
                itemLine = productLines[x];
            }
        }
        return { itemLine, itemIndex }
    }

    const getFulfillments = (TOs) => {
        const toRelatedRecs = {};
        const itemfulfillmentSearchObj = search.create({
            type: "itemfulfillment",
            filters:
                [
                    ["type", "anyof", "ItemShip"],
                    "AND",
                    ["createdfrom.type", "anyof", "TrnfrOrd"],
                    "AND",
                    ["mainline", "is", "F"],
                    "AND",
                    ["shipping", "is", "F"],
                    "AND",
                    ["taxline", "is", "F"],
                    "AND",
                    ["item.isserialitem", "is", "F"],
                    "AND",
                    ["createdfrom", "anyof", TOs],
                    "AND",
                    ["account", "anyof", "216"]
                ],
            columns:
                [
                    search.createColumn({ name: "subsidiary", label: "Subsidiary" }),
                    search.createColumn({
                        name: "tranid",
                        sort: search.Sort.ASC,
                        label: "Document Number"
                    }),
                    search.createColumn({
                        name: "trandate",
                        sort: search.Sort.ASC,
                        label: "Date"
                    }),
                    search.createColumn({ name: "postingperiod", label: "Period" }),
                    search.createColumn({ name: "statusref", label: "Status" }),
                    search.createColumn({ name: "tranpickeddate", label: "Picked Date" }),
                    search.createColumn({ name: "tranpackeddate", label: "Packed Date" }),
                    search.createColumn({ name: "transhippeddate", label: "Actual Ship Date" }),
                    search.createColumn({
                        name: "internalid",
                        join: "item",
                        label: "Item Int ID"
                    }),
                    search.createColumn({ name: "item", label: "Item" }),
                    search.createColumn({ name: "quantity", label: "Quantity" }),
                    search.createColumn({ name: "location", label: "Source Location" }),
                    search.createColumn({ name: "transferlocation", label: "Destination Location" }),
                    search.createColumn({ name: "account", label: "Account" }),
                    search.createColumn({ name: "amount", label: "Amount" }),
                    search.createColumn({ name: "debitamount", label: "Amount (Debit)" }),
                    search.createColumn({ name: "creditamount", label: "Amount (Credit)" }),
                    search.createColumn({
                        name: "internalid",
                        join: "createdFrom",
                        label: "New T.O. Ext ID"
                    }),
                    search.createColumn({ name: "createdfrom", label: "Source T.O." }),
                    search.createColumn({
                        name: "statusref",
                        join: "createdFrom",
                        label: "Source T.O. Status"
                    }),
                    search.createColumn({ name: "memomain", label: "Memo (Main)" })
                ]
        });


        let myPagedData = itemfulfillmentSearchObj.runPaged({ pageSize: 1000 });

        myPagedData.pageRanges.forEach(function (pageRange) {
            let myPage = myPagedData.fetch({ index: pageRange.index });
            myPage.data.forEach(function (result) {
                let internalId = result.id;
                let externalId = result.getValue({
                    name: 'internalid',
                    join: 'createdFrom',
                });
                let fulfillmentId = result.getValue('tranid');
                let fulfillmentDate = result.getValue('trandate');
                let fulfillmentPeriod = result.getValue('postingperiod');
                let fulfillmentMemo = result.getValue('memomain');
                let fulfillmentStatus = result.getValue('statusref');
                let pickedDate = result.getValue('tranpickeddate');
                let packedDate = result.getValue('tranpackeddate');
                let shippedDate = result.getValue('transhippeddate');
                let item = result.getValue('item');
                let qty = result.getValue('quantity');


                if (!toRelatedRecs[externalId]) {
                    toRelatedRecs[externalId] = {
                        [fulfillmentId]: {
                            internalId,
                            fulfillmentDate,
                            fulfillmentPeriod,
                            fulfillmentMemo,
                            fulfillmentStatus,
                            pickedDate,
                            packedDate,
                            shippedDate,
                            lines: [{ item, qty, processed: false }]
                        }
                    }
                }
                else if (toRelatedRecs[externalId]) {
                    if (toRelatedRecs[externalId][fulfillmentId]) {
                        toRelatedRecs[externalId][fulfillmentId].lines.push({ item, qty, processed: false })
                    } else {
                        toRelatedRecs[externalId][fulfillmentId] = {
                            internalId,
                            fulfillmentDate,
                            fulfillmentPeriod,
                            fulfillmentMemo,
                            fulfillmentStatus,
                            pickedDate,
                            packedDate,
                            shippedDate,
                            lines: [{ item, qty, processed: false }]
                        }
                    }
                }

            });
        });

        log.audit('toRelatedRecs', toRelatedRecs);
        return toRelatedRecs;
    };

    const getReceipts = (TOs) => {
        const toRelatedRecs = {};
        const itemreceiptSearchObj = search.create({
            type: "itemreceipt",
            filters:
                [
                    ["type", "anyof", "ItemRcpt"],
                    "AND",
                    ["createdfrom.type", "anyof", "TrnfrOrd"],
                    "AND",
                    ["mainline", "is", "F"],
                    "AND",
                    ["shipping", "is", "F"],
                    "AND",
                    ["taxline", "is", "F"],
                    "AND",
                    ["item.isserialitem", "is", "F"],
                    "AND",
                    ["createdfrom", "anyof", TOs],
                    "AND",
                    ["formulatext: case when {transferlocation}={location} then 0 else 1 end", "is", "1"]
                ],
            columns:
                [
                    search.createColumn({ name: "subsidiary", label: "Subsidiary" }),
                    search.createColumn({
                        name: "tranid",
                        sort: search.Sort.ASC,
                        label: "Document Number"
                    }),
                    search.createColumn({
                        name: "trandate",
                        sort: search.Sort.ASC,
                        label: "Date"
                    }),
                    search.createColumn({ name: "postingperiod", label: "Period" }),
                    search.createColumn({
                        name: "internalid",
                        join: "item",
                        label: "Item Int ID"
                    }),
                    search.createColumn({ name: "item", label: "Item" }),
                    search.createColumn({ name: "quantity", label: "Quantity" }),
                    search.createColumn({ name: "transferlocation", label: "Source Location" }),
                    search.createColumn({ name: "location", label: "Destination Location" }),
                    search.createColumn({ name: "account", label: "Account" }),
                    search.createColumn({ name: "amount", label: "Amount" }),
                    search.createColumn({ name: "debitamount", label: "Amount (Debit)" }),
                    search.createColumn({ name: "creditamount", label: "Amount (Credit)" }),
                    search.createColumn({
                        name: "internalid",
                        join: "createdFrom",
                        label: "New T.O. Ext ID"
                    }),
                    search.createColumn({ name: "createdfrom", label: "Source T.O." }),
                    search.createColumn({
                        name: "statusref",
                        join: "createdFrom",
                        label: "Source T.O. Status"
                    }),
                    search.createColumn({ name: "memomain", label: "Memo (Main)" })
                ]
        });


        let myPagedData = itemreceiptSearchObj.runPaged({ pageSize: 1000 });
        myPagedData.pageRanges.forEach(function (pageRange) {
            let myPage = myPagedData.fetch({ index: pageRange.index });
            myPage.data.forEach(function (result) {
                let internalId = result.id;
                let externalId = result.getValue({
                    name: 'internalid',
                    join: 'createdFrom',
                });
                let receiptId = result.getValue('tranid');
                let receiptDate = result.getValue('trandate');
                let receiptPeriod = result.getValue('postingperiod');
                let receiptMemo = result.getValue('memomain');
                let receiptStatus = result.getValue('statusref');
                let item = result.getValue('item');
                let qty = result.getValue('quantity');


                if (!toRelatedRecs[externalId]) {
                    toRelatedRecs[externalId] = {
                        [receiptId]: {
                            internalId,
                            receiptDate,
                            receiptPeriod,
                            receiptMemo,
                            receiptStatus,
                            lines: [{ item, qty, processed: false }]
                        }
                    }
                }
                else if (toRelatedRecs[externalId]) {
                    if (toRelatedRecs[externalId][receiptId]) {
                        toRelatedRecs[externalId][receiptId].lines.push({ item, qty, processed: false })
                    } else {
                        toRelatedRecs[externalId][receiptId] = {
                            internalId,
                            receiptDate,
                            receiptPeriod,
                            receiptMemo,
                            receiptStatus,
                            lines: [{ item, qty, processed: false }]
                        }
                    }
                }

            });
        });
        return toRelatedRecs;
    };


    const closeTransferOrder = (transferOrderList) => {
        const transferOrderIds = transferOrderList.map(i => i.id);
        const toFulfillmentsList = getFulfillments(transferOrderIds);
        const toReceiptsList = getReceipts(transferOrderIds);
        var toProcessing = null;
        try {
            transferOrderIds.forEach(toId => {
                toProcessing = toId;
                log.audit('processing TO =', toId);
                Object.keys(toReceiptsList[toId]).forEach(itemRec => {
                    const irObj = toReceiptsList[toId][itemRec];
                    var irId = record.delete({
                        type: record.Type.ITEM_RECEIPT,
                        id: irObj.internalId,
                    });
                    log.audit('deleted irId', irId);
                });

                Object.keys(toFulfillmentsList[toId]).forEach(itemFul => {
                    const ifObj = toFulfillmentsList[toId][itemFul];
                    var ifId = record.delete({
                        type: record.Type.ITEM_FULFILLMENT,
                        id: ifObj.internalId,
                    });
                    log.audit('deleted ifId', ifId);
                });

                const toRec = record.load({ type: record.Type.TRANSFER_ORDER, id: toId });
                for (var i = 0; i < toRec.getLineCount('item'); i++) {
                    toRec.setSublistValue({ sublistId: 'item', fieldId: 'isclosed', line: i, value: true });
                }
                toRec.save();

            })
        }
        catch (ex) {
            log.audit('error', ex);
            avLog.error_message({
                transaction: toProcessing,
                var1: `Error occurred while deleting fulfilments/receipts for TO: InternalID ${toProcessing}.`,
                var2: JSON.stringify(ex)
            });
        }

    }



    return {
        copyTransferOrder,
        closeTransferOrder
    };
});
