/**
 ** Copyright (c) 2022 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 *
 * Author: Tuba Mohsin
 * Date: 14.08.2023
 * create fulfillments for TO
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */

define([
    "N/search",
    "N/task",
    "N/runtime",
    "N/record",
    "N/format",
    '../pimCoreIntegration/av_int_log.js'
], function (search, task, runtime, record, format, avLog) {
    const STATUS_MAP = {
        'picked': 'A',
        'packed': 'B',
        'shipped': 'C'

    }
    const TRANSACTION_TYPE = {
        ITEM_FULFILLMENT: 32,
        ITEM_RECEIPT: 16
    }

    const getInputData = () => {
        log.audit("getInputData", `-Script Started on ${new Date()}-`);
        let toRelatedRecs = null;
        try {
            const currentRuntime = runtime.getCurrentScript();
            const relatedRecord = currentRuntime.getParameter({ name: "custscript_av_to_corr_related_rec" });
            log.audit('relatedRecord', relatedRecord);
            if (!relatedRecord) {
                log.error('Related Record Param not selected');
                return;
            }

            if (relatedRecord == TRANSACTION_TYPE.ITEM_FULFILLMENT) {
                toRelatedRecs = getFulfillments();
            } else {
                toRelatedRecs = getReceipts();
            }

            if (!toRelatedRecs) return;

            return getTransferOrdersList(toRelatedRecs)
        } catch (ex) {
            log.error({ title: "getInputData : ex", details: ex });
        }
    };

    const map = (context) => {
        log.audit({ title: "map", details: 'triggered' });
        const data = JSON.parse(context.value);
        const currentRuntime = runtime.getCurrentScript();
        const relatedRecord = currentRuntime.getParameter({ name: "custscript_av_to_corr_related_rec" });

        try {
            if (relatedRecord == TRANSACTION_TYPE.ITEM_FULFILLMENT) {
                createItemFulfillment(data);
            } else {
                createItemReceipt(data);
            }

        } catch (ex) {
            log.audit('error', ex);
            // avLog.error_message({
            //     transaction: data.internalId,
            //     var1: `Error occurred while creating fulfilments for TO: InternalID ${data.internalId}, ExternalID: ${data.externalId}.`,
            //     var2: JSON.stringify(ex)
            // });
        }
    };


    const summarize = () => {
        log.audit("summarize", `-Script Finished on ${new Date()}-`);
    };

    const createItemReceipt = (data) => {
        log.audit('data', data);
        Object.keys(data.toRelatedRecs).forEach(element => {
            const toReceipt = data.toRelatedRecs[element];
            const productLines = toReceipt.lines;

            let itemReceipt = record.transform({
                fromType: record.Type.TRANSFER_ORDER,
                fromId: data.internalId,
                toType: record.Type.ITEM_RECEIPT,
                isDynamic: true
            });

            // itemReceipt.setValue({ fieldId: 'shipstatus', value: STATUS_MAP[toReceipt.fulfillmentStatus] });
            itemReceipt.setValue({ fieldId: 'trandate', value: format.parse({ value: toReceipt.receiptDate, type: format.Type.DATE }) });
            itemReceipt.setValue({ fieldId: 'postingperiod', value: toReceipt.receiptPeriod });
            itemReceipt.setValue({ fieldId: 'memo', value: toReceipt.receiptMemo });

            const lineItemCount = itemReceipt.getLineCount({
                sublistId: 'item'
            });

            if (lineItemCount == 0) {
                log.error(`No Valid Line item for TO: ${data.internalId}`, 'You must have at least one valid line item for this transaction.')
                return;
            }
            for (let i = 0; i < lineItemCount; i++) {
                itemReceipt.selectLine({
                    sublistId: 'item',
                    line: i
                });
                let itemId = itemReceipt.getCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'item'
                });
                let itemLine = productLines.find(line => line.item == itemId);
                log.audit('itemLine', itemLine);
                if (itemLine) {
                    itemReceipt.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'quantity',
                        value: itemLine.qty
                    });
                    itemReceipt.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'itemreceive',
                        value: true
                    });
                } else {
                    itemReceipt.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'itemreceive',
                        value: false
                    });
                }
                itemReceipt.commitLine({
                    sublistId: 'item'
                });
            }

            const receiptId = itemReceipt.save({
                enableSourcing: true,
                ignoreMandatoryFields: true
            });
            log.audit('item Receipt created = ', receiptId);
        });
    }

    const createItemFulfillment = (data) => {
        Object.keys(data.toRelatedRecs).forEach(element => {
            const toFulfill = data.toRelatedRecs[element];
            const productLines = toFulfill.lines;

            let itemFulfill = record.transform({
                fromType: record.Type.TRANSFER_ORDER,
                fromId: data.internalId,
                toType: record.Type.ITEM_FULFILLMENT,
                isDynamic: true
            });

            itemFulfill.setValue({ fieldId: 'shipstatus', value: STATUS_MAP[toFulfill.fulfillmentStatus] });
            itemFulfill.setValue({ fieldId: 'trandate', value: format.parse({ value: toFulfill.fulfillmentDate, type: format.Type.DATE }) });
            itemFulfill.setValue({ fieldId: 'postingperiod', value: toFulfill.fulfillmentPeriod });
            itemFulfill.setValue({ fieldId: 'memo', value: toFulfill.fulfillmentMemo });

            if (toFulfill.pickedDate) itemFulfill.setValue({ fieldId: 'pickeddate', value: format.parse({ value: toFulfill.pickedDate, type: format.Type.DATE }) });
            if (toFulfill.packedDate) itemFulfill.setValue({ fieldId: 'packeddate', value: format.parse({ value: toFulfill.packedDate, type: format.Type.DATE }) });
            if (toFulfill.shippedDate) itemFulfill.setValue({ fieldId: 'shippeddate', value: format.parse({ value: toFulfill.shippedDate, type: format.Type.DATE }) });

            log.audit('itemFulfill', itemFulfill);

            const lineItemCount = itemFulfill.getLineCount({
                sublistId: 'item'
            });

            if (lineItemCount == 0) {
                log.error(`No Valid Line item for TO: ${data.internalId}`, 'You must have at least one valid line item for this transaction.')
                return;
            }
            for (let i = 0; i < lineItemCount; i++) {
                itemFulfill.selectLine({
                    sublistId: 'item',
                    line: i
                });
                let itemId = itemFulfill.getCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'item'
                });
                let itemLine = productLines.find(line => line.item == itemId);
                log.audit('itemLine', itemLine);
                if (itemLine) {
                    itemFulfill.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'quantity',
                        value: itemLine.qty
                    });
                } else {
                    itemFulfill.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'itemreceive',
                        value: false
                    });
                }
                itemFulfill.commitLine({
                    sublistId: 'item'
                });
            }

            const fulfillmentId = itemFulfill.save({
                enableSourcing: true,
                ignoreMandatoryFields: true
            });
            log.audit('item fulfillment created = ', fulfillmentId);
        });
    }

    const getFulfillments = () => {
        const toRelatedRecs = {};
        const currentRuntime = runtime.getCurrentScript();
        let itemSearch = search.load({
            id: currentRuntime.getParameter({ name: "custscript_av_to_corr_full_list" }),
        });

        let myPagedData = itemSearch.runPaged({ pageSize: 1000 });

        myPagedData.pageRanges.forEach(function (pageRange) {
            let myPage = myPagedData.fetch({ index: pageRange.index });
            myPage.data.forEach(function (result) {
                let internalId = result.id;
                let externalId = result.getValue({
                    name: 'internalid',
                    join: 'createdFrom',
                });
                let fulfillmentId = result.getValue('tranid');
                let fulfillmentDate = result.getValue('trandate');
                let fulfillmentPeriod = result.getValue('postingperiod');
                let fulfillmentMemo = result.getValue('memomain');
                let fulfillmentStatus = result.getValue('statusref');
                let pickedDate = result.getValue('tranpickeddate');
                let packedDate = result.getValue('tranpackeddate');
                let shippedDate = result.getValue('transhippeddate');
                let item = result.getValue('item');
                let qty = result.getValue('quantity');


                if (!toRelatedRecs[externalId]) {
                    toRelatedRecs[externalId] = {
                        [fulfillmentId]: {
                            fulfillmentDate,
                            fulfillmentPeriod,
                            fulfillmentMemo,
                            fulfillmentStatus,
                            pickedDate,
                            packedDate,
                            shippedDate,
                            lines: [{ item, qty }]
                        }
                    }
                }
                else if (toRelatedRecs[externalId]) {
                    if (toRelatedRecs[externalId][fulfillmentId]) {
                        toRelatedRecs[externalId][fulfillmentId].lines.push({ item, qty })
                    } else {
                        toRelatedRecs[externalId][fulfillmentId] = {
                            fulfillmentDate,
                            fulfillmentPeriod,
                            fulfillmentMemo,
                            fulfillmentStatus,
                            pickedDate,
                            packedDate,
                            shippedDate,
                            lines: [{ item, qty }]
                        }
                    }
                }

            });
        });

        log.audit('toRelatedRecs', toRelatedRecs);
        return toRelatedRecs;
    };

    const getReceipts = () => {
        const toRelatedRecs = {};
        const currentRuntime = runtime.getCurrentScript();
        let itemSearch = search.load({
            id: currentRuntime.getParameter({ name: "custscript_av_to_corr_full_list" }),
        });

        let myPagedData = itemSearch.runPaged({ pageSize: 1000 });

        myPagedData.pageRanges.forEach(function (pageRange) {
            let myPage = myPagedData.fetch({ index: pageRange.index });
            myPage.data.forEach(function (result) {
                let internalId = result.id;
                let externalId = result.getValue({
                    name: 'internalid',
                    join: 'createdFrom',
                });
                let receiptId = result.getValue('tranid');
                let receiptDate = result.getValue('trandate');
                let receiptPeriod = result.getValue('postingperiod');
                let receiptMemo = result.getValue('memomain');
                let receiptStatus = result.getValue('statusref');
                let item = result.getValue('item');
                let qty = result.getValue('quantity');


                if (!toRelatedRecs[externalId]) {
                    toRelatedRecs[externalId] = {
                        [receiptId]: {
                            receiptDate,
                            receiptPeriod,
                            receiptMemo,
                            receiptStatus,
                            lines: [{ item, qty }]
                        }
                    }
                }
                else if (toRelatedRecs[externalId]) {
                    if (toRelatedRecs[externalId][receiptId]) {
                        toRelatedRecs[externalId][receiptId].lines.push({ item, qty })
                    } else {
                        toRelatedRecs[externalId][receiptId] = {
                            receiptDate,
                            receiptPeriod,
                            receiptMemo,
                            receiptStatus,
                            lines: [{ item, qty }]
                        }
                    }
                }

            });
        });

        log.audit('toRelatedRecs', toRelatedRecs);
        return toRelatedRecs;
    };

    const getTransferOrdersList = (relatedRecList) => {
        let newTO = []; // [{internalId, externalId, toRelatedRecs}]
        const currentRuntime = runtime.getCurrentScript();

        let itemSearch = search.load({
            id: currentRuntime.getParameter({ name: "custscript_av_to_corr_list" }),
        });

        let myPagedData = itemSearch.runPaged({ pageSize: 1000 });

        myPagedData.pageRanges.forEach(function (pageRange) {
            let myPage = myPagedData.fetch({ index: pageRange.index });
            myPage.data.forEach(function (result) {
                let internalId = result.id;
                let externalId = result.getValue('externalid');
                let toRelatedRecs = relatedRecList[externalId]

                newTO.push({ internalId, externalId, toRelatedRecs })
            });
        });


        return newTO;
    };

    return {
        getInputData: getInputData,
        map: map,
        summarize: summarize,
    };
});
