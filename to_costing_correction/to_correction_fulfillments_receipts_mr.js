/**
 ** Copyright (c) 2022 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 *
 * Author: Tuba Mohsin
 * Date: 14.08.2023
 * create fulfillments for TO
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */

define([
    "N/search",
    "N/task",
    "N/runtime",
    "N/record",
    "N/format",
    '../pimCoreIntegration/av_int_log.js',
    './to_correction_fulfillments_receipts_lib'
], function (search, task, runtime, record, format, avLog, lib) {
    const STATUS_MAP = {
        'picked': 'A',
        'packed': 'B',
        'shipped': 'C'

    }
    const TRANSACTION_TYPE = {
        ITEM_FULFILLMENT: 32,
        ITEM_RECEIPT: 16
    }

    const getInputData = () => {
        log.audit("getInputData", `-Script Started on ${new Date()}-`);
        try {
            return getTransferOrdersList()
        } catch (ex) {
            log.error({ title: "getInputData : ex", details: ex });
        }
    };

    const map = (context) => {
        log.audit({ title: "map", details: 'triggered' });
        const data = JSON.parse(context.value);
        log.audit('map data', data);
        try {
            lib.copyTransferOrder(data);
            lib.closeTransferOrder(data);
        } catch (ex) {
            log.audit('error', ex);
        }
    };


    const summarize = () => {
        log.audit("summarize", `-Script Finished on ${new Date()}-`);
    };

    const getTransferOrdersList = () => {
        let TOList = [];
        const currentRuntime = runtime.getCurrentScript();

        let itemSearch = search.load({
            id: currentRuntime.getParameter({ name: "custscript_av_to_corr_list" }),
        });

        let myPagedData = itemSearch.runPaged({ pageSize: 1000 });

        myPagedData.pageRanges.forEach(function (pageRange) {
            let myPage = myPagedData.fetch({ index: pageRange.index });
            myPage.data.forEach(function (result) {
                let internalId = result.id || result.getValue('internalid') || result.getValue({ name: "internalid", summary: "GROUP" });
                let tranDate = result.getValue('trandate') || result.getValue({ name: "trandate", summary: "GROUP" });
                TOList.push({ id: internalId, trandate: tranDate })
            });
        });

        let resultGroups = [];
        const size = 10;

        for (let i = 0; i < TOList.length; i += size) {
            resultGroups.push(TOList.slice(i, i + size));
        }
        log.audit('resultGroups', resultGroups);
        return resultGroups;
    };

    return {
        getInputData: getInputData,
        map: map,
        summarize: summarize,
    };
});
