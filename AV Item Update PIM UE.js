/**
* Copyright (c) 2023 Alta Via Consulting GmbH
* All Rights Reserved.
*
* This software is the confidential and proprietary information of
* Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
* such Confidential Information and shall use it only in accordance
* with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
* @author: [Polina Melnykova]
* @NApiVersion 2.1
* @NScriptType UserEventScript
* @description : Script for Item Update PIM
*/


define(['N/record'], function (record) {

    function beforeSubmit(context) {
        if (context.type == 'edit') {

            try {

                checkChangesForP01(context);
                checkChangesForP04(context);
                // var oldArr = extractValues(context.oldRecord);
                // var newArr = extractValues(context.newRecord);

                // var oldDelivery = extractDelivery(context.oldRecord);
                // var newDelivery = extractDelivery(context.newRecord);

                // if (comparation(oldArr.flat(1), newArr.flat(1))) {
                //     context.newRecord.setValue({
                //         fieldId: 'custitem_msg_pim_request_sync',
                //         value: true
                //     });
                // }

                // if (comparation(oldDelivery, newDelivery)) {
                //     context.newRecord.setValue({
                //         fieldId: 'custitem_msg_pim_request_sync_stock',
                //         value: true
                //     });
                // }


            } catch (e) {
                log.error('Error', e);
            }
        }


    };

    function checkChangesForP01(context) {
        var oldArr = extractValues(context.oldRecord);
        var newArr = extractValues(context.newRecord);
        if (comparation(oldArr.flat(1), newArr.flat(1))) {
            context.newRecord.setValue({
                fieldId: 'custitem_msg_pim_request_sync',
                value: true
            });
        }
    }

    function checkChangesForP04(context) {
        var oldValues = extractP04RelatedFields(context.oldRecord);
        var newValues = extractP04RelatedFields(context.newRecord);
        log.audit('item ID', context.newRecord.id);
        // log.audit('oldValues', oldValues);
        // log.audit('newValues', newValues);
        if (comparation(oldValues, newValues)) {
            context.newRecord.setValue({
                fieldId: 'custitem_msg_pim_request_sync_stock',
                value: true
            });
        }
    }

    function extractP04RelatedFields(rec) {
        var arr = [];
        arr.push(rec.getValue('custitem_msg_deliverytext'));
        arr.push(rec.getText('custitem_msg_deliverydate'));
        arr.push(rec.getValue('custitem_av_pim_product_inactive'));
        arr.push(rec.getValue('custitem_msg_item_status'));
        return arr;
    }

    function extractValues(rec) {
        var arr = [];
        arr.push(rec.getValue('itemid'));
        arr.push(rec.getValue('displayname'));
        arr.push(rec.getValue('upccode'));
        arr.push(rec.getValue('subsidiary'));
        arr.push(rec.getValue('cseg_msg_igt'));
        arr.push(rec.getValue('cseg_msg_brands'));
        arr.push(rec.getValue('taxschedule'));
        arr.push(rec.getValue('custitem_av_is_shop_online'));
        arr.push(rec.getValue('vendorname'));
        arr.push(rec.getValue('custitem_av_vendor_item_number'));
        arr.push(rec.getValue('custitem_msg_un_number'));
        arr.push(rec.getValue('custitem_msg_height'));
        arr.push(rec.getValue('custitem_msg_width'));
        arr.push(rec.getValue('custitem_msg_length'));
        arr.push(rec.getValue('weight'));
        arr.push(rec.getValue('custitem_av_ref_unit'));
        arr.push(rec.getValue('custitem_av_purchase_unit'));
        arr.push(rec.getValue('custitem_av_unit_key'));
        arr.push(rec.getValue('custitem_msg_product_type'));
        arr.push(rec.getValue('parent'));
        arr.push(rec.getValue('custitem_msg_main_product_kit'));
        arr.push(rec.getValue('custitem_av_pim_product_inactive'));



        var type = rec.type;
        if (type == 'kititem') {
            var mem = rec.getLineCount({
                sublistId: 'member'
            });
            for (var j = 0; j < mem; j++) {
                var memberId = rec.getSublistValue({
                    sublistId: 'member',
                    fieldId: 'item',
                    line: j
                });
                arr.push(memberId);
            };
        }
        return arr;
    }

    function comparation(a, b) {

        if (a.length != b.length) {
            return true;
        } else {
            for (var i = 0; i < a.length; i++) {
                if (a[i] != b[i]) {
                    return true;
                };
            }
        }
        return false;
    }


    return {
        beforeSubmit: beforeSubmit
    };
});