/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 ** @author: [Roberto Cideos]
 ** @dated: [27.07.2022]
 ** @Description: FTP Integration between NetSuite and BMD
 *
 *  @NApiVersion 2.1
 *  @NScriptType WorkflowActionScript
 */

define([
    'N/record',
    'N/runtime',
    'N/search',
    'N/sftp',
    'N/file',
    //'./av_bmd_file_lib_sv2.js',
    '../libs/av_sftp_lib_sv2.js',
    '../pimCoreIntegration/av_int_log.js',
    'N/encode'],
    function (record,
        runtime,
        search,
        sftp,
        file,
        //bmdFile,
        bmdLib,
        avLog,
        encode) {

        var self = this;

        var CONSTANTS = {
            INTERFACES: {
                "BMD1": {
                    onActionFunction: _sendTransaction,
                    sendFile: _sendFileToServer,
                    successfullyTransmitted: true,
                    updateFieldsOnSuccess: _updateTransactionFields
                }
            },
            HTTP_CODE: {
                SUCCESS: 200,
                ERROR: 401,
                TEST: "Test"
            },
            INTERFACE_STATE: {
                NEW: 1,
                DONE: 3,
                ERROR: 2
            },
            ERROR_MESSAGE: {
                CONNECTION_UNSUCCESSFUL: "BMDE01"
            }
        }


        function onAction(context) {

            var scriptObj = runtime.getCurrentScript()

            if (runtime.envType == "SANDBOX") {
                //setup sandbox global variables

                self.ifaceId = scriptObj.getParameter({ name: 'custscript_av_bmd_iface' });
                self.ifaceName = _lookUpName(scriptObj.getParameter({ name: 'custscript_av_bmd_iface' }));
                self.isTest = scriptObj.getParameter({ name: 'custscript_av_bmd_is_test' });
                self.outboundFolderId = scriptObj.getParameter({ name: 'custscript_av_bmd_out_folder' });
                self.outboundDirectory = scriptObj.getParameter({ name: 'custscript_av_bmd_ns_fid' });
                self.configId = scriptObj.getParameter({ name: 'custscript_av_bmd_config_rec' });
                self.currentRecord = context.newRecord;

            } else if (runtime.envType == "PRODUCTION") {
                //setup production global variables

                self.ifaceId = scriptObj.getParameter({ name: 'custscript_av_bmd_iface' });
                self.ifaceName = _lookUpName(scriptObj.getParameter({ name: 'custscript_av_bmd_iface' }));
                self.isTest = scriptObj.getParameter({ name: 'custscript_av_bmd_is_test' });
                self.outboundFolderId = scriptObj.getParameter({ name: 'custscript_av_bmd_out_folder' });
                self.outboundDirectory = scriptObj.getParameter({ name: 'custscript_av_bmd_ns_fid' });
                self.configId = scriptObj.getParameter({ name: 'custscript_av_bmd_config_rec' });
                self.currentRecord = context.newRecord;
            }

            self.credentials = bmdLib._getConfigObj(self.configId)

            return CONSTANTS.INTERFACES[self.ifaceName].onActionFunction(context)

        }

        function _sendTransaction(context) {

            self.logId = avLog.create({
                iface: self.ifaceName,
                state: CONSTANTS.INTERFACE_STATE.NEW,
                transaction: self.currentRecord.id,
                url: self.credentials.custrecord_av_integration_base_url
            })

            return CONSTANTS.INTERFACES[self.ifaceName].sendFile()

        }

        function _sendFileToServer() {

            var fileObj = file.load({
                id: self.currentRecord.getValue("custbody_av45_pdf_file_link")
            }) // THIS IS A TEST FILE, IGNORE IT!!!!

            try {

                var connectionObj = sftp.createConnection({
                    username: self.credentials.custrecord_av_integration_username,
                    passwordGuid: self.credentials.custrecord_av_integration_sftp_guid,
                    url: self.credentials.custrecord_av_integration_base_url,
                    port: 22,
                    hostKeyType: 'rsa',
                    hostKey: self.credentials.custrecord_av_integration_sftp_hkey
                });

                connectionObj.upload({
                    filename: fileObj.name,
                    file: fileObj,
                    replaceExisting: true
                });

                avLog.update({
                    id: self.logId,
                    responseCode: CONSTANTS.HTTP_CODE.SUCCESS,
                    state: CONSTANTS.INTERFACE_STATE.DONE
                })

                _attachFileToLog(fileObj.id)

                return CONSTANTS.INTERFACES[self.ifaceName].updateFieldsOnSuccess()

            } catch (e) {

                avLog.update({
                    id: self.logId,
                    responseCode: CONSTANTS.HTTP_CODE.ERROR,
                    state: CONSTANTS.INTERFACE_STATE.ERROR
                })

                avLog.error_message({
                    logid: self.logId,
                    var1: JSON.stringify(e),
                    msg: CONSTANTS.ERROR_MESSAGE.CONNECTION_UNSUCCESSFUL
                })

                log.debug("Error", JSON.stringify(e))

            }
        }

        function _updateTransactionFields() {

            self.currentRecord.setValue({
                fieldId: "custbody_av_msg_file_transmitted",
                value: CONSTANTS.INTERFACES[self.ifaceName].successfullyTransmitted
            })

        }

        function _lookUpName(id) {

            return search.lookupFields({
                type: "customrecord_av_interface_type",
                id: id,
                columns: "name"
            }).name || null

        }

        function _attachFileToLog(fileId) {

            return record.attach({
                record: {
                    type: 'file',
                    id: fileId
                },
                to: {
                    type: 'customrecord_av_integration_log',
                    id: self.logId
                }
            });

        }

        return {
            onAction: onAction
        };

    })