/**
 ** Copyright (c) 2015 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.0
 ** @author: Ons/Roberto
 ** @dated: 27.07.2022
 ** @Description: This library is used to generate pdf files to send to BMD
 * *@NApiVersion 2.1
 */

define(['N/record', 'N/render', 'N/file', 'N/search', 'N/format', '../libs/av_sftp_lib_sv2.js', 'N/runtime'],
    function (record, render, file, search, format, sftpConfig, runtime) {


        //in wfa makes more sense if config changes
        function _getBMDConfiguration() {

            return sftpConfig._getConfigObj(runtime.getCurrentScript().getParameter({ name: "custscript_av45_config_id" }))

        }

        function _getPDF(rec) {
            try {

                let recordType = rec.type;
                let objRecord = record.load({
                    type: recordType,
                    id: rec.id
                });
                let pdfFileLink = objRecord.getValue('custbody_av45_pdf_file_link')
                if (!pdfFileLink) {
                    let pdfSearchObject = search.create({
                        type: rec.type,
                        filters:
                            [
                                ["internalid", "anyof", rec.id],
                                "AND",
                                ["mainline", "is", "T"],
                                "AND",
                                ["shipping", "is", "F"],
                                "AND",
                                ["taxline", "is", "F"]
                            ],
                        columns:
                            [
                                search.createColumn({
                                    name: "name",
                                    join: "file"
                                }),
                                search.createColumn({
                                    name: "internalid",
                                    join: "file"
                                }),
                                search.createColumn({
                                    name: "created",
                                    join: "file",
                                    sort: search.Sort.ASC
                                })
                            ]
                    });
                    let searchResultCount = pdfSearchObject.runPaged().count;

                    if (searchResultCount > 0) {
                        let contentId;
                        pdfSearchObject.run().each(function (result) {
                            contentId = result.getValue({
                                name: "internalid",
                                join: "file"
                            });
                            let fileName = result.getValue({
                                name: "name",
                                join: "file"
                            })

                            if(!contentId) return;
                            
                            record.submitFields({
                                id: rec.id,
                                type: rec.type,
                                values: {
                                    'custbody_av45_pdf_file_link': contentId,
                                    'custbody_av45_bmd_file_url': `${_getBMDConfiguration().custrecord_av_integration_base_url}/${fileName}`
                                }
                            })
                        });
                        let fileObj = file.load(contentId);
                        return fileObj
                    }
                    else {
                        //add a note saying there's no file?
                        return;
                    }

                }
                else {
                    // add a note that field is already updated?
                    return;
                }
            } catch (e) {
                log.debug("Debug", JSON.stringify(e))
            }
        }

        /*use this function to generate the pdf file*/
        function _createPdfFile(rec) {
            let scriptObj = runtime.getCurrentScript();
            let objRecord = record.load({
                type: rec.type,
                id: rec.id
            });
            let pdfFileLink = objRecord.getValue('custbody_av45_pdf_file_link')
            let date = format.format({
                value: new Date(),
                type: format.Type.DATE
            })
            if (!pdfFileLink) {
                var content = render.transaction({
                    entityId: rec.id,
                    printMode: render.PrintMode.PDF
                });
                //could be added as a script param
                content.folder = scriptObj.getParameter({ name: "custscript_av45_folder_storage_id" });
                content.name = `${scriptObj.getParameter({ name: "custscript_av45_file_prefix" })}${rec.getValue('tranid')}-${date}.pdf`
                let contentId = content.save()
                if (contentId) {
                    record.submitFields({
                        type: rec.type,
                        id: rec.id,
                        values: {
                            'custbody_av45_pdf_file_link': contentId,
                            'custbody_av45_bmd_file_url': `sftp://${_getBMDConfiguration().custrecord_av_integration_base_url}/${content.name}`
                        }
                    })
                }
                else {
                    //CREATE AN ERROR RECORD
                    //createErrorRecord('No file found','error type',rec.id)
                }

                return content;

            }

        }
        return {
            _getPDF,
            _createPdfFile
        }

    })