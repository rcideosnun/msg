/**
** Copyright (c) 2020 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*@author: [Ons Sellami]
*@dated: [27.07.2022]
*@NApiVersion 2.1
 *@NScriptType WorkflowActionScript
 */
 define(['./av_bmd_file_lib_sv2.js'], function(bmdFileLib) {

    function onAction(scriptContext) {
        let currentRecord = scriptContext.newRecord;
        // let file = bmdFileLib._getPDF(currentRecord)
        let file = _getFunction(currentRecord)
        return file;
    }


    function _getFunction(currentRecord){
        let eventRouter = {
            'invoice' : bmdFileLib._createPdfFile,
            'cashsale' : bmdFileLib._createPdfFile,
            'creditmemo' : bmdFileLib._createPdfFile,
            'vendorbill' : bmdFileLib._getPDF,
            'vendorcredit' : bmdFileLib._getPDF,
            'Journal' : bmdFileLib._createPdfFile
        };
    
        if (typeof eventRouter[currentRecord.type] != 'function'){
            return;
        }
        else{
            return eventRouter[currentRecord.type](currentRecord);
        }
    }

    return {
        onAction: onAction
    }
});