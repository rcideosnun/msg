/** 
 * Copyright (c) 2019 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @author: [Polina Melnykova]
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 * @description : 
 * Spec: Duplicate Check for Vendor Name (Herstellerartikelnummer)
 */

define(['N/currentRecord', 'N/log', 'N/search', 'N/ui/dialog'],
    function (currentRecord, log, search, dialog) {

        function pageInit(context) {
            return true;
        }

        function fieldChanged(context) {

            try {

                var record = currentRecord.get();

                if (context.fieldId === 'vendorname' && record.getValue('vendorname')) {
                    if (checkVendorName(record)) {

                        dialog.alert({
                            title: "WARNING",
                            message: 'Es existiert bereits ein Artikel mit dieser Herstellerartikelnummer.'
                        });
                    }
                }

            } catch (e) {
                log.error("error", e)
            };

        };


        function checkVendorName(newItem) {

            var vendorName = newItem.getValue('vendorname');
            var currentId = newItem.getValue('internalid');
            var manufacturer = newItem.getValue('custitem_av_manufacturer');

            var itemSearchObj = search.create({

                type: "item",
                filters: filter = [

                    ["custitem_av_manufacturer", "anyof", manufacturer],
                    "AND",
                    ["vendorname", "is", vendorName],
                    "AND",
                    ["isinactive", "is", "F"]
                ],
                columns:
                    [
                        search.createColumn({ name: "internalid" })
                    ]
            });

            var sameNameItem = [];

            itemSearchObj.run().each(function (result) {
                if (!currentId || result.getValue('internalid') != currentId) {

                    sameNameItem.push(result.getValue('internalid'));
                }
                return true;
            });

            if (sameNameItem[0]) {
                return true;
            } else {
                return false;
            }

        };

        return {
            pageInit: pageInit,
            fieldChanged: fieldChanged
        };
    }
);