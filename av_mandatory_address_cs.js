/**
 *@NApiVersion 2.1
 *@NScriptType ClientScript
 */
define(['N/ui/message'], function(message) {

    function pageInit(context) {
        
    }

    function saveRecord(context) {
        let address = context.currentRecord.getValue('defaultaddress');
        let addressBookCount = context.currentRecord.getLineCount('addressbook');
        debugger;
        if (!address || address == '' || addressBookCount == 0)
        {
            let myMsg3 = message.create({
                title: 'Mandatory Field Missing',
                message: 'At least one address must be added! <br> Please add at least one location under Address tab',
                type: message.Type.WARNING,
                duration: 20000
            });
            myMsg3.show(); // will disappear after 20s
            return false;
        }
        else return true;

    }



    return {
        pageInit: pageInit,
        saveRecord: saveRecord,
       
    }
});
