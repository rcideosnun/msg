/** 
 * Copyright (c) 2019 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @author: [Polina Melnykova]
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 * @description : MapReduce script for Updating Item Prices from custom record
 * Spec: Updating Item Prices from custom record
 */

define(['N/record', 'N/search', 'N/runtime', './av_updating_item_prices_lib.js'],
    function (record, search, runtime, lib) {
        const RECORD_TYPE = {
            InvtPart: 'inventoryitem',
            Kit: 'kititem',
            GiftCert: 'giftcertificateitem',
            Service: 'serviceitem',
            Assembly: 'assemblyitem',
            Discount: 'discountitem'
        };
        function getInputData() {

            var currentSc = runtime.getCurrentScript();

            var itemSearch = search.load({
                id: currentSc.getParameter({ name: "custscript_av_price_update_item_search" }),
            });

            var idArr = [];
            var retArr = [];

            itemSearch = getAllResults(itemSearch);
            itemSearch.forEach(function (result) {
                
                if (!idArr.includes(result.id)) {
                    var arr = [result.id, result.getValue('type')];
                    idArr.push(result.id);
                    retArr.push(arr);
                }
                return true;
            });

            return retArr;
        }

        function reduce(context) {

            try {
                let value = JSON.parse(context.values);
                var itemId = value[0];
                var type = value[1];

                var updRec = record.load({
                    type: RECORD_TYPE[type],
                    id: itemId,
                    isDynamic: false
                });

                lib.update(updRec);

                updRec.save({
                    ignoreMandatoryFields: true
                });


            }
            catch (e) {
                log.error("error", e)
            }


        }

        function getAllResults(s) {
            var results = s.run();
            var searchResults = [];
            var searchid = 0;
            do {
                var resultslice = results.getRange({
                    start: searchid,
                    end: searchid + 1000,
                });
                resultslice.forEach(function (slice) {
                    searchResults.push(slice);
                    searchid++;
                });
            } while (resultslice.length >= 1000); // >=1000
            return searchResults;
        }

        return {
            getInputData: getInputData,
            reduce: reduce,
        };
    });