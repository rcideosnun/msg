/**
 * Copyright (c) 2022 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @author: [Theodore Koukouves]
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 */
 define(['N/search','./oauth','./secret','N/https','./cryptojs'], function(search,oauth,secret,https,cryptojs) {

  function pageInit(context) {
    debugger;
      window.dranDrop = _dragNdRop
      let random = new Date().getTime()
      let pdf_input = document.querySelector('#pdf_data')
      pdf_input.name = `${pdf_input.name}_${random}`
      try{
          _dragNdRop()
      }catch(e){
          log.error(e.name,e.message)
      }
  }


  function _dragNdRop(){
      document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
          const dropZoneElement = inputElement.closest(".drop-zone");
        
          dropZoneElement.addEventListener("click", (e) => {
            inputElement.click();
          });
        
          inputElement.addEventListener("change", (e) => {
            if (inputElement.files.length) {
              updateThumbnail(dropZoneElement, inputElement.files[0]);
            }
          });
        
          dropZoneElement.addEventListener("dragover", (e) => {
            e.preventDefault();
            dropZoneElement.classList.add("drop-zone--over");
          });
        
          ["dragleave", "dragend"].forEach((type) => {
            dropZoneElement.addEventListener(type, (e) => {
              dropZoneElement.classList.remove("drop-zone--over");
            });
          });
        
          dropZoneElement.addEventListener("drop", (e) => {
            e.preventDefault();
        
            if (e.dataTransfer.files.length) {
              inputElement.files = e.dataTransfer.files;
              updateThumbnail(dropZoneElement, e.dataTransfer.files[0]);
              setup_reader(e.dataTransfer.files[0]);
            }

        
            dropZoneElement.classList.remove("drop-zone--over");
          });
        });
        
        /**
         * Updates the thumbnail on a drop zone element.
         *
         * @param {HTMLElement} dropZoneElement
         * @param {File} file
         */
        function updateThumbnail(dropZoneElement, file) {
          let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");
        
          if (dropZoneElement.querySelector(".drop-zone__prompt")) {
            dropZoneElement.querySelector(".drop-zone__prompt").remove();
          }
        
          // First time - there is no thumbnail element, so lets create it
          if (!thumbnailElement) {
            thumbnailElement = document.createElement("div");
            thumbnailElement.classList.add("drop-zone__thumb");
            dropZoneElement.appendChild(thumbnailElement);
          }
        
          thumbnailElement.dataset.label = file.name;
        
          // Show thumbnail for image files
          if (file.type.startsWith("image/")) {
            const reader = new FileReader();
        
            reader.readAsDataURL(file);
            reader.onload = () => {
              thumbnailElement.style.backgroundImage = `url('${reader.result}')`;
            };
          } else {
            thumbnailElement.style.backgroundImage = null;
          }
        }
  }


  function saveRecord(context){
      let input = document.querySelector('.drop-zone__input')
      let file = input.files[0]

      if(file){
        return true
      }else{
          alert('Please select a file to upload.')
          return false
      }
  }


  function setup_reader(file) {

      if (file.type.startsWith("application/pdf")) {
          const reader = new FileReader();
          let pdf_input = document.querySelector('#pdf_data')

          reader.readAsDataURL(file);
          reader.onload = () => {
              pdf_input.value = `${reader.result}`;
          };
        } else {
          pdf_input.value = ``;
        }
      
  }



  return {
      pageInit: pageInit,
      saveRecord:saveRecord,
  }
});
