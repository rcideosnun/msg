/**
 * Copyright (c) 2022 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @author: [Theodore Koukouves]
 * @dated: [8.2022]
 *@NApiVersion 2.1
 *@NScriptType Suitelet
 */
define(['N/ui/serverWidget','N/file','N/redirect','N/file','N/render','N/runtime','N/record','N/search','N/ui/message'], function(serverWidget,file,redirect,file,render,runtime,record,search,message) {

    function onRequest(context) {
        let request = context.request
        let response = context.response
        let params = request.parameters
        let httpMethod = request.method

        let methoHandler ={
            'GET':getMethod,
            'POST':postMethod
        }

        try{
            if(typeof(methoHandler[httpMethod]) === 'function'){
                methoHandler[httpMethod](request,response,params)
            }
        }catch(e){
            log.error(e.name,e.message)
        }
    }

    function getMethod(request,response,params){
        let newForm = generateSearchPage(params);
        let scriptUrl = "./av_drandrop_cs.js";
        if (scriptUrl) {
          try {
            newForm.clientScriptModulePath = scriptUrl;
          } catch (err) {
            log.error("Client Script ERROR", "Cannot find Script file.");
            newForm.addPageInitMessage({
              type: "ERROR",
              message:
                "Client script error - cannot find script file in current path. Page will not work properly - please refresh page or contact administrator of functionality.",
              duration: 15000,
            });
          }
        }
        newForm = _customMessages(newForm,params)
      
        response.writePage(newForm);
        return true;
    }

    function postMethod(request,response,params){
        let savePDF = savePDFFile(params)
        let postParams = generatePostParams(params,savePDF);
        redirect.toSuitelet({
          scriptId: request.parameters.script,
          deploymentId: request.parameters.deploy,
          parameters: postParams,
          isExternal :true
        });
        return true;
    }

    function _customMessages(newForm,params){
      if(params.custpage_pdfsaved ==='true'){
        newForm.addPageInitMessage({
          type: message.Type.CONFIRMATION,
          message:
            "File successfully uploaded.",
          duration: 5000,
        });
      }
      if(params.custpage_pdfsaved ==='false' || params.custpage_notfound ==='true'){
        newForm.addPageInitMessage({
          type: message.Type.ERROR,
          message:
            "Error uploading the file. File name should match the purchase order number in the system.",
          duration: 5000,
        });
      }
      return newForm
    }

    function generatePostParams(params,savePDF){
      let parameters = {}
 
        if(savePDF.attached){
          parameters.custpage_pdfsaved = true
        }else{
          parameters.custpage_pdfsaved = false
        }
        if(savePDF.custpage_notfound){
          parameters.custpage_notfound = true
        }else{
          parameters.custpage_notfound = false
        }
        return parameters
    }

    function savePDFFile(params){
        let parameters = cleanParams(params)
        let pdfData = parameters.custpage_pdf_file
        let saved = false
        let PO = findPO(params)
        if(pdfData){

            let template = '<?xml version="1.0"?>';
            template = template + "<!DOCTYPE pdf>";
            template = template + "<pdfset>";
            template += `<pdf src="${pdfData}"/>`;
            template += "</pdfset>";

           saved = _createFile(template,params,PO)

        }
        return saved
    }

    function findPO(params){
      let fileName = params.custpage_myfile.split('.')[0]

      if(!fileName) return {}

      let results = {}
      var purchaseorderSearchObj = search.create({
        type: "purchaseorder",
        filters:
        [
           ["type","anyof","PurchOrd"], 
           "AND", 
           ["numbertext","is",fileName], 
           "AND", 
           ["mainline","is","T"]
        ],
        columns:
        [
           "recordtype",
        ]
     });

     purchaseorderSearchObj.run().each(function(result){
      results.id = result.id
      results.type = result.getValue('recordtype')
        return true;
     });
     return results
    }

    function cleanParams(params){

      for(let param in params){
        if(String(param).startsWith('custpage_pdf_file')){
          params['custpage_pdf_file'] = params[param]
        }
      }
      return params
    
    }

    function _createFile(template,params,PO){
      let attached = false
      let renderer = render.create();
      renderer.templateContent = template;
      let custpage_notfound = false
      if(!PO.id) custpage_notfound = true
      let xmlStr = renderer.renderAsString();
      let replacedXmlStr = xmlStr.replace(/&/gm, "&amp;");
      let newFile = render.xmlToPdf({
        xmlString: replacedXmlStr,
      });

      newFile.folder = runtime.getCurrentScript().getParameter('custscript_av_msg_pdf_folder');
      newFile.name = `${params.custpage_myfile.split('.')[0]}-${new Date().getTime()}.pdf`;
      let fileId = newFile.save()

      if(fileId && !custpage_notfound){
        try{
          record.attach({
            record: {
              type: 'file',
              id:fileId
              },
            to: {
              type: PO.type,
              id: PO.id
              },
          })
          attached = true
        }catch(e){
          log.error(`Error while attaching PO File to ${PO.type}: ${PO.id}`,JSON.stringify(e))
          attached = false
        }
       
      }
      return {attached:attached,custpage_notfound:custpage_notfound}
    }

    let generateSearchPage =(params) =>{

        let form = serverWidget.createForm({
            title: 'MSG Drag N Drop Module',
          });


          let HTMLBody = form.addField({
            id: "custpage_htmlfield",
            type: serverWidget.FieldType.INLINEHTML,
            label: "whatever - dose not matter",
          });

          let htmlValue = file.load('975').getContents()
          if(htmlValue){
            HTMLBody.defaultValue = htmlValue
          }

          return form
    }

    return {
        onRequest: onRequest
    }
});
