/**
 *  Copyright (c) 2022 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @author: [Theodore Koukouves]
 *@NApiVersion 2.1
 *@NScriptType Restlet
 */
define(['N/search'], function(search) {


    function _post(datain) {
        let foundPO =  findPO(datain.file)
        log.debug('Found',foundPO)
        return JSON.stringify({status:foundPO})
    }

    function findPO(file){
        try{
            let fileName = file.split('.')[0]
            log.debug('fileName',fileName)

            if(!fileName) return false
            var purchaseorderSearchObj = search.create({
                type: "purchaseorder",
                filters:
                [
                   ["type","anyof","PurchOrd"], 
                   "AND", 
                   ["numbertext","is",fileName], 
                   "AND", 
                   ["mainline","is","T"]
                ],
                columns:
                [
                   "type",
                   "tranid",
                ]
             });
             return purchaseorderSearchObj.runPaged().count > 0;
        }catch(e){
            log.error(e.name,e.message)
        }
       
  
    }



    return {
        post: _post,
    }
});
