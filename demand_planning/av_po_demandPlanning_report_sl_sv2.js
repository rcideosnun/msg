/**
 *@NApiVersion 2.1
 *@NScriptType Suitelet
 */
define(['N/search', 'N/ui/serverWidget', 'N/format','N/file'], function(search, sw, format, file) {

    function onRequest(context) {
        let params = {
            p1Start : context.request.parameters.p1Start,
            p1End : context.request.parameters.p1End,
            p2Start : context.request.parameters.p2Start,
            p2End : context.request.parameters.p2End,
            p3Start : context.request.parameters.p3Start,
            p3End : context.request.parameters.p3End,
            itemId: context.request.parameters.itemId,
            itemName: context.request.parameters.itemName,
            locationId: context.request.parameters.locationId,
            locationName: context.request.parameters.locationName
        };
        let tableData = {
            p1: getPeriodData(params.p1Start, params.p1End, params.itemId, params.locationId),
            p2: getPeriodData(params.p2Start, params.p2End, params.itemId, params.locationId),
            p3: getPeriodData(params.p3Start, params.p3End, params.itemId, params.locationId),
        };
        let htmlTable = buildHtmlTable(tableData, params);
        const form = generateForm(htmlTable);
        context.response.writePage(form);
    }
    function buildHtmlTable(tableData, params){
        let css = file.load({
            id: './av_po_demandPlanning_css.css'
        });
        let BodyTable = '<html><head>' + css.getContents(); + '</head></html>';
        BodyTable += 
        '<div class="loaderCust"></div>' +
        '<div class="fgroup_title" style="er-bottom: 1px solid #CCC; white-space:nowrap;height: 20px;margin-left: 20px!important;margin-right: 20px!important;margin-bottom: 10px!important;background: #e0e6ef;"><span style="margin-left: 5px;">Report für : '+ params.itemName + ' : ' + params.locationName +'</span></div>'+
        '<table class="ReportTable">'+
        '<thead>' +
        '<tr>' +
        '<th>Datum</th>' +
        '<th>Datum</th>' +
        '<th>Verkaufte Menge gesamt</th>' +
        '<th>Verkaufte Menge Deutschlandberg	</th>' +
        '<th>Verkaufte Menge Hauptlager</th>' +
        '<th>Anzahl Aufträge</th>' +
        '<th>Verhältnis Verkaufter Menge gesamt und Anzahl Aufträge</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>'+
        '<tr>'+
        '<td colspan ="6" style="background-color: lightgray;">Periode 1</td>'+
        '</tr>'+

        '<tr>'+
        '<td>' + tableData.p1.from + '</td>'+
        '<td>' + tableData.p1.to + '</td>'+
        '<td>' + tableData.p1.groupQuantity+ '</td>'+
        '<td>' + tableData.p1.deutschlandsberg + '</td>'+
        '<td>' + tableData.p1.bestellungen+ '</td>'+
        '<td>' + tableData.p1.so + '</td>'+
        '<td>' + tableData.p1.ratio + '</td>'+
        '</tr>'+

        '<tr>'+
        '<td colspan ="8" style="background-color: lightgray;">Periode 2</td>'+
        '</tr>'+

        '<tr>'+
        '<td>' + tableData.p2.from  + '</td>'+
        '<td>' + tableData.p2.to + '</td>'+
        '<td>' + tableData.p2.groupQuantity+ '</td>'+
        '<td>' + tableData.p2.deutschlandsberg+ '</td>'+
        '<td>' + tableData.p2.wien + '</td>'+
        '<td>' + tableData.p2.bestellungen+ '</td>'+
        '<td>' + tableData.p2.so + '</td>'+
        '<td>' + tableData.p2.ratio + '</td>'+
        '</tr>'+

        '<tr>'+
        '<td colspan ="8" style="background-color: lightgray;">Periode 3</td>'+
        '</tr>'+

        '<tr>'+
        '<td>' + tableData.p3.from + '</td>'+
        '<td>' + tableData.p3.to + '</td>'+
        '<td>' + tableData.p3.groupQuantity+ '</td>'+
        '<td>' + tableData.p3.deutschlandsberg+ '</td>'+
        '<td>' + tableData.p3.wien + '</td>'+
        '<td>' + tableData.p3.bestellungen + '</td>'+
        '<td>' + tableData.p3.so + '</td>'+
        '<td>' + tableData.p3.ratio + '</td>'+
        '</tr>'+
        
        '</tbody>'+
        '</table></div>';
        return BodyTable;
    }
    function generateForm(htmlTable){
        const form = sw.createForm({title: 'Artikel bestellen', hideNavBar: true});
        let reportTable = form.addField({
            id: 'custpage_htmlfield',
            type: sw.FieldType.INLINEHTML,
            label: 'Table',
        });
        reportTable.defaultValue = htmlTable;
        return form;
    }
    function getPeriodData(dateStart, dateEnd, item, locationId){
        let periodData = {
            from: dateStart || '',
            to: dateEnd || '',
            groupQuantity: 0,
            deutschlandsberg: 0,
            wien: 0,
            bestellungen: 0,
            so: 0,
            ratio: 0
        };
        if (dateStart != '' && dateEnd != '' && item != ''){
            // let startDate = format.format({value:dateStart,type:format.Type.DATE});
            // let endDate = format.format({value:dateEnd,type:format.Type.DATE});
            let searchObj = [];
            let filters = [
                ['mainline','is','F'], 
                'AND', 
                ['item.type','anyof','InvtPart'], 
                'AND', 
                ['type','anyof','CashSale','SalesOrd'], 
                'AND',
                ['location','anyof','3','1','2'], 
                //['location','anyof', locationId], 
                'AND', 
            ];
            if (dateStart && dateEnd){
                filters.push(['trandate','within',dateStart, dateEnd],'AND',);
            }
            if (item){
                filters.push(['item','anyof',item]);
            }
            var transactionSearchObj = search.create({
                type: 'transaction',
                filters:filters,
                columns:
            [
                'item',
                search.createColumn({
                    name: 'displayname',
                    join: 'item'
                }),
                'quantity',
                'location',
                'type'
            ]
            });

            transactionSearchObj.run().each(function(result){
                let record = {};
                record.periodStart = dateStart,
                record.periodEnd = dateEnd,
                record.itemId = result.getValue('item');
                record.itemDisplay = result.getValue({name: 'displayname',join: 'item'});
                record.quantity = parseInt(result.getValue({name: 'quantity'}));
                record.location = result.getValue('location');
                record.locationTxt = result.getText('location');
                record.type = result.getValue('type');
                searchObj.push(record);
                return true;
            });
            // group by itemId & location
            // Not sure if needed
            let groupedResault = groupByMultiple(searchObj, function( item)
            {
                return [item.itemId, item.location];
            });


            if (searchObj.length > 0){
                searchObj.forEach(element => {
                    periodData.groupQuantity += element.quantity;
                    periodData.deutschlandsberg += element.location == 1 ? element.quantity : 0;
                    periodData.wien += element.location == 2 ? element.quantity : 0;
                    periodData.bestellungen += element.location == 3 ? element.quantity : 0; 
                    periodData.so += element.type == 'SalesOrd' ? 1 : 0;
                });
                if (periodData.groupQuantity > 0 && periodData.so > 0)
                    periodData.ratio = Math.round(((periodData.groupQuantity / periodData.so) + Number.EPSILON) * 100) / 100;
            }

            return periodData;
        }
        else return periodData;
    }
    function groupByMultiple( array , f )
    {
        var groups = {};
        array.forEach( function( o )
        {
            var group = JSON.stringify( f(o) );
            groups[group] = groups[group] || [];
            groups[group].push( o );  
        });
        return Object.keys(groups).map( function( group )
        {
            return groups[group]; 
        });
    }
    return {
        onRequest: onRequest
    };
});
