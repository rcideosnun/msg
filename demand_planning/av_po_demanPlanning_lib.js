/**
 * demand planning Lib
 * @NApiVersion 2.1
 */
define(['N/search', 'N/format','N/url', 'N/query','N/runtime'], function(search, format, url, query, runtime) {

    function generateReorderList(params){

        //let vendorList = getVendorList();
        let itemsList = getItemsList(params);
        let finalItemList = renderResultObj(itemsList, params);

        return finalItemList;

    }
    function getSpecificItemVendorPurchasePrice(item, itemCatRebate){
        let final = [];
        let itemListObj = {};
        let customrecord_av_vendor_price_listSearchObj = search.create({
            type: "customrecord_av_vendor_price_list",
            filters:
            [
               ["custrecord_av_item.internalid","anyof",item], 
               "AND", 
               ["custrecord_av_valid_from","onorbefore","today"], 
               "AND", 
               [
                [
                    ["custrecord_av_valid_to","onorafter","today"]
                ],
                "OR",
                [
                    ["custrecord_av_valid_to","isempty",""]
                ]
            ]
            ],
            columns:
            [
                search.createColumn({
                    name: "internalid",
                    join: "CUSTRECORD_AV_ITEM"
                }),
                "custrecord_av_item",
                "custrecord_av_vendor",
                search.createColumn({
                    name: "displayname",
                    join: "CUSTRECORD_AV_ITEM"
                }),
                "custrecord_av_uvp",
                "custrecord_av_ek",
                "custrecord_av_rabatt1",
                "custrecord_av_rabatt2",
                search.createColumn({
                    name: "custrecord_av_valid_from",
                    sort: search.Sort.DESC
                }),
                "custrecord_av_valid_to",
                search.createColumn({
                    name: "custentity_av_rebate_vendor",
                    join: "CUSTRECORD_AV_VENDOR"
                }),
                search.createColumn({
                    name: "cseg_msg_igt",
                    join: "CUSTRECORD_AV_ITEM"
                })
            ]
         });
         //var searchResultCount = customrecord_av_vendor_price_listSearchObj.runPaged().count;
         customrecord_av_vendor_price_listSearchObj.run().each(function(result){
            let item = result.getValue({name: 'internalid',join: 'CUSTRECORD_AV_ITEM'}); 
            let vendorId = result.getValue({name: 'custrecord_av_vendor'});
            if(!itemListObj[item]){ 
                itemListObj[item] = {}
            }
            if (!itemListObj[item][vendorId]){
                    itemListObj[item][vendorId] = {
                        manufacturerVendorName : result.getText({name: 'custrecord_av_vendor'}),
                        manufacturerVendorId : result.getValue({name: 'custrecord_av_vendor'}),
                        itemCategoryRebate : parsePercent(result.getValue({name: 'cseg_msg_igt',join: 'CUSTRECORD_AV_ITEM'})) || 0,
                        uvp : parseFloat(result.getValue({name: 'custrecord_av_uvp'})) || 0,
                        ek : parseFloat(result.getValue({name: 'custrecord_av_ek'})) || 0,
                        reb1 : parsePercent(result.getValue({name: 'custrecord_av_rabatt1'})) || 0,
                        reb2 : parsePercent(result.getValue({name: 'custrecord_av_rabatt2'})) || 0,
                        venReb : parsePercent(result.getValue({name: 'custentity_av_rebate_vendor',join: 'CUSTRECORD_AV_VENDOR'})) || 0,
                        itemCategoryReb : itemCatRebate || 0
                    }
            }
            return true;
         });

         let finalObject = {};
         for (const item in itemListObj){
            for (const vendor in itemListObj[item]){
                itemListObj[item][vendor].purchasePrice = calulatePurchasePrice(itemListObj[item][vendor]).toFixed(2); 
                finalObject[vendor] = itemListObj[item][vendor];
            }
         }
         return finalObject;
    }
    function getItemsList(params){
        let itemListObj = {};
        //params.vendor = 261 // 280 // atodo: remove, update search
        if (params && params.vendor){



            let filter = [
                [
                    ["custrecord_av_item.isinactive","is","F"],
                    "AND",
                    //  ["custrecord_av_item","anyof","2344"],
                    //  "AND",
                    ["custrecord_av_item","noneof","@NONE@"],
                    "AND",
                    ["custrecord_av_item.inventorylocation","anyof","3","1"],
                    "AND",
                    ["custrecord_av_item.custitem_msg_item_status","noneof","2","1"],
                    "AND",
                    ["custrecord_av_vendor","noneof","@NONE@"],
                    "AND",
                    ["custrecord_av_vendor.internalid","anyof", params.vendor],
                    "AND",
                    ["custrecord_av_valid_from","onorbefore","today"]
                ], 
                "AND", 
                [
                    [
                        ["custrecord_av_valid_to","onorafter","today"]
                    ],
                    "OR",
                    [
                        ["custrecord_av_valid_to","isempty",""]
                    ]
                ], 
                "AND",   
                [
                    [
                        ["custrecord_av_item.locationquantityavailable","greaterthan","0"]
                    ],
                    "OR",
                    [
                        ["custrecord_av_item.preferredstocklevel","greaterthan","0"]
                    ],
                    "OR",
                    [
                        ["custrecord_av_item.locationquantitybackordered","greaterthan","0"],
                    ]
                ]
             ]   
            var itemSearchObj = search.create({
                type: 'customrecord_av_vendor_price_list',
                filters: filter,
    
                columns:
                [
                    'custrecord_av_item',
                    'custrecord_av_vendor',
                    search.createColumn({
                        name: 'inventorylocation',
                        join: 'CUSTRECORD_AV_ITEM'
                    }),
                    search.createColumn({
                        name: 'locationquantityavailable',
                        join: 'CUSTRECORD_AV_ITEM'
                    }),
                    search.createColumn({
                        name: 'locationquantitybackordered',
                        join: 'CUSTRECORD_AV_ITEM'
                    }),
                    search.createColumn({
                        name: 'locationquantityonorder',
                        join: 'CUSTRECORD_AV_ITEM'
                    }),
                    search.createColumn({
                        name: 'locationpreferredstocklevel',
                        join: 'CUSTRECORD_AV_ITEM'
                    }),
                    search.createColumn({
                        name: 'locationquantityonhand',
                        join: 'CUSTRECORD_AV_ITEM'
                    }),
                    search.createColumn({
                        name: 'internalid',
                        join: 'CUSTRECORD_AV_ITEM'
                    }),
                    search.createColumn({
                        name: 'displayname',
                        join: 'CUSTRECORD_AV_ITEM'
                    }),
                    search.createColumn({
                        name: 'safetystocklevel',
                        join: 'CUSTRECORD_AV_ITEM'
                    }),
                    search.createColumn({
                        name: 'leadtime',
                        join: 'CUSTRECORD_AV_ITEM'
                    }),
                    search.createColumn({
                        name: 'vendorname',
                        join: 'CUSTRECORD_AV_ITEM'
                    }),
                    search.createColumn({
                        name: 'itemid',
                        join: 'CUSTRECORD_AV_ITEM'
                    }),
                    search.createColumn({
                        name: 'type',
                        join: 'CUSTRECORD_AV_ITEM'
                    }),
                    search.createColumn({
                        name: 'cseg_msg_igt',
                        join: 'CUSTRECORD_AV_ITEM'
                    }),
                    'custrecord_av_uvp',
                    'custrecord_av_ek',
                    'custrecord_av_rabatt1',
                    'custrecord_av_rabatt2',
                    search.createColumn({
                        name: "custrecord_av_valid_from",
                        sort: search.Sort.DESC
                     }),
                    'custrecord_av_valid_to',

                    search.createColumn({
                        name: 'custentity_av_rebate_vendor',
                        join: 'CUSTRECORD_AV_VENDOR'
                    }),
                    search.createColumn({
                        name: 'custentity_av_vendor_mindestbestellwert',
                        join: 'CUSTRECORD_AV_VENDOR'
                    }),
                    search.createColumn({
                        name: "custitem_msg_item_status",
                        join: "CUSTRECORD_AV_ITEM"
                     })
                ]
            });

            var scriptObj = runtime.getCurrentScript();
            log.debug('Remaining governance units: ' + scriptObj.getRemainingUsage());

            var searchResult = getAllResults(itemSearchObj); 
            //var searchResult =  itemSearchObj.run().getRange({start: 0,end: 100}); // todoA: replace with above

            log.debug('Remaining governance units: ' + scriptObj.getRemainingUsage());

            searchResult.forEach(function(result) {
                let item = result.getValue({name: 'internalid',join: 'CUSTRECORD_AV_ITEM'});
                let inventorylocation = result.getValue({name: 'inventorylocation',join: 'CUSTRECORD_AV_ITEM'});
                //let validFrom = result.getValue({name: 'custrecord_av_valid_from'});
                if(!itemListObj[item]){
                    itemListObj[item] = {
                        quantityInAllLocations: 0,
                        quantityOnHandAllLocations: 0,
                        quantityOnBackOrderAllLocations: 0,
                        quantityOnOrderAllLocatons: 0,
                        quantityDeutschlandsberg: 0,
                        quantityHauptlager: 0,
                        openInSo: 0,
                        openInPo: 0,
                        preferredstocklevel: 0,
                        quantityToOrder: 0,
                        ps_quantity: 0,
                        ps_is_available: 'NEIN',
                        itemsSold: 0,
                        itemCategoryReb: 0,
                        purchasePrice: 0,
                        safetystocklevel: parseInt(result.getValue({name: 'safetystocklevel',join: 'CUSTRECORD_AV_ITEM'})) || 0,
                        itemIdNumber : result.getValue({name: 'custrecord_av_item'}),
                        manufacturerVendorId : result.getValue({name: 'custrecord_av_vendor'}),
                        manufacturerVendorName : result.getText({name: 'custrecord_av_vendor'}),
                        uvp : parseFloat(result.getValue({name: 'custrecord_av_uvp'})) || 0,
                        ek : parseFloat(result.getValue({name: 'custrecord_av_ek'})) || 0,
                        reb1 : parsePercent(result.getValue({name: 'custrecord_av_rabatt1'})) || 0,
                        reb2  : parsePercent(result.getValue({name: 'custrecord_av_rabatt2'})) || 0,
                        inventorylocation : result.getValue({name: 'inventorylocation',join: 'CUSTRECORD_AV_ITEM'}),
                        itemId : result.getValue({name: 'internalid',join: 'CUSTRECORD_AV_ITEM'}),
                        itemDisplayName : result.getValue({name: 'displayname',join: 'CUSTRECORD_AV_ITEM'}) || result.getValue({name: 'itemid',join: 'CUSTRECORD_AV_ITEM'}) ,
                        Lieferantenartikelnummer : result.getValue({name: 'vendorname',join: 'CUSTRECORD_AV_ITEM'}),
                        Artikelnummer : result.getValue({name: 'itemid',join: 'CUSTRECORD_AV_ITEM'}),
                        itemType : result.getValue({name: 'type',join: 'CUSTRECORD_AV_ITEM'}),
                        itemCategory : result.getValue({name: 'cseg_msg_igt',join: 'CUSTRECORD_AV_ITEM'}),
                        venReb : parsePercent(result.getValue({name: 'custentity_av_rebate_vendor',join: 'CUSTRECORD_AV_VENDOR'})) || 0,
                        minimumOrderValue : result.getValue({name: 'custentity_av_vendor_mindestbestellwert',join: 'CUSTRECORD_AV_VENDOR'}) || 0,
                        itemStatus:  result.getText({name: 'custitem_msg_item_status',join: 'CUSTRECORD_AV_ITEM'}) || 0,
                        vpl_validfrom: result.getValue({name: "custrecord_av_valid_from",sort: search.Sort.DESC}),
                        vpl_validTo: result.getValue({name: 'custrecord_av_valid_to'}),
                        itemUrl : _generateUrl('inventoryitem', result.getValue({name: 'internalid',join: 'CUSTRECORD_AV_ITEM'})),
                        VendorUrl: _generateUrl('vendor', result.getValue({name: 'custrecord_av_vendor'})),


                    };
                }

                if (!itemListObj[item][inventorylocation]){
                    itemListObj[item][inventorylocation] = {
                        inventorylocation : result.getValue({name: 'inventorylocation',join: 'CUSTRECORD_AV_ITEM'}),
                        inventorylocationName : result.getText({name: 'inventorylocation',join: 'CUSTRECORD_AV_ITEM'}),
                        quantityBackOrdered : result.getValue({name: 'locationquantitybackordered',join: 'CUSTRECORD_AV_ITEM'}),
                        quantityOnOrder : result.getValue({name: 'locationquantityonorder',join: 'CUSTRECORD_AV_ITEM'}),
                        quantityOnHand : result.getValue({name: 'locationquantityonhand',join: 'CUSTRECORD_AV_ITEM'}),
                        quantityAvailable : result.getValue({name: 'locationquantityavailable',join: 'CUSTRECORD_AV_ITEM'}),
                        preferredstocklevel : result.getValue({name: 'locationpreferredstocklevel',join: 'CUSTRECORD_AV_ITEM'}),
                    };

                    itemListObj[item].quantityInAllLocations += parseInt(result.getValue({name: 'locationquantityavailable',join: 'CUSTRECORD_AV_ITEM'})) || 0
                    itemListObj[item].quantityOnHandAllLocations += parseInt(result.getValue({name: 'locationquantityonhand',join: 'CUSTRECORD_AV_ITEM'})) || 0
                    itemListObj[item].quantityOnBackOrderAllLocations += parseInt(result.getValue({name: 'locationquantitybackordered',join: 'CUSTRECORD_AV_ITEM'})) || 0
                    itemListObj[item].quantityOnOrderAllLocatons += parseInt(result.getValue({name: 'locationquantityonorder',join: 'CUSTRECORD_AV_ITEM'})) || 0
                 
                    // No longer a factor since location specific value is no longer used in main table, used only to get preferredStockLevel
                    if (inventorylocation == 1){
                        itemListObj[item].quantityDeutschlandsberg += parseInt(result.getValue({name: 'locationquantityavailable',join: 'CUSTRECORD_AV_ITEM'})) || 0
                    }

                    else if (inventorylocation == 3){
                        itemListObj[item].quantityHauptlager += parseInt(result.getValue({name: 'locationquantityavailable',join: 'CUSTRECORD_AV_ITEM'})) || 0
                            // according to requirments, we should always use  Hauptlager locations for preffered stock level
                        itemListObj[item].preferredstocklevel = parseInt(result.getValue({name: 'locationpreferredstocklevel',join: 'CUSTRECORD_AV_ITEM'})) || 0 
                    }
                        
                }

                
            });
        }
        return itemListObj;
    }
    function renderResultObj(itemsObjList, params){

        let checkedItems = params.checkedLines;
        let uniqueItemIdsList = Object.keys(itemsObjList);


        if (uniqueItemIdsList.length){
            log.debug('uniqueItemIdsList', uniqueItemIdsList.length)
            let itemInOpenTransactions = getItemsInOpenTransactionsList(uniqueItemIdsList);
            //log.debug('itemInOpenTransactions', itemInOpenTransactions)
            let finalItemsObjList = {};

            //// Filter out items with no quantities to order ////
            for (const item in itemsObjList) {
                for (const openTrans in itemInOpenTransactions){
                    if (item == openTrans){

                        if(itemInOpenTransactions[openTrans]['SalesOrd']){
                            itemsObjList[item].openInSo = parseInt(itemInOpenTransactions[openTrans]['SalesOrd'].quantity) || 0;
                        }
                        if(itemInOpenTransactions[openTrans]['PurchOrd']){
                            itemsObjList[item].openInPo = parseInt(itemInOpenTransactions[openTrans]['PurchOrd'].quantity) || 0;
                        }
                    }
                    
                }
            
                let SafetyCheck = ((((itemsObjList[item].quantityInAllLocations + itemsObjList[item].openInPo) - itemsObjList[item].safetystocklevel) <= 0)                       
                                  || itemsObjList[item].quantityOnBackOrderAllLocations) > 0; 
               
                        if (SafetyCheck){
                    itemsObjList[item].quantityToOrder = itemsObjList[item].preferredstocklevel - (itemsObjList[item].quantityInAllLocations + itemsObjList[item].openInPo) + itemsObjList[item].quantityOnBackOrderAllLocations
                }

                
                                                                                      
             
                // if (!isAboveSafety){
                //     itemsObjList[item].quantityToOrder = itemsObjList[item].preferredstocklevel - (itemsObjList[item].quantityInAllLocations + itemsObjList[item].openInPo)
                //     // itemsObjList[item].quantityToOrder = itemsObjList[item].preferredstocklevel - (itemsObjList[item].quantityInAllLocations + itemsObjList[item].openInPo + itemsObjList[item].quantityOnBackOrderAllLocations)
                // }
                // if (itemsObjList[item].preferredstocklevel == 0 && itemsObjList[item].quantityOnBackOrderAllLocations > 0 && itemsObjList[item].quantityToOrder == 0){
                //     itemsObjList[item].quantityToOrder = itemsObjList[item].quantityOnBackOrderAllLocations - itemsObjList[item].openInPo;
                // }                                                
                if (itemsObjList[item].quantityToOrder > 0){
                    finalItemsObjList[item] = itemsObjList[item];
                }
                //log.debug('itemsObjList[item]',itemsObjList[item])
            }
            //// Fetch additional data based on remaining items ids////
            let finalItemIdList = Object.keys(finalItemsObjList);
            log.debug('finalItemIdList',finalItemIdList)
            let itemCategoryList = getItemCatRebatesList(params.vendor);
            log.debug('itemCategoryList',itemCategoryList)
            let producerStockList = getProducerStockObjList(params.vendor, finalItemIdList);
            //let itemsSoldList = getItemsSoldObjList(params.vendor, finalItemIdList)
    
    
            //// merge data  ////
            for (const item in finalItemsObjList) {
                for (const producerStock in producerStockList){
                    if (item == producerStock){
                        itemsObjList[item].ps_quantity = parseInt(producerStockList[producerStock].ps_quantity) || 0;
                        itemsObjList[item].ps_is_available = producerStockList[producerStock].ps_is_available == true ? 'JA' : 'NEIN';
                    }           
                }
                // for (const soldItem in itemsSoldList){
                //     if (item == soldItem){
                //         itemsObjList[item].itemsSold =  Math.ceil(itemsSoldList[soldItem] / 14) || 0 // 14 can be replaced with days from itemSoldList if needed in future
                //     }
                // }
    
                let findItemCategory = itemCategoryList.find( a=> a.vandorid == finalItemsObjList[item].manufacturerVendorId && a.itemCategory == finalItemsObjList[item].itemCategory);
                //log.debug('findItemCategory',findItemCategory)
                if (findItemCategory) itemsObjList[item].itemCategoryReb = parsePercent(findItemCategory.itemCategoryRebate) || 0;
                //log.debug(itemsObjList[item].itemIdNumber + ' rebate',itemsObjList[item].itemCategoryReb);
    
                itemsObjList[item].purchasePrice = calulatePurchasePrice(itemsObjList[item]).toFixed(2); 
                itemsObjList[item].uniqueLineId = itemsObjList[item].itemIdNumber + itemsObjList[item].Lieferantenartikelnummer + itemsObjList[item].inventorylocation + itemsObjList[item].manufacturerVendorId
                itemsObjList[item].isPreChecked = false;
    
                let isChecked = checkedItems ? checkedItems.find(a=>a.uniqueLineId == itemsObjList[item].uniqueLineId) : false;
                if (isChecked){
                    itemsObjList[item].isPreChecked = true;
                    itemsObjList[item].quantityToOrder = isChecked.quantity;
                }
    
            }
    
            const sorted = Object.keys(finalItemsObjList)
            .sort()
            .reduce((accumulator, key) => {
              accumulator[key] = finalItemsObjList[key];
          
              return accumulator;
            }, {});
            return sorted;

        }
        else return {};

    }
    function getItemCatRebatesList(vendorId){
        let final = [];
        var customrecord_av_item_category_rabattSearchObj = search.create({
            type: "customrecord_av_item_category_rabatt",
            filters:
            [
               ["custrecord_av_vendor_parent","anyof",vendorId], 
               "AND", 
               ["custrecord_av_item_category_rebatt","isnotempty",""], 
               "AND", 
               ["custrecord_av_item_category","noneof","@NONE@"]
            ],
            columns:
            [
               "custrecord_av_item_category_rebatt",
               "custrecord_av_vendor_parent",
               "custrecord_av_item_category"
            ]
         });

        customrecord_av_item_category_rabattSearchObj.run().each(function(result){
            let record = {};
            record.vandorid = result.getValue('custrecord_av_vendor_parent');
            record.itemCategory = result.getValue('custrecord_av_item_category');
            record.itemCategoryRebate = result.getValue('custrecord_av_item_category_rebatt');
            final.push(record);
            return true;
        });
        return final;
    }
    function _generateUrl(type, recId){
        // try to generate URL for specific record, else return neutral #
        let recordUrl;
        try {
            recordUrl = url.resolveRecord({recordType: type, recordId: recId,isEditMode: false});
            return recordUrl;
        } catch (er) {
            return '#';
        }

    }
    function getItemsSoldObjList(finalItemIdList){
        let soldItems = {};
        let days = 14; // Oryginaly this was supposed to be a variable, hardcoded per last requirment
        let final = [];
        let endDate = format.format({type: format.Type.DATE, value: new Date()});
        let today = new Date();
        let stDate = new Date(today.setDate(today.getDate() - days));
        let startDate = format.format({type: format.Type.DATE, value: stDate});

        var transactionSearchObj = search.create({
            type: "transaction",
            filters:
            [
               ["type","anyof","CashSale","SalesOrd"], 
               "AND", 
               ["item.type","anyof","InvtPart"], 
               "AND", 
               ["trandate","within",startDate,endDate], 
               "AND", 
               ["location","anyof","3","1"],
               "AND", 
               ["item","anyof",finalItemIdList]
            ],
            columns:
            [
               search.createColumn({
                  name: "item",
                  summary: "GROUP"
               }),
               search.createColumn({
                  name: "displayname",
                  join: "item",
                  summary: "GROUP"
               }),
               search.createColumn({
                  name: "quantity",
                  summary: "SUM"
               })
            ]
         });
        
        transactionSearchObj.run().each(function(result){
            let item = result.getValue({name: "item",summary: "GROUP"});
            soldItems[item] = {
                item: result.getValue({name: 'item',summary: 'GROUP'}),
                quantity : result.getValue({name: 'quantity',summary: 'SUM'}),
                itemName : result.getText({ name: 'displayname',join: 'item',summary: 'GROUP'}),
                soldInDays: days
            }
            return true;
        });
        return soldItems;

    }
    function getProducerStockObjList(vendorId, finalItemIdList){
        let producerStock = {};
        if (vendorId && finalItemIdList.length){
            var producer_stockSearchObj = search.create({
            type: "customrecord_msg_producer_stock",
            filters:
            [
               ["custrecord_msg_vsa_vendor","anyof",vendorId], 
               "AND", 
               ["custrecord_msg_vsa_item","anyof",finalItemIdList]
            ],
            columns:
            [
               search.createColumn({
                  name: "scriptid",
                  sort: search.Sort.ASC
               }),
               "custrecord_msg_vsa_item",
               "custrecord_msg_vsa_vendor",
               "custrecord_msg_vsa_availability",
               "custrecord_msg_vsa_quantity",
               "custrecord_msg_vsa_is_available"
            ]
            });
            let myResults = getAllResults(producer_stockSearchObj);
            myResults.forEach(function(result) {
            let item = result.getValue({name: 'custrecord_msg_vsa_item'});
            producerStock[item] = {
                ps_item : result.getValue({name: 'custrecord_msg_vsa_item'}),
                ps_vendorId : result.getValue({name: 'custrecord_msg_vsa_vendor'}),
                ps_availability : result.getValue({name: 'custrecord_msg_vsa_availability'}),
                ps_quantity : result.getValue({name: 'custrecord_msg_vsa_quantity'}),
                ps_is_available : result.getValue({name: 'custrecord_msg_vsa_is_available'}),
            }
             return true;
            });
        }
         return producerStock;
    }
    function getItemsInOpenTransactionsList(uniqueItemIdsList){
       let itemInOpenTransactions = {};
       if (uniqueItemIdsList.length){
            let transactionSearchObj = search.create({
            type: "transaction",
            filters:
            [
               ["type","anyof","SalesOrd","PurchOrd"], 
               "AND", 
               ["status","anyof","PurchOrd:A","PurchOrd:B","PurchOrd:C","PurchOrd:D","PurchOrd:E","PurchOrd:P","SalesOrd:A","SalesOrd:B","SalesOrd:D","SalesOrd:E","SalesOrd:F"], 
               "AND", 
               ["item.type","anyof","InvtPart"], 
               "AND", 
               ["item","anyof",uniqueItemIdsList],
               "AND", 
               ["shiprecvstatusline","is","F"]
            ],
            columns:
            [
               search.createColumn({
                  name: "item",
                  summary: "GROUP"
               }),
               search.createColumn({
                  name: "quantity",
                  summary: "SUM"
               }),
               search.createColumn({
                  name: "type",
                  summary: "GROUP"
               })
            ]
            });
    
            transactionSearchObj.run().each(function(result){
            let item = result.getValue({name: "item",summary: "GROUP"});
            let type = result.getValue({name: "type",summary: "GROUP"});
            if (!itemInOpenTransactions[item]){
                itemInOpenTransactions[item] = {}
            }
            if (!itemInOpenTransactions[item][type]){
                itemInOpenTransactions[item][type] = {
                    quantity : result.getValue({name: "quantity",summary: "SUM"}),
                    type : result.getValue({name: "type",summary: "GROUP"})
                }
            }
            return true;
            });
       }
     return itemInOpenTransactions;
    }    
    function parsePercent(percent) {
        return parseFloat(percent) / 100;
    }
    function calulatePurchasePrice(item){
        let purchasePrice = 0;
        if (item.ek > 0) purchasePrice = item.ek;
        if (item.reb1 > 0) purchasePrice *= (1 - item.reb1);
        if (item.reb2 > 0) purchasePrice *= (1 - item.reb2);
        if (item.venReb > 0) purchasePrice *= (1 - item.venReb);
        if (item.itemCategoryReb > 0) purchasePrice *= (1 -item.itemCategoryReb);
        return purchasePrice;

    }
    function getAllResults(s) {
        var results = s.run();
        var searchResults = [];
        var searchid = 0;
        do {
            var resultslice = results.getRange({start:searchid,end:searchid+1000});
            resultslice.forEach(function(slice) {
                searchResults.push(slice);
                searchid++;
                }
            );
        } while (resultslice.length >=1000);
        return searchResults;
    }
    function getVendorList() {
        var scriptObj = runtime.getCurrentScript();
        let vendorQuery = query.create({
            type: query.Type.VENDOR,
            columns: [{fieldId : 'entityid'}, {fieldId : 'id'}],
            distinct: true
        });

        let cond1 = vendorQuery.createCondition({
            fieldId: 'id',
            operator: query.Operator.GREATER,
            values: 0
        });
        let cond2 = vendorQuery.createCondition({
            fieldId: 'isinactive',
            operator: query.Operator.IS,
            values: false
        });

        vendorQuery.condition = vendorQuery.and(cond1, cond2) 
        let vendorNameList = vendorQuery.run().asMappedResults();
        log.debug('Remaining governance units: ' + scriptObj.getRemainingUsage());

        return vendorNameList;
    }
    return {
        generateReorderList : generateReorderList,
        getSpecificItemVendorPurchasePrice: getSpecificItemVendorPurchasePrice


    };
});
