/**
 *@NApiVersion 2.1
 *@NScriptType Suitelet
 */
define(['N/ui/serverWidget', 'N/search', 'N/format', 'N/record', 'N/ui/message','N/runtime','N/file','N/url','N/redirect','./av_po_demanPlanning_lib'], function (sw, search, format, record, message,runtime, file, url, redirect,dpLib) {
    function onRequest(context) {
        try {
            var scriptObj = runtime.getCurrentScript();
            if (context.request.method == 'GET') {
                let createPoDetails;
                let createdPoParam = context.request.parameters.poArrayList;
                if (createdPoParam){
                    let createPoArray = createdPoParam.split(',');
                    createPoDetails = generateCreatedPoMsg(createPoArray);
                }

                let params = {
                    vendor: context.request.parameters.searchVendor,
                    pageNr: context.request.parameters.page || 1,
                    //dateRanges: context.request.parameters.dateRanges ? JSON.parse(context.request.parameters.dateRanges) || null : null,
                    checkedLines: []
                };
                let reorderList = dpLib.generateReorderList(params);
                const form = generateForm(reorderList, params, createPoDetails);
                context.response.writePage(form);
            }
            else {
                let createPoDetails;
                let createdPoParam = context.request.parameters.poArrayList;
                if (createdPoParam){
                    let createPoArray = createdPoParam.split(',');
                    createPoDetails = generateCreatedPoMsg(createPoArray);
                }
                let paginationCheck = context.request.parameters.custpage_paginationcheck || 'F';
 
                let params = {
                    vendor: context.request.parameters.custpage_vendor,
                    pageNr: context.request.parameters.custpage_pageselect || 1,
                    dateRanges: getSelectedDates(context.request.parameters),
                    checkedLines: []
                };
                
                let itemListCount = context.request.getLineCount('cust_items_list');
                let selectedListCount = context.request.getLineCount('cust_selected_items');

                if (itemListCount > 0){
                    for (let i = 0 ; i < itemListCount; i++)
                    {
                        if (context.request.getSublistValue('cust_items_list','custcol_checkbox_field',i) == 'T'){
                            let record = {};
                            record.vendor = context.request.getSublistValue('cust_items_list','custcol_vendor',i);
                            record.location = context.request.getSublistValue('cust_items_list','custcol_location',i);
                            record.itemInternalID = context.request.getSublistValue('cust_items_list','custcol_iteminternal',i);
                            record.quantity = context.request.getSublistValue('cust_items_list','custcol_quantitytoorder',i);
                            record.uniqueLineId = context.request.getSublistValue('cust_items_list','custcol_itemuniqueid',i);
                            params.checkedLines.push(record);
                        }
                    }
                }
                
                log.debug('params.checkedLines 1', params.checkedLines)
                if (selectedListCount > 0){
                    for (let i = 0 ; i < selectedListCount; i++)
                    {
                        if (context.request.getSublistValue('cust_selected_items','custcol_checkbox_selected_field',i) == 'T'){
                            let record = {};
                            record.vendor = context.request.getSublistValue('cust_selected_items','custcol_selected_vendor',i);
                            record.location = context.request.getSublistValue('cust_selected_items','custcol_selected_location',i);
                            record.itemInternalID = context.request.getSublistValue('cust_selected_items','custcol_selected_iteminternal',i);
                            record.quantity = context.request.getSublistValue('cust_selected_items','custcol_selected_quantitytoorder',i);
                            record.uniqueLineId = context.request.getSublistValue('cust_selected_items','custcol_selected_uniquelineid',i);
                            params.checkedLines.push(record);
                        }
                    }
                }

                log.debug('params.checkedLines 2', params.checkedLines)
                let uniqueCheckedLines = getUniqueListBy(params.checkedLines, 'uniqueLineId')
                params.checkedLines = uniqueCheckedLines;
                log.debug('params.checkedLines 3', params.checkedLines)


                if (paginationCheck == 'T'){
                    let reorderList = dpLib.generateReorderList(params);
                    const form = generateForm(reorderList, params, createPoDetails);
                    context.response.writePage(form);
                }
                else {
                // remove lines with zero quantity
                let itemListForPo = params.checkedLines.filter(a=>a.quantity > 0);
                
                let createdPO = createPo(itemListForPo);
                let createdPoArray = createdPO.join(',');

                    redirect.toSuitelet({
                        scriptId: 'customscript_av_po_demandplanning',
                        deploymentId: 'customdeploy_av_po_demandplanning',
                        parameters: {
                            'poArrayList': createdPoArray || [],
                            'searchVendor' : params.vendor,
                            'pageNr' : params.pageNr,
                        }
                    });
                }
            } 
        }
        catch (e) {
            log.debug('onRequest:error', e);
        }
    }
    function generateCreatedPoMsg(poIdArray){
        let createPoList = [];
        poIdArray.forEach(poId => {
            let poUrl = url.resolveRecord({
                recordType: 'purchaseorder',
                recordId: poId,
                isEditMode: false
            });
    
            let poTranName = search.lookupFields({
                'type': 'purchaseorder',
                'id': poId,
                'columns': ['tranid']
            }).tranid;

            if (poTranName){
                createPoList.push({poId : poId, poUrl: poUrl, poName: poTranName });
            }
           
        });
        return createPoList;
        
    }
    function getSelectedDates(params){
        let dateRanges = {
            p1Start : params.custpage_period_one_start || null,
            p1End: params.custpage_period_one_end || null,
            p2Start : params.custpage_period_two_start || null,
            p2End: params.custpage_period_two_end || null,
            p3Start : params.custpage_period_three_start || null,
            p3End: params.custpage_period_three_end || null,
        }
        return dateRanges;
    }
    function createPo(list){
        var createPoArray = [];
        var scriptObj = runtime.getCurrentScript();
        log.debug('createPo Start: ' + scriptObj.getRemainingUsage());
        var result = groupByMultiple(list, function(item)
        {
            return [item.vendor, item.location];
        });
        log.debug('groupByMultiple', result);
        result.forEach(el => {
            try {
                var PurcheseOrder = record.create({
                    type: record.Type.PURCHASE_ORDER,
                    isDynamic: true
                });

                PurcheseOrder.setValue({
                    fieldId: 'entity',
                    value: el[0].vendor
                });      
                PurcheseOrder.setValue({
                    fieldId: 'location',
                    value: el[0].location
                });
                PurcheseOrder.setValue({
                    fieldId: 'custbody_av_enable_price_engine',
                    value: true
                });
                for (let i = 0; i < el.length; i++){
                    PurcheseOrder.selectNewLine({
                        sublistId: 'item'
                    });
                    PurcheseOrder.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'item',
                        value: el[i].itemInternalID
                    });
                    PurcheseOrder.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'quantity',
                        value: el[i].quantity
                    });
                    PurcheseOrder.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'location',
                        value: el[i].location
                    });
                    PurcheseOrder.commitLine({ sublistId: 'item' });

                }
                var poId = PurcheseOrder.save({
                    enableSourcing: false,
                    ignoreMandatoryFields: true
                });
                createPoArray.push(poId);
            }
            catch (error) {
                log.error('po creation failed', error.message);
            }
        });

        
        
        
        log.debug('createPo End: ' + scriptObj.getRemainingUsage());
        return createPoArray;

        // list.forEach(el => {
            
        // });

    }
    function generateForm(itemsObjList, params, createdPo) {
        const form = sw.createForm({title: 'Artikel bestellen'});
        log.debug('createdPo',createdPo)
        if (createdPo && createdPo.length) {
            var msg = 'Bestellung erstellt: ';
            createdPo.forEach(element => {msg += '<a href="' + element.poUrl + '">' + element.poName + '</a>' + ', ';});
            form.addPageInitMessage({type: message.Type.INFORMATION, message: msg, duration: 50000});
        }

        form.addFieldGroup({
            id: 'custpage_grp_primary',
            label: 'Artikel bestellen',
        });
        form.addFieldGroup({
            id: 'custpage_grp_periodes',
            label: 'Report periods',
        });
        var vendorFilter = form.addField({
            id: 'custpage_vendor',
            type: 'select',
            label: 'Lieferant',
            source: 'vendor',
            container: 'custpage_grp_primary',
        });
        let reportTable = form.addField({
            id: 'custpage_htmlfield',
            type: sw.FieldType.INLINEHTML,
            label: 'Table',
        });
        let loadCssTbl = form.addField({
            id: 'custpage_cssfield',
            type: sw.FieldType.INLINEHTML,
            label: 'css',
        });
        let paginationCheck = form.addField({
            id: 'custpage_paginationcheck',
            type: sw.FieldType.CHECKBOX,
            label: 'pagination check',
        });
        paginationCheck.updateDisplayType({displayType : sw.FieldDisplayType.HIDDEN});

        let p1Start = form.addField({
            id: 'custpage_period_one_start',
            type: 'DATE',
            label: 'Perioden 1 start',
            container: 'custpage_grp_periodes',
        });
        let p1End = form.addField({
            id: 'custpage_period_one_end',
            type: 'DATE',
            label: 'Perioden 1 ende',
            container: 'custpage_grp_periodes',
        });
        let p2Start = form.addField({
            id: 'custpage_period_two_start',
            type: 'DATE',
            label: 'Perioden 2 start',
            container: 'custpage_grp_periodes',
        });
        let p2End = form.addField({
            id: 'custpage_period_two_end',
            type: 'DATE',
            label: 'Perioden 2 ende',
            container: 'custpage_grp_periodes',
        });
        let p3Start = form.addField({
            id: 'custpage_period_three_start',
            type: 'DATE',
            label: 'Perioden 3 start',
            container: 'custpage_grp_periodes',
        });
        let p3End = form.addField({
            id: 'custpage_period_three_end',
            type: 'DATE',
            label: 'Perioden 3 ende',
            container: 'custpage_grp_periodes',
        });
        let lineTemp = form.addField({
            id: 'custpage_temp',
            type: 'TEXT',
            label: 'temp storage',
            container: 'custpage_grp_primary',
        });
        let pageSelect = form.addField({
            id: 'custpage_pageselect',
            label: 'Seite',
            type: sw.FieldType.SELECT,
            source: 'page',
            container: 'custpage_grp_primary',
          });
        p1Start.updateLayoutType({layoutType: sw.FieldLayoutType.STARTROW});
        p1End.updateLayoutType({layoutType: sw.FieldLayoutType.ENDROW});
        p2Start.updateLayoutType({layoutType: sw.FieldLayoutType.STARTROW});
        p2End.updateLayoutType({layoutType: sw.FieldLayoutType.MIDROW});
        p3Start.updateLayoutType({layoutType: sw.FieldLayoutType.STARTROW});
        p3End.updateLayoutType({layoutType: sw.FieldLayoutType.ENDROW});
        p2Start.updateBreakType({breakType : sw.FieldBreakType.STARTCOL});
        p3Start.updateBreakType({breakType : sw.FieldBreakType.STARTCOL});

        lineTemp.updateDisplayType({displayType : sw.FieldDisplayType.NODISPLAY});
        // Check if preSelected
        if (params.dateRanges){
            p1Start.defaultValue = params.dateRanges.p1Start 
            p1End.defaultValue = params.dateRanges.p1End

            p2Start.defaultValue = params.dateRanges.p2Start
            p2End.defaultValue = params.dateRanges.p2End

            p3Start.defaultValue = params.dateRanges.p3Start
            p3End.defaultValue = params.dateRanges.p3End
           
        }
        else {
            var today = new Date();
            var priorDate30 = new Date(new Date().setDate(today.getDate() - 30));
            var priorDate90 = new Date(new Date().setDate(today.getDate() - 180));
            var priorDate365 = new Date(new Date().setDate(today.getDate() - 365));
    
            p1End.defaultValue = new Date();
            p1Start.defaultValue = priorDate30;
            p2End.defaultValue = new Date();
            p2Start.defaultValue = priorDate90;
            p3End.defaultValue = new Date();
            p3Start.defaultValue = priorDate365;

        }
        if (params && params.vendor) {
            vendorFilter.defaultValue = params.vendor;
        }
        reportTable.defaultValue = getReportTable();
        loadCssTbl.defaultValue = loadCss();
        form.clientScriptModulePath = './av_po_demandPlanning_cs_sv2.js';


        //////// sublist main report //////////  
        var itemListSublist = form.addSublist({id: 'cust_items_list',label: 'Artikel',type: sw.SublistType.LIST});
        generateItemListSublist(itemListSublist);
        let itemsList = Object.entries(itemsObjList).map(entry => entry[1]); // Convert to List of Objects, to not have to rebuild UI structure for now.
        log.debug('itemsList', itemsList.length)
        let uncheckedItemList = itemsList.filter(a=>!a.isPreChecked) // filter out checked items
        log.debug('uncheckedItemList.length',uncheckedItemList.length)
        let pageSize = 100;
        let page = parseInt(params.pageNr);
        let pageCount = Math.ceil(itemsList.length / pageSize);
        log.debug('pageCount',pageCount)
        for (var i = 1; i <= pageCount; i++) {
            pageSelect.addSelectOption({
                value: i,
                text: i,
                isSelected: i === page
            });
        }
        let startIndex = (page - 1) * pageSize;
        let endIndex = startIndex + pageSize;

        //log.debug('index range',startIndex + ' to ' + endIndex )

        fillItemListSublist(itemListSublist, startIndex, endIndex, itemsList)
        var selectedSublist = form.addSublist({
            id: 'cust_selected_items',
            label: 'Ausgewählte Artikel',
            type: sw.SublistType.LIST
        });
        generateSelectedSublist(selectedSublist);
        fillSelectedSublist(selectedSublist,itemsList);




        if (page > 1) {
            itemListSublist.addButton({
                id: 'previous',
                label: 'Vorherige Seite',
                functionName: 'goToPage(' + (page - 1) + ')'
            });
        }
          // add a navigation button for the next page
        if (endIndex < itemsList.length) {
            itemListSublist.addButton({
              id: 'next',
              label: 'Nachste Seite',
              functionName: 'goToPage(' + (page + 1) + ')'
            });
        }
        itemListSublist.addButton({
            id: 'addbtn',
            label: 'Artikel hinzufuegen',
            functionName: 'goToPage(' + (page) + ')'
          });
      


        form.addSubmitButton({
            label: 'Bestellung erstellen'
        });
        return form;
    }
    function generateItemListSublist(itemListSublist){
        itemListSublist.addButton({
          id : 'markallbtn',
          label : 'Alle Markieren',
          functionName: 'markall()'
        });
        itemListSublist.addButton({
          id : 'unmarkallbtn',
          label : 'Alle Markierungen enfernen',
          functionName: 'unmarkall()'
        });
        itemListSublist.addField({id: 'custcol_checkbox_field',
          label: 'Verarbeiten',
          type: sw.FieldType.CHECKBOX
        });


        itemListSublist.addField({
            id : 'custcol_artikelnummer',
            label : 'AN',
            type : sw.FieldType.TEXT,
        });
        let itemurl = itemListSublist.addField({
            id : 'custcol_item',
            label : 'Name',
            type : sw.FieldType.TEXTAREA,
        });
        itemListSublist.addField({
            id : 'custcol_lieferantenartikelnummer',
            label : 'Ex-AN',
            type : sw.FieldType.TEXT,
        });
        let quantityToOrder = itemListSublist.addField({ // ENTRY
            id : 'custcol_quantitytoorder',
            label :'Bestellmenge',
            type : sw.FieldType.INTEGER
        });
        itemListSublist.addField({
            id : 'custcol_quantity_location_on_hand',
            label :'Bestand gesamt', // qty avilable on hand
            type : sw.FieldType.INTEGER
        });
        itemListSublist.addField({
            id : 'custcol_quantity_location',
            label :'verfügbarer Bestand', // qty avilable all locations
            type : sw.FieldType.INTEGER
        });
        itemListSublist.addField({
            id : 'custcol_quantityopenpo',
            label :'offene </br>  Lieferanten </br>  Bestellungen', // open in po
            type : sw.FieldType.INTEGER
        });      
        itemListSublist.addField({
            id : 'custcol_quantityopenso',
            label :'offene </br> Kunden </br> Aufträge', // open in so
            type : sw.FieldType.INTEGER
        });
        itemListSublist.addField({
            id : 'custcol_producerstockqt',
            label :'Lieferanten </br> Bestand </br> Menge', // producer stock qty
            type : sw.FieldType.FLOAT
        });
        itemListSublist.addField({
            id : 'custcol_producerstockisavailable',
            label :'Lieferanten </br> Bestand', // producer stock available
            type : sw.FieldType.TEXT
        });
        itemListSublist.addField({
            id : 'custcol_saeftystock',
            label :'Min',
            type : sw.FieldType.FLOAT
        });
        itemListSublist.addField({
            id : 'custcol_prefstocklevel',
            label :'Soll',
            type : sw.FieldType.FLOAT
        });
        itemListSublist.addField({ // TOBEADDED
            id : 'custcol_itemstatus',
            label :'Status',
            type : sw.FieldType.TEXT
        });
        itemListSublist.addField({
            id : 'custcol_purchaseprice',
            label :'Einkaufspreis',
            type : sw.FieldType.FLOAT
        });
        itemListSublist.addField({
            id : 'custcol_salesprice',
            label :'Verkaufspreis',
            type : sw.FieldType.FLOAT
        });
        itemListSublist.addField({
            id : 'custcol_minimum_order_value',
            label :'Mindestbestellwert',
            type : sw.FieldType.FLOAT
        });
        let vendorName = itemListSublist.addField({ // 18
            id : 'custcol_vendor_name',
            label : 'Lieferant',
            type : sw.FieldType.TEXTAREA
        });
        let locationCol = itemListSublist.addField({
            id : 'custcol_location',
            label : 'Standort',
            type : 'select',
            source: 'location'
        });
        let lineId = itemListSublist.addField({ // Hidden
            id : 'custcol_lineid',
            label : 'Line id',
            type : sw.FieldType.TEXT,
        });
        let itemIdCol = itemListSublist.addField({ // Hidden
            id : 'custcol_iteminternal',
            label : 'Artikel id',
            type : sw.FieldType.TEXT,
        });
        let locationIdCol = itemListSublist.addField({ // hidden
            id : 'custcol_locationid',
            label : 'Standort id',
            type : sw.FieldType.TEXT,
            source: 'location'
        });
        let vendorCol = itemListSublist.addField({ // hidden
            id : 'custcol_vendor',
            label : 'Lieferant',
            type : sw.FieldType.INTEGER
        });
        let itemDisplayName = itemListSublist.addField({ // hidden
            id : 'custcol_itemdisplayname',
            label :'item name',
            type : sw.FieldType.TEXT
        });
        let itemUniqueId =  itemListSublist.addField({ // hidden
            id : 'custcol_itemuniqueid',
            label :'item guid',
            type : sw.FieldType.TEXT
        });
        let itemCategoryRebate =  itemListSublist.addField({ // hidden
            id : 'custcol_itemcatrebate',
            label :'item cat rebate',
            type : sw.FieldType.FLOAT
        });
        
      lineId.updateDisplayType({displayType : sw.FieldDisplayType.HIDDEN});
      locationCol.updateDisplayType({displayType : sw.FieldDisplayType.DISABLED});
      quantityToOrder.updateDisplayType({displayType : sw.FieldDisplayType.ENTRY});
      itemIdCol.updateDisplayType({displayType : sw.FieldDisplayType.HIDDEN});
      locationIdCol.updateDisplayType({displayType : sw.FieldDisplayType.HIDDEN});
      vendorCol.updateDisplayType({displayType : sw.FieldDisplayType.HIDDEN});
      itemDisplayName.updateDisplayType({displayType : sw.FieldDisplayType.HIDDEN});
      itemUniqueId.updateDisplayType({displayType : sw.FieldDisplayType.HIDDEN})
      itemurl.updateDisplayType({displayType : sw.FieldDisplayType.INLINE});
      itemCategoryRebate.updateDisplayType({displayType : sw.FieldDisplayType.HIDDEN});


    }
    function fillItemListSublist(sublist, startIndex, endIndex, itemsListFull){
        let counter = 0
        let itemsList = itemsListFull.slice(startIndex,endIndex);

       


        //log.debug('itemsList.length',itemsList.length)
        for (let i = 0; i < itemsList.length; i++) {  
                if (itemsList[i].isPreChecked){
                    sublist.setSublistValue({id: 'custcol_checkbox_field',line: counter,value: 'T'}); 
                }
                sublist.setSublistValue({id: 'custcol_lineid',line: counter,value: 'dm_table_line_' + counter});   
                sublist.setSublistValue({id: 'custcol_item',line: counter,value: '<a target="_blank" href="' + itemsList[i].itemUrl + '">' + itemsList[i].itemDisplayName + '</a>'});
                sublist.setSublistValue({id: 'custcol_iteminternal',line: counter,value: itemsList[i].itemIdNumber != '' ? itemsList[i].itemIdNumber : '-'});
                sublist.setSublistValue({id: 'custcol_location',line: counter,value: 3});
                sublist.setSublistValue({id: 'custcol_locationid',line: counter,value: 3});
                sublist.setSublistValue({id: 'custcol_lieferantenartikelnummer',line: counter,value: itemsList[i].Lieferantenartikelnummer != '' ? itemsList[i].Lieferantenartikelnummer : '-'});
                sublist.setSublistValue({id: 'custcol_artikelnummer',line: counter,value: itemsList[i].Artikelnummer != '' ? itemsList[i].Artikelnummer : '-'});
                sublist.setSublistValue({id: 'custcol_vendor',line: counter,value: itemsList[i].manufacturerVendorId});
                sublist.setSublistValue({id: 'custcol_vendor_name',line: counter,value: '<a target="_blank" href="' + itemsList[i].VendorUrl + '">' + itemsList[i].manufacturerVendorName + '</a>'});
                sublist.setSublistValue({id: 'custcol_quantitytoorder',line: counter,value: String(itemsList[i].quantityToOrder)});
                sublist.setSublistValue({id: 'custcol_quantity_location',line: counter,value: itemsList[i].quantityInAllLocations != '' ? itemsList[i].quantityInAllLocations : 0});
                sublist.setSublistValue({id: 'custcol_quantityopenso',line: counter,value: itemsList[i].openInSo != '' ? itemsList[i].openInSo : 0});
                sublist.setSublistValue({id: 'custcol_quantityopenpo',line: counter,value: itemsList[i].openInPo != '' ? itemsList[i].openInPo : 0});
                sublist.setSublistValue({id: 'custcol_producerstockqt',line: counter,value: itemsList[i].ps_quantity != '' ? itemsList[i].ps_quantity : 0});
                sublist.setSublistValue({id: 'custcol_producerstockisavailable',line: counter,value: itemsList[i].ps_is_available != '' ? itemsList[i].ps_is_available : 'Nein'});
                sublist.setSublistValue({id: 'custcol_purchaseprice',line: counter,value: itemsList[i].purchasePrice != '' ? itemsList[i].purchasePrice : 0});
                sublist.setSublistValue({id: 'custcol_salesprice',line: counter,value: itemsList[i].uvp != '' ? itemsList[i].uvp : 0});
                sublist.setSublistValue({id: 'custcol_saeftystock',line: counter,value: itemsList[i].safetystocklevel != '' ? itemsList[i].safetystocklevel : 0});
                sublist.setSublistValue({id: 'custcol_prefstocklevel',line: counter,value: itemsList[i].preferredstocklevel != '' ? itemsList[i].preferredstocklevel : 0});
                sublist.setSublistValue({id: 'custcol_minimum_order_value',line: counter,value: itemsList[i].minimumOrderValue != '' ? itemsList[i].minimumOrderValue : 0});
                sublist.setSublistValue({id: 'custcol_itemdisplayname',line: counter,value: itemsList[i].itemDisplayName || ' '});
                sublist.setSublistValue({id: 'custcol_itemuniqueid',line: counter,value: itemsList[i].uniqueLineId || ' '})
                sublist.setSublistValue({id: 'custcol_itemcatrebate',line: counter,value: itemsList[i].itemCategoryReb || 0})
                sublist.setSublistValue({id: 'custcol_itemstatus',line: counter,value: itemsList[i].itemStatus || ' '})
                sublist.setSublistValue({id: 'custcol_quantity_location_on_hand',line: counter,value: itemsList[i].quantityOnHandAllLocations || 0})
                
                


                counter++
            
        }

    }
    function fillSelectedSublist(sublist, itemsList){
        let preSelected = itemsList.filter(a=>a.isPreChecked)
        let counter = 0
        if (preSelected.length){
            for (let i = 0; i < preSelected.length; i++) {
                sublist.setSublistValue({id: 'custcol_selected_iteminternal',line: counter,value: preSelected[i].itemIdNumber != '' ? preSelected[i].itemIdNumber : '-'});
                sublist.setSublistValue({id: 'custcol_selected_location',line: counter,value: 3});
                sublist.setSublistValue({id: 'custcol_selected_vendor',line: counter,value: preSelected[i].manufacturerVendorId});
              
                sublist.setSublistValue({id: 'custcol_checkbox_selected_field',line: counter,value: 'T'});
                sublist.setSublistValue({id: 'custcol_selected_lineid',line: counter,value: 'dm_table_line_' + counter});   
                sublist.setSublistValue({id: 'custcol_selected_item',line: counter,value: '<a target="_blank" href="' + preSelected[i].itemUrl + '">' + preSelected[i].itemDisplayName + '</a>'});
                sublist.setSublistValue({id: 'custcol_selected_quantitytoorder',line: counter,value: String(preSelected[i].quantityToOrder)});
                sublist.setSublistValue({id: 'custcol_selected_purchaseprice',line: counter,value: preSelected[i].purchasePrice != '' ? preSelected[i].purchasePrice : 0});
                sublist.setSublistValue({id: 'custcol_selected_uniquelineid',line: counter,value: preSelected[i].uniqueLineId || ' '})
                counter++
          }
        }
    }
    function generateSelectedSublist(selectedSublist){

        // selectedSublist.addButton({
        //     id : 'markallbtn',
        //     label : 'Alle Markieren',
        //     functionName: 'markall()'
        // });
        // selectedSublist.addButton({
        //     id : 'unmarkallbtn',
        //     label : 'Alle Markierungen enfernen',
        //     functionName: 'unmarkall()'
        // });
        selectedSublist.addField({
            id: 'custcol_checkbox_selected_field',
            label: 'Verarbeiten',
            type: sw.FieldType.CHECKBOX
        });
        let itemurl = selectedSublist.addField({
            id : 'custcol_selected_item',
            label : 'Name',
            type : sw.FieldType.TEXTAREA,
        });

        let itemIdCol = selectedSublist.addField({
            id : 'custcol_selected_iteminternal',
            label : 'Artikel id',
            type : sw.FieldType.TEXT,
        });
        let locationCol = selectedSublist.addField({
            id : 'custcol_selected_location',
            label : 'Standort',
            type : 'select',
            source: 'location'
        });

        selectedSublist.addField({
            id : 'custcol_selected_vendor',
            label : 'vendor ID',
            type : sw.FieldType.TEXT,
        });
        let quantityToOrder = selectedSublist.addField({
            id : 'custcol_selected_quantitytoorder',
            label : 'Bestellmenge',
            type : sw.FieldType.INTEGER,
        });
        selectedSublist.addField({
            id : 'custcol_selected_purchaseprice',
            label : 'Einkaufspreis',
            type : sw.FieldType.FLOAT,
        });
        selectedSublist.addField({
            id : 'custcol_selected_uniquelineid',
            label : 'item guid',
            type : sw.FieldType.TEXT,
        });

        quantityToOrder.updateDisplayType({displayType : sw.FieldDisplayType.ENTRY});
        locationCol.updateDisplayType({displayType : sw.FieldDisplayType.DISABLED});
        itemIdCol.updateDisplayType({displayType : sw.FieldDisplayType.HIDDEN});
      //lineId.updateDisplayType({displayType : sw.FieldDisplayType.HIDDEN});


    }
    function groupByMultiple( array , f )
    {
        var groups = {};
        array.forEach( function( o )
        {
            var group = JSON.stringify( f(o) );
            groups[group] = groups[group] || [];
            groups[group].push( o );  
        });
        return Object.keys(groups).map( function( group )
        {
            return groups[group]; 
        });
    }
    function loadCss(){
        let css = file.load({
            id: './av_po_demandPlanning_css.css'
        });

        return css.getContents();
    }
    function getReportTable(){
        // let css = file.load({
        //     id: './av_po_demandPlanning_css.css'
        // });

        let bodyTable =
        //  '' + css.getContents(); + ''+
        '<div id="itemReport">' +
        '<div class="loaderCust"></div>' +
        '</div>';

        
        return bodyTable;
    }
    function getUniqueListBy(arr, key) {
        return [...new Map(arr.map(item => [item[key], item])).values()]
    }

    return {
        onRequest,
    };
});
