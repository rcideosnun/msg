/**
 *@NApiVersion 2.1
 *@NScriptType ClientScript
 */
define(['N/currentRecord', 'N/url', 'N/search','N/runtime','N/format', './av_po_demanPlanning_lib'], function(currentRecord, url, search, runtime, format, dpLib) {

    function pageInit(context){
        var a = jQuery('#itemReport');
        var b = jQuery('#div__body');
        jQuery(a).insertAfter(b);
        
        var lineCount = context.currentRecord.getLineCount('cust_items_list');
        for (let a = 0; a < lineCount; a++){
            let quantity = context.currentRecord.getSublistValue('cust_items_list','custcol_quantitytoorder',a);
            let purchasePrice = context.currentRecord.getSublistValue('cust_items_list','custcol_purchaseprice',a);
            let minimum = context.currentRecord.getSublistValue('cust_items_list','custcol_minimum_order_value',a);
            let lineId = context.currentRecord.getSublistValue('cust_items_list','custcol_lineid', a);
            if ((quantity * purchasePrice) < minimum) {
                markYellow(lineId);
            }
        }
  
        var table = document.getElementById('cust_items_list_splits');
        var rows = table.getElementsByTagName('tr');

        var currentRow = table.rows[0];
        var dropReport = function(row){
            return function() {
                jQuery('#rowReport').remove();
            }

        }
        currentRow.onclick = dropReport(currentRow);

        for (let i = 1; i < rows.length; i++) {

            var currentRow = table.rows[i];
            var createClickHandler = function(row) {             
                return function() {
                    // Refactor and cleanup if there is time.
                    
                    let convertTableToObj = convertTable();
                    context.currentRecord.setValue('custpage_temp','aaa');
                    jQuery('.loaderCust').show();
                    let cell = row.getElementsByTagName('td')[20];
                    let id = cell.childNodes[0].defaultValue; // Ugly way of hacking around 
                    //let itemName = row.getElementsByTagName('td')[1].textContent;
                    let itemId = row.getElementsByTagName('td')[20].getElementsByTagName('input')[0].value;
                    let itemCatRebate = parseFloat(row.getElementsByTagName('td')[25].getElementsByTagName('input')[0].value);
                    //let vendorName = row.getElementsByTagName('td')[7].textContent;
                    //let vendorId = row.getElementsByTagName('td')[6].getElementsByTagName('input')[0].value;
                    //let locationId = row.getElementsByTagName('td')[5].getElementsByTagName('input')[0].value;
                    //let locationtxt = row.getElementsByTagName('td')[4].getElementsByTagName('input')[0].title; 


                    // let itemCategoryRebate = 


                    let p1Start = context.currentRecord.getValue('custpage_period_one_start');
                    let p1End = context.currentRecord.getValue('custpage_period_one_end');
            
                    let periodOne = getPeriodData(p1Start, p1End, id);
                    let p2Start = context.currentRecord.getValue('custpage_period_two_start');
                    let p2End = context.currentRecord.getValue('custpage_period_two_end');
                    let periodTwo = getPeriodData(p2Start, p2End, id);
                    let p3Start = context.currentRecord.getValue('custpage_period_three_start');
                    let p3End = context.currentRecord.getValue('custpage_period_three_end');
                    let periodThree = getPeriodData(p3Start, p3End, id);
                    //let params = {location: locationId, item: itemId};
                    let reorderList = dpLib.getSpecificItemVendorPurchasePrice(itemId, itemCatRebate);

                    let htmlTable = buildHtmlTable(periodOne, periodTwo, periodThree, reorderList);


                    jQuery('#rowReport').remove();
                    jQuery(this).after(htmlTable);

                    // scriptObj.getRemainingUsage = function () { return 1000; };
                    // scriptObj.getRemainingUsage = function () { return this.getTotalUsage() - (this.usage[this.getScriptId()] == null ? 0 : parseInt(this.usage[this.getScriptId()])); }
                    var scriptObj = runtime.getCurrentScript();

                    console.log('Remaining governance units: ' + scriptObj.getRemainingUsage());

                    // window.scrollTo(0, document.body.scrollHeight);
                    jQuery('.loaderCust').hide();                    // alert('id:' + id);
                };
            };
            currentRow.onclick = createClickHandler(currentRow);
        }
          
    }
    function convertTable(){
        let trDom =  jQuery('#cust_items_list_splits tr.uir-list-row-tr').map(function(i, v) {
            var $td =  jQuery('td', this);
            var colCount = document.getElementById("cust_items_list_splits").rows[0].cells.length;
            return {
                id: ++i,
                vendorId: $td.eq(7).text(),
                purchasePrice: $td.eq(20).text(),
            };
        }).get();
        return trDom;
    }
 
    function buildHtmlTable(p1,p2,p3, reorderList){
        let itemsList = Object.entries(reorderList).map(entry => entry[1]);

        let reorderListCount = itemsList.length;
        if (reorderListCount > 0)
        {
            itemsList.sort((a,b) => a.purchasePrice - b.purchasePrice); 
            itemsList.length=3;
        }

        let reportHtml = 
           '<tr id="rowReport">' +
        '<td colspan=11>'+
        '<div id="my_box">'+
           //'<div class="fgroup_title" style="er-bottom: 1px solid #CCC; white-space:nowrap;height: 20px;margin-left: 20px!important;margin-right: 20px!important;margin-bottom: 10px!important;background: #e0e6ef;"><span style="margin-left: 5px;">Report für : '+ itemName + ' : ' + locationtxt +'</span></div>'+
  
        '<table class="ReportTable">'+
        '<thead>' +
        '<tr>' +
        '<th></th>' +
        '<th>Datum</th>' +
        '<th>Datum</th>' +
        '<th>Verkaufte Menge gesamt</th>' +
        '<th>Verkaufte Menge im Shop</th>' +
        '<th>Verkaufte Menge online</th>' +
        '<th>Anzahl Aufträge</th>' +
        '<th>Verhältnis Verkauf zu Anzahl Aufträge</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>'+


        '<tr>'+
        '<td>' + 'Periode 1' + '</td>'+
        '<td>' + p1.from + '</td>'+
        '<td>' + p1.to + '</td>'+
        '<td>' + p1.groupQuantity+ '</td>'+
        '<td>' + p1.deutschlandsberg + '</td>'+
        '<td>' + p1.hauptlager+ '</td>'+
        '<td>' + p1.numberOfTransactions + '</td>'+
        '<td>' + p1.ratio + '</td>'+
        '</tr>'+



        '<tr>'+
        '<td>' + 'Periode 2'  + '</td>'+
        '<td>' + p2.from  + '</td>'+
        '<td>' + p2.to + '</td>'+
        '<td>' + p2.groupQuantity+ '</td>'+
        '<td>' + p2.deutschlandsberg+ '</td>'+
        '<td>' + p2.hauptlager+ '</td>'+
        '<td>' + p2.numberOfTransactions + '</td>'+
        '<td>' + p2.ratio + '</td>'+
        '</tr>'+


        '<tr>'+
        '<td>' + 'Periode 3'  + '</td>'+
        '<td>' + p3.from + '</td>'+
        '<td>' + p3.to + '</td>'+
        '<td>' + p3.groupQuantity+ '</td>'+
        '<td>' + p3.deutschlandsberg+ '</td>'+
        '<td>' + p3.hauptlager+ '</td>'+
        '<td>' + p3.numberOfTransactions + '</td>'+
        '<td>' + p3.ratio + '</td>'+
        '</tr>'+
        
        '</tbody>'+
        '</table>'+
        '</div></td>' +
        '<td colspan=4>'+
        '<div id="my_box2">'+
        '<table class="ReportTable">'+
        '<thead>' +
        '<tr>' +
        '<th>Lieferant</th>' +
        '<th>Einkaufspreis</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>';      
        if (reorderListCount > 0)
        {
            itemsList.forEach(element => {
                reportHtml += '<tr>' +
                '<td>' + element.manufacturerVendorName + '</td>'+
                '<td>' + element.purchasePrice + '</td>'+
                '</tr>';
            });
            // fill with blanks
            let lineCount = 3 - reorderListCount;
            for (let i = 0; i < lineCount ; i++){
                reportHtml +='<tr>' +'<td>-</td>'+'<td>-</td>'+'</tr>';
            }
        }
        else {
            '<tr>' +
                '<td>-</td>'+
                '<td>-</td>'+
            '</tr>' +
            '<tr>' +
                '<td>-</td>'+
                '<td>-</td>'+
            '</tr>' +
            '<tr>' +
                '<td>-</td>'+
                '<td>-</td>'+
            '</tr>';
        }


        reportHtml +='</tbody>'+'</table>'+'</div>'+'</td>'+'</tr>';
 
        return reportHtml;
    }
    function formatDate(dateParam){
        if (dateParam && dateParam != ''){
            try {
                return  format.format({value:dateParam, type:format.Type.DATE}) ;
            } catch (error) {
                return '';
            }
        }
        else return '';
    }
    function getPeriodData(dateStart, dateEnd, item){
        // Refactor when there is time
        // use single search for all periods, take min max dates from inputs.
        // split them to 3 different periods after the search.

        let periodData = {
            from: dateStart != '' ? format.format({value:dateStart,type:format.Type.DATE}) : '',
            to: dateEnd !=''? format.format({value:dateEnd,type:format.Type.DATE}): '',
            groupQuantity: 0,
            deutschlandsberg: 0,
            wien: 0,
            hauptlager: 0,
            uniqueTransactionIds: [],
            numberOfTransactions: 0,
            ratio: 0
        };

        if (dateStart != '' && dateEnd != '' && item != ''){
            let startDate = format.format({value:dateStart,type:format.Type.DATE});
            let endDate = format.format({value:dateEnd,type:format.Type.DATE});
            let searchObj = [];
            let filters = [
                ['mainline','is','F'], 
                'AND', 
                ['item.type','anyof','InvtPart'], 
                'AND', 
                ['type','anyof','CashSale','SalesOrd'],  
                'AND',
                ['location','anyof', 1,3,7], 
                'AND', 
            ];
            if (dateStart && dateEnd){
                filters.push(['trandate','within',startDate,endDate],'AND',);
            }
            if (item){
                filters.push(['item','anyof',item]);
            }
            var transactionSearchObj = search.create({
                type: 'transaction',
                filters:filters,
                columns:
            [
                'item',
                search.createColumn({
                    name: 'displayname',
                    join: 'item'
                }),
                'quantity',
                'location',
                'type'
            ]
            });

            transactionSearchObj.run().each(function(result){
                let record = {};
                record.periodStart = dateStart,
                record.periodEnd = dateEnd,
                record.itemId = result.getValue('item');
                record.itemDisplay = result.getValue({name: 'displayname',join: 'item'});
                record.quantity = parseInt(result.getValue({name: 'quantity'}));
                record.location = result.getValue('location');
                record.locationTxt = result.getText('location');
                record.type = result.getValue('type');
                record.id = result.id
                searchObj.push(record);
                return true;
            });

            if (searchObj.length > 0){
                searchObj.forEach(element => {
                    periodData.groupQuantity += element.quantity;
                    periodData.deutschlandsberg += element.location == 1 ? element.quantity : 0;
                    periodData.hauptlager += (element.location == 3 || element.location == 7) ? element.quantity : 0; 
                    if (periodData.uniqueTransactionIds.indexOf(element.id) === -1){
                        periodData.uniqueTransactionIds.push(element.id)
                        console.log('unqiueId: ' + element.id);
                        periodData.numberOfTransactions += 1;
                    }

                });
                console.log('uniqueTransactionIds: ' + periodData.uniqueTransactionIds);
                if (periodData.groupQuantity > 0 && periodData.numberOfTransactions > 0)
                    periodData.ratio = Math.round(((periodData.groupQuantity / periodData.numberOfTransactions) + Number.EPSILON) * 100) / 100;
            }
            else {
                periodData.ratio = 0;
            }

            return periodData;
        }
        else {
            periodData.groupQuantity = '-'
            periodData.deutschlandsberg = '-'
            periodData.wien = '-'
            periodData.hauptlager = '-'
            periodData.numberOfTransactions = '-'
            periodData.ratio = '-'
            return periodData
        }

    }
    function markall() {

        // var rec = _currentRecord.get();
        // rec.getLineCount('cust_items_list');

        var count=nlapiGetLineItemCount('cust_items_list'); //gets the count of lines
        for(var i=1;i<=count;i++) {
            nlapiSelectLineItem('cust_items_list',i);
            nlapiSetCurrentLineItemValue('cust_items_list','custcol_checkbox_field','T',true,true); //'custcol_checkbox_field' is checkbox's field ID.
        }
        nlapiCommitLineItem('cust_items_list');
    }
    function unmarkall() {
    
        var count=nlapiGetLineItemCount('cust_items_list');
        for(var i=1;i<=count;i++) {
            nlapiSelectLineItem('cust_items_list',i);
            nlapiSetCurrentLineItemValue('cust_items_list','custcol_checkbox_field','F',true,true); //'custcol_checkbox_field' is checkbox's field ID.
        }
        nlapiCommitLineItem('cust_items_list');
    }
    function fieldChanged(context) {
        if (context.fieldId == 'custpage_vendor' || context.fieldId == 'custpage_location')
        {
            //var location = context.currentRecord.getValue('custpage_location');
            var vendor = context.currentRecord.getValue('custpage_vendor');
            
            var output = url.resolveScript({
                scriptId: 'customscript_av_po_demandplanning',
                deploymentId: 'customdeploy_av_po_demandplanning',
                params: {'searchVendor': vendor, 'searchlocation': location},
                returnExternalUrl: false
            });
            window.location = output;
        }
        if (context.fieldId == 'custcol_quantitytoorder')
        {
            let changedLine = context.line;
            let lineId = context.currentRecord.getSublistValue('cust_items_list','custcol_lineid',changedLine);
            let quantity = context.currentRecord.getSublistValue('cust_items_list','custcol_quantitytoorder',changedLine);
            let lineGuid = context.currentRecord.getSublistValue('cust_items_list','custcol_itemuniqueid',changedLine)
            let findLine = context.currentRecord.findSublistLineWithValue({sublistId: 'cust_selected_items',fieldId: 'custcol_selected_uniquelineid',value: lineGuid});

            if (quantity < 0){
                var curLine = context.currentRecord.selectLine({sublistId: 'cust_items_list',line: changedLine});
                curLine.setCurrentSublistValue({
                    sublistId: 'cust_items_list',
                    fieldId: 'custcol_quantitytoorder',
                    value: 1,
                    ignoreFieldChange: true
                });
            }

            let purchasePrice = context.currentRecord.getSublistValue('cust_items_list','custcol_purchaseprice',changedLine);
            let minimum = context.currentRecord.getSublistValue('cust_items_list','custcol_minimum_order_value',changedLine);
            if ((quantity * purchasePrice) < minimum) {
                markYellow(lineId);
            }
            else {
                unmarkYellow(lineId);
            }

            if (findLine >= 0){
                context.currentRecord.selectLine({sublistId: 'cust_selected_items',line: findLine});
                context.currentRecord.getCurrentSublistValue
                context.currentRecord.setCurrentSublistValue({
                    sublistId: 'cust_selected_items',
                    fieldId: 'custcol_selected_quantitytoorder',
                    value: quantity,
                    ignoreFieldChange: true,
                })
                context.currentRecord.commitLine({sublistId: 'cust_selected_items'});
            }
        }
        if (context.fieldId == 'custcol_checkbox_field'){
            let lineSelected = context.line;
            let lineGuid = context.currentRecord.getSublistValue('cust_items_list','custcol_itemuniqueid',lineSelected)
            let lineChecked = context.currentRecord.getSublistValue('cust_items_list','custcol_checkbox_field',lineSelected)
            let findLine = context.currentRecord.findSublistLineWithValue({
                sublistId: 'cust_selected_items',
                fieldId: 'custcol_selected_uniquelineid',
                value: lineGuid
            });
            if (findLine >= 0){
                context.currentRecord.selectLine({sublistId: 'cust_selected_items',line: findLine});
                context.currentRecord.getCurrentSublistValue
                context.currentRecord.setCurrentSublistValue({
                    sublistId: 'cust_selected_items',
                    fieldId: 'custcol_checkbox_selected_field',
                    value: lineChecked,
                    ignoreFieldChange: true,
                })
                context.currentRecord.commitLine({sublistId: 'cust_selected_items'});
            }

        }
        if (context.fieldId == 'custcol_checkbox_selected_field'){
            let lineSelected = context.line;
            let lineGuid = context.currentRecord.getSublistValue('cust_selected_items','custcol_selected_uniquelineid',lineSelected)
            let lineChecked = context.currentRecord.getSublistValue('cust_selected_items','custcol_checkbox_selected_field',lineSelected)
            let findLine = context.currentRecord.findSublistLineWithValue({
                sublistId: 'cust_items_list',
                fieldId: 'custcol_itemuniqueid',
                value: lineGuid
            });
            if (findLine >= 0){
                context.currentRecord.selectLine({sublistId: 'cust_items_list',line: findLine});
                context.currentRecord.getCurrentSublistValue
                context.currentRecord.setCurrentSublistValue({
                    sublistId: 'cust_items_list',
                    fieldId: 'custcol_checkbox_field',
                    value: lineChecked,
                    ignoreFieldChange: true,
                })
                context.currentRecord.commitLine({sublistId: 'cust_items_list'});
                let tt = 'stop';
            }


        }
        if (context.fieldId == 'custcol_selected_quantitytoorder')
        {
            let changedLine = context.line;
            let quantity = context.currentRecord.getSublistValue('cust_selected_items','custcol_selected_quantitytoorder',changedLine);
            let lineGuid = context.currentRecord.getSublistValue('cust_selected_items','custcol_selected_uniquelineid',changedLine)
            let findLine = context.currentRecord.findSublistLineWithValue({sublistId: 'cust_items_list',fieldId: 'custcol_itemuniqueid',value: lineGuid});

            if (findLine >= 0){
                context.currentRecord.selectLine({sublistId: 'cust_items_list',line: findLine});
                context.currentRecord.setCurrentSublistValue({
                    sublistId: 'cust_items_list',
                    fieldId: 'custcol_quantitytoorder',
                    value: quantity,
                    ignoreFieldChange: true,
                })
                context.currentRecord.commitLine({sublistId: 'cust_items_list'});
            }
        }
        if (context.fieldId == 'custpage_pageselect'){
            let selectedPage = context.currentRecord.getValue("custpage_pageselect");
            goToPage(selectedPage);
        }
        // if (context.fieldId == 'custpage_pageselect')
        // {
        //     let pgNr = Number(context.currentRecord.getValue('custpage_pageselect'));
        //     context.currentRecord.setValue('custpage_vendor',pgNr);
        //     goToPage();


        // }





    }
    function makeDivisible(qty,pcgSize){
        var tempValue = qty/pcgSize;
        var isDividable = tempValue % 1;
        if (!isDividable == 0){
            return Math.ceil(tempValue) * pcgSize;
        }
        else return qty;
    }
    function groupByMultiple( array , f )
    {
        var groups = {};
        array.forEach( function( o )
        {
            var group = JSON.stringify( f(o) );
            groups[group] = groups[group] || [];
            groups[group].push( o );  
        });
        return Object.keys(groups).map( function( group )
        {
            return groups[group]; 
        });
    }
    function markYellow(line){
        jQuery('tr input[value="'+line+'"').parent().parent().find('td').attr('style', function(i,s) { 
            if (s && s.indexOf('background: #fdff7c !important') > 0){
                return  (s || '');
            }
            else  return (s || '') + ' ;background: #fdff7c !important;';
          

        });
    }
    function unmarkYellow(line){
        jQuery('tr input[value="'+line+'"').parent().parent().find('td').attr('style', function(i,s) { 
            if (s) {
                s = s.replace(';background: #fdff7c !important;','');
            }
            return (s || '');
        });
        
    }
    function goToPage(page) {
        debugger;
        let cr = currentRecord.get();
        cr.setValue({fieldId: 'custpage_pageselect',value: parseInt(page),ignoreFieldChange: true,forceSyncSourcing: true});
        cr.setValue({fieldId: 'custpage_paginationcheck',value: true, ignoreFieldChange: true,forceSyncSourcing: true});
        if (window.save_record()) {
            window.main_form.submit();
        }
    }

    return {
        goToPage: goToPage,
        pageInit: pageInit,
        markall: markall,
        unmarkall: unmarkall,
        fieldChanged: fieldChanged,
    };
});
