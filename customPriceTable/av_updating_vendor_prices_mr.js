/**
 ** Copyright (c) 2022 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 *
 * Author: Tuba Mohsin
 * Date: 01.09.2023
 * update the vendor purchase price on the item
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */

define([
    "N/search",
    "N/runtime",
    './av_updating_vendor_prices_lib'
], function (search, runtime, lib) {

    const getInputData = () => {
        log.audit("getInputData", `-Script Started on ${new Date()}-`);
        try {
            return getItems()
        } catch (ex) {
            log.error({ title: "getInputData : ex", details: ex });
        }
    };

    const map = (context) => {
        log.audit({ title: "map", details: 'triggered' });
        const data = JSON.parse(context.value);
        log.audit('map data', data);
        try {
            lib.updateVendorPrices(data);
        } catch (ex) {
            log.audit('error', ex);
        }
    };


    const summarize = () => {
        log.audit("summarize", `-Script Finished on ${new Date()}-`);
    };

    const getItems = () => {
        const currentRuntime = runtime.getCurrentScript();
        let itemList = [];
        let itemSearch = search.load({
            id: currentRuntime.getParameter({ name: "custscript_av_item_list" }),
        });

        let myPagedData = itemSearch.runPaged({ pageSize: 1000 });

        myPagedData.pageRanges.forEach(function (pageRange) {
            let myPage = myPagedData.fetch({ index: pageRange.index });
            myPage.data.forEach(function (result) {
                let internalId = result.id || result.getValue('internalid') || result.getValue({ name: "internalid", summary: "GROUP" });
                let type = result.recordType || null
                itemList.push({ id: internalId, type: type })
            });
        });

        let resultGroups = [];
        const size = 50;

        for (let i = 0; i < itemList.length; i += size) {
            resultGroups.push(itemList.slice(i, i + size));
        }
        log.audit('resultGroups', resultGroups);
        return resultGroups;
    };

    return {
        getInputData: getInputData,
        map: map,
        summarize: summarize,
    };
});
