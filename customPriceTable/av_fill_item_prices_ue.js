/** 
 * Copyright (c) 2023 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @author: Tuba Mohsin
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @description: Check if new record has any price = 0, fill the price from the earlier date in case this price is still valid 
 */


define(['N/search', 'N/record'], function (search, record) {
    const LOGS = {};
    const beforeSubmit = (context) => {
        if (context.type == 'delete') return;
        LOGS.contextType = context.type;

        try {
            const newRecord = context.newRecord;
            const itemId = context.newRecord.getValue('custrecord_msg_ip_item');
            const { priceIsZero, zeroPricesList } = checkPriceIsZero(newRecord);
            LOGS.recordId = newRecord.id;
            LOGS.itemId = itemId;
            LOGS.zeroPricesList = zeroPricesList;

            if (priceIsZero || context.type == 'create') {
                const validPrices = findValidPrices(itemId, context.newRecord.id);
                if (priceIsZero) fillValidPrices(zeroPricesList, validPrices, newRecord);
                if (context.type == 'create' && validPrices.length > 0) {
                    const isPriceUpdated = comparePricetoLast(newRecord, validPrices[0]);
                    LOGS.isPriceUpdated = isPriceUpdated;
                    isPriceUpdated ? updateItemFlag(newRecord) : null;
                } else if (context.type == 'create') {
                    updateItemFlag(newRecord);
                }
            }

        } catch (e) {
            log.error('Error', e);
        }
        log.debug('LOGS', LOGS);
    }

    const updateItemFlag = (newRecord) => {
        const itemId = newRecord.getValue({
            fieldId: 'custrecord_msg_ip_item'
        });

        record.submitFields({
            type: getRecordType(itemId),
            id: itemId,
            values: {
                custitem_msg_pim_request_sync_price: true
            }
        });
    }

    const getRecordType = (itemId) => {
        const itemSearch = search.create({
            type: search.Type.ITEM,
            filters:
                [['internalid', search.Operator.ANYOF, itemId]],
            columns: []
        }).run().getRange({
            start: 0,
            end: 1
        });

        return itemSearch.length ? itemSearch[0].recordType : null;
    }


    const checkPriceIsZero = (newRecord) => {
        let priceIsZero = false;
        let zeroPricesList = [];

        if (Number(newRecord.getValue('custrecord_msg_ip_base')) == 0 || !Number(newRecord.getValue('custrecord_msg_ip_base'))) {
            priceIsZero = true;
            zeroPricesList.push('custrecord_msg_ip_base');
        } if (Number(newRecord.getValue('custrecord_msg_ip_b2b')) == 0 || !Number(newRecord.getValue('custrecord_msg_ip_b2b'))) {
            priceIsZero = true;
            zeroPricesList.push('custrecord_msg_ip_b2b');
        } if (Number(newRecord.getValue('custrecord_msg_ip_srp')) == 0 || !Number(newRecord.getValue('custrecord_msg_ip_srp'))) {
            priceIsZero = true;
            zeroPricesList.push('custrecord_msg_ip_srp');
        } if (Number(newRecord.getValue('custrecord_msg_ip_minimal')) == 0 || !Number(newRecord.getValue('custrecord_msg_ip_minimal'))) {
            priceIsZero = true;
            zeroPricesList.push('custrecord_msg_ip_minimal');
        }

        return { priceIsZero, zeroPricesList };
    }

    const findValidPrices = (itemId, currentId) => {
        if (!itemId) return [];
        const validPrices = [];
        const filters = [
            ["custrecord_msg_ip_item", "anyof", itemId],
            "AND",
            ["custrecord_msg_ip_date_from", "onorbefore", "today"],
            "AND",
            [["custrecord_msg_ip_date_to", "isempty", ""], "OR", ["custrecord_msg_ip_date_to", "onorafter", "today"]]
        ]
        if (currentId) {
            filters.push('AND');
            filters.push(["internalid", "noneof", currentId])
        }
        const customrecord_msg_item_pricesSearchObj = search.create({
            type: "customrecord_msg_item_prices",
            filters: filters,
            columns:
                [
                    search.createColumn({ name: "custrecord_msg_ip_item", label: "Item" }),
                    search.createColumn({
                        name: "custrecord_msg_ip_date_from",
                        sort: search.Sort.DESC,
                        label: "Date from"
                    }),
                    search.createColumn({ name: "custrecord_msg_ip_date_to", label: "Date to" }),
                    search.createColumn({ name: "custrecord_msg_ip_base", label: "Sales Price" }),
                    search.createColumn({ name: "custrecord_msg_ip_b2b", label: "B2B Price Net" }),
                    search.createColumn({ name: "custrecord_msg_ip_srp", label: "SRP Price" }),
                    search.createColumn({ name: "custrecord_msg_ip_minimal", label: "Minimal Price" })
                ]
        });

        let myPagedData = customrecord_msg_item_pricesSearchObj.runPaged({ pageSize: 1000 });
        myPagedData.pageRanges.forEach(function (pageRange) {
            let myPage = myPagedData.fetch({ index: pageRange.index });
            myPage.data.forEach(function (result) {
                validPrices.push({
                    custrecord_msg_ip_base: result.getValue('custrecord_msg_ip_base'),
                    custrecord_msg_ip_b2b: result.getValue('custrecord_msg_ip_b2b'),
                    custrecord_msg_ip_srp: result.getValue('custrecord_msg_ip_srp'),
                    custrecord_msg_ip_minimal: result.getValue('custrecord_msg_ip_minimal')
                })
            });
        });

        return validPrices;
    }

    const fillValidPrices = (zeroPricesList, validPrices, newRecord) => {
        for (let x = 0; x < validPrices.length; x++) {
            zeroPricesList = setPriceOnRecord(zeroPricesList, validPrices[x], newRecord);
            if (!zeroPricesList.length > 0) break;
        }
    }

    const setPriceOnRecord = (zeroPricesList, validPrice, newRecord) => {
        let updatedZeroPriceList = zeroPricesList;
        zeroPricesList.forEach(price => {
            if (validPrice[price]) {
                LOGS[price] = 'updating price';
                newRecord.setValue(price, validPrice[price]);
                updatedZeroPriceList = updatedZeroPriceList.filter(x => x != price);
            }
        });
        return updatedZeroPriceList;
    }

    const comparePricetoLast = (newRecord, validPrice) => {
        log.audit('validPrice', validPrice);
        if (newRecord.getValue('custrecord_msg_ip_base') != validPrice['custrecord_msg_ip_base']) {
            return true;
        } else if (newRecord.getValue('custrecord_msg_ip_srp') != validPrice['custrecord_msg_ip_srp']) {
            return true;
        } else if (newRecord.getValue('custrecord_msg_ip_b2b') != validPrice['custrecord_msg_ip_b2b']) {
            return true;
        } else if (newRecord.getValue('custrecord_msg_ip_minimal') != validPrice['custrecord_msg_ip_minimal']) {
            return true;
        }

        return false;
    }

    return {
        beforeSubmit: beforeSubmit,
    };
});