/** 
 * Copyright (c) 2023 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @author: Tuba Mohsin
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @description: Check if new record has any price = 0, fill the price from the earlier date in case this price is still valid 
 * applicable to item prices, vendor price list
 */


define(['N/search', 'N/record'], function (search, record) {
    const LOGS = {};
    const beforeSubmit = (context) => {
        if (context.type == 'delete') return;
        LOGS.contextType = context.type;

        try {
            const newRecord = context.newRecord;
            const itemId = context.newRecord.getValue('custrecord_av_item');
            const vendor = context.newRecord.getValue('custrecord_av_vendor');
            const { priceIsZero, zeroPricesList } = checkPriceIsZero(newRecord);
            LOGS.recordId = newRecord.id;
            LOGS.itemId = itemId;
            LOGS.zeroPricesList = zeroPricesList;

            if (priceIsZero || context.type == 'create') {
                const validPrices = findValidPrices(itemId, vendor, context.newRecord.id);
                if (priceIsZero) fillValidPrices(zeroPricesList, validPrices, newRecord);
                if (context.type == 'create' && validPrices.length > 0) {
                    const isPriceUpdated = comparePricetoLast(newRecord, validPrices[0]);
                    LOGS.isPriceUpdated = isPriceUpdated;
                    isPriceUpdated ? updateItemFlag(newRecord) : null;
                }
            }

        } catch (e) {
            log.error('Error', e);
        }
        log.debug('LOGS', LOGS);
    }

    const updateItemFlag = (newRecord) => {
        const itemId = newRecord.getValue({
            fieldId: 'custrecord_av_item'
        });

        record.submitFields({
            type: getRecordType(itemId),
            id: itemId,
            values: {
                custitem_msg_pim_request_sync_price: true,
                custitem_msg_update_vendor_price: true
            }
        });
    }

    const getRecordType = (itemId) => {
        const itemSearch = search.create({
            type: search.Type.ITEM,
            filters:
                [['internalid', search.Operator.ANYOF, itemId]],
            columns: []
        }).run().getRange({
            start: 0,
            end: 1
        });

        return itemSearch.length ? itemSearch[0].recordType : null;
    }


    const checkPriceIsZero = (newRecord) => {
        let priceIsZero = false;
        let zeroPricesList = [];

        if (Number(newRecord.getValue('custrecord_av_uvp')) == 0 || !Number(newRecord.getValue('custrecord_av_uvp'))) {
            priceIsZero = true;
            zeroPricesList.push('custrecord_av_uvp');
        } if (Number(newRecord.getValue('custrecord_av_ek')) == 0 || !Number(newRecord.getValue('custrecord_av_ek'))) {
            priceIsZero = true;
            zeroPricesList.push('custrecord_av_ek');
        } if (Number(newRecord.getValue('custrecord_av_rabatt1')) == 0 || !Number(newRecord.getValue('custrecord_av_rabatt1'))) {
            priceIsZero = true;
            zeroPricesList.push('custrecord_av_rabatt1');
        } if (Number(newRecord.getValue('custrecord_av_rabatt2')) == 0 || !Number(newRecord.getValue('custrecord_av_rabatt2'))) {
            priceIsZero = true;
            zeroPricesList.push('custrecord_av_rabatt2');
        }

        return { priceIsZero, zeroPricesList };
    }

    const findValidPrices = (itemId, vendor, currentId) => {
        if (!itemId) return [];
        const validPrices = [];
        const filters = [
            ["custrecord_av_item", "anyof", itemId],
            "AND",
            ["custrecord_av_vendor", "anyof", vendor],
            "AND",
            ["custrecord_av_valid_from", "onorbefore", "today"],
            "AND",
            [["custrecord_av_valid_to", "isempty", ""], "OR", ["custrecord_av_valid_to", "onorafter", "today"]]
        ]
        if (currentId) {
            filters.push('AND');
            filters.push(["internalid", "noneof", currentId])
        }
        const customrecord_msg_item_pricesSearchObj = search.create({
            type: "customrecord_av_vendor_price_list",
            filters: filters,
            columns:
                [
                    search.createColumn({ name: "custrecord_av_item", label: "Item" }),
                    search.createColumn({
                        name: "custrecord_av_valid_from",
                        sort: search.Sort.DESC,
                    }),
                    search.createColumn({ name: "custrecord_av_valid_to" }),
                    search.createColumn({ name: "custrecord_av_uvp" }),
                    search.createColumn({ name: "custrecord_av_ek" }),
                    search.createColumn({ name: "custrecord_av_rabatt1" }),
                    search.createColumn({ name: "custrecord_av_rabatt2" })
                ]
        });

        let myPagedData = customrecord_msg_item_pricesSearchObj.runPaged({ pageSize: 1000 });
        myPagedData.pageRanges.forEach(function (pageRange) {
            let myPage = myPagedData.fetch({ index: pageRange.index });
            myPage.data.forEach(function (result) {
                validPrices.push({
                    custrecord_av_uvp: result.getValue('custrecord_av_uvp'),
                    custrecord_av_ek: result.getValue('custrecord_av_ek'),
                    custrecord_av_rabatt1: result.getValue('custrecord_av_rabatt1'),
                    custrecord_av_rabatt2: result.getValue('custrecord_av_rabatt2')
                })
            });
        });

        return validPrices;
    }

    const fillValidPrices = (zeroPricesList, validPrices, newRecord) => {
        for (let x = 0; x < validPrices.length; x++) {
            zeroPricesList = setPriceOnRecord(zeroPricesList, validPrices[x], newRecord);
            if (!zeroPricesList.length > 0) break;
        }
    }

    const setPriceOnRecord = (zeroPricesList, validPrice, newRecord) => {
        let updatedZeroPriceList = zeroPricesList;
        zeroPricesList.forEach(price => {
            if (validPrice[price]) {
                LOGS[price] = 'updating price';
                log.audit('price', price);
                if (price == 'custrecord_av_rabatt1' || price == 'custrecord_av_rabatt2') {
                    newRecord.setValue(price, _parseFloat(validPrice[price]))
                } else {
                    newRecord.setValue(price, validPrice[price]);
                }

                updatedZeroPriceList = updatedZeroPriceList.filter(x => x != price);
            }
        });
        return updatedZeroPriceList;
    }
    const _parseFloat = (percentValue) => {
        return parseFloat(percentValue) || 0;
    }
    const getNumberFromPercentage = (percentValue) => {
        log.audit('percentValue', percentValue);
        const regex1 = /[-+]?\d+(\.\d+)/;
        if (percentValue.match(regex1)[0]) {
            log.audit('regex1', percentValue.match(regex1)[0]);
            return Number(percentValue.match(regex1)[0])
        }
        return 0;
    }
    const comparePricetoLast = (newRecord, validPrice) => {
        log.audit('validPrice', validPrice);
        if (newRecord.getValue('custrecord_av_uvp') != validPrice['custrecord_av_uvp']) {
            return true;
        } else if (newRecord.getValue('custrecord_av_ek') != validPrice['custrecord_av_ek']) {
            return true;
        } else if (newRecord.getValue('custrecord_av_rabatt1') != validPrice['custrecord_av_rabatt1']) {
            return true;
        } else if (newRecord.getValue('custrecord_av_rabatt2') != validPrice['custrecord_av_rabatt2']) {
            return true;
        }

        return false;
    }

    return {
        beforeSubmit: beforeSubmit,
    };
});