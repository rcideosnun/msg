/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.x
 ** @author: [Tuba Mohsin]
 ** @Description: Lib functions for updating valid vendor price on item record
 */
define(['N/record', 'N/search', 'N/task', 'N/format', '../pimCoreIntegration/av_int_log.js'], function (record, search, task, format, avLog) {

    const findValidVendorPrice = (itemId) => {
        if (!itemId) return [];
        let uniqueIds = [];
        let itemVendorPrice = {};
        let vendorIds = [];
        const filters = [
            ["custrecord_av_item", "anyof", itemId],
            "AND",
            ["custrecord_av_valid_from", "onorbefore", "today"],
            "AND",
            [["custrecord_av_valid_to", "isempty", ""], "OR", ["custrecord_av_valid_to", "onorafter", "today"]],
            "AND",
            ["custrecord_av_vendor.internalid", "noneof", "@NONE@"],
        ]
        const customrecord_msg_item_pricesSearchObj = search.create({
            type: "customrecord_av_vendor_price_list",
            filters: filters,
            columns:
                [
                    search.createColumn({ name: "custrecord_av_item", label: "Item" }),
                    search.createColumn({
                        name: "custrecord_av_valid_from",
                        sort: search.Sort.DESC,
                    }),
                    search.createColumn({ name: "custrecord_av_vendor" }),
                    search.createColumn({ name: "currency", join: "custrecord_av_vendor" }),
                    search.createColumn({ name: "custrecord_av_ek" }),
                ]
        });

        let myPagedData = customrecord_msg_item_pricesSearchObj.runPaged({ pageSize: 1000 });
        myPagedData.pageRanges.forEach(function (pageRange) {
            let myPage = myPagedData.fetch({ index: pageRange.index });
            myPage.data.forEach(function (result) {

                if (uniqueIds.indexOf(`${result.getValue('custrecord_av_item')}-${result.getValue('custrecord_av_vendor')}`) == -1) {
                    if (itemVendorPrice[result.getValue('custrecord_av_item')]) {
                        itemVendorPrice[result.getValue('custrecord_av_item')]['validPrices'].push({
                            custrecord_av_ek: Number(result.getValue('custrecord_av_ek')),
                            vendor: result.getValue('custrecord_av_vendor'),
                            currency: result.getValue({ name: "currency", join: "custrecord_av_vendor" })
                        })
                    } else {
                        itemVendorPrice[result.getValue('custrecord_av_item')] = {
                            validPrices: [{
                                custrecord_av_ek: Number(result.getValue('custrecord_av_ek')),
                                vendor: result.getValue('custrecord_av_vendor'),
                                currency: result.getValue({ name: "currency", join: "custrecord_av_vendor" })
                            }]
                        }
                    }
                    vendorIds.push(result.getValue('custrecord_av_vendor'));
                    uniqueIds.push(`${result.getValue('custrecord_av_item')}-${result.getValue('custrecord_av_vendor')}`);
                }
            });
        });

        return { itemVendorPrice, vendorIds };
    }

    const getAllVendorCurrencies = (vendorIds) => {
        let vendorCurrencies = {};
        const vendorSearchObj = search.create({
            type: "vendor",
            filters:
                [
                    ["internalid", "anyof", vendorIds]
                ],
            columns:
                [
                    search.createColumn({
                        name: "currency",
                        join: "VendorCurrencyBalance",
                        label: "Currency"
                    })
                ]
        });

        let myPagedData = vendorSearchObj.runPaged({ pageSize: 1000 });
        myPagedData.pageRanges.forEach(function (pageRange) {
            let myPage = myPagedData.fetch({ index: pageRange.index });
            myPage.data.forEach(function (result) {
                const curr = result.getValue({ name: "currency", join: "VendorCurrencyBalance" })
                if (vendorCurrencies[result.id]) {
                    vendorCurrencies[result.id]['currencies'].push(curr)
                } else {
                    vendorCurrencies[result.id] = { 'currencies': [curr] }
                }
            });
        });

        return vendorCurrencies;
    }

    const checkIfVendorWithMultipleCurrency = (vendorCurrencyList, vendorIds) => {
        let error = false;

        vendorIds.forEach(vendor => {
            log.audit('vendorCurrencyList[vendor]', vendorCurrencyList[vendor]['currencies'])
            if (vendorCurrencyList[vendor]['currencies'].length > 1) {
                error = true;
                return error;
            }
        });

        return error;
    }

    const updateVendorPrices = (itemList) => {
        const itemIds = itemList.map(i => i.id);
        const { itemVendorPrice, vendorIds } = findValidVendorPrice(itemIds);
        const vendorCurrencyList = getAllVendorCurrencies(vendorIds);

        itemList.forEach(data => {
            let newValidPrices = itemVendorPrice[data.id].validPrices;
            let itemVendors = newValidPrices.map(i => i.vendor);

            if (checkIfVendorWithMultipleCurrency(vendorCurrencyList, itemVendors)) {
                avLog.error_message({
                    item: data.id,
                    msg: 'VP00001',
                    var1: `Vendor ID: ${itemVendors}`,
                });

                return;
            }

            log.audit('processing item');
            let rec = record.load({
                type: data.type,
                id: data.id,
                isDynamic: false
            });

            const lineCount = rec.getLineCount('itemvendor');
            const oldVendorPriceList = [];

            for (let index = 0; index < lineCount; index++) {
                oldVendorPriceList.push({
                    custrecord_av_ek: rec.getSublistValue({
                        sublistId: 'itemvendor',
                        fieldId: 'purchaseprice',
                        line: index
                    }),
                    vendor: rec.getSublistValue({
                        sublistId: 'itemvendor',
                        fieldId: 'vendor',
                        line: index
                    }),
                    currency: rec.getSublistValue({
                        sublistId: 'itemvendor',
                        fieldId: 'vendorcurrencyid',
                        line: index
                    })
                })
            }

            if (comparation(oldVendorPriceList.flat(1), newValidPrices.flat(1))) {
                log.audit('updating vendor price');
                if (lineCount > 0) {
                    rec = _cleanSublist(rec);
                }

                for (let x = 0; x < newValidPrices.length; x++) {
                    rec.insertLine({ sublistId: 'itemvendor', line: x });
                    rec.setSublistValue({
                        sublistId: 'itemvendor',
                        fieldId: 'vendor',
                        line: x,
                        value: newValidPrices[x].vendor
                    });
                    rec.setSublistValue({
                        sublistId: 'itemvendor',
                        fieldId: 'vendorcurrencyid',
                        line: x,
                        value: newValidPrices[x].currency
                    });
                    rec.setSublistValue({
                        sublistId: 'itemvendor',
                        fieldId: 'purchaseprice',
                        line: x,
                        value: newValidPrices[x].custrecord_av_ek
                    });
                }

            }
            rec.setValue('custitem_msg_update_vendor_price', false);
            rec.save({
                ignoreMandatoryFields: true
            });
        });
    }

    function _cleanSublist(rec) {
        const lineCount = rec.getLineCount('itemvendor');

        for (var i = lineCount - 1; i >= 0; i--) {

            rec.removeLine({
                sublistId: 'itemvendor',
                line: i,
                ignoreRecalc: true
            });

        }

        return rec
    }

    function comparation(a, b) {
        if (a.length != b.length) {
            return true;
        } else {
            for (var i = 0; i < a.length; i++) {
                if (JSON.stringify(a[i]) != JSON.stringify(b[i])) {
                    return true;
                };
            }
        }
        return false;
    }
    return {
        updateVendorPrices,
    };
});
