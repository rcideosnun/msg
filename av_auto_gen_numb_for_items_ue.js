/** 
 * Copyright (c) 2019 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @author: [Polina Melnykova]
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @description :
 * Scpec: MSG Auto Generated Numbers For Items
 */


define(['N/search'], function (search) {

    function beforeSubmit(context) {

        try {

            var newItem = context.newRecord;

            if (context.type == 'create') {

                var itemSearchObj = search.create({
                    type: 'item',
                    filters:
                    [
                    ],
                    columns:
                        [
                            search.createColumn({ name: "itemid", label: "Name" }),
                        ]
                });

                var nameArr = [];

                itemSearchObj = getAllResults(itemSearchObj);
                itemSearchObj.forEach(function (result) {

                    if(!isNaN(result.getValue('itemid')) && !nameArr.includes(result.getValue('itemid'))){

                        nameArr.push(Number(result.getValue('itemid')));
                    }
                    return true;
                });

                var maxName = Math.max(...nameArr);

                if (maxName < 350000) {
                    maxName = 350000;
                }

                var newName = maxName + 1;

                newItem.setValue({
                    fieldId: 'itemid',
                    value: Math.ceil(newName),
                    ignoreFieldChange: true
                });

                newItem.setValue({
                    fieldId: 'externalid',
                    value: Math.ceil(newName),
                    ignoreFieldChange: true
                });
            }

        }
        catch (e) {
            log.error('error', e)
        }

    };

    function getAllResults(s) {
        var results = s.run();
        var searchResults = [];
        var searchid = 0;
        do {
            var resultslice = results.getRange({
                start: searchid,
                end: searchid + 1000,
            });
            resultslice.forEach(function (slice) {
                searchResults.push(slice);
                searchid++;
            });
        } while (resultslice.length >= 1000); // >=1000
        return searchResults;
    }

    return {
        beforeSubmit: beforeSubmit
    };
});