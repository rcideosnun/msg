/**
 ** Copyright (c) 2022 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 *
 * Author: Tuba Mohsin
 * Date: 10.05.2023
 * MR script to calculate delivery time based on flag on the item
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */

define([
  "N/search",
  "N/task",
  "N/runtime",
  "./av_msg_pim_item_lib.js",
  "./av_int_log.js",
], function (search, task, runtime, lib) {


  const getInputData = () => {
    log.audit("getInputData", `-Script Started on ${new Date()}-`);
    try {
      return getItemsToCalcDeliveryTime();
    } catch (ex) {
      log.error({ title: "getInputData : ex", details: ex });
    }
  };

  const map = (context) => {
    log.audit({ title: "map", details: 'triggered' });
    const itemIds = JSON.parse(context.value);
    lib.handleDeliveryTimeRequest(itemIds);
  };


  const summarize = () => {
    log.audit("summarize", `-Script Finished on ${new Date()}-`);
  };

  const getItemsToCalcDeliveryTime = (index) => {
    const currentRuntime = runtime.getCurrentScript();
    let results = [];

    let itemSearch = search.load({
      id: currentRuntime.getParameter({ name: "custscript_av_pim_delivery_cal_search" }),
    });

    let myPagedData = itemSearch.runPaged({ pageSize: 1000 });

    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });
      myPage.data.forEach(function (result) {
        let internalId = result.id || result.getValue('internalid') || result.getValue({ name: "internalid", summary: "GROUP" });
        // results.push(internalId);
        results.push({ id: internalId, type: result.recordType || null });
      });
    });

    let resultGroups = [];
    const size = 100; // processing it in a batch of 100.
    for (let i = 0; i < results.length; i += size) {
      resultGroups.push(results.slice(i, i + size));
    }

    return resultGroups;
  };

  return {
    getInputData: getInputData,
    map: map,
    summarize: summarize,
  };
});
