/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tuba Mohsin
* Date: 27/03/2023
* Description: Updates item stock flag when transaction is created
* @NApiVersion 2.1
* @NScriptType UserEventScript
*/

define(['N/record', 'N/search', 'N/task'], function (record, search, task) {

  const afterSubmit = (context) => {
    try {
      const newRecord = context.newRecord;
      const oldRecord = context.oldRecord;
      let items = [];
      if (context.type === context.UserEventType.CREATE) {
        items = getAllItems(newRecord);
      } else if (context.type === context.UserEventType.EDIT || context.type === context.UserEventType.XEDIT) {
        const oldValues = extractValues(oldRecord);
        items = getUpdatedItems(newRecord, oldValues);
      }
      log.audit('updated items', items);
      if (items && items.length > 0) {
        const itemDetails = getRecordType(items);
        if (itemDetails.length > 95) {
          triggerMapReduceScript(items);
          return;
        }

        itemDetails.forEach(item => {
          if (item) {
            record.submitFields({
              type: item.type,
              id: item.id,
              values: {
                custitem_msg_pim_request_sync_stock: true,
                custitem_av_pos_req_stock_sync: true
              }
            })
          }
        });
      }
    } catch (ex) {
      log.error({ title: 'afterSubmit : ex', details: ex });
    }
  }

  const getAllItems = (tranRecord) => {
    const tranRecordType = tranRecord.type;
    const sublistId = tranRecordType == 'inventoryadjustment' ? 'inventory' : 'item';
    const items = [];
    const itemLineCount = tranRecord.getLineCount({
      sublistId: sublistId
    });
    for (var i = 0; i < itemLineCount; i++) {
      const id = tranRecord.getSublistValue({
        sublistId: sublistId,
        fieldId: 'item',
        line: i
      });
      if (tranRecordType == 'itemreceipt') {
        const itemReceive = tranRecord.getSublistValue(sublistId, 'itemreceive', i);
        if (itemReceive == 'F' || !itemReceive) continue;
      }
      items.push(id)
    };
    return items;
  }

  function extractValues(rec) {
    const tranRecordType = rec.type;
    const sublistId = tranRecordType == 'inventoryadjustment' ? 'inventory' : 'item';
    const qtyField = tranRecordType == 'inventoryadjustment' ? 'adjustqtyby' : 'quantity';
    const sublistCount = rec.getLineCount(sublistId);
    var arr = {};
    for (let i = 0; i < sublistCount; i++) {
      arr[rec.getSublistValue(sublistId, 'item', i)] = [rec.getSublistValue(sublistId, qtyField, i)]
    }
    return arr;
  }

  const getUpdatedItems = (rec, oldRecord) => {
    const tranRecordType = rec.type;
    const sublistId = tranRecordType == 'inventoryadjustment' ? 'inventory' : 'item';
    const qtyField = tranRecordType == 'inventoryadjustment' ? 'adjustqtyby' : 'quantity';
    const items = []
    const itemLineCount = rec.getLineCount({ sublistId: sublistId });
    for (var i = 0; i < itemLineCount; i++) {
      const newArr = [];
      const id = rec.getSublistValue(sublistId, 'item', i);
      newArr.push(rec.getSublistValue(sublistId, qtyField, i));
      const oldArr = oldRecord[id] || [];
      delete oldRecord[id];

      if (oldArr && comparation(oldArr.flat(1), newArr.flat(1))) {
        items.push(id);
      }
    };
    Object.keys(oldRecord).forEach(id => items.push(id));
    return items;
  }

  const getRecordType = (itemIds) => {
    const itemDetails = [];
    const searchObj = search.create({
      type: search.Type.ITEM,
      filters:
        [['internalid', search.Operator.ANYOF, itemIds]],
      columns: []
    });

    let myPagedData = searchObj.runPaged({ pageSize: 1000 });
    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });
      myPage.data.forEach(function (result) {
        itemDetails.push({ id: result.id, type: result.recordType })
      });
    });

    return itemDetails;
  }

  const triggerMapReduceScript = (itemIds) => {
    const mapReduceTask = task.create({
      taskType: task.TaskType.MAP_REDUCE,
      scriptId: 'customscript_av_pim_update_stock_flag_mr',
      params: { custscript_itemids: itemIds },
    });
    mapReduceTask.submit();
  }

  function comparation(a, b) {

    if (a.length != b.length) {
      return true;
    } else {
      for (var i = 0; i < a.length; i++) {
        if (a[i] != b[i]) {
          return true;
        };
      }
    }
    return false;
  }

  return {
    afterSubmit: afterSubmit
  }
})