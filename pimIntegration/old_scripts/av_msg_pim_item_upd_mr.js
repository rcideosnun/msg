/**
** Copyright (c) 2020 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Roberto Cideos
* Date: 17/11/2021
* Send/Update Items to PIM
* @NApiVersion 2.1
* @NScriptType MapReduceScript
*/

define(['N/search', 'N/log', 'N/workflow', 'N/runtime'],
    function (search, log, workflow, runtime) {

        const CONST = {
            BATCH_PROCESS_TYPE: {
                1: "Create",
                2: "Update"
            },
            WORKFLOW_SUFFIX_TYPE: {
                "Create": "create_item",
                "Update": "update_item"
            }
        }

        function getInputData() {
            log.debug("DEBUG", `-Script Starting on ${new Date()}-`)
			var scriptObj = runtime.getCurrentScript();
            var batchProcess = CONST.BATCH_PROCESS_TYPE[scriptObj.getParameter({ name: 'custscript_av_pim_process_type' })];
            log.debug("batchProcess", JSON.stringify(batchProcess))
            var itemFulfillments = _getItems();
            log.debug("DATA", JSON.stringify(itemFulfillments))
            return itemFulfillments
        }

        function map(context) {

            var scriptObj = runtime.getCurrentScript();
            var batchProcess = CONST.BATCH_PROCESS_TYPE[scriptObj.getParameter({ name: 'custscript_av_pim_process_type' })];
            var itemObj = JSON.parse(context.value)
            try {
                log.debug("DEBUG", `${itemObj.type} with id ${itemObj.id}`)
                
                var instanceId = workflow.initiate({
                    recordType: itemObj.type,
                    recordId: itemObj.id,
                    workflowId: "customworkflow_av_pim_send_items"
                });

                var taskId = workflow.trigger({
                    recordType: itemObj.type,
                    workflowId: "customworkflow_av_pim_send_items",
                    recordId: itemObj.id,
                    workflowInstanceId: instanceId,
                    actionId: "workflowaction13",
                    stateId: "workflowstate3"
                })
                

                log.debug("DEBUG", `${batchProcess} process was trigger in item with id ${itemId} and taskId ${taskId}`)

            } catch (e) {
                log.error('Unable to trigger WF to send items' + JSON.stringify(curr_rec_ID), JSON.stringify(e))
            }


        }

        function summarize() {
            log.debug("DEBUG", `-Script Finished on ${new Date()}-`)
        }

        function _getItems() {
            var itemsToBeProcessed = [];

            var scriptObj = runtime.getCurrentScript();
            var batchLimit = scriptObj.getParameter({ name: 'custscript_av_pim_limit_batch' }) || 1000;

            var filters = [
                [
                    ["isinactive","is","F"], 
                    "AND", 
                    ["custitem_av_pim_approved_item","is","T"], 
                    "AND", 
                    ["custitem_av_pim_item_prefix","noneof","@NONE@"], 
                    "AND", 
                    ["custitem_av_pim_product_inactive","is","F"], 
                    "AND", 
                    ["custitem_av_pim_department_id","noneof","@NONE@"], 
                    "AND", 
                    ["custitem_av_pim_product_type_id","noneof","@NONE@"],
                  	"AND",
                  	["custitem_av_pim_product_id","isempty",""]
                 ]
            ]


            var item_search_obj = search.create({
                type: "item",
                filters: filters,
                columns: []
            });

            item_search_obj = getAllResults(item_search_obj, batchLimit)

            log.debug("Results length not sliced", JSON.stringify(item_search_obj.length))

            item_search_obj.forEach(function (result) {

                let itemObj = {
                    id: result.id,
                    type: result.recordType
                }

                itemsToBeProcessed.push(itemObj)

                return true;
            });

            itemsToBeProcessed = itemsToBeProcessed.slice(0,batchLimit)

            log.debug("Results length sliced", JSON.stringify(itemsToBeProcessed.length))

            return itemsToBeProcessed
        }

        function getAllResults(s, l) {
            var results = s.run();
            var searchResults = [];
            var searchid = 0;
            do {
                var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
                resultslice.forEach(function (slice) {
                    searchResults.push(slice);
                    searchid++;
                }
                );
            } while (searchResults.length <= l);
            return searchResults;
        }

        return {
            getInputData: getInputData,
            map: map,
            summarize: summarize
        };


    })