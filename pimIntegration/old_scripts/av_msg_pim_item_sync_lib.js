/**
 ** Copyright (c) 2015 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 ** @author: Tehseen Ahmed
 ** @dated: 27/12/2022
 ** @Description: This library contains functions related to PIM item sync interface
 * *@NApiVersion 2.1
 */

define(['N/format', 'N/record', 'N/search', 'N/runtime', 'N/https', './av_msg_lib_sv2.js', "./av_int_log.js"],
  function (format, record, search, runtime, https, settings, avLog) {
    var CONSTANTS_LIB = {
      INTERFACES: {
        "P01": {
          onActionHttpRequest: "POST",
          connectionRequired: true
        },
        "P02": {
          onActionHttpRequest: "PUT",
          connectionRequired: true
        }
      },
      HTTP_CODE: {
        SUCCESS_UPDATE: 200,
        SUCCESS_CREATE: 201,
        ERROR: 401
      },
      INTERFACE_STATE: {
        NEW: 1,
        ERROR: 2,
        DONE: 3
      },
      PIM_ERROR_TYPES: {
        PIM1_INTERNAL_ERROR: "PIM2",
        PIM2_INTERNAL_ERROR: "PIM3"
      },
      ITEM_TYPE: {
        GIFT_CERTIFICATE_TYPE: "giftcertificateitem",
        GC_NS_TYPE: "GiftCert",
        KIT_ITEM_TYPE: "kititem",
        SERVICE_TYPE: "serviceitem"
      }
    }

    function getItemDetails(itemRecord) {
      const nsId = itemRecord.id;
      const itemType = itemRecord.type;
      const itemsById = getItemsById();
      const itemNumber = itemRecord.getValue("itemid");
      const itemDisplayName = itemRecord.getValue("displayname");
      const itemEan = itemRecord.getValue("upccode");
      const itemIsActive = !itemRecord.getValue("custitem_is_shop_online");
      const itemHeight = parseInt(itemRecord.getValue("custitem_msg_height"));
      const itemWidth = parseInt(itemRecord.getValue("custitem_msg_width"));
      const itemLength = parseInt(itemRecord.getValue("custitem_msg_length"));
      const itemWeight = itemRecord.getValue("weight") ? parseFloat(itemRecord.getValue("weight")) : null;
      const itemBrand = itemRecord.getText("cseg_msg_brands");
      const itemProductType = itemRecord.getText("custitem_msg_product_type");
      const itemUNnumber = itemRecord.getText("custitem_msg_un_number");
      const itemComponents = _getItemComponents(itemRecord, itemsById);
      const itemGroupType = itemRecord.getText("cseg_msg_igt");
      const taxScheduleKey = itemRecord.getText("taxschedule");
      const brandProductNumber = itemRecord.getValue("mpn");
      const refUnit = itemRecord.getValue("custitem_av_ref_unit");
      const purchaseUnit = itemRecord.getValue("custitem_av_purchase_unit");
      const unitKey = itemRecord.getText("custitem_av_unit_key");
      const lastSyncDate = itemRecord.getValue("custitem_msg_time_sent_pim");
      let itemParent = _getParent(itemRecord.getValue("parent")) || itemRecord.getValue("custitem_msg_main_product_kit");
      let subsidiaryKeys = itemRecord.getText("subsidiary");
      subsidiaryKeys = subsidiaryKeys.map(function (subsidiary) {
        return subsidiary.indexOf(' : ') > -1 ? subsidiary.split(' : ').pop() : subsidiary;
      })

      if (itemType == CONSTANTS_LIB.ITEM_TYPE.KIT_ITEM_TYPE) {
        itemParent = itemsById.itemById[Number(itemRecord.getValue("custitem_msg_main_product_kit"))];
      }

      const itemObj = {
        "nsId": nsId,
        "productNumber": itemNumber,
        "displayName": itemDisplayName || itemNumber,
        "ean": itemEan || "1",
        "subsidiaryKeys": typeof subsidiaryKeys == 'string' ? [subsidiaryKeys] : subsidiaryKeys,
        "itemGroupType": itemGroupType,
        "brand": _setNullAsDef(itemBrand),
        "taxScheduleKey": taxScheduleKey,
        "isActive": itemIsActive,
        "brandProductNumber": _setNullAsDef(brandProductNumber),
        "hazmatTypeKeys": _setNullAsDef(itemUNnumber),
        "height": _setNullAsDef(itemHeight),
        "width": _setNullAsDef(itemWidth),
        "length": _setNullAsDef(itemLength),
        "weight": _setNullAsDef(itemWeight),
        "refUnit": _setNullAsDef(refUnit),
        "purchaseUnit": _setNullAsDef(purchaseUnit),
        "unitKey": _setNullAsDef(unitKey),
        "productType": itemProductType || '',
        "parentProductNumber": _setNullAsDef(itemParent),
        "comboProductNumbers": _setNullAsDef(itemComponents),
        "lastSyncDate": _setNullAsDef(lastSyncDate)
      }

      return itemObj;
    }

    function getItemPrice(itemRecord) {
      const nsId = itemRecord.id;
      const itemType = itemRecord.type;
      const itemNumber = itemRecord.getValue("itemid");
      const today = format.format({
        value: new Date(),
        type: format.Type.DATE
      })

      const lineCount = itemRecord.getLineCount({
        sublistId: 'recmachcustrecord_msg_ip_item'
      });

      let applicablePrices = []

      for (let i = 0; i < lineCount; i++) {
        let dateFrom = itemRecord.getSublistValue({
          sublistId: 'recmachcustrecord_msg_ip_item',
          fieldId: 'custrecord_msg_ip_date_from',
          line: i
        })
        let dateTo = itemRecord.getSublistValue({
          sublistId: 'recmachcustrecord_msg_ip_item',
          fieldId: 'custrecord_msg_ip_date_to',
          line: i
        })

        dateFrom = dateFrom ? format.format({
          value: dateFrom,
          type: format.Type.DATE
        }) : dateFrom;

        dateTo = dateTo ? format.format({
          value: dateTo,
          type: format.Type.DATE
        }) : dateTo;

        if (today >= dateFrom && (!dateTo || today <= dateTo)) {
          applicablePrices.push({
            basePrice: itemRecord.getSublistValue({
              sublistId: 'recmachcustrecord_msg_ip_item',
              fieldId: 'custrecord_msg_ip_base',
              line: i
            }),
            b2bPrice: itemRecord.getSublistValue({
              sublistId: 'recmachcustrecord_msg_ip_item',
              fieldId: 'custrecord_msg_ip_b2b',
              line: i
            }),
            srpPrice: itemRecord.getSublistValue({
              sublistId: 'recmachcustrecord_msg_ip_item',
              fieldId: 'custrecord_msg_ip_srp',
              line: i
            }),
            minimalPrice: itemRecord.getSublistValue({
              sublistId: 'recmachcustrecord_msg_ip_item',
              fieldId: 'custrecord_msg_ip_minimal',
              line: i
            }),
            dateFrom: dateFrom,
            dateTo: dateTo
          });
        }
      }

      let priceObject = {};
      applicablePrices.forEach(function (price) {
        applicablePrices.forEach(function (price2) {
          if (price.dateFrom >= price2.dateFrom && (!price.dateTo || price.dateTo <= price2.dateTo)) {
            priceObject = price;
          }
        })
      })

      let basePrice = itemType != CONSTANTS_LIB.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(priceObject.basePrice) : _getPriceGiftCert(itemRecord);
      let b2bPrice = itemType != CONSTANTS_LIB.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(priceObject.b2bPrice) : _getPriceGiftCert(itemRecord);
      let srpPrice = itemType != CONSTANTS_LIB.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(priceObject.srpPrice) : _getPriceGiftCert(itemRecord);
      let minimalPrice = itemType != CONSTANTS_LIB.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(priceObject.minimalPrice) : _getPriceGiftCert(itemRecord);
      let contributionMarign = parseFloat(itemRecord.getValue('custitem_av_item_contributionmargin'));
      return {
        "nsId": nsId,
        "productNumber": itemNumber,
        "netBasePrice": basePrice,
        "netB2BPrice": _setNullAsDef(b2bPrice),
        "netSRPPrice": _setNullAsDef(srpPrice),
        "netMinimalPrice": _setNullAsDef(minimalPrice),
        "lastSyncDate": _setNullAsDef(itemRecord.getValue("custitem_msg_time_sent_pim")),
        "contributionMargin": _setNullAsDef(contributionMarign)
      }
    }

    function getItemsById() {
      let itemById = {};
      const itemSearchObj = _getAllResults(search.create({
        type: "item",
        filters: [],
        columns:
          ["itemid", "parent", "type"]
      }));

      itemSearchObj.every(function (result) {
        if (result.getValue("parent")) {
          itemById[result.id] = result.getValue("itemid").split(" ")[2];
        } else {
          itemById[result.id] = result.getValue("itemid");
        }
        return true;
      });

      return {
        itemById: itemById
      }
    }

    function sendRequest(payload, method, context) {
      const currentRuntime = runtime.getCurrentScript();
      const CONFIG_ID = currentRuntime.getParameter({ name: 'custscript_av_integration_id' }) || currentRuntime.getParameter({ name: 'custscript_av_pim_item_sync_config' }) || null;
      const iface = currentRuntime.getParameter({ name: 'custscript_av_item_iface' }) || currentRuntime.getParameter({ name: 'custscript_av_pim_item_sync_iface' });

      log.audit('payload => sendRequest', payload)
      const connectionObj = new settings(CONFIG_ID, false, CONSTANTS_LIB.INTERFACES[iface].connectionRequired);

      const headers = {
        "Content-type": "application/json",
        "Authorization": `Bearer ${connectionObj.accessToken}`,
        "Accept": 'application/json',
      }

      let requestUrl = '';
      switch (iface) {
        case 'P01':
          requestUrl = connectionObj.custrecord_av_integration_base_url.concat(connectionObj.custrecord_av_integration_product_post)
          break;
        case 'P02':
          requestUrl = connectionObj.custrecord_av_integration_base_url.concat(connectionObj.custrecord_av_integration_product_put)
          break;
      }

      // link avLog with the first item in the list.
      const nsId = payload && payload.length > 0 ?  payload[0].nsId : null;
      
      payload.forEach( obj => { delete obj.nsId });
      const logId = avLog.create({
        iface: iface,
        send: JSON.stringify(payload),
        url: requestUrl,
        state: CONSTANTS_LIB.INTERFACE_STATE.NEW,
        item: nsId
      })


      const response = https.request({
        method: CONSTANTS_LIB.INTERFACES[iface].onActionHttpRequest,
        url: requestUrl,
        body: JSON.stringify(payload),
        headers: headers
      });

      setTimeStamp(context, payload, response, logId, iface)

      return {
        response: response,
        logId: logId
      };
    }

    function setTimeStamp(context, payload, response, logId, iface) {
      const body = JSON.parse(response.body);
      if(body.success && (body.success?.length > 0 || Object.keys(body.success).length > 0)) {
        avLog.update({
          id: logId,
          receive: response.body,
          responseCode: response.code,
          state: CONSTANTS_LIB.INTERFACE_STATE.DONE
      });

        if (context) {
          const today = new Date();
          let formattedDate = format.format({
            value: today,
            type: format.Type.DATETIME
          })
          // This needs to be done because we compare the modified date to this date in order to sync the item.
          // Since we are updating the record by setting this value, it always changes the last modified date
          // which will always be higher than this date.
          // If you are reading this and have a better solution, please recommend it to me (Tehseen Ahmed)
          const lastIndex = formattedDate.lastIndexOf(':');
          const timeBeforeSecond = formattedDate.slice(0, lastIndex) + ':59';
          const parsedDate = format.parse({
            value: timeBeforeSecond,
            type: format.Type.DATETIME
          })

          context.newRecord.setValue('custitem_msg_time_sent_pim', parsedDate);
          if (!payload.productId) {
            context.newRecord.setValue("custitem_av_pim_product_id", JSON.parse(response.body).id)
          }
        }
      } else {
        var message = '';
        switch (iface) {
            case 'P01':
                message = CONSTANTS_LIB.PIM_ERROR_TYPES.PIM1_INTERNAL_ERROR
                break;
            case 'P02':
                message = CONSTANTS_LIB.PIM_ERROR_TYPES.PIM2_INTERNAL_ERROR
                break;
        }
        if(body.failed && body.failed?.length > 0) {

            body.failed.forEach(err => {
                let concatenatedErrorMsg = `Error for product ${err?.productNumber}: `;
                if(err.errors && err.errors.length > 0) {
                  concatenatedErrorMsg += err.errors.map(u => u.message).join(', ')
                }
                avLog.update({
                    id: logId,
                    responseCode: response.code,
                    state: CONSTANTS_LIB.INTERFACE_STATE.ERROR,
                    receive: response.body
                })
                avLog.error_message({
                    logid: logId,
                    msg: message,
                    var1: concatenatedErrorMsg
                })
            })
        } else {
            avLog.update({
                id: logId,
                responseCode: response.code,
                state: CONSTANTS_LIB.INTERFACE_STATE.ERROR,
                receive: response.body
            })

            avLog.error_message({
                logid: logId,
                msg: message,
                var1: response.body
            })
        }
    }

      // if (response.code == CONSTANTS_LIB.HTTP_CODE.SUCCESS_UPDATE || response.code == CONSTANTS_LIB.HTTP_CODE.SUCCESS_CREATE) {
        // avLog.update({
        //   id: logId,
        //   receive: response.body,
        //   responseCode: response.code,
        //   state: CONSTANTS_LIB.INTERFACE_STATE.DONE
        // });

        // if (context) {
        //   const today = new Date();
        //   let formattedDate = format.format({
        //     value: today,
        //     type: format.Type.DATETIME
        //   })

        //   // This needs to be done because we compare the modified date to this date in order to sync the item.
        //   // Since we are updating the record by setting this value, it always changes the last modified date
        //   // which will always be higher than this date.
        //   // If you are reading this and have a better solution, please recommend it to me (Tehseen Ahmed)
        //   const lastIndex = formattedDate.lastIndexOf(':');
        //   const timeBeforeSecond = formattedDate.slice(0, lastIndex) + ':59';
        //   const parsedDate = format.parse({
        //     value: timeBeforeSecond,
        //     type: format.Type.DATETIME
        //   })

        //   context.newRecord.setValue('custitem_msg_time_sent_pim', parsedDate);
        //   if (!payload.productId) {
        //     context.newRecord.setValue("custitem_av_pim_product_id", JSON.parse(response.body).id)
        //   }
        // }


      // } else {

      //   avLog.update({
      //     id: logId,
      //     responseCode: response.code,
      //     state: CONSTANTS_LIB.INTERFACE_STATE.ERROR,
      //     receive: response.body
      //   })

      //   var message = '';
      //   switch (iface) {
      //     case 'P01':
      //       message = CONSTANTS_LIB.PIM_ERROR_TYPES.PIM1_INTERNAL_ERROR
      //       break;
      //     case 'P02':
      //       message = CONSTANTS_LIB.PIM_ERROR_TYPES.PIM2_INTERNAL_ERROR
      //       break;
      //   }

      //   avLog.error_message({
      //     logid: logId,
      //     msg: message,
      //     var1: response.body
      //   })
      // }  
    }

    function _getAllResults(s) {
      var results = s.run();
      var searchResults = [];
      var searchid = 0;
      do {
        var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
        resultslice.forEach(function (slice) {
          searchResults.push(slice);
          searchid++;
        }
        );
      } while (resultslice.length >= 1000);
      return searchResults;
    }

    function _getItemComponents(rec, itemsById) {
      let itemSkus = [];

      if (rec.type != CONSTANTS_LIB.ITEM_TYPE.KIT_ITEM_TYPE) return itemSkus;

      const itemComponentsL = rec.getLineCount({ sublistId: "member" })

      for (var i = 0; i < itemComponentsL; i++) {
        let memberId = Number(rec.getSublistValue({
          sublistId: "member",
          fieldId: "item",
          line: i
        }));
        if (itemsById.itemById[memberId])
          itemSkus.push(itemsById.itemById[memberId]
          )
      }

      return itemSkus
    }

    function _setNullAsDef(val) {
      return !val || (val && val.length == 0) ? null : val
    }

    function _getParent(itemId) {
      if (!itemId) return null;
      try {
        var parentSearch = search.create({
          type: search.Type.ITEM,
          filters: [
            ['internalid', search.Operator.ANYOF, itemId]
          ],
          columns: [
            search.createColumn({ name: 'itemid'})
          ]
        }).run().getRange({
          start: 0,
          end: 1
        });
  
        return parentSearch.length
          ? parentSearch[0].getValue({ name: 'itemid' })
          : null;
      } catch (ex) {
        log.error({ title: '_getParent : ex', details: ex });
      }
    }

    return {
      getItemDetails: getItemDetails,
      getItemPrice: getItemPrice,
      sendRequest: sendRequest
    };
  }
)