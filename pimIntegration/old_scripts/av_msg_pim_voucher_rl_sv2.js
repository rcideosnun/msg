/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
** @version: 2.1
** @author: [Roberto Cideos]
** @dated: [03.06.2022]
** @Description: Integration between PIMCore and NetSuite (Voucher)
*
*  @NApiVersion 2.1
*  @NScriptType Restlet
*/


define(['N/record',
    'N/search',
    'N/format',
    './av_int_log',
    'N/runtime'],
    function (
        record,
        search,
        format,
        avLog,
        runtime) {

        var self = this;

        var CONSTANTS = {
            INTERFACES: {
                "customdeploy_interface_i07_get": {
                    onActionFunction: _getVoucherDetails,
                    iface: "I07",
                    httpMethod: "GET"
                }
            },
            HTTP_CODE: {
                SUCCESS: 200,
                ERROR: 400
            },
            INTERFACE_STATE: {
                NEW: 1,
                ERROR: 2,
                DONE: 3
            },
            ERROR_MESSAGES: {
                DUPLICATED_ORDER: "SW1",
                MISSING_ITEMS: "SW2",
                INTERNAL_ERROR: "SW3"
            },
            SUBSIDIARY: {
                WEB_STORE_DEFAULT: 3
            },
            RMA_VALIDITY_STATUS: {
                ISVALID: "valid_all_rma",
                ISNOTVALID: "valid_no_rma",
                UNAUTHORIZED: "invalid_authentication"
            },
            VALID_SHIP_COUNTRIES: ["DE", "AT"],
            RETURN_PERIOD_MS: "15552000000"
        }

        function _entryPoint(ctx) {

            self.logId = avLog.create({
                iface: CONSTANTS.INTERFACES[runtime.getCurrentScript().deploymentId].iface,
                state: CONSTANTS.INTERFACE_STATE.NEW
            })

            return CONSTANTS.INTERFACES[runtime.getCurrentScript().deploymentId].onActionFunction(ctx)
        }

        function _getVoucherDetails(ctx) {

            try {

                const body = ctx;

                avLog.update({
                    id: self.logId,
                    receive: body
                })

                return _searchVouchers(body)


            } catch (e) {

                avLog.update({
                    id: self.logId,
                    responseCode: CONSTANTS.HTTP_CODE.ERROR,
                    state: CONSTANTS.INTERFACE_STATE.ERROR
                })

                avLog.error_message({
                    logid: self.logId,
                    var1: e,
                    msg: CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR
                })

                return JSON.stringify({
                    code: 406,
                    message: e
                })
            }

        }

        function _methodNotImplemented(ctx) {
            return JSON.stringify({ message: 'This method has not been implemented' })
        }

        function _searchVouchers(body) {

            var filters = [];
            var res = [];
            var gcData = {};

            if (body.voucherCode) {
                filters.push(["custrecord_av_cs_gc_code", "is", body.voucherCode])
            }

            var gcSearch = _getAllResults(search.create({
                type: "customrecord_av_cs_gc",
                filters: filters,
                columns:
                    ["created",
                        "custrecord_av_cs_gc_code",
                        "custrecord_av_cs_gc_from",
                        "custrecord_av_cs_gc_rec",
                        "custrecord_av_cs_gc_email",
                        "custrecord_av_cs_gc_exp",
                        "custrecord_av_cs_gc_or_amt",
                        "custrecord_av_cs_gc_curr",
                        "custrecord_av_cs_gc_rem_amt",
                        "custrecord_av_cs_gc_parent",
                        search.createColumn({
                            name: "custrecord_av_gc_amount",
                            join: "CUSTRECORD_AV_PARENT",
                            label: "Amount"
                        }),
                        search.createColumn({
                            name: "custrecord_av_gc_currency",
                            join: "CUSTRECORD_AV_PARENT",
                            label: "Currency"
                        }),
                        search.createColumn({
                            name: "custrecord_av_exchange_rate",
                            join: "CUSTRECORD_AV_PARENT",
                            label: "Exchange rate"
                        }),
                        search.createColumn({
                            name: "custrecord_av_gc_remaining_amount",
                            join: "CUSTRECORD_AV_PARENT",
                            label: "Remaining Amount"
                        })]
            }));

            gcSearch.every(function (result) {

                if(!gcData[result.id]){
                    gcData[result.id] = {
                        "code": result.getValue("custrecord_av_cs_gc_code"),
                        "customer": result.getValue("custrecord_av_cs_gc_from"),
                        "email": result.getValue("custrecord_av_cs_gc_email"),
                        "dataAmount": [{
                            "currency": result.getText({
                                name: "custrecord_av_gc_currency",
                                join: "CUSTRECORD_AV_PARENT"
                            }),
                            "originalAmount": result.getValue({
                                name: "custrecord_av_gc_amount",
                                join: "CUSTRECORD_AV_PARENT"
                            }),
                            "remainingAmount" : result.getValue({
                                name: "custrecord_av_gc_remaining_amount",
                                join: "CUSTRECORD_AV_PARENT"
                            })   
                        }]
                    }
                }else{
                    gcData[result.id].dataAmount.push({
                        "currency": result.getText({
                            name: "custrecord_av_gc_currency",
                            join: "CUSTRECORD_AV_PARENT"
                        }),
                        "originalAmount": result.getValue({
                            name: "custrecord_av_gc_amount",
                            join: "CUSTRECORD_AV_PARENT"
                        }),
                        "remainingAmount" : result.getValue({
                            name: "custrecord_av_gc_remaining_amount",
                            join: "CUSTRECORD_AV_PARENT"
                        })   
                    })
                }

                return true;
            });

            avLog.update({
                id: self.logId,
                send: Object.values(gcData),
                state: CONSTANTS.INTERFACE_STATE.DONE
            })

            return JSON.stringify(Object.values(gcData))

        }

        function _getAllResults(s) {
            var results = s.run();
            var searchResults = [];
            var searchid = 0;
            do {
                var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
                resultslice.forEach(function (slice) {
                    searchResults.push(slice);
                    searchid++;
                }
                );
            } while (resultslice.length >= 1000);
            return searchResults;

        }

        function _parseDateFromNetSuite(dateNotFormatted) {

            if (!dateNotFormatted) return null;

            var parsedDate = dateNotFormatted ? format.parse({
                value: dateNotFormatted,
                type: format.Type.DATE
            }) : null;

            return parsedDate

        }

        return {
            get: _entryPoint,
            post: _methodNotImplemented,
            put: _methodNotImplemented,
            delete: _methodNotImplemented,
        };
    });