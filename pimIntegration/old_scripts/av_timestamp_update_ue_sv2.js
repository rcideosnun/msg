/* **************************************************************************************
 ** Copyright (c) 2018 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 1.0
 **
 ** @author: Roberto Cideos
 ** @dated: 05/11/2021
 * *@NApiVersion 1.0
 ** @Description: set timestamp for access of changes of items
 **
 ************************************************************************************** */

 function beforeSubmit(type) {

	if ((type == 'edit') || (type == 'xedit') || (type == 'approve')) {
			if (hasChanged(nlapiGetNewRecord(), nlapiGetOldRecord())) {
				var ts = new Date().getTime();
				if (ts) {
					nlapiSetFieldValue('custitem_av_update_timestamp', ts);
				}
			}
	}
	if((type == 'create')){
		var parentItemId = hasChangedSubitemsCount(nlapiGetNewRecord(), nlapiGetOldRecord(),type)
		var ts = new Date().getTime();
		if (ts && parentItemId) {
			nlapiSubmitField(nlapiGetRecordType(),parentItemId,'custitem_av_update_timestamp',ts)
		}
	}
}

function beforeSubmit_is(type) {
	// inbound shipment

	if ((type == 'edit') || (type == 'xedit') || (type == 'create')) {

		if (hasChanged(nlapiGetNewRecord(), nlapiGetOldRecord())) {
			var ts = new Date().getTime() + 32400000;
			if (ts) {
				nlapiSetFieldValue('custrecord_fs_timestamp', ts);
			}
		}
	}

}

function beforeSubmit_il(type) {
	// integration log

	if ((type == 'edit') || (type == 'xedit') || (type == 'create')) {

		if (hasChanged(nlapiGetNewRecord(), nlapiGetOldRecord())) {
			var ts = new Date().getTime() + 32400000;
			if (ts) {
				nlapiSetFieldValue('custrecord_av_il_timestamp', ts);
			}
		}
	}
}

function beforeSubmit_fsc(type) {
	// integration log

	if ((type == 'edit') || (type == 'xedit') || (type == 'create')) {

		if (hasChanged(nlapiGetNewRecord(), nlapiGetOldRecord()), type) {
			var ts = new Date().getTime() + 32400000;
			if (ts) {
				nlapiSetFieldValue('custrecord_fs_fsc_timestamp', ts);
			}
		}
	}
}

/**
 * 
 * As soon as one of the exclude fields is changed, timestamp is NOT updated.
 * 
 */
function hasChanged(newrec, oldrec, type) {

	if(type == 'create' || !oldrec){
		return true;
	}

	var allFields = getFieldsToCheck();
	var changedFields = [];

	for (var i = 0; allFields && i < allFields.length; i++) {
		var field = allFields[i];
		var of = notNull(oldrec.getFieldValue(field));
		var nf = notNull(newrec.getFieldValue(field));
		if ( of != nf) {
			changedFields.push(field);
		}
	}

	if (changedFields.length) {
		return true;
	}else{
        return false
    }
}

function changedFieldsIsExcludeField(changedFields) {
	
	var excludeFields = [];
	var hasChanged = false;
	for (var i = 0; excludeFields && i < excludeFields.length; i++) {
		if (changedFields.indexOf(excludeFields[i]) != -1) {
			hasChanged = true;
			break;
		} else {
			hasChanged = false;
		}
	}
	return hasChanged;
}

function hasChangedSubitemsCount(newrec, oldrec, type) {

	if(type == 'create'){
		var parentId = newrec.getFieldValue("parent")
		var ts = new Date().getTime();
		if (ts) {
			nlapiSetFieldValue('custitem_av_update_timestamp', ts);
		}
	}else{
		var parentId = oldrec.getFieldValue("parent")
	}

	return parentId
}

function getFieldsToCheck(){
    return [
        "custitem_av_pim_item_prefix",
        "custitem_av_pim_department_id",
        "custitem_av_pim_product_type_id",
        "upccode",
        "displayname",
        "isinactive"
    ]
}


function notNull(value) {
	if (value) {
		return value;
	} else {
		return '';
	}
}