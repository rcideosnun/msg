/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 ** @author: [Tuba Mohsin]
 ** @dated: [26.01.2023]
 ** @Description: Integration between PIMCore and NetSuite
 *
 *  @NApiVersion 2.1
 *  @NScriptType WorkflowActionScript
 */
define([
  "N/runtime",
  "./av_msg_pim_item_lib",
], function (
  runtime,
  lib
) {

  var deltaTime, startTime;
  /**
   *
   * @param {Object} context
   * @returns
   */

  const onAction = (context) => {
    try {
      startTime = new Date().getTime();
      if (runtime.envType == "SANDBOX") {
        //setup sandbox global variables
        var scriptObj = runtime.getCurrentScript();
        var config = scriptObj.getParameter({ name: "custscript_av_integration_id" }) || null;
        var iface =  scriptObj.getParameter({ name: "custscript_av_item_iface" }) || null;

      } else if (runtime.envType == "PRODUCTION") {
        //setup production global variables
        var scriptObj = runtime.getCurrentScript();
        var config = scriptObj.getParameter({ name: "custscript_av_integration_id" }) || null;
        var iface =  scriptObj.getParameter({ name: "custscript_av_item_iface" }) || null;
      }
      let itemId = [context.newRecord.id];
      log.debug('Worflow Triggered', `Processing interface: ${iface} for item: ${itemId}`);
      lib.handleRequest(itemId, iface, config);

      deltaTime = (new Date().getTime() - startTime) / 1000;
      log.debug("Time Delta", `Time Usage: ${deltaTime}s`);
      
    } catch (e) {
      log.debug("Error", JSON.stringify(e));
    }
  }



//   function _sendItemToPim(context) {
//     var payload = _getItemDataNew(context);
//     log.audit("payload", payload);

//     var reqHeaders = {
//       "Content-type": "application/json",
//       Accept: "application/json",
//       Authorization: `Bearer ${self.connectionObj.accessToken}`,
//     };

//     var requestUrl =
//       self.connectionObj.custrecord_av_integration_base_url.concat(
//         self.connectionObj.custrecord_av_integration_product_post
//       );
//     log.debug("requestUrl", requestUrl);

//     var method = CONSTANTS.INTERFACES[self.iface].onActionHttpRequest;
//     //payload[0].lastSyncDate ? 'PUT' : CONSTANTS.INTERFACES[self.iface].onActionHttpRequest;
//     // delete payload[0].lastSyncDate;

//     var logId = avLog.create({
//       iface: self.iface,
//       send: payload,
//       url: requestUrl,
//       state: CONSTANTS.INTERFACE_STATE.NEW,
//       item: context.newRecord.id,
//     });

//     if (payload && payload.length > 0 && !payload[0].displayName) {
//       log.debug("Missing Display Name", logId);
//       avLog.update({
//         id: logId,
//         state: CONSTANTS.INTERFACE_STATE.ERROR,
//       });

//       avLog.error_message({
//         logid: logId,
//         msg: CONSTANTS.ERROR_TYPES.MISSING_DISPLAY_NAME,
//         var1: JSON.stringify(payload),
//       });

//       return null;
//     } else if (
//       payload &&
//       payload.length > 0 &&
//       payload[0].itemType == CONSTANTS.ITEM_TYPE.KIT_ITEM_TYPE &&
//       !payload[0].parentProductNumber
//     ) {
//       avLog.update({
//         id: logId,
//         state: CONSTANTS.INTERFACE_STATE.ERROR,
//       });

//       avLog.error_message({
//         logid: logId,
//         msg: CONSTANTS.ERROR_TYPES.MISSING_PARENT,
//         var1: JSON.stringify(payload),
//       });
//     }

//     var request = https.request({
//       method: method,
//       url: requestUrl,
//       body: JSON.stringify(payload),
//       headers: reqHeaders,
//     });

//     setTimeStamp(context, payload, request, logId);

//     deltaTime = (new Date().getTime() - startTime) / 1000;

//     log.debug("Time Delta", `Send Item Time Usage: ${deltaTime}s`);
//   }

  /**
   *
   * @param {Object} context
   * @returns
   */

//   function _sendItemPriceToPim(context) {
//     var payload = _getItemPrice(context);

//     var reqHeaders = {
//       "Content-type": "application/json",
//       Authorization: `Bearer ${self.connectionObj.accessToken}`,
//       Accept: "application/json",
//     };

//     var requestUrl =
//       self.connectionObj.custrecord_av_integration_base_url.concat(
//         self.connectionObj.custrecord_av_integration_product_put
//       );

//     var logId = avLog.create({
//       iface: self.iface,
//       send: payload,
//       url: requestUrl,
//       state: CONSTANTS.INTERFACE_STATE.NEW,
//       item: context.newRecord.id,
//     });

//     var request = https.request({
//       method: CONSTANTS.INTERFACES[self.iface].onActionHttpRequest,
//       url: requestUrl,
//       body: payload,
//       headers: reqHeaders,
//     });
//     log.debug("request ==> P02", request);
//     setTimeStamp(context, payload, request, logId);

//     deltaTime = (new Date().getTime() - startTime) / 1000;

//     log.debug("Time Delta", `Update Item Time Usage: ${deltaTime}s`);
//   }

  /**
   *
   * @param {Object} context
   * @param {Object} payload
   * @param {Object} response
   * @param {Object} logId
   * @returns
   */
//   function setTimeStamp(context, payload, response, logId) {
//     const body = JSON.parse(response.body);
//     if (
//       body.success &&
//       (body.success?.length > 0 || Object.keys(body.success).length > 0)
//     ) {
//       avLog.update({
//         id: logId,
//         receive: response.body,
//         responseCode: response.code,
//         state: CONSTANTS.INTERFACE_STATE.DONE,
//       });

//       if (context) {
//         const today = new Date();
//         let formattedDate = format.format({
//           value: today,
//           type: format.Type.DATETIME,
//         });
//         // This needs to be done because we compare the modified date to this date in order to sync the item.
//         // Since we are updating the record by setting this value, it always changes the last modified date
//         // which will always be higher than this date.
//         // If you are reading this and have a better solution, please recommend it to me (Tehseen Ahmed)
//         const lastIndex = formattedDate.lastIndexOf(":");
//         const timeBeforeSecond = formattedDate.slice(0, lastIndex) + ":59";
//         const parsedDate = format.parse({
//           value: timeBeforeSecond,
//           type: format.Type.DATETIME,
//         });

//         context.newRecord.setValue("custitem_msg_time_sent_pim", parsedDate);
//         if (!payload.productId) {
//           context.newRecord.setValue(
//             "custitem_av_pim_product_id",
//             JSON.parse(response.body).id
//           );
//         }
//       }
//     } else {
//       var message = "";
//       switch (self.iface) {
//         case "P01":
//           message = CONSTANTS.PIM_ERROR_TYPES.PIM1_INTERNAL_ERROR;
//           break;
//         case "P02":
//           message = CONSTANTS.PIM_ERROR_TYPES.PIM2_INTERNAL_ERROR;
//           break;
//       }

//       if (body.failed && body.failed?.length > 0) {
//         body.failed.forEach((err) => {
//           let concatenatedErrorMsg = `Error for product ${err?.productNumber}: `;
//           if (err.errors && err.errors.length > 0) {
//             concatenatedErrorMsg += err.errors.map((u) => u.message).join(", ");
//           }
//           avLog.update({
//             id: logId,
//             responseCode: response.code,
//             state: CONSTANTS.INTERFACE_STATE.ERROR,
//             receive: response.body,
//           });
//           avLog.error_message({
//             logid: logId,
//             msg: message,
//             var1: concatenatedErrorMsg,
//           });
//         });
//       } else {
//         avLog.update({
//           id: logId,
//           responseCode: response.code,
//           state: CONSTANTS.INTERFACE_STATE.ERROR,
//           receive: response.body,
//         });

//         avLog.error_message({
//           logid: logId,
//           msg: message,
//           var1: response.body,
//         });
//       }
//     }
//   }

  /**
   *
   * @param {Object} context
   * @returns
   */

//   function _sendItemStockToPim(context) {
//     var stocks = stockLib.getStock(context.newRecord.id);
//     // if (context.newRecord.type != CONSTANTS.ITEM_TYPE.SERVICE_TYPE && context.newRecord.type != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE) {
//     //     stockS = _getItemStock(context)
//     // } else {
//     //     stockS = _getFakeStock(context.newRecord)
//     // }

//     var reqHeaders = {
//       "Content-type": "application/json",
//       Authorization: `Bearer ${self.connectionObj.accessToken}`,
//       Accept: "application/json",
//       // "X-HTTP-Method-Override": "PATCH"
//     };
//     var payloads = [];
//     stocks.forEach(function (stock) {
//       var index = payloads.findIndex(function (payload) {
//         return payload.productNumber == stock.productNumber;
//       });

//       if (index > -1) {
//         payloads[index].salesLocations.push({
//           location: stock.location,
//           quantity: stock.quantity,
//         });
//       } else {
//         payloads.push({
//           productNumber: stock.productNumber,
//           salesLocations: [
//             {
//               location: stock.location,
//               quantity: stock.quantity,
//             },
//           ],
//         });
//       }
//     });

//     log.debug({ title: "_sendItemStockToPim : payloads", details: payloads });
//     var requestUrl =
//       self.connectionObj.custrecord_av_integration_base_url.concat(
//         self.connectionObj.custrecord_av_integration_stock_post
//       );

//     var logId = avLog.create({
//       iface: self.iface,
//       send: payloads,
//       url: requestUrl,
//       state: CONSTANTS.INTERFACE_STATE.NEW,
//       item: context.newRecord.id,
//     });

//     log.debug({
//       title: "_sendItemStockToPim : reqHeaders",
//       details: reqHeaders,
//     });

//     var request = https.request({
//       method: CONSTANTS.INTERFACES[self.iface].onActionHttpRequest,
//       url: requestUrl,
//       body: JSON.stringify(payloads),
//       headers: reqHeaders,
//     });

//     log.debug({ title: "_sendItemStockToPim : request", details: request });

//     const body = JSON.parse(request.body);
//     if (
//       body.success &&
//       (body.success?.length > 0 || Object.keys(body.success).length > 0)
//     ) {
//       avLog.update({
//         id: logId,
//         responseCode: request.code,
//         state: CONSTANTS.INTERFACE_STATE.DONE,
//         receive: request.body,
//       });
//     } else {
//       avLog.update({
//         id: logId,
//         responseCode: request.code,
//         state: CONSTANTS.INTERFACE_STATE.ERROR,
//         receive: request.body,
//       });

//       if (body.failed && body.failed?.length > 0) {
//         body.failed.forEach((err) => {
//           let concatenatedErrorMsg = `Error for product ${err?.productNumber}: `;
//           if (err.errors && err.errors.length > 0) {
//             concatenatedErrorMsg += err.errors.map((u) => u.message).join(", ");
//           }
//           avLog.error_message({
//             logid: logId,
//             msg: CONSTANTS.PIM_ERROR_TYPES.PIM4_INTERNAL_ERROR,
//             var1: concatenatedErrorMsg,
//           });
//         });
//       } else {
//         avLog.error_message({
//           logid: logId,
//           msg: CONSTANTS.PIM_ERROR_TYPES.PIM4_INTERNAL_ERROR,
//           var1: request.body,
//         });
//       }
//     }
//   }

  /**
   *
   * @param {Object} context
   * @returns
   */

//   function _getItemDataNew(context) {
//     try {
//       var currRec = context.newRecord;
//       var itemType = currRec.type;
//       var itemsById = _getItemsById();
//       var itemNumber = currRec.getValue("itemid");
//       var itemDisplayName = currRec.getValue("displayname");
//       var itemEan = currRec.getValue("upccode");
//       var itemIsActive = !currRec.getValue("custitem_is_shop_online");
//       var itemHeight = parseInt(currRec.getValue("custitem_msg_height"));
//       var itemWidth = parseInt(currRec.getValue("custitem_msg_width"));
//       var itemLength = parseInt(currRec.getValue("custitem_msg_length"));
//       var itemWeight = currRec.getValue("weight")
//         ? parseFloat(currRec.getValue("weight"))
//         : null;
//       var itemBrand = currRec.getText("cseg_msg_brands");
//       var itemProductType = currRec.getText("custitem_msg_product_type");
//       var itemUNnumber = currRec.getText("custitem_msg_un_number");
//       let itemParent =
//         _getParent(currRec.getValue("parent")) ||
//         currRec.getValue("custitem_msg_main_product_kit");
//       var itemComponents = _getItemComponents(currRec, itemsById);
//       var subsidiaryKeys = currRec.getText("subsidiary");
//       subsidiaryKeys = subsidiaryKeys.map(function (subsidiary) {
//         return subsidiary.indexOf(" : ") > -1
//           ? subsidiary.split(" : ").pop()
//           : subsidiary;
//       });
//       var itemGroupType = currRec.getText("cseg_msg_igt");
//       var taxScheduleKey = currRec.getText("taxschedule");
//       var brandProductNumber = currRec.getValue("mpn");
//       var refUnit = currRec.getValue("custitem_av_ref_unit");
//       var purchaseUnit = currRec.getValue("custitem_av_purchase_unit");
//       var unitKey = currRec.getText("custitem_av_unit_key");
//       var lastSyncDate = currRec.getValue("custitem_msg_time_sent_pim");

//       if (itemType == CONSTANTS.ITEM_TYPE.KIT_ITEM_TYPE) {
//         itemParent =
//           itemsById.itemById[
//             Number(currRec.getValue("custitem_msg_main_product_kit"))
//           ];
//       }

//       var itemObj = [
//         {
//           productNumber: itemNumber,
//           displayName: itemDisplayName || itemNumber,
//           ean: itemEan || "1",
//           subsidiaryKeys:
//             typeof subsidiaryKeys == "string"
//               ? [subsidiaryKeys]
//               : subsidiaryKeys,
//           itemGroupType: itemGroupType,
//           brand: _setNullAsDef(itemBrand),
//           taxScheduleKey: taxScheduleKey,
//           isActive: itemIsActive,
//           brandProductNumber: _setNullAsDef(brandProductNumber),
//           hazmatTypeKeys: _setNullAsDef(itemUNnumber),
//           height: _setNullAsDef(itemHeight),
//           width: _setNullAsDef(itemWidth),
//           length: _setNullAsDef(itemLength),
//           weight: _setNullAsDef(itemWeight),
//           refUnit: _setNullAsDef(refUnit),
//           purchaseUnit: _setNullAsDef(purchaseUnit),
//           unitKey: _setNullAsDef(unitKey),
//           productType: itemProductType || "",
//           parentProductNumber: _setNullAsDef(itemParent),
//           comboProductNumbers: _setNullAsDef(itemComponents),
//           lastSyncDate: _setNullAsDef(lastSyncDate),
//           itemType: itemType,
//         },
//       ];

//       return itemObj;
//     } catch (e) {
//       log.audit("error", e);
//     }
//   }

  /**
   *
   * @param {Object} context
   * @returns
   */

//   function _getItemPrice(context) {
//     var currRec = context.newRecord;
//     var itemType = currRec.type;

//     var itemNumber = currRec.getValue("itemid");
//     var pimItemId = Number(currRec.getValue("custitem_av_pim_product_id"));
//     var today = format.format({
//       value: new Date(),
//       type: format.Type.DATE,
//     });

//     var lineCount = currRec.getLineCount({
//       sublistId: "recmachcustrecord_msg_ip_item",
//     });

//     var applicablePrices = [
//       // {
//       //     dateFrom: '1.4.2022',
//       //     dateTo: ''
//       // },
//       // {
//       //     dateFrom: '1.4.2022',
//       //     dateTo: '1.2.2023'
//       // },
//       // {
//       //     dateFrom: '1.4.2022',
//       //     dateTo: '1.11.2022'
//       // },
//       // {
//       //     dateFrom: '1.4.2023',
//       //     dateTo: '1.11.2023'
//       // },
//       // {
//       //     dateFrom: '20.12.2022',
//       //     dateTo: '15.1.2023'
//       // }
//     ];

//     for (var i = 0; i < lineCount; i++) {
//       var dateFrom = currRec.getSublistValue({
//         sublistId: "recmachcustrecord_msg_ip_item",
//         fieldId: "custrecord_msg_ip_date_from",
//         line: i,
//       });
//       var dateTo = currRec.getSublistValue({
//         sublistId: "recmachcustrecord_msg_ip_item",
//         fieldId: "custrecord_msg_ip_date_to",
//         line: i,
//       });

//       dateFrom = dateFrom
//         ? format.format({
//             value: dateFrom,
//             type: format.Type.DATE,
//           })
//         : dateFrom;

//       dateTo = dateTo
//         ? format.format({
//             value: dateTo,
//             type: format.Type.DATE,
//           })
//         : dateTo;

//       if (today >= dateFrom && (!dateTo || today <= dateTo)) {
//         applicablePrices.push({
//           basePrice: currRec.getSublistValue({
//             sublistId: "recmachcustrecord_msg_ip_item",
//             fieldId: "custrecord_msg_ip_base",
//             line: i,
//           }),
//           b2bPrice: currRec.getSublistValue({
//             sublistId: "recmachcustrecord_msg_ip_item",
//             fieldId: "custrecord_msg_ip_b2b",
//             line: i,
//           }),
//           srpPrice: currRec.getSublistValue({
//             sublistId: "recmachcustrecord_msg_ip_item",
//             fieldId: "custrecord_msg_ip_srp",
//             line: i,
//           }),
//           minimalPrice: currRec.getSublistValue({
//             sublistId: "recmachcustrecord_msg_ip_item",
//             fieldId: "custrecord_msg_ip_minimal",
//             line: i,
//           }),
//           dateFrom: dateFrom,
//           dateTo: dateTo,
//         });
//       }
//     }

//     var priceObject = {};
//     applicablePrices.forEach(function (price) {
//       applicablePrices.forEach(function (price2) {
//         if (
//           price.dateFrom >= price2.dateFrom &&
//           (!price.dateTo || price.dateTo <= price2.dateTo)
//         ) {
//           priceObject = price;
//         }
//       });
//     });

//     var basePrice =
//       itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE
//         ? parseFloat(priceObject.basePrice)
//         : _getPriceGiftCert(currRec);
//     var b2bPrice =
//       itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE
//         ? parseFloat(priceObject.b2bPrice)
//         : _getPriceGiftCert(currRec);
//     var srpPrice =
//       itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE
//         ? parseFloat(priceObject.srpPrice)
//         : _getPriceGiftCert(currRec);
//     var minimalPrice =
//       itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE
//         ? parseFloat(priceObject.minimalPrice)
//         : _getPriceGiftCert(currRec);
//     var contributionMarign = parseFloat(
//       currRec.getValue("custitem_av_item_contributionmargin")
//     );
//     log.audit("contributionMarign", contributionMarign);
//     var itemObj = [
//       {
//         productNumber: itemNumber,
//         netBasePrice: basePrice,
//         netB2BPrice: _setNullAsDef(b2bPrice),
//         netSRPPrice: _setNullAsDef(srpPrice),
//         netMinimalPrice: _setNullAsDef(minimalPrice),
//         contributionMargin: _setNullAsDef(contributionMarign),
//       },
//     ];

//     log.debug({ title: "itemObj", details: itemObj });

//     return JSON.stringify(itemObj);
//   }
  
  /**
   *
   * @param {SuiteScript Record Object} currRec
   * @returns
   */

//   function _getPriceGiftCert(currRec) {
//     return currRec.getSublistValue({
//       sublistId: "price",
//       fieldId: "price_1_",
//       line: 0,
//     });
//   }


//   function _getParent(itemId) {
//     if (!itemId) return null;
//     try {
//       var parentSearch = search
//         .create({
//           type: search.Type.ITEM,
//           filters: [["internalid", search.Operator.ANYOF, itemId]],
//           columns: [search.createColumn({ name: "itemid" })],
//         })
//         .run()
//         .getRange({
//           start: 0,
//           end: 1,
//         });

//       return parentSearch.length
//         ? parentSearch[0].getValue({ name: "itemid" })
//         : null;
//     } catch (ex) {
//       log.error({ title: "_getParent : ex", details: ex });
//     }
//   }




  /**
   *
   * @param {SuiteScript Search Object} s
   * @returns
   */

//   function _getAllResults(s) {
//     var results = s.run();
//     var searchResults = [];
//     var searchid = 0;
//     do {
//       var resultslice = results.getRange({
//         start: searchid,
//         end: searchid + 1000,
//       });
//       resultslice.forEach(function (slice) {
//         searchResults.push(slice);
//         searchid++;
//       });
//     } while (resultslice.length >= 1000);
//     return searchResults;
//   }

  /**
   *
   * @param {SuiteScript Record Object} rec
   * @returns
   */

//   function _getItemComponents(rec, itemsById) {
//     var itemSkus = [];

//     if (rec.type != CONSTANTS.ITEM_TYPE.KIT_ITEM_TYPE) return itemSkus;

//     var itemComponentsL = rec.getLineCount({ sublistId: "member" });

//     for (var i = 0; i < itemComponentsL; i++) {
//       let memberId = Number(
//         rec.getSublistValue({
//           sublistId: "member",
//           fieldId: "item",
//           line: i,
//         })
//       );
//       if (itemsById.itemById[memberId])
//         itemSkus.push(itemsById.itemById[memberId]);
//     }

//     return itemSkus;
//   }

  /**
   *
   * @param {String} val
   * @returns
   */

//   function _setNullAsDef(val) {
//     return !val || (val && val.length == 0) ? null : val;
//   }

  /**
   *
   * @param {SuiteScript Record Object} stagingRec
   * @returns
   */

//   function _getItemsById() {
//     var itemById = {};

//     var itemSearchObj = _getAllResults(
//       search.create({
//         type: "item",
//         filters: [],
//         columns: ["itemid", "parent", "type"],
//       })
//     );

//     itemSearchObj.every(function (result) {
//       if (result.getValue("parent")) {
//         itemById[result.id] = result.getValue("itemid").split(" ")[2];
//       } else {
//         itemById[result.id] = result.getValue("itemid");
//       }

//       return true;
//     });

//     return {
//       itemById: itemById,
//     };
//   }

return {
    onAction: onAction,
  };
});
