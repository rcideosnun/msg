/**
 ** Copyright (c) 2015 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
** @version: 2.0
** @author: Roberto Cideos
** @dated: 10/08/2022
** @Description: Send updated status of SO to PIM, in case the sales order contains ONLY vouchers, we need to send an extra request.
*
*@NApiVersion 2.1
*@NScriptType UserEventScript
*
*/

define(["./av_msg_lib_sv2",
    "N/runtime",
    "N/https",
    "N/search",
    "./av_int_log.js",
    "N/record"],
    function (settings,
        runtime,
        https,
        search,
        avLog,
        record) {

        var self = this;
        const CONSTANTS = {
            ORDER_STATUS_MAP: {
                "Billed": "complete",
                "Pending Billing": "complete",
                "Pending Fulfillment": "process",
                "Pending Fulfillment/Pending Billing": "process",
                "Pending Billing/Partially Fulfilled": "process",
                "Cancelled": "cancel",
                "Default": "open",
                "Pending Refund": "complete",
                "DefaultProcess": "process"
            },
            HTTP_CODE: {
                SUCCESS_UPDATE: 200,
                SUCCESS_CREATE: 201,
                ERROR: 401
            },
            INTERFACE_STATE: {
                NEW: 1,
                ERROR: 2,
                DONE: 3
            },
            INTERFACES: {
                "SW3": {
                    onActionHttpRequest: "POST",
                    connectionRequired: true,
                    updatePath: "/api/_action/order/ORDER_ID/state/" //ATTENTION: THIS IS NOT THE URL WHERE THE REQUEST IS SENT
                }
            },
            SEND_EMAIL: false,
            ERROR_TYPES: {
                DUPLICATED_ORDER: "SW1",
                ITEM_NOT_FOUND: "SW2",
                INTERNAL_ERROR: "SW3",
                ADDRESS_ERROR: "SW4",
                MISSING_PAYMENT: "SW5",
                MISSING_EMAIL: "SW6",
                MISSING_CUSTOMER: "SW7"
            }
        }

        /**
         * 
         * @param {SuiteScript Record Object} context 
         * @returns 
         */

        function afterSubmit(context) {
            try {
                let scriptObj = runtime.getCurrentScript();
                self.oldRec = context.oldRecord;
                self.newRec = context.newRecord;

                if (runtime.envType == "SANDBOX") {
                    //setup sandbox global variables, GC are not listed as available records in the List/Record setup
                    self.iface = _lookUpName(scriptObj.getParameter({ name: "custscript_av_so_iface" }));
                    self.configId = scriptObj.getParameter({ name: "custscript_av_so_config" });

                } else if (runtime.envType == "PRODUCTION") {
                    //setup production global variables, GC are not listed as available records in the List/Record setup
                    self.iface = _lookUpName(scriptObj.getParameter({ name: "custscript_av_so_iface" }));
                    self.configId = scriptObj.getParameter({ name: "custscript_av_so_config" });
                }

                let eventRouter = {
                    "create": _handleCreateEvent,
                    "edit": _handleEditEvent,
                    "xedit": _handleEditEvent
                }

                if (typeof eventRouter[context.type] !== "function") {
                    return;
                }

                self.connectionObj = new settings(self.configId, context, CONSTANTS.INTERFACES[self.iface].connectionRequired);

                eventRouter[context.type](context);
            } catch (e) {
                log.debug("Error", JSON.stringify(e))
            }
        }

        /**
         * 
         * @param {SuiteScript Record Object} rec 
         * @returns 
         */

        function _sendRequest(rec, overrideStatus = null) {
            let orderStatus;
            try {
                //We load the record, otherwise cannot use getText
                if (!self.rec) {
                    rec = self.rec = record.load({
                        type: rec.type,
                        id: rec.id
                    })
                } else {
                    rec = self.rec
                }

                if (overrideStatus) {
                    orderStatus = overrideStatus
                } else {
                    orderStatus = CONSTANTS.ORDER_STATUS_MAP[rec.getText("status")] || CONSTANTS.ORDER_STATUS_MAP["Default"];
                }

                let payload = JSON.stringify({
                    "channelId": rec.getValue("custbody_av_sales_channel_id") || null,
                    "method": CONSTANTS.INTERFACES[self.iface].onActionHttpRequest,
                    "urlPath": `${CONSTANTS.INTERFACES[self.iface].updatePath.replace("ORDER_ID", rec.getValue("custbody_av_sw_order_id"))}${orderStatus}`,
                    "body": {
                        "sendEmail": CONSTANTS.SEND_EMAIL
                    }
                });

                let requestUrl = self.connectionObj.custrecord_av_integration_base_url.concat(self.connectionObj.custrecord_av_integration_so_post)

                self.logId = avLog.create({
                    iface: self.iface,
                    send: payload,
                    url: requestUrl,
                    state: CONSTANTS.INTERFACE_STATE.NEW,
                    transaction: self.newRec.id
                })

                let reqHeaders = {
                    "Content-type": "application/json",
                    "Authorization": `Bearer ${self.connectionObj.accessToken}`
                }

                return https.request({
                    method: CONSTANTS.INTERFACES[self.iface].onActionHttpRequest,
                    url: requestUrl,
                    body: payload,
                    headers: reqHeaders
                })
            } catch (e) {
                log.error("Error", JSON.stringify(e))
            }
        }


        /**
         * @param {Number} id
         */

        function _lookUpName(id) {

            return search.lookupFields({
                type: "customrecord_av_interface_type",
                id: id,
                columns: "name"
            }).name || null

        }


        function _handleCreateEvent() {

            return _handleRequest()
        }

        function _handleEditEvent() {

            return _handleRequest()

        }

        function _handleRequest() {
            let request = _sendRequest(self.newRec)
            if (request.code == CONSTANTS.HTTP_CODE.SUCCESS_CREATE) {

                avLog.update({
                    id: self.logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.DONE,
                    receive: request.body
                })

            } else {
                avLog.update({
                    id: self.logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.ERROR,
                    receive: request.body
                })

                avLog.error_message({
                    logid: self.logId,
                    msg: CONSTANTS.ERROR_TYPES.INTERNAL_ERROR,
                    var1: request.body
                })
            }

            let voucherOrder = _salesOrderIsOnlyVoucher()

            if (voucherOrder && CONSTANTS.ORDER_STATUS_MAP[self.rec.getText("status")] == CONSTANTS.ORDER_STATUS_MAP["Default"]) {

                let request = _sendRequest(self.rec, CONSTANTS.ORDER_STATUS_MAP["DefaultProcess"]);

                if (request.code == CONSTANTS.HTTP_CODE.SUCCESS_CREATE) {

                    avLog.update({
                        id: self.logId,
                        responseCode: request.code,
                        state: CONSTANTS.INTERFACE_STATE.DONE,
                        receive: request.body
                    })
    
                } else {
                    avLog.update({
                        id: self.logId,
                        responseCode: request.code,
                        state: CONSTANTS.INTERFACE_STATE.ERROR,
                        receive: request.body
                    })
    
                    avLog.error_message({
                        logid: self.logId,
                        msg: CONSTANTS.ERROR_TYPES.INTERNAL_ERROR,
                        var1: request.body
                    })
                }

                return;
            } else {
                return;
            }
        }

        function _salesOrderIsOnlyVoucher() {

            let hasGc = true;

            var soLineCount = self.rec.getLineCount({
                sublistId: "item"
            })

            for (var i = 0; i < soLineCount; i++) {

                let hasGc = self.rec.getSublistValue({
                    sublistId: 'item',
                    fieldId: 'giftcertkey',
                    line: i
                });

                if (!hasGc) {
                    hasGc = false
                }

            }

            return hasGc

        }

        return {
            afterSubmit: afterSubmit
        };

    })