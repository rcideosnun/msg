/**
 ** Copyright (c) 2022 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 *
 * Author: Tehseen Ahmed
 * Date: 27/12/2022
 * Add and update items to PIM
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */

define([
  "N/search",
  "N/record",
  "N/task",
  "N/runtime",
  "./av_msg_pim_item_lib.js",
  // "./av_msg_pim_item_sync_lib.js",
  // "./av_msg_item_stock_lib.js",
], function (search, record, task, runtime, lib) {

  const getInputData = () => {
    log.audit({
      title: "getInputData",
      details: `-Script Started on ${new Date()}-`,
    });

    const currentRuntime = runtime.getCurrentScript();
    const iface = currentRuntime.getParameter({name: "custscript_av_pim_item_sync_iface"});
    let index = currentRuntime.getParameter({name: "custscript_av_pim_item_sync_index"});
    index = parseInt(index);

    try {
      const resultGroups = getItemsToSync(index, currentRuntime);
      let itemsToSync = [];
   
      resultGroups.forEach((itemIds) => {
        
        // let itemsToAddUpdate = { type: "POST", data: [] };

        if(!itemIds || !itemIds.length > 0 ) return;

        let interface = lib.getInterface(iface);
        let payload = interface.getPayloadFunction(itemIds, interface)
     
        itemsToSync(payload);
        
        // resultGroup.forEach((searchResult) => {
        //   if (iface == "P04") {
        //     itemIds.push(searchResult.id);
        //   } else {
        //     let itemRecord = record.load({
        //       type: searchResult.type,
        //       id: searchResult.id,
        //       isDynamic: true,
        //     });

        //     let payload = {};
        //     switch (iface) {
        //       case "P01":
        //         payload = itemSyncLib.getItemDetails(itemRecord);
        //         break;
        //       case "P02":
        //         payload = itemSyncLib.getItemPrice(itemRecord);
        //         break;
        //     }

        //     itemsToAddUpdate.data.push(payload);
        //     delete payload.lastSyncDate;

        //     // if (payload.lastSyncDate) {
        //     //   delete payload.lastSyncDate;
        //     //   itemsToUpdate.data.push(payload);
        //     // } else {
        //     //   itemsToAdd.data.push(payload);
        //     // }
        //   }
        // });

        // var stocks = stockLib.getStock(itemIds);

        // itemsToSync.push([itemsToUpdate, itemsToAdd]);
      
      });

      return itemsToSync;
    } catch (ex) {
      log.error({ title: "getInputData : ex", details: ex });
    }
  };

  const map = (context) => {
    log.audit({ title: "map", details: "triggered" });
    const searchResult = JSON.parse(context.value);
    log.audit({ title: "map searchResult", details: searchResult });
    try {
      let response = {};
      if (searchResult.data)
        response = itemSyncLib.sendRequest(searchResult.data, "POST");

      // searchResult.forEach(searchResult => {
      //   if (searchResult.data.length) {
      //     switch (searchResult.type) {
      //       case 'POST':
      //         response = itemSyncLib.sendRequest(searchResult.data, 'POST');
      //         break;
      //       case 'PUT':
      //         response = itemSyncLib.sendRequest(searchResult.data, 'PUT');
      //         break;
      //     }
      //   }
      // });
      log.audit({ title: "map response", details: response });
      context.write({
        key: response.logId,
        value: JSON.stringify({
          searchResult: searchResult,
          response: response.response,
        }),
      });
    } catch (ex) {
      log.error({ title: "map : ex", details: ex });
      logLib.error_message({
        msg: constants.ITEM_RECORD.ERRORS.EX,
        item: searchResult.id,
        var1: JSON.stringify(ex),
      });
    }
  };

  const reduce = (context) => {
    log.audit({ title: "reduce", details: "triggered" });

    const logId = JSON.parse(context.key);
    const body = JSON.parse(context.values[0]);

    log.audit({ title: "reduce: logId", details: logId });
    log.audit({ title: "reduce: body", details: body });

    // TODO: Process the response object and set last sync date on the item.
    // Pending because we don't know what will be the response object look like.

    // let itemRecord = record.load({
    //   type: searchResult.type,
    //   id: searchResult.id,
    //   isDynamic: true
    // });

    // const posSettings = posLib.getPosSettings(true);
    // const posConfig = posLib.getPosConfig();
    // const posFields = itemSyncLib.getPosItemFields();

    // if (!posSettings || !posConfig || !posFields) return;

    // const currentRuntime = runtime.getCurrentScript();
    // const iface = currentRuntime.getParameter({
    //   name: 'custscript_av_pim_item_sync_iface'
    // });
    // switch (iface) {
    //   case 'P01':
    //     itemSyncLib.sendItemToPim(itemRecord, iface);
    //      break;
    //   case 'P02':
    //     itemSyncLib.sendItemPriceToPim(itemRecord, iface);
    //      break;
    // }

    // itemRecord.save({
    //   enableSourcing: false,
    //   ignoreMandatoryFields: true
    // });
  };

  const summarize = () => {
    log.audit({ title: "summarize", details: "triggered" });
    try {
      const currentRuntime = runtime.getCurrentScript();
      let index = currentRuntime.getParameter({
        name: "custscript_av_pim_item_sync_index",
      });
      index = parseInt(index) + 1000;
      log.audit({ title: "summarize: index", details: index });
      let itemsToSync = getItemsToSync(index, currentRuntime);
      if (itemsToSync.length > 0) {
        //reschedule the map reduce script
        let mrTask = task.create({
          taskType: task.TaskType.MAP_REDUCE,
          scriptId: currentRuntime.id,
          deploymentId: currentRuntime.deploymentId,
        });
        mrTask.params = {
          custscript_av_pim_item_sync_index: index,
        };
        const mrTaskId = mrTask.submit();
        log.audit({ title: "summarize", details: `mrTaskId: ${mrTaskId}` });
      }
    } catch (ex) {
      log.error({ title: "summarize : ex", details: ex });
    }

    log.audit({
      title: "summarize",
      details: `-Script Finished on ${new Date()}-`,
    });
  };

  const getItemsToSync = (index, currentRuntime) => {
    let results = [];
    let itemSearch = search.load({
      id: currentRuntime.getParameter({name: "custscript_av_pim_item_sync_search"}),
    });

    var currentRange = itemSearch.run().getRange({
      start: index,
      end: index + 1000,
    });

    for (var i = 0; !!currentRange && i < currentRange.length; i++) {
      results.push(currentRange[i].id)
        // {id: currentRange[i].id, type: currentRange[i].recordType});
    }

    let resultGroups = [];
    const size = currentRuntime.getParameter({name: "custscript_av_pim_item_sync_size"}) || 100;

    for (let i = 0; i < results.length; i += size) {
      resultGroups.push(results.slice(i, i + size));
    }
    return resultGroups;
  };

  return {
    getInputData: getInputData,
    map: map,
    reduce: reduce,
    summarize: summarize,
  };
});
