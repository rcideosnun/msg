/**
 ** Copyright (c) 2015 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 ** @author: Tehseen Ahmed
 ** @dated: 10/1/2023
 ** @Description: This library contains functions related to stock interface
 * *@NApiVersion 2.1
 */

define(['N/search'], function (search) {
  function getStock(itemIds) {
    itemIds = typeof itemIds !== 'object' ? [itemIds] : itemIds;
    var salesLocations = getSalesLocations();
    return getLocationStocks(itemIds, salesLocations);
  }

  function getSalesLocations() {
    var locations = [];
    var locationSearch = search.create({
      type: 'customrecord_msg_sales_location',
      filters: [['isinactive', 'is', false]],
      columns: [
        search.createColumn({ name: 'name' }),
        search.createColumn({ name: 'custrecord_msg_sl_location' })
      ]
    });

    locationSearch.run().each(function (result) {
      var location = result.getValue({ name: 'custrecord_msg_sl_location' });
      location = typeof location === 'string' ? [location] : location;
      locations.push({
        id: result.id,
        location: location,
        name: result.getValue({ name: 'name' })
      });
      return true;
    });

    // return [
    //   {
    //     id: 1,
    //     name: 'test 1',
    //     location: ['1', '2']
    //   },
    //   {
    //     id: 2,
    //     name: 'test 2',
    //     location: ['3', '4']
    //   },
    //   {
    //     id: 3,
    //     name: 'test 3',
    //     location: ['2', '5']
    //   },
    //   {
    //     id: 4,
    //     name: 'test 4',
    //     location: ['3']
    //   },
    //   {
    //     id: 5,
    //     name: 'test 5',
    //     location: ['6']
    //   }
    // ]

    return locations;
  }

  function getLocationStocks(itemIds, salesLocations) {
    var stockByLoc = [];
    salesLocations.forEach(function (location) {
      var stockSearch = _getAllResults(search.create({
        type: search.Type.ITEM,
        filters: [
          ['internalid', 'anyof', itemIds],
          'AND',
          ['inventorylocation', 'anyof', location.location],
          'AND',
          ['isinactive', 'is', false]
        ],
        columns: [
          search.createColumn({
            name: 'itemid',
            summary: 'GROUP',
            sort: search.Sort.ASC,
            label: 'Name'
          }),
          search.createColumn({
            name: 'formulanumeric',
            summary: 'SUM',
            formula: 'case when {locationquantityavailable} is null then 0 else {locationquantityavailable} end',
            label: 'Formula (Numeric)'
          })
        ]
      }))

      stockSearch.every(function (result) {
        stockByLoc.push({
          productNumber: result.getValue({ name: 'itemid', summary: 'GROUP' }),
          location: location.name,
          quantity: Number(result.getValue({ name: 'formulanumeric', summary: 'SUM' })) || 0
        })
        return true;
      });
    });

    return stockByLoc;
  }

  function getFakeStock(currRec) {
    var stockByLoc = [];
    _getAllResults(search.create({
      type: "location",
      filters: [],
      columns: ["custrecord_av_msg_pim_location_id"]
    })).every(function (result) {
      if (Number(result.getValue("custrecord_av_msg_pim_location_id"))) {
        stockByLoc.push({
          productNumber: String(currRec.getValue("itemid")),
          storeId: Number(result.getValue("custrecord_av_msg_pim_location_id")),
          stock: CONSTANTS.FAKE_STOCK
        })
      }
      return true;
    });

    return stockByLoc
  }

  function _getAllResults(s) {
    var results = s.run();
    var searchResults = [];
    var searchid = 0;
    do {
      var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
      resultslice.forEach(function (slice) {
        searchResults.push(slice);
        searchid++;
      }
      );
    } while (resultslice.length >= 1000);
    return searchResults;
  }

  return {
    getStock: getStock,
    getFakeStock: getFakeStock
  };
})