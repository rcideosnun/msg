/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 ** @author: [Roberto Cideos]
 ** @dated: [23.7.2021]
 ** @Description: Integration between PIMCore and NetSuite
 *
 *  @NApiVersion 2.1
 *  @NScriptType WorkflowActionScript
 */

define(['./av_int_log.js', 'N/search', 'N/record'], function (avLog, search, record) {
    var self = this;
    var CONSTANTS = {
        INTERFACES: {
            "R02": {
                onActionFunction: _createRmaFromStaging,
                connectionRequired: false
            }
        },
        INTERFACE_STATE: {
            NEW: 1,
            ERROR: 2,
            DONE: 3
        },
        ERROR_TYPES: {
            ITEM_NOT_FOUND: "RMA0012",
            INTERNAL_ERROR: "RMA0009",
            ORDER_NOT_EXIST: "RMA0011"
        }
    }

    function onAction(context) {
        var currRec = context.newRecord;
        var iface = self.iface = currRec.getText("custrecord_av_rma_iface");
        self.logId = currRec.getText("custrecord_av_rma_order_log");
        return CONSTANTS.INTERFACES[iface].onActionFunction(context)
    }

    function _createRmaFromStaging(context) {
        try {
            var stagingRec = context.newRecord, anyItemNotFound = false, itemsNotFound = [];
            var orderDetails = _findOrderDetails(stagingRec.getValue("name"));
            if (!orderDetails.id) {
                stagingRec.setValue("custrecord_av_rma_state", CONSTANTS.INTERFACE_STATE.ERROR)
                avLog.error_message({
                    logid: self.logId,
                    rmaStaging: stagingRec.id,
                    msg: CONSTANTS.ERROR_TYPES.ORDER_NOT_EXIST,
                    var1: stagingRec.getValue("name")
                })
                return;
            }

            var rmaRec = _cleanSublist(record.transform({
                fromType: record.Type.SALES_ORDER,
                fromId: orderDetails.id,
                toType: record.Type.RETURN_AUTHORIZATION,
                isDynamic: true
            }))

            rmaRec.setValue("custbody_av_sw_order_number", stagingRec.getValue("name"));

            var stagingLines = stagingRec.getLineCount({
                sublistId: "recmachcustrecord_av_rma_parent"
            })

            var itemS = _getItems(stagingRec);
            var itemIds = itemS.itemIds;

            for (var i = 0; i < stagingLines; i++) {
                var itemId = itemIds[stagingRec.getSublistValue("recmachcustrecord_av_rma_parent", "custrecord_av_rma_l_item", i)];

                if (itemId) {

                    var rmaLine = rmaRec.selectNewLine({
                        sublistId: "item"
                    })

                    rmaLine.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'item',
                        value: itemId
                    });

                    rmaLine.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'quantity',
                        value: stagingRec.getSublistValue("recmachcustrecord_av_rma_parent", "custrecord_av_rma_l_quantity", i)
                    });

                    rmaLine.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'location',
                        value: orderDetails.orderDetails[itemId]
                    });

                    rmaLine.setCurrentSublistText({
                        sublistId: 'item',
                        fieldId: 'custcol_av_return_of_reason',
                        text: stagingRec.getSublistValue("recmachcustrecord_av_rma_parent", "custrecord_av_rma_l_ror", i)
                    });

                    rmaLine.commitLine("item", false)

                } else {
                    itemsNotFound.push(
                        stagingRec.getSublistValue("recmachcustrecord_av_rma_parent", "custrecord_av_rma_l_item", i)
                    )
                    anyItemNotFound = true
                }

            }

            if (anyItemNotFound) {
                stagingRec.setValue("custrecord_av_rma_state", CONSTANTS.INTERFACE_STATE.ERROR)
                avLog.error_message({
                    logid: self.logId,
                    rmaStaging: stagingRec.id,
                    msg: CONSTANTS.ERROR_TYPES.ITEM_NOT_FOUND,
                    var1: itemsNotFound,
                })
                return;
            }

            var rmaId = rmaRec.save({
                enableSourcing: false,
                ignoreMandatoryFields: true
            });

            if (rmaId) {
                stagingRec.setValue("custrecord_av_rma_related_rma", rmaId)
                stagingRec.setValue("custrecord_av_rma_state", CONSTANTS.INTERFACE_STATE.DONE)
            }

        } catch (e) {
            log.error("Error", JSON.stringify(e));
            stagingRec.setValue("custrecord_av_rma_state", CONSTANTS.INTERFACE_STATE.ERROR);
            avLog.error_message({
                logid: self.logId,
                rmaStaging: stagingRec.id,
                msg: CONSTANTS.ERROR_TYPES.INTERNAL_ERROR,
                var1: e.message ? e.message : JSON.stringify(e),
            })
        }

    }

    function _getItems(stagingRec) {

        var itemIds = [], filters = [["isinactive", "is", "F"]], itemIds = {}, itemTypes = {};

        var stagingLines = stagingRec.getLineCount({
            sublistId: "recmachcustrecord_av_rma_parent"
        })

        for (var i = 0; i < stagingLines; i++) {

            try {

                var itemSku = stagingRec.getSublistValue("recmachcustrecord_av_rma_parent", "custrecord_av_rma_l_item", i)

                if (itemSku) {
                    itemIds.push(itemSku)
                }

            } catch (e) {
                log.error("Error", JSON.stringify(e))
            }

        }

        for (var i = 0; i < itemIds.length; i++) {
            if (i == 0) {
                filters.push(["name", "is", itemIds[i]])
            } else {
                filters.push("OR")
                filters.push(["name", "is", itemIds[i]])
            }
        }

        var itemSearchObj = _getAllResults(search.create({
            type: "item",
            filters: filters,
            columns:
                ["itemid", "parent", "type"]
        }));


        itemSearchObj.every(function (result) {

            if (result.getValue("parent")) {

                itemIds[result.getValue("itemid").split(" ")[2]] = result.id;
                itemTypes[result.getValue("itemid").split(" ")[2]] = result.getValue("type");

            } else {
                itemIds[result.getValue("itemid")] = result.id;
                itemTypes[result.getValue("itemid")] = result.getValue("type");
            }

            return true;
        });

        return {
            itemIds: itemIds,
            itemTypes: itemTypes
        }
    }

    function _findOrderDetails(orderNumber) {
        var orderId = '';
        var orderDetails = {};

        if (!orderNumber) return;

        var itemSearchObj = _getAllResults(search.create({
            type: "salesorder",
            filters:
                [
                    ["custbody_av_sw_order_number", "is", orderNumber],
                    "AND",
                    ["taxline", "is", "F"],
                    "AND",
                    ["shipping", "is", "F"],
                ],
            columns: ["item", "location"]
        }));

        itemSearchObj.every(function (result) {
            orderId = result.id;
            orderDetails[result.getValue("item")] = result.getValue("location")
            return true;
        });

        return {
            id: orderId,
            orderDetails: orderDetails
        }
    }

    function _getAllResults(s) {
        var results = s.run();
        var searchResults = [];
        var searchid = 0;
        do {
            var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
            resultslice.forEach(function (slice) {
                searchResults.push(slice);
                searchid++;
            }
            );
        } while (resultslice.length >= 1000);
        return searchResults;
    }

    function _cleanSublist(rmaRec) {
        var itemL = rmaRec.getLineCount("item");

        for (var i = itemL - 1; i >= 0; i--) {

            rmaRec.removeLine({
                sublistId: 'item',
                line: i,
                ignoreRecalc: true
            });

        }

        return rmaRec
    }

    return {
        onAction: onAction
    };
})