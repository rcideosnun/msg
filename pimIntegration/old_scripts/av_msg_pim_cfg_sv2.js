/* **************************************************************************************
 ** Copyright (c) 2020 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 **
 ** @author: Roberto Cideos
 ** @dated: 21.09.2021
 ** @Description: Integration between PIMCore and NetSuite
 **
 ************************************************************************************** */

/**
 *@NApiVersion 2.1
 */

define(["N/search"], function (search) {

    /*var itemObj = {
        "number": itemNumber,
        "displayName": itemDisplayName,
        "ean": itemEan,
        "isActive": itemIsActive,
        "prefix": itemPrefix,
        "netPrice": itemNetPrice,
        "netPriceUVP": itemNetPrice,
        "netPriceMinimal": itemNetPrice,
        "taxRates": {
            "S-DE": "16"
        },
        "productTypeId": itemProductTypeId,
        "departmentIds": [
            itemDepartmentId
        ]
    }*/

    const itemFieldsSchemaGET = {

        number: search.createColumn({
            name: "itemid",
            sort: search.Sort.ASC,
            label: "Name"
        }),
        displayName: "displayname",
        ean: "upccode",
        isActive: "isinactive",
        pimId: "custitem_av_pim_product_id",
        prefix: "custitem_av_pim_item_prefix",
        netPrice: search.createColumn({
            name: "unitprice",
            join: "pricing",
            label: "Unit Price"
        }),
        netPriceUVP: search.createColumn({
            name: "unitprice",
            join: "pricing",
            label: "Unit Price"
        }),
        netPriceMinimal: search.createColumn({
            name: "unitprice",
            join: "pricing",
            label: "Unit Price"
        }),
        productTypeId: "custitem_av_pim_product_type_id",
        deparmentIds: search.createColumn({
            name: "custrecord_av_pim_department_id",
            join: "custitem_av_pim_department_id",
            label: "Department Id"
        })
    };

    const itemFieldsSchemaPUT = {

        displayName: "displayname"

    }

    const itemUrlFieldsSchemaPOST = {
        productNumber: "itemid",
        imageUrl: "custitem_av_item_image_url"
    }

    return {
        itemFields: itemFieldsSchemaGET,
        itemUpdateMapping: itemFieldsSchemaPUT,
        itemUrlUpdateMapping: itemUrlFieldsSchemaPOST
    };
});