/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
** @version: 2.1
** @author: [Roberto Cideos]
** @dated: [23.7.2021]
** @Description: Integration between PIMCore and NetSuite
*
*  @NApiVersion 2.1
*  @NScriptType WorkflowActionScript
*/

// test

define(['./av_msg_lib_sv2',
    'N/https',
    './av_int_log.js',
    './av_msg_item_stock_lib.js',
    'N/runtime',
    'N/search',
    'N/record',
    'N/format'],
    function (settings,
        https,
        avLog,
        stockLib,
        runtime,
        search,
        record,
        format) {

        var self = this;
        var deltaTime, startTime;

        var CONSTANTS = {
            INTERFACES: {
                "P01": {
                    onActionFunction: _sendItemToPim,
                    onActionHttpRequest: "POST",
                    connectionRequired: true
                },
                "P02": {
                    onActionFunction: _sendItemPriceToPim,
                    onActionHttpRequest: "PUT",
                    connectionRequired: true
                },
                "P04": {
                    onActionFunction: _sendItemStockToPim,
                    onActionHttpRequest: "PUT", //PATCH
                    connectionRequired: true
                },
                "I10": {
                    onActionFunction: _sendItemStockSummaryToPim,
                    onActionHttpRequest: "POST",
                    connectionRequired: true
                },
                "I09": {
                    onActionFunction: _sendLocationToPim,
                    onActionHttpRequest: "POST", //PATCH
                    connectionRequired: true
                },
                // "I05": {
                //     onActionFunction: _updateItemToPim,
                //     onActionHttpRequest: "PUT",
                //     connectionRequired: true
                // },
                "I06": {
                    onActionFunction: _sendBrandToPim,
                    onActionHttpRequest: "POST",
                    connectionRequired: true
                },
                "S01": {
                    onActionFunction: _createSalesOrderFromStaging,
                    connectionRequired: false
                }
            },
            HTTP_CODE: {
                SUCCESS_UPDATE: 200,
                SUCCESS_CREATE: 201,
                ERROR: 401
            },
            INTERFACE_STATE: {
                NEW: 1,
                ERROR: 2,
                DONE: 3
            },
            TAX_SCHEDULES_MAPPING: {
                1: "standardRate",
                2: "free"
            },
            PIM_ERROR_TYPES: {
                PIM1_INTERNAL_ERROR: "PIM2",
                PIM2_INTERNAL_ERROR: "PIM3",
                PIM4_INTERNAL_ERROR: "PIM4"
            },
            SHOP_ERRORS: {
            ITEM_NOT_EXIST: 'S10001',
            PM_NOT_EXIST: 'S10002',
            CUSTOMER_INACTIVE: 'S10003',
            CURR_NOT_ASSOCIATED: 'S10004',
            MISSING_EMAIL: 'S10005',
            MISSING_NAME: 'S10006',
            MISSING_PAYMENT_TYPE: 'S10007',
            EX: 'S10008',
            DUPLICATED_ORDER: "S10009"
        },
            ERROR_TYPES: {
                DUPLICATED_ORDER: "SW1",
                ITEM_NOT_FOUND: "SW2",
                INTERNAL_ERROR: "SW3",
                ADDRESS_ERROR: "SW4",
                MISSING_PAYMENT: "SW5",
                MISSING_EMAIL: "SW6",
                MISSING_CUSTOMER: "SW7",
                MISSING_DISPLAY_NAME: "PIM10001",
                MISSING_PARENT: "PIM10003"
            },
            SHOPWARE_PRODUCT_TYPE: {
                PRODUCT: "product",
                PROMOTION: "promotion",
                DISCOUNT: "discount",
                GC_REDEMPTION: "xgx-coupon"
            },
            PRICE_TYPE: {
                BASE_PRICE: 1,
                CUSTOM_PRICE: -1
            },
            DEFAULT_SHIP_METHOD: {
                DHL: 411
            },
            ITEM_TYPE: {
                GIFT_CERTIFICATE_TYPE: "giftcertificateitem",
                GC_NS_TYPE: "GiftCert",
                KIT_ITEM_TYPE: "kititem",
                SERVICE_TYPE: "serviceitem"
            },
            SUBSIDIARY: {
                WEB_STORE_DEFAULT: 3
            },
            EXCHANGE_RATES: {
                "EUR": {
                    "EUR": 1,
                    "CHF": 1.067,
                    "Hungarian Forint": 401
                },
                "CHF": {
                    "EUR": 0.937207,
                    "CHF": 1,
                    "Hungarian Forint": 375.82
                },
                "HUF": {
                    "EUR": 0.002493,
                    "CHF": 0.00266,
                    "Hungarian Forint": 1
                }
            },
            PROCESS_INVOICE: {
                BY_PAYMENT_METHOD: {
                    "Vorkasse": false
                }
            },
            ORDER_STATUS_ENTRY: {
                "Vorkasse": "A",
                "DEFAULT": "B"
            },
            QUARANTINE_LOCATIONS: ["5", "4", "6"],
            FAKE_STOCK: 999999999, //FOR SERVICE ITEMS WE SEND ALWAYS FAKE STOCK
            SHIPPIG_METHODS: {
                CLICK_COLLECT: "Click & Collect"
            },
            CLICK_COLLECT_LOCATIONS: [1, 2]
        }

        /**
         * 
         * @param {Object} context 
         * @returns 
         */

        function onAction(context) {
            try {
                startTime = new Date().getTime()

                if (runtime.envType == "SANDBOX") {
                    //setup sandbox global variables
                    var scriptObj = runtime.getCurrentScript();
                    var CONFIG_ID = scriptObj.getParameter({ name: 'custscript_av_integration_id' }) || scriptObj.getParameter({ name: 'custscript_av_staging_config' }) || null;
                    var iface = self.iface = scriptObj.getParameter({ name: 'custscript_av_item_iface' }) || scriptObj.getParameter({ name: 'custscript_av_tran_iface' });
                    self.gcItem = scriptObj.getParameter({ name: 'custscript_av_msg_item_gc' })

                } else if (runtime.envType == "PRODUCTION") {
                    //setup production global variables
                    var scriptObj = runtime.getCurrentScript();
                    var CONFIG_ID = scriptObj.getParameter({ name: 'custscript_av_integration_id' }) || scriptObj.getParameter({ name: 'custscript_av_staging_config' }) || null;
                    var iface = self.iface = scriptObj.getParameter({ name: 'custscript_av_item_iface' }) || scriptObj.getParameter({ name: 'custscript_av_tran_iface' });
                    self.gcItem = scriptObj.getParameter({ name: 'custscript_av_msg_item_gc' })

                }
                log.debug("Debug", JSON.stringify(self))
                self.connectionObj = new settings(CONFIG_ID, context, CONSTANTS.INTERFACES[iface].connectionRequired);

                return CONSTANTS.INTERFACES[iface].onActionFunction(context)
            } catch (e) {
                log.debug("Error", JSON.stringify(e))
            }
        }

        /**
         * 
         * @param {Object} context 
         * @returns 
         */

        function _sendItemToPim(context) {
            var payload = _getItemDataNew(context)
            log.audit('payload', payload);

            var reqHeaders = {
                "Content-type": "application/json",
                "Accept": 'application/json',
                "Authorization": `Bearer ${self.connectionObj.accessToken}`
            }
        

            var requestUrl = self.connectionObj.custrecord_av_integration_base_url.concat(self.connectionObj.custrecord_av_integration_product_post)
            log.debug("requestUrl", requestUrl)

            var method = CONSTANTS.INTERFACES[self.iface].onActionHttpRequest;
            //payload[0].lastSyncDate ? 'PUT' : CONSTANTS.INTERFACES[self.iface].onActionHttpRequest;
            // delete payload[0].lastSyncDate;

            var logId = avLog.create({
                iface: self.iface,
                send: payload,
                url: requestUrl,
                state: CONSTANTS.INTERFACE_STATE.NEW,
                item: context.newRecord.id
            })

            if(payload && payload.length >0 && !payload[0].displayName) {
                log.debug('Missing Display Name', logId)
                    avLog.update({
                    id: logId,
                    state: CONSTANTS.INTERFACE_STATE.ERROR,
                    });

                    avLog.error_message({
                    logid: logId,
                    msg: CONSTANTS.ERROR_TYPES.MISSING_DISPLAY_NAME,
                    var1: JSON.stringify(payload)
                    });

                    return null;
            } else if(payload && payload.length >0 && payload[0].itemType == CONSTANTS.ITEM_TYPE.KIT_ITEM_TYPE && !payload[0].parentProductNumber) {
                avLog.update({
                    id: logId,
                    state: CONSTANTS.INTERFACE_STATE.ERROR,
                    });

                    avLog.error_message({
                    logid: logId,
                    msg: CONSTANTS.ERROR_TYPES.MISSING_PARENT,
                    var1: JSON.stringify(payload)
                    });
            }
            

            var request = https.request({
                method: method,
                url: requestUrl,
                body: JSON.stringify(payload),
                headers: reqHeaders
            })

            setTimeStamp(context, payload, request, logId);

            deltaTime = (new Date().getTime() - startTime) / 1000

            log.debug("Time Delta", `Send Item Time Usage: ${deltaTime}s`)
        }

        /**
         * 
         * @param {Object} context 
         * @returns 
         */

        function _sendItemPriceToPim(context) {

            var payload = _getItemPrice(context)

            var reqHeaders = {
                "Content-type": "application/json",
                "Authorization": `Bearer ${self.connectionObj.accessToken}`,
                "Accept": 'application/json',
            }

            var requestUrl = self.connectionObj.custrecord_av_integration_base_url.concat(self.connectionObj.custrecord_av_integration_product_put)


            var logId = avLog.create({
                iface: self.iface,
                send: payload,
                url: requestUrl,
                state: CONSTANTS.INTERFACE_STATE.NEW,
                item: context.newRecord.id
            })

            var request = https.request({
                method: CONSTANTS.INTERFACES[self.iface].onActionHttpRequest,
                url: requestUrl,
                body: payload,
                headers: reqHeaders
            })
            log.debug('request ==> P02', request)
            setTimeStamp(context, payload, request, logId);

            deltaTime = (new Date().getTime() - startTime) / 1000

            log.debug("Time Delta", `Update Item Time Usage: ${deltaTime}s`)

        }

        /**
         * 
         * @param {Object} context 
         * @param {Object} payload 
         * @param {Object} response 
         * @param {Object} logId 
         * @returns 
         */
        function setTimeStamp(context, payload, response, logId) {
            const body = JSON.parse(response.body);
            if(body.success && (body.success?.length > 0 || Object.keys(body.success).length > 0)) {
                avLog.update({
                    id: logId,
                    receive: response.body,
                    responseCode: response.code,
                    state: CONSTANTS.INTERFACE_STATE.DONE
                });

                if (context) {
                    const today = new Date();
                    let formattedDate = format.format({
                        value: today,
                        type: format.Type.DATETIME
                    })
                    // This needs to be done because we compare the modified date to this date in order to sync the item.
                    // Since we are updating the record by setting this value, it always changes the last modified date
                    // which will always be higher than this date.
                    // If you are reading this and have a better solution, please recommend it to me (Tehseen Ahmed)
                    const lastIndex = formattedDate.lastIndexOf(':');
                    const timeBeforeSecond = formattedDate.slice(0, lastIndex) + ':59';
                    const parsedDate = format.parse({
                        value: timeBeforeSecond,
                        type: format.Type.DATETIME
                    })
            
                    context.newRecord.setValue('custitem_msg_time_sent_pim', parsedDate);
                    if (!payload.productId) {
                        context.newRecord.setValue("custitem_av_pim_product_id", JSON.parse(response.body).id)
                    }
                    }

            }
            else {
                var message = '';
                switch (self.iface) {
                    case 'P01':
                        message = CONSTANTS.PIM_ERROR_TYPES.PIM1_INTERNAL_ERROR
                        break;
                    case 'P02':
                        message = CONSTANTS.PIM_ERROR_TYPES.PIM2_INTERNAL_ERROR
                        break;
                }
            
                if(body.failed && body.failed?.length > 0) {
                    body.failed.forEach(err => {
                        let concatenatedErrorMsg = `Error for product ${err?.productNumber}: `;
                        if(err.errors && err.errors.length > 0) {
                            concatenatedErrorMsg += err.errors.map(u => u.message).join(', ')
                        }
                        avLog.update({
                            id: logId,
                            responseCode: response.code,
                            state: CONSTANTS.INTERFACE_STATE.ERROR,
                            receive: response.body
                        })
                        avLog.error_message({
                            logid: logId,
                            msg: message,
                            var1: concatenatedErrorMsg
                        })
                    })
                }  else {
                    avLog.update({
                        id: logId,
                        responseCode: response.code,
                        state: CONSTANTS.INTERFACE_STATE.ERROR,
                        receive: response.body
                    })

                    avLog.error_message({
                        logid: logId,
                        msg: message,
                        var1: response.body
                    })
                }
            }
        }

        /**
         * 
         * @param {Object} context 
         * @returns 
         */

        function _sendItemStockToPim(context) {
            var stocks = stockLib.getStock(context.newRecord.id);
            // if (context.newRecord.type != CONSTANTS.ITEM_TYPE.SERVICE_TYPE && context.newRecord.type != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE) {
            //     stockS = _getItemStock(context)
            // } else {
            //     stockS = _getFakeStock(context.newRecord)
            // }

            var reqHeaders = {
                "Content-type": "application/json",
                "Authorization": `Bearer ${self.connectionObj.accessToken}`,
                "Accept": 'application/json',
                // "X-HTTP-Method-Override": "PATCH"
            }
            var payloads = [];
            stocks.forEach(function (stock) {
                var index = payloads.findIndex(function(payload){
                    return payload.productNumber == stock.productNumber;
                });

                if (index > -1) {
                    payloads[index].salesLocations.push({
                        location: stock.location,
                        quantity: stock.quantity
                    });
                } else {
                    payloads.push({
                        productNumber: stock.productNumber,
                        salesLocations: [{
                            location: stock.location,
                            quantity: stock.quantity
                        }]
                    });
                }
            });

            log.debug({ title: '_sendItemStockToPim : payloads', details: payloads });
            var requestUrl = self.connectionObj.custrecord_av_integration_base_url.concat(self.connectionObj.custrecord_av_integration_stock_post)

            var logId = avLog.create({
                iface: self.iface,
                send: payloads,
                url: requestUrl,
                state: CONSTANTS.INTERFACE_STATE.NEW,
                item: context.newRecord.id
            })

            log.debug({ title: '_sendItemStockToPim : reqHeaders', details: reqHeaders });

            var request = https.request({
                method: CONSTANTS.INTERFACES[self.iface].onActionHttpRequest,
                url: requestUrl,
                body: JSON.stringify(payloads),
                headers: reqHeaders
            })

            log.debug({ title: '_sendItemStockToPim : request', details: request });
            
            const body = JSON.parse(request.body);
            if(body.success && (body.success?.length > 0 || Object.keys(body.success).length > 0)) { 
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.DONE,
                    receive: request.body
                })
            } else {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.ERROR,
                    receive: request.body
                })

                if(body.failed && body.failed?.length > 0) {
                    body.failed.forEach(err => {
                        let concatenatedErrorMsg = `Error for product ${err?.productNumber}: `;
                        if(err.errors && err.errors.length > 0) {
                            concatenatedErrorMsg += err.errors.map(u => u.message).join(', ')
                        }
                        avLog.error_message({
                            logid: logId,
                            msg: CONSTANTS.PIM_ERROR_TYPES.PIM4_INTERNAL_ERROR,
                            var1: concatenatedErrorMsg
                        })
                    })
                } else {
                    avLog.error_message({
                        logid: logId,
                        msg:  CONSTANTS.PIM_ERROR_TYPES.PIM4_INTERNAL_ERROR,
                        var1: request.body
                    })
                }
            }
                
        }

        /**
         * 
         * @param {Object} context 
         * @returns 
         */

        function _sendItemStockSummaryToPim(context) {

            var stockQty;

            if (context.newRecord.type != CONSTANTS.ITEM_TYPE.SERVICE_TYPE && context.newRecord.type != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE) {
                stockQty = _getStockSummary(context.newRecord.id);
            } else {
                stockQty = Number(CONSTANTS.FAKE_STOCK);
            }

            var payload = {
                "productId": Number(context.newRecord.getValue("custitem_av_pim_product_id")),
                "stock": Number(stockQty)
            };

            var reqHeaders = {
                "Content-type": "application/json",
                "Authorization": `Bearer ${self.connectionObj.accessToken}`,
                "X-HTTP-Method-Override": "PATCH"
            }

            var requestUrl = self.connectionObj.custrecord_av_integration_base_url.concat(self.connectionObj.custrecord_av_integration_stock_patch)

            var logId = avLog.create({
                iface: self.iface,
                send: JSON.stringify(payload),
                url: requestUrl,
                state: CONSTANTS.INTERFACE_STATE.NEW,
                item: context.newRecord.id
            })

            var request = https.request({
                method: CONSTANTS.INTERFACES[self.iface].onActionHttpRequest,
                url: requestUrl,
                body: JSON.stringify(payload),
                headers: reqHeaders
            })

            if (request.code == CONSTANTS.HTTP_CODE.SUCCESS_UPDATE) {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.DONE,
                    receive: request.body
                })

            } else {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.ERROR,
                    receive: request.body
                })

                avLog.error_message({
                    logid: logId,
                    var1: request.body
                })
            }

        }


        /**
         * 
         * @param {Object} context 
         * @returns 
         */

        function _createSalesOrderFromStaging(context) {

        try {
            var stagingRec = context.newRecord, anyItemNotFound = false, itemsNotFound = [], hasGc = false, gcCodesIds = [], gcCodesRedeemed = [], soId;
            if (!stagingRec.getValue("custrecord_av_sw_order_cx_email")) {
                stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
                avLog.error_message({
                    msg: CONSTANTS.SHOP_ERRORS.MISSING_EMAIL,
                    staging: stagingRec.id,
                    iface: stagingRec.getValue("custrecord_av_sw_order_iface"),
                    logid: stagingRec.getValue("custrecord_av_sw_order_log")
                });
                return;
                }
        
                if (!stagingRec.getValue("custrecord_av_sw_order_cx_fn")) {
                stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
                avLog.error_message({
                    msg: CONSTANTS.SHOP_ERRORS.MISSING_NAME,
                    staging: stagingRec.id,
                    iface: stagingRec.getValue("custrecord_av_sw_order_iface"),
                    logid: stagingRec.getValue("custrecord_av_sw_order_log")
                });
                return;
                }
        
                if (!stagingRec.getValue("custrecord_av_sw_order_payment_method")) {
                stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
                avLog.error_message({
                    msg: CONSTANTS.SHOP_ERRORS.MISSING_PAYMENT_TYPE,
                    staging: stagingRec.id,
                    iface: stagingRec.getValue("custrecord_av_sw_order_iface"),
                    logid: stagingRec.getValue("custrecord_av_sw_order_log")
                });
                return;
                }
            
            var orderCurrency = stagingRec.getValue("custrecord_av_sw_order_currency");
            var giftCertsS = _getGiftCertificates();
            var isOrderNew = _checkIfOrderIsNew(stagingRec.getValue("custrecord_av_sw_order_number"));
            var paymentMethod = stagingRec.getText("custrecord_av_sw_order_payment_method");
            var currencyS = _lookUpCurrencybyIso(stagingRec.getValue("custrecord_av_sw_order_currency"));

            if (!isOrderNew) {
                stagingRec.setValue("custrecord_av_sw_ns_transaction", self.oldSalesOrderId)
                stagingRec.setValue("custrecord_av_sw_order_cx_entity", self.entitiyId)
                stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.DONE)

                avLog.error_message({
                    iface: stagingRec.getValue("custrecord_av_sw_order_iface"),
                    logid: stagingRec.getValue("custrecord_av_sw_order_log"),
                    staging: stagingRec.id,
                    msg: CONSTANTS.SHOP_ERRORS.DUPLICATED_ORDER,
                    var1: self.oldSalesOrderId
                })

                return;
            }

            var entitiyId = stagingRec.getValue("custrecord_av_sw_order_cx_entity") ? stagingRec.getValue("custrecord_av_sw_order_cx_entity") : _findEntity(stagingRec, currencyS.currenciesIds)
            stagingRec.setValue("custrecord_av_sw_order_cx_entity", entitiyId)
            if (!entitiyId) return stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)

            var addressS = _getAddresses(entitiyId);
            var shipAddressId = _getAddressId(addressS, stagingRec, entitiyId, 'custrecord_av_sw_order_ship_address');
            var billAddressId = _getAddressId(addressS, stagingRec, entitiyId, 'custrecord_av_sw_order_bill_address');

            var soRec = record.transform({
                fromType: record.Type.CUSTOMER,
                fromId: entitiyId,
                toType: record.Type.SALES_ORDER,
                isDynamic: true
            });

            if (shipAddressId) {
                soRec.setValue("shipaddresslist", shipAddressId);
            }
            if (billAddressId) {
                soRec.setValue("billaddresslist", billAddressId);
            }

            soRec = _setMainFields(soRec, stagingRec, currencyS);

            if (CONSTANTS.ORDER_STATUS_ENTRY[paymentMethod]) {
                soRec.setValue("orderstatus", CONSTANTS.ORDER_STATUS_ENTRY[paymentMethod])
            } else {
                soRec.setValue("orderstatus", CONSTANTS.ORDER_STATUS_ENTRY.DEFAULT)
            }

            var stagingLines = stagingRec.getLineCount({
                sublistId: "recmachcustrecord_av_sw_order"
            })

            var itemS = _getItems(stagingRec);
            var itemIds = itemS.itemIds;
            var nsItemType = itemS.itemTypes;
            var itemsById = itemS.itemById;

            var itemLocS = itemIds && itemIds.length ? _getLocationsByItem(itemIds) : {};
            for (var i = 0; i < stagingLines; i++) {

                try {

                    var itemId = itemIds[stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_item", i)];
                    var gcCode = stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_item", i);
                    var pimProdType = stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_type", i);
                    var nsProdType = nsItemType[stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_item", i)];
                    var itemTaxRate = Number(stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_taxrate", i));
                    var grossAmt = Number(stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_total_price", i));
                    var itemRate = Number(stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_unit_price", i));
                    var qty = stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_qty", i);

                    if (itemId && nsProdType != CONSTANTS.ITEM_TYPE.GC_NS_TYPE) {

                        var soLine = soRec.selectNewLine({
                            sublistId: "item"
                        })

                        soLine.setCurrentSublistValue({
                            sublistId: 'item',
                            fieldId: 'item',
                            value: itemId
                        });

                        if (itemLocS[itemId]) {

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'location',
                                value: itemLocS[itemId].locId
                            });

                        }

                        soLine.setCurrentSublistValue({
                            sublistId: 'item',
                            fieldId: 'quantity',
                            value: qty
                        });

                        if (soLine.getCurrentSublistValue({
                            sublistId: 'item',
                            fieldId: 'itemtype'
                        }) == CONSTANTS.ITEM_TYPE.GC_NS_TYPE) {

                            var gcQ = parseInt(stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_qty", i))

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'giftcertfrom',
                                value: stagingRec.getValue("custrecord_av_sw_order_cx_fn")
                            });

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'giftcertrecipientname',
                                value: stagingRec.getValue("custrecord_av_sw_order_cx_fn")
                            });

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'giftcertrecipientemail',
                                value: stagingRec.getValue("custrecord_av_sw_order_cx_email")
                            });

                        }

                        soLine.setCurrentSublistValue({
                            sublistId: 'item',
                            fieldId: 'price',
                            value: CONSTANTS.PRICE_TYPE.CUSTOM_PRICE
                        });

                        soLine.setCurrentSublistValue({
                            sublistId: 'item',
                            fieldId: 'rate',
                            value: itemRate
                        });

                        soLine.setCurrentSublistValue({
                            sublistId: 'item',
                            fieldId: 'amount',
                            value: grossAmt
                        });

                        soLine.setCurrentSublistValue({
                            sublistId: 'item',
                            fieldId: 'taxrate1',
                            value: itemTaxRate
                        });

                        soLine.commitLine("item")

                        // if (itemNetAmt < grossAmt) {
                        //     soLine.setCurrentSublistValue({
                        //         sublistId: 'item',
                        //         fieldId: 'rate',
                        //         value: (itemNetAmt / qty).toFixed(2)
                        //     });
                        // } else {
                        //     if (itemTaxRate) {
                        //         let itemRate = (grossAmt / qty) * (1 - itemTaxRate / 100).toFixed(2);
                        //         soLine.setCurrentSublistValue({
                        //             sublistId: 'item',
                        //             fieldId: 'rate',
                        //             value: itemRate
                        //         });
                        //     } else {
                        //         soLine.setCurrentSublistValue({
                        //             sublistId: 'item',
                        //             fieldId: 'rate',
                        //             value: (grossAmt / qty).toFixed(2)
                        //         });
                        //     }
                        // }

                        

                    // } else if (pimProdType == CONSTANTS.SHOPWARE_PRODUCT_TYPE.DISCOUNT || pimProdType == CONSTANTS.SHOPWARE_PRODUCT_TYPE.PROMOTION) {

                    //     var soLine = soRec.selectNewLine({
                    //         sublistId: "item"
                    //     })

                    //     soLine.setCurrentSublistValue({
                    //         sublistId: 'item',
                    //         fieldId: 'item',
                    //         value: self.connectionObj.discountItem
                    //     });

                    //     soLine.setCurrentSublistValue({
                    //         sublistId: 'item',
                    //         fieldId: 'price',
                    //         value: CONSTANTS.PRICE_TYPE.CUSTOM_PRICE
                    //     });

                    //     soLine.setCurrentSublistValue({
                    //         sublistId: 'item',
                    //         fieldId: 'taxrate1',
                    //         value: itemTaxRate
                    //     });

                    //     soLine.setCurrentSublistValue({
                    //         sublistId: 'item',
                    //         fieldId: 'grossamt',
                    //         value: grossAmt
                    //     });

                    //     soLine.commitLine("item", false)

                    // } else if (itemId && pimProdType == CONSTANTS.SHOPWARE_PRODUCT_TYPE.PRODUCT && nsProdType == CONSTANTS.ITEM_TYPE.GC_NS_TYPE) {

                    //     var reqQ = Number(stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_qty", i))

                    //     for (var j = 0; j < reqQ; j++) {

                    //         var soLine = soRec.selectNewLine({
                    //             sublistId: "item"
                    //         })

                    //         soLine.setCurrentSublistValue({
                    //             sublistId: 'item',
                    //             fieldId: 'item',
                    //             value: itemId
                    //         });

                    //         if (itemLocS[itemId]) {

                    //             soLine.setCurrentSublistValue({
                    //                 sublistId: 'item',
                    //                 fieldId: 'location',
                    //                 value: itemLocS[itemId].locId
                    //             });

                    //         }

                    //         soLine.setCurrentSublistValue({
                    //             sublistId: 'item',
                    //             fieldId: 'quantity',
                    //             value: stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_qty", i)
                    //         });

                    //         soLine.setCurrentSublistValue({
                    //             sublistId: 'item',
                    //             fieldId: 'taxrate1',
                    //             value: itemTaxRate
                    //         })

                    //         soLine.setCurrentSublistValue({
                    //             sublistId: 'item',
                    //             fieldId: 'grossamt',
                    //             value: grossAmt
                    //         });

                    //         soLine.setCurrentSublistValue({
                    //             sublistId: 'item',
                    //             fieldId: 'giftcertfrom',
                    //             value: stagingRec.getValue("custrecord_av_sw_order_cx_fn")
                    //         });

                    //         soLine.setCurrentSublistValue({
                    //             sublistId: 'item',
                    //             fieldId: 'giftcertrecipientname',
                    //             value: stagingRec.getValue("custrecord_av_sw_order_cx_fn")
                    //         });

                    //         soLine.setCurrentSublistValue({
                    //             sublistId: 'item',
                    //             fieldId: 'giftcertrecipientemail',
                    //             value: stagingRec.getValue("custrecord_av_sw_order_cx_email")
                    //         });

                    //         soLine.commitLine("item", false)
                    //     }

                    //     hasGc = true;

                    // } else if (gcCode && pimProdType == CONSTANTS.SHOPWARE_PRODUCT_TYPE.GC_REDEMPTION) {
                    //     var soLine = soRec.selectNewLine({
                    //         sublistId: "item"
                    //     })

                    //     soLine.setCurrentSublistValue({
                    //         sublistId: 'item',
                    //         fieldId: 'item',
                    //         value: self.gcItem
                    //     });

                    //     soLine.setCurrentSublistValue({
                    //         sublistId: 'item',
                    //         fieldId: 'price',
                    //         value: CONSTANTS.PRICE_TYPE.CUSTOM_PRICE
                    //     });

                    //     soLine.setCurrentSublistValue({
                    //         sublistId: 'item',
                    //         fieldId: 'description',
                    //         value: gcCode
                    //     });

                    //     soLine.setCurrentSublistValue({
                    //         sublistId: 'item',
                    //         fieldId: 'grossamt',
                    //         value: -Number(grossAmt)
                    //     });

                    //     soLine.commitLine("item", false);

                    //     gcCodesRedeemed.push({
                    //         code: gcCode,
                    //         amtUsed: grossAmt
                    //     });
                    //     gcCodesIds.push(gcCode);
                    //     hasGc = true;
                    // } else {
                    } else {
                        itemsNotFound.push(
                            stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_item", i)
                        )
                        anyItemNotFound = true;
                        continue;
                    }

                } catch (e) {
                    log.debug("Error", JSON.stringify(e))
                }

            }

            if (anyItemNotFound) {

                stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)

                avLog.error_message({
                    staging: stagingRec.id,
                    msg: CONSTANTS.SHOP_ERRORS.ITEM_NOT_EXIST,
                    var1: itemsNotFound,
                    iface: stagingRec.getValue("custrecord_av_sw_order_iface"),
                    logid: stagingRec.getValue("custrecord_av_sw_order_log")
                })

                return;
            }

            soId = soRec.save({
                enableSourcing: true,
                ignoreMandatoryFields: true
            });

            stagingRec.setValue("custrecord_av_sw_ns_transaction", soId)
            stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.DONE)

            if (hasGc && soId) {

                soRec = record.load({
                    type: "salesorder",
                    id: soId
                })
                let gcCodesIdsResave = [];

                if (gcCodesIds.length) {
                    _updateGcCustom(gcCodesRedeemed, giftCertsS, orderCurrency);
                }
                var gcLines = soRec.getLineCount({
                    sublistId: "item"
                })

                for (var i = 0; i < gcLines; i++) {

                    gcCodesIdsResave.push({
                        id: soRec.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'giftcertkey',
                            line: i
                        })
                    });

                    soRec.setSublistValue({
                        sublistId: 'item',
                        fieldId: 'description',
                        value: soRec.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'giftcertnumber',
                            line: i
                        }),
                        line: i
                    })

                    //giftcertnumber

                }

                if (gcCodesIdsResave.length) {
                    _updateGcCrs(gcCodesIdsResave)
                }

                if (CONSTANTS.PROCESS_INVOICE.BY_PAYMENT_METHOD[paymentMethod] === false) return;

                soRec.save({
                    enableSourcing: false,
                    ignoreMandatoryFields: true
                })

                var invRec = record.transform({
                    fromType: "salesorder",
                    fromId: soId,
                    toType: "invoice",
                    isDynamic: true
                })

                var invLineCount = invRec.getLineCount({
                    sublistId: "item"
                })

                for (var i = 0; i < invLineCount; i++) {

                    let hasGc = invRec.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'giftcertkey',
                        line: i
                    });

                    if (!hasGc) {
                        invRec.removeLine({
                            sublistId: "item",
                            line: i
                        })
                    }

                }

                invRec.save.promise().then(function (response) {
                    stagingRec.setValue("custrecord_av_sw_ns_inv", response)
                    log.debug("Invoice Id", JSON.stringify(response))
                }, function (error) {
                    log.debug("Error creating invoice", JSON.stringify(error))
                });

            }

        } catch (e) {
            stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
            avLog.error_message({
                staging: stagingRec.id,
                msg: CONSTANTS.SHOP_ERRORS.EX,
                var1: JSON.stringify(e),
                iface: stagingRec.getValue("custrecord_av_sw_order_iface"),
                logid: stagingRec.getValue("custrecord_av_sw_order_log")
            })
            log.debug("Error", JSON.stringify(e))
        }

    }

        /**
         * 
         * @param {SuiteScript Record Object} soRec 
         * @param {SuiteScript Record Object} stagingRec 
         * @param {Object} currencyS 
         * @returns 
         */

        function _setMainFields(soRec, stagingRec, currencyS) {
        var date = new Date(stagingRec.getValue("custrecord_av_sw_order_date_time"));
        var parsedDate = format.parse({
            value: date,
            type: format.Type.DATE
        })
        
        soRec.setValue("trandate", parsedDate);
        var shipMethod = _pullShipMethod(stagingRec.getValue("custrecord_av_sw_order_ship_method"));
        soRec.setValue("custbody_av_sw_order_number", stagingRec.getValue("custrecord_av_sw_order_number"));
        soRec.setValue("custbody_av_payment_method", stagingRec.getValue("custrecord_av_sw_order_payment_method"));
        soRec.setText("custbody_av_msg_payment_method", stagingRec.getValue("custrecord_av_sw_order_payment_method"));
        if (shipMethod) {
            soRec.setText("shipmethod", shipMethod);
        }
        soRec.setValue("shippingcost", _getShipCost(stagingRec));
        soRec.setText("currency", currencyS.orderCurrencyId);
        //soRec.setValue("custbody_av_sales_channel_id", stagingRec.getValue("custrecord_av_sw_channel_id"));
        soRec.setValue("custbody_av_sw_order_id", stagingRec.getValue("custrecord_av_sw_order_id"));
        var salesChannel = stagingRec.getValue("custrecord_av_sw_channel");
        if (salesChannel) {
            soRec.setValue("cseg_msg_sales_chan", salesChannel);
        }

        if (shipMethod == CONSTANTS.SHIPPIG_METHODS.CLICK_COLLECT) {
            if (_isShippingAddressCc(stagingRec, soRec)) {
                soRec.setValue("custbody_av_click_and_collect", true);
            }
        }

        return soRec
    }

        /**
         * 
         * @param {SuiteScript Record Object} stagingRec 
         */

        function _isShippingAddressCc(stagingRec, soRec) {
            let isCc = false;

            try {
                let orderAddresses = JSON.parse(stagingRec.getValue("custrecord_av_sw_order_ship_address"));
                let locData = _getLocationsAddress(CONSTANTS.CLICK_COLLECT_LOCATIONS);

                for (locAddress of locData) {
                    for (orderAddress in orderAddresses) {
                        if (_removeSpecialAddressChars(locAddress.zipCode) == _removeSpecialAddressChars(orderAddresses[orderAddress].zip) &&
                            _removeSpecialAddressChars(locAddress.street) == _removeNumChars(_removeSpecialAddressChars(orderAddresses[orderAddress].street)) &&
                            _removeSpecialAddressChars(locAddress.city) == _removeSpecialAddressChars(orderAddresses[orderAddress].city)) {
                            soRec.setValue("location", locAddress.id)
                            isCc = true
                        }
                    }
                }
            } catch (e) {
                log.debug("Debug", JSON.stringify(e))
            }

            return isCc
        }

        /**
         * 
         * @param {String} str 
         * @returns 
         */

        function _removeSpecialAddressChars(str) {
            return str.replace(/[^A-Z0-9]/ig, "")
        }

        /**
         * 
         * @param {String} str 
         * @returns 
         */

        function _removeNumChars(str) {
            return str.replace(/[^A-Z]/ig, "")
        }

        /**
         * 
         * @param {String} eid 
         * @returns 
         */

        function _getAddresses(eid) {
            var addresses = [];

            _getAllResults(search.create({
                type: "customer",
                filters:
                    [
                        ["isinactive", "is", "F"],
                        "AND",
                        ["internalid", "anyof", eid]
                    ],
                columns:
                    [
                        search.createColumn({
                            name: "zipcode",
                            join: "Address",
                            label: "Zip Code"
                        }),
                        search.createColumn({
                            name: "address1",
                            join: "Address",
                            label: "Address Line 1"
                        }),
                        search.createColumn({
                            name: "addressinternalid",
                            join: "Address",
                            label: "Address Internal ID"
                        })
                    ]
            })).every(function (result) {
                addresses.push({
                    id: result.getValue({
                        name: "addressinternalid",
                        join: "Address"
                    }),
                    zipcode: result.getValue({
                        name: "zipcode",
                        join: "Address"
                    }),
                    address1: result.getValue({
                        name: "address1",
                        join: "Address"
                    }),
                });

                return true
            });

            return addresses
        }

        /**
         * 
         * @param {Array} adresses 
         * @param {SuiteScript Record Object} stagingRec 
         * @param {String} eid 
         * @returns 
         */
        function _getAddressId (adresses, stagingRec, eid, fieldId) {
            var orderAddress = stagingRec.getValue(fieldId) ? JSON.parse(stagingRec.getValue(fieldId)) : null;
            var addressId = '';
            if (adresses.length) {
                adresses.forEach( adress => {
                    if (adress.zipcode == orderAddress.zipcode && adress.address1 == orderAddress.street) {
                        addressId = adress.id;
                    }
                });
            }
            
            if (addressId) return addressId;

            var customerRec = record.load({
                type: "customer",
                id: eid,
                isDynamic: true
            })

            customerRec.selectNewLine({
                sublistId: 'addressbook'
            });

            var addressSubrecord = customerRec.getCurrentSublistSubrecord({
                sublistId: 'addressbook',
                fieldId: 'addressbookaddress'
            });

            addressSubrecord.setValue({
                fieldId: 'addr1',
                value: orderAddress.street,
            });

            addressSubrecord.setValue({
                fieldId: 'country',
                value: orderAddress.country.iso,
            });

            addressSubrecord.setValue({
                fieldId: 'zip',
                value: orderAddress.zip,
            });

            addressSubrecord.setValue({
                fieldId: 'addr2',
                value: orderAddress.additionalAddressLine1,
            });

            addressSubrecord.setValue({
                fieldId: 'city',
                value: orderAddress.city,
            });

            addressSubrecord.setValue({
                fieldId: 'custrecord_av_shopware_address_id',
                value: orderAddress.swid,
            });

            addressSubrecord.setValue({
                fieldId: 'attention',
                value: orderAddress.attention || "",
            });

            addressSubrecord.setValue({
                fieldId: 'defaultshipping',
                value: true
            });

            customerRec.commitLine({
                sublistId: 'addressbook'
            });

            customerRec.save();

            var updatedAddresses = _getAddresses(eid);

            updatedAddresses.forEach( adress => {
                if (adress.zipcode == orderAddress.zipcode && adress.address1 == orderAddress.street) {
                    addressId = adress.id;
                }
            });

            return addressId;
        }

        function _getGiftCertificates() {
            let gcByCode = {};
            search.create({
                type: "customrecord_av_cs_gc",
                filters: [],
                columns: ["custrecord_av_cs_gc_code", "custrecord_av_cs_gc_rem_amt"]
            }).run().each(function (result) {

                gcByCode[String(result.getValue("custrecord_av_cs_gc_code"))] = {
                    id: result.id,
                    amt: result.getValue("custrecord_av_cs_gc_rem_amt")
                }

                return true
            })

            return gcByCode
        }

        /**
         * 
         * @param {Array} gcCodes 
         * @param {Object} giftCertsS 
         * @param {String} orderCurrency 
         * @returns 
         */

        function _updateGcCustom(gcCodes, giftCertsS, orderCurrency) {

            for (let i = 0; i < gcCodes.length; i++) {

                let cusGcRec = record.load({
                    type: "customrecord_av_cs_gc",
                    id: giftCertsS[gcCodes[i].code].id,
                    isDynamic: true
                })

                let baseCurrency = cusGcRec.getText("custrecord_av_cs_gc_curr");

                let remAmt = Number(cusGcRec.getValue("custrecord_av_cs_gc_rem_amt"));

                let amtUsedToBase = CONSTANTS.EXCHANGE_RATES[orderCurrency][baseCurrency] * gcCodes[i].amtUsed;

                cusGcRec.setValue({
                    fieldId: "custrecord_av_cs_gc_rem_amt",
                    value: remAmt + amtUsedToBase
                });

                let gcLines = cusGcRec.getLineCount({
                    sublistId: "recmachcustrecord_av_parent"
                })

                for (let j = 0; j < gcLines; j++) {

                    let lineCurrency = cusGcRec.getSublistText({
                        sublistId: "recmachcustrecord_av_parent",
                        fieldId: "custrecord_av_gc_currency",
                        line: j
                    });

                    let oldRemAmt = cusGcRec.getSublistValue({
                        sublistId: "recmachcustrecord_av_parent",
                        fieldId: "custrecord_av_gc_remaining_amount",
                        line: j
                    });//from lineCurrency to base

                    let newRemAmt = CONSTANTS.EXCHANGE_RATES[baseCurrency][lineCurrency] * Number(remAmt + amtUsedToBase);//

                    cusGcRec.selectLine({
                        sublistId: "recmachcustrecord_av_parent",
                        line: j
                    });

                    cusGcRec.setCurrentSublistValue({
                        sublistId: "recmachcustrecord_av_parent",
                        fieldId: "custrecord_av_gc_remaining_amount",
                        value: newRemAmt // TODO: Calculate the amount based on the currency used by the sales order
                    });

                    cusGcRec.commitLine({
                        sublistId: "recmachcustrecord_av_parent"
                    });
                }

                cusGcRec.save();
            }

        }

        /**
         * 
         * @param {Array} gcCodes 
         */

        function _updateGcCrs(gcCodes) {

            for (gcC in gcCodes) {

                if (gcCodes[gcC].id)
                    record.load({
                        type: "giftcertificate",
                        id: gcCodes[gcC].id
                    }).save()

            }

        }

        /**
         * 
         * @param {Number} shipMethodVal 
         * @returns 
         */

        function _pullShipMethod(shipMethodVal) {

            if (!shipMethodVal) return;

            var shipArr = JSON.parse(shipMethodVal)

            if (!shipArr.length) return;

            return shipArr[0].name
        }

        /**
         * 
         * @param {Object} context 
         * @returns 
         */

        function _sendBrandToPim(context) {

            var payload = _getBrand(context)

            var reqHeaders = {
                "Content-type": "application/json",
                "Authorization": `Bearer ${self.connectionObj.accessToken}`
            }

            var requestUrl = self.connectionObj.custrecord_av_integration_base_url.concat(self.connectionObj.custrecord_av_integration_brand_post)

            var logId = avLog.create({
                iface: self.iface,
                send: payload,
                url: requestUrl,
                state: CONSTANTS.INTERFACE_STATE.NEW,
                brand: context.newRecord.id
            })

            var request = https.request({
                method: CONSTANTS.INTERFACES[self.iface].onActionHttpRequest,
                url: requestUrl,
                body: payload,
                headers: reqHeaders
            })

            if (request.code == CONSTANTS.HTTP_CODE.SUCCESS_CREATE) {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.DONE,
                    receive: request.body
                })

                context.newRecord.setValue("custitem_av_vendor_sent_to_pim", true)

            } else {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.ERROR,
                    receive: request.body
                })

                avLog.error_message({
                    logid: logId,
                    var1: request.body
                })
            }

        }

        /**
         * 
         * @param {Object} context 
         * @returns 
         */

        function _getBrand(context) {

            var currRec = context.newRecord

            var vId = currRec.id;
            var dN = currRec.getValue("name");

            var vendorObj = {
                "id": vId,
                "displayName": dN
            }

            return JSON.stringify(vendorObj)
        }

        /**
         * 
         * @param {Object} context 
         * @returns 
         */

        function _getItemDataNew(context) {
            try{
            var currRec = context.newRecord;
            var itemType = currRec.type;
            var itemsById = _getItemsById();
            var itemNumber = currRec.getValue("itemid");
            var itemDisplayName = currRec.getValue("displayname");
            var itemEan = currRec.getValue("upccode");
            var itemIsActive = !currRec.getValue("custitem_is_shop_online");
            var itemHeight = parseInt(currRec.getValue("custitem_msg_height"));
            var itemWidth = parseInt(currRec.getValue("custitem_msg_width"));
            var itemLength = parseInt(currRec.getValue("custitem_msg_length"));
            var itemWeight = currRec.getValue("weight") ? parseFloat(currRec.getValue("weight")) : null;
            var itemBrand = currRec.getText("cseg_msg_brands");
            var itemProductType = currRec.getText("custitem_msg_product_type");
            var itemUNnumber = currRec.getText("custitem_msg_un_number");
            let itemParent = _getParent(currRec.getValue("parent")) || currRec.getValue("custitem_msg_main_product_kit");
            var itemComponents = _getItemComponents(currRec, itemsById);
            var subsidiaryKeys = currRec.getText("subsidiary");
            subsidiaryKeys = subsidiaryKeys.map(function (subsidiary) {
                return subsidiary.indexOf(' : ') > -1 ? subsidiary.split(' : ').pop() : subsidiary;
            })
            var itemGroupType = currRec.getText("cseg_msg_igt");
            var taxScheduleKey = currRec.getText("taxschedule");
            var brandProductNumber = currRec.getValue("mpn");
            var refUnit = currRec.getValue("custitem_av_ref_unit");
            var purchaseUnit = currRec.getValue("custitem_av_purchase_unit");
            var unitKey = currRec.getText("custitem_av_unit_key");
            var lastSyncDate = currRec.getValue("custitem_msg_time_sent_pim");

            if (itemType == CONSTANTS.ITEM_TYPE.KIT_ITEM_TYPE) {
                itemParent = itemsById.itemById[Number(currRec.getValue("custitem_msg_main_product_kit"))];
            }

            var itemObj = [{
                "productNumber": itemNumber,
                "displayName": itemDisplayName || itemNumber,
                "ean": itemEan || "1",
                "subsidiaryKeys": typeof subsidiaryKeys == 'string' ? [subsidiaryKeys] : subsidiaryKeys,
                "itemGroupType": itemGroupType,
                "brand": _setNullAsDef(itemBrand),
                "taxScheduleKey": taxScheduleKey,
                "isActive": itemIsActive,
                "brandProductNumber": _setNullAsDef(brandProductNumber),
                "hazmatTypeKeys": _setNullAsDef(itemUNnumber),
                "height": _setNullAsDef(itemHeight),
                "width": _setNullAsDef(itemWidth),
                "length": _setNullAsDef(itemLength),
                "weight": _setNullAsDef(itemWeight),
                "refUnit": _setNullAsDef(refUnit),
                "purchaseUnit": _setNullAsDef(purchaseUnit),
                "unitKey": _setNullAsDef(unitKey),
                "productType": itemProductType || '',
                "parentProductNumber": _setNullAsDef(itemParent),
                "comboProductNumbers": _setNullAsDef(itemComponents),
                "lastSyncDate": _setNullAsDef(lastSyncDate),
                "itemType": itemType
            }]

            return itemObj
        } catch(e) {
            log.audit('error', e);
        }
        }

        /**
         * 
         * @param {Object} context 
         * @returns 
         */

        function _getItemPrice(context) {

            var currRec = context.newRecord;
            var itemType = currRec.type;

            var itemNumber = currRec.getValue("itemid");
            var pimItemId = Number(currRec.getValue("custitem_av_pim_product_id"));
            var today = format.format({
                value: new Date(),
                type: format.Type.DATE
            })

            var lineCount = currRec.getLineCount({
                sublistId: 'recmachcustrecord_msg_ip_item'
            });

            var applicablePrices = [
                // {
                //     dateFrom: '1.4.2022',
                //     dateTo: ''
                // },
                // {
                //     dateFrom: '1.4.2022',
                //     dateTo: '1.2.2023'
                // },
                // {
                //     dateFrom: '1.4.2022',
                //     dateTo: '1.11.2022'
                // },
                // {
                //     dateFrom: '1.4.2023',
                //     dateTo: '1.11.2023'
                // },
                // {
                //     dateFrom: '20.12.2022',
                //     dateTo: '15.1.2023'
                // }
            ]

            for (var i = 0; i < lineCount; i++) {
                var dateFrom = currRec.getSublistValue({
                    sublistId: 'recmachcustrecord_msg_ip_item',
                    fieldId: 'custrecord_msg_ip_date_from',
                    line: i
                })
                var dateTo = currRec.getSublistValue({
                    sublistId: 'recmachcustrecord_msg_ip_item',
                    fieldId: 'custrecord_msg_ip_date_to',
                    line: i
                })

                dateFrom = dateFrom ? format.format({
                    value: dateFrom,
                    type: format.Type.DATE
                }) : dateFrom;

                dateTo = dateTo ? format.format({
                    value: dateTo,
                    type: format.Type.DATE
                }) : dateTo;

                if (today >= dateFrom && (!dateTo || today <= dateTo)) {
                    applicablePrices.push({
                        basePrice: currRec.getSublistValue({
                            sublistId: 'recmachcustrecord_msg_ip_item',
                            fieldId: 'custrecord_msg_ip_base',
                            line: i
                        }),
                        b2bPrice: currRec.getSublistValue({
                            sublistId: 'recmachcustrecord_msg_ip_item',
                            fieldId: 'custrecord_msg_ip_b2b',
                            line: i
                        }),
                        srpPrice: currRec.getSublistValue({
                            sublistId: 'recmachcustrecord_msg_ip_item',
                            fieldId: 'custrecord_msg_ip_srp',
                            line: i
                        }),
                        minimalPrice: currRec.getSublistValue({
                            sublistId: 'recmachcustrecord_msg_ip_item',
                            fieldId: 'custrecord_msg_ip_minimal',
                            line: i
                        }),
                        dateFrom: dateFrom,
                        dateTo: dateTo
                    });
                }
            }

            var priceObject = {};
            applicablePrices.forEach(function (price) {
                applicablePrices.forEach(function (price2) {
                    if (price.dateFrom >= price2.dateFrom && (!price.dateTo || price.dateTo <= price2.dateTo)) {
                        priceObject = price;
                    }
                })
            })

            var basePrice = itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(priceObject.basePrice) : _getPriceGiftCert(currRec);
            var b2bPrice = itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(priceObject.b2bPrice) : _getPriceGiftCert(currRec);
            var srpPrice = itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(priceObject.srpPrice) : _getPriceGiftCert(currRec);
            var minimalPrice = itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(priceObject.minimalPrice) : _getPriceGiftCert(currRec);
            var contributionMarign = parseFloat(currRec.getValue('custitem_av_item_contributionmargin'));
            log.audit('contributionMarign', contributionMarign)
            var itemObj = [{
                "productNumber": itemNumber,
                "netBasePrice": basePrice,
                "netB2BPrice": _setNullAsDef(b2bPrice),
                "netSRPPrice": _setNullAsDef(srpPrice),
                "netMinimalPrice": _setNullAsDef(minimalPrice),
                "contributionMargin": _setNullAsDef(contributionMarign)
            }]

            log.debug({ title: "itemObj", details: itemObj })

            return JSON.stringify(itemObj)
        }

        /**
         * 
         * @param {Object} context 
         * @returns 
         */

        function _sendLocationToPim(context) {

            var payload = _getLocationData(context.newRecord.id)

            var reqHeaders = {
                "Content-type": "application/json",
                "Authorization": `Bearer ${self.connectionObj.accessToken}`
            }

            var requestUrl = self.connectionObj.custrecord_av_integration_base_url.concat(self.connectionObj.custrecord_av_integration_store_post)

            var logId = avLog.create({
                iface: self.iface,
                send: payload,
                url: requestUrl,
                state: CONSTANTS.INTERFACE_STATE.NEW
            })

            var request = https.request({
                method: CONSTANTS.INTERFACES[self.iface].onActionHttpRequest,
                url: requestUrl,
                body: payload,
                headers: reqHeaders
            })

            if (request.code == CONSTANTS.HTTP_CODE.SUCCESS_CREATE) {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.DONE,
                    receive: request.body
                })
                context.newRecord.setValue("custrecord_av_msg_pim_location_id", JSON.parse(request.body).id)
                context.newRecord.setValue("custitem_av_item_sent_to_pim", true)
                context.newRecord.setValue("custitem_av_pim_product_id", JSON.parse(request.body).id)

            } else {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.ERROR,
                    receive: request.body
                })

                avLog.error_message({
                    logid: logId,
                    msg: CONSTANTS.ERROR_TYPES.INTERNAL_ERROR,
                    var1: request.body
                })
            }

            deltaTime = (new Date().getTime() - startTime) / 1000

            log.debug("Time Delta", `Send Item Time Usage: ${deltaTime}s`)
        }

        /**
         * 
         * @param {String} ids 
         * @returns 
         */

        function _getLocationData(ids) {
            var locData = {};
            _getAllResults(search.create({
                type: "location",
                filters:
                    ["internalid", "anyof", ids],
                columns:
                    [
                        search.createColumn({
                            name: "name",
                            sort: search.Sort.ASC,
                            label: "Name"
                        }),
                        search.createColumn({ name: "city", label: "City" }),
                        search.createColumn({ name: "state", label: "State/Province" }),
                        search.createColumn({ name: "country", label: "Country" }),
                        search.createColumn({ name: "custrecord_av_inactive", label: "Inactive" }),
                        search.createColumn({ name: "address1", label: "Address 1" }),
                        search.createColumn({ name: "address2", label: "Address 2" }),
                        search.createColumn({ name: "zip", label: "Zip" }),
                        search.createColumn({ name: "latitude", label: "Latitude" }),
                        search.createColumn({ name: "longitude", label: "Longitude" })
                    ]
            })).every(function (result) {
                let streetS = result.getValue("address1").split(" ");
                let streetName = streetS[0];
                let streetNumber = streetS[streetS.length - 1];

                locData = {
                    "id": _setNullAsDef(ids),
                    "displayName": _setNullAsDef(result.getValue("name")),
                    "street": _setNullAsDef(streetName),
                    "houseNumber": _setNullAsDef(streetNumber),
                    "zipCode": _setNullAsDef(result.getValue("zip")),
                    "city": _setNullAsDef(result.getValue("city")),
                    "countryIsoCode": _setNullAsDef(result.getValue("country")),
                    "latitude": result.getValue("latitude") ? parseFloat(result.getValue("latitude")) : "",
                    "longitude": result.getValue("longitude") ? parseFloat(result.getValue("longitude")) : "",
                    "active": !result.getValue("custrecord_av_inactive") ? true : false
                }

                return true;
            });

            return JSON.stringify(locData)
        }

        /**
         * 
         * @param {String} ids 
         * @returns 
         */

        function _getLocationsAddress(ids) {
            var locData = {}, locAddress = [];
            _getAllResults(search.create({
                type: "location",
                filters:
                    ["internalid", "anyof", ids],
                columns:
                    [
                        search.createColumn({
                            name: "name",
                            sort: search.Sort.ASC,
                            label: "Name"
                        }),
                        search.createColumn({ name: "city", label: "City" }),
                        search.createColumn({ name: "state", label: "State/Province" }),
                        search.createColumn({ name: "country", label: "Country" }),
                        search.createColumn({ name: "custrecord_av_inactive", label: "Inactive" }),
                        search.createColumn({ name: "address1", label: "Address 1" }),
                        search.createColumn({ name: "address2", label: "Address 2" }),
                        search.createColumn({ name: "zip", label: "Zip" }),
                        search.createColumn({ name: "latitude", label: "Latitude" }),
                        search.createColumn({ name: "longitude", label: "Longitude" })
                    ]
            })).every(function (result) {
                let streetS = result.getValue("address1").split(" ");
                let streetName = streetS[0];
                let streetNumber = streetS[streetS.length - 1];

                locData = {
                    "id": result.id,
                    "displayName": _setNullAsDef(result.getValue("name")),
                    "street": _setNullAsDef(streetName),
                    "houseNumber": _setNullAsDef(streetNumber),
                    "zipCode": _setNullAsDef(result.getValue("zip")),
                    "city": _setNullAsDef(result.getValue("city")),
                    "countryIsoCode": _setNullAsDef(result.getValue("country"))
                }

                locAddress.push(locData)

                return true;
            });

            return locAddress
        }

        /**
         * 
         * @param {SuiteScript Record Object} currRec 
         * @returns 
         */

        function _getPriceNet(currRec) {
            return currRec.getSublistValue({
                sublistId: "price1",
                fieldId: "price_1_",
                line: 0
            })
        }

        /**
         * 
         * @param {SuiteScript Record Object} currRec 
         * @returns 
         */

        function _getPriceUVP(currRec) {
            return currRec.getSublistValue({
                sublistId: "price1",
                fieldId: "price_1_",
                line: 4
            })
        }

        /**
         * 
         * @param {SuiteScript Record Object} currRec 
         * @returns 
         */

        function _getPriceMinimal(currRec) {
            return currRec.getSublistValue({
                sublistId: "price1",
                fieldId: "price_1_",
                line: 3
            })
        }

        /**
         * 
         * @param {SuiteScript Record Object} currRec 
         * @returns 
         */

        function _getPriceB2b(currRec) {
            return currRec.getSublistValue({
                sublistId: "price1",
                fieldId: "price_1_",
                line: 2
            })
        }

        /**
         * 
         * @param {SuiteScript Record Object} currRec 
         * @returns 
         */

        function _getPriceGiftCert(currRec) {
            return currRec.getSublistValue({
                sublistId: "price",
                fieldId: "price_1_",
                line: 0
            })
        }

        function _getRecordFieldValue(recType, recId, fieldName) {

            try {

                return Number(search.lookupFields({
                    type: recType,
                    id: recId,
                    columns: fieldName
                })[fieldName])

            } catch (e) {
                log.debug({
                    title: "Error",
                    details: JSON.stringify(e)
                })

                return null
            }

        }

        /**
         * 
         * @param {SuiteScript Record Object} context 
         * @returns 
         */

        function _getItemStock(context) {

            var currRec = context.newRecord;
            var pimProductId = Number(currRec.getValue("custitem_av_pim_product_id"));
            var stockQty = _getStocks(currRec.id, pimProductId);

            return stockQty

        }

        /**
         * 
         * @param {String} itemId 
         * @returns 
         */

        function _getStockSummary(itemId) {
            let stockQty = 0;

            _getAllResults(search.create({
                type: "item",
                filters:
                    [
                        ["internalid", "anyof", itemId],
                        "AND",
                        ["inventorylocation", "noneof", CONSTANTS.QUARANTINE_LOCATIONS]
                    ],
                columns:
                    ["itemid",
                        "displayname",
                        "salesdescription",
                        "type",
                        "locationquantityavailable"
                    ]
            })).every(function (result) {
                stockQty += Number(result.getValue("locationquantityavailable"))
                return true;
            });

            return stockQty
        }

        /**
         * 
         * @param {String} itemId 
         * @param {String} pimId 
         * @returns 
         */

        function _getStocks(itemId, pimId) {
            var qtyAvailable = 0;
            var stockByLoc = [];
            var itemSearchObj = _getAllResults(search.create({
                type: "item",
                filters:
                    [
                        "internalid", "anyof", itemId
                    ],
                columns:
                    ["itemid",
                        "displayname",
                        "salesdescription",
                        "type",
                        "quantityavailable"
                    ]
            }));

            var stockS = _getAllResults(search.create({
                type: "item",
                filters:
                    [["internalid", "anyof", itemId],
                        "AND",
                    ["isinactive", "is", "F"]
                    ],
                columns:
                    [
                        search.createColumn({
                            name: "itemid",
                            sort: search.Sort.ASC,
                            label: "Name"
                        }),
                        search.createColumn({ name: "locationquantityavailable", label: "Location Available" }),
                        search.createColumn({
                            name: "custrecord_av_msg_pim_location_id",
                            join: "inventoryLocation"
                        }),
                        "parent"
                    ]
            }))

            stockS.every(function (result) {
                let storeId = Number(result.getValue({
                    name: "custrecord_av_msg_pim_location_id",
                    join: "inventoryLocation"
                }));

                if (storeId) {
                    if (result.getValue("parent")) {

                        stockByLoc.push({
                            productNumber: String(result.getValue("itemid").split(" ")[2]),
                            storeId: storeId,
                            stock: Number(result.getValue("locationquantityavailable")) || 0
                        })

                    } else {

                        stockByLoc.push({
                            productNumber: String(result.getValue("itemid")),
                            storeId: storeId,
                            stock: Number(result.getValue("locationquantityavailable")) || 0
                        })
                    }
                }

                return true;
            });

            return stockByLoc
        }

        /**
         * 
         * @param {SuiteScript Record Object} currRec 
         * @returns 
         */

        function _getFakeStock(currRec) {
            var stockByLoc = [];
            _getAllResults(search.create({
                type: "location",
                filters: [],
                columns: ["custrecord_av_msg_pim_location_id"]
            })).every(function (result) {
                if (Number(result.getValue("custrecord_av_msg_pim_location_id"))) {
                    stockByLoc.push({
                        productNumber: String(currRec.getValue("itemid")),
                        storeId: Number(result.getValue("custrecord_av_msg_pim_location_id")),
                        stock: CONSTANTS.FAKE_STOCK
                    })
                }
                return true;
            });

            return stockByLoc
        }

        /**
         * 
         * @param {SuiteScript Record Object} stagingRec 
         * @returns 
         */

        function _getItems(stagingRec) {

        var itemSkus = [], filters = [], itemIds = {}, itemTypes = {}, itemById = {};

        var stagingLines = stagingRec.getLineCount({
            sublistId: "recmachcustrecord_av_sw_order"
        })

        for (var i = 0; i < stagingLines; i++) {

            try {

                var itemSku = stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_item", i)

                if (itemSku) {
                    itemSkus.push(itemSku)
                }

            } catch (e) {
                log.debug("Error", JSON.stringify(e))
            }

        }

        if (itemSkus.length) {
            for (var i = 0; i < itemSkus.length; i++) {
                if (i == 0) {
                    filters.push(["name", "is", itemSkus[i]])
                } else {
                    filters.push("OR")
                    filters.push(["name", "is", itemSkus[i]])
                }
            }

            var itemSearchObj = _getAllResults(search.create({
                type: "item",
                filters: filters,
                columns:
                    ["itemid", "parent", "type"]
            }));


            itemSearchObj.every(function (result) {

                if (result.getValue("parent")) {

                    itemIds[result.getValue("itemid").split(" ")[2]] = result.id;
                    itemTypes[result.getValue("itemid").split(" ")[2]] = result.getValue("type");
                    itemById[result.id] = result.getValue("itemid").split(" ")[2];

                } else {
                    itemIds[result.getValue("itemid")] = result.id;
                    itemTypes[result.getValue("itemid")] = result.getValue("type");
                    itemById[result.id] = result.getValue("itemid");
                }

                return true;
            });
        }
        

        return {
            itemIds: itemIds,
            itemTypes: itemTypes,
            itemById: itemById
        }
    }

        function _getParent(itemId) {
            if (!itemId) return null;
            try {
                var parentSearch = search.create({
                    type: search.Type.ITEM,
                    filters: [
                        ['internalid', search.Operator.ANYOF, itemId]
                    ],
                    columns: [
                        search.createColumn({ name: 'itemid' })
                    ]
                }).run().getRange({
                    start: 0,
                    end: 1
                });

                return parentSearch.length
                    ? parentSearch[0].getValue({ name: 'itemid' })
                    : null;
            } catch (ex) {
                log.error({ title: '_getParent : ex', details: ex });
            }
        }

        function _getLocationsByItem(itemsByName) {

            var stockByItem = {};
            var itemInternalIds = Object.values(itemsByName)

            if (!itemInternalIds.length) return stockByItem

            var itemSearchObj = _getAllResults(search.create({
                type: "item",
                filters:
                    [
                        ["isinactive", "is", "F"],
                        "AND",
                        ["inventorylocation", "anyof", self.connectionObj.fulfillableLocations],
                        "AND",
                        ["internalid", "anyof", itemInternalIds]
                    ],
                columns:
                    ["itemid", "inventoryLocation", "locationquantityavailable"]
            }));

            itemSearchObj.every(function (result) {

                var itemId = result.id;
                var locId = result.getValue("inventoryLocation");
                var qtyAvailable = Number(result.getValue("locationquantityavailable")) || 0;

                if (!stockByItem[itemId]) {

                    stockByItem[itemId] = {
                        locId: locId,
                        qtyAvailable: qtyAvailable
                    }

                } else {

                    if (locId && (qtyAvailable > stockByItem[itemId].qtyAvailable)) {
                        stockByItem[itemId] = {
                            locId: locId,
                            qtyAvailable: qtyAvailable
                        }
                    }

                }

                return true;
            });

            return stockByItem
        }

        function _checkIfOrderIsNew(orderNumber) {

            self.oldSalesOrderId;

            var itemSearchObj = _getAllResults(search.create({
                type: "salesorder",
                filters: [
                    [ "custbody_av_sw_order_number", "is", orderNumber],
                    "AND", 
                    ["mainline","is","T"]
                ],
                columns: [
                    search.createColumn({ name: "entity", label: "Customer" })
                ]
            }));

            itemSearchObj.every(function (result) {

                if (result.id)
                    self.oldSalesOrderId = result.id
                    self.entitiyId = result.getValue('entity')

                return true;
            });

            if (self.oldSalesOrderId) {
                return false
            } else {
                return true
            }

        }

        function _findEntity(rec, currenciesIds) {
        try {
            var addObj = JSON.parse(rec.getValue("custrecord_av_sw_order_ship_address"))
            var cusData = {
                "email": rec.getValue("custrecord_av_sw_order_cx_email"),
                "country": addObj.country && addObj.country.iso,
                "zip": addObj.zip,
                "street": addObj.street,
                "city": addObj.city
            }

            var searchResult = search.create({
                type: record.Type.CUSTOMER,
                filters: [
                    ['email', search.Operator.IS, cusData.email]
                ],
                columns: []
            }).run().getRange({
                start: 0,
                end: 1
            });
            if (searchResult.length && addObj) {
                var entityIid = searchResult[0].id;
                var orderShipCode = cusData.zip;
                var orderStreet = cusData.street;
                var isAddressInSys = false;

                search.create({
                    type: record.Type.CUSTOMER,
                    filters: [
                        ['email', search.Operator.IS, cusData.email]
                    ],
                    columns: [
                        search.createColumn({
                            name: "zipcode",
                            join: "Address",
                            label: "Zip Code"
                        }),
                        search.createColumn({
                            name: "address1",
                            join: "Address",
                            label: "Address 1"
                        })]
                }).run().each(function (result) {

                    var customerZipCode = result.getValue({
                        name: "zipcode",
                        join: "Address"
                    })

                    var customerStreet = result.getValue({
                        name: "address1",
                        join: "Address"
                    })

                    if ((customerZipCode == orderShipCode) && (customerStreet == orderStreet)) {
                        isAddressInSys = true;
                    }

                    return true
                });

                if (!isAddressInSys) {

                    var customerRec = record.load({
                        type: record.Type.CUSTOMER,
                        id: entityIid,
                        isDynamic: true
                    })

                    customerRec.selectNewLine({
                        sublistId: 'addressbook'
                    });

                    customerRec.setCurrentSublistValue({
                        sublistId: 'addressbook',
                        fieldId: 'defaultbilling',
                        value: true
                    })

                    customerRec.setCurrentSublistValue({
                        sublistId: 'addressbook',
                        fieldId: 'defaultshipping',
                        value: true
                    })

                    var addressSubrecord = customerRec.getCurrentSublistSubrecord({
                        sublistId: 'addressbook',
                        fieldId: 'addressbookaddress'
                    });

                    _setValue(addressSubrecord, "country", cusData.country)
                    _setValue(addressSubrecord, "zip", cusData.zip)
                    _setValue(addressSubrecord, "addr1", cusData.street)

                    if (String(cusData.phoneNumber).length > 7) {
                        _setValue(addressSubrecord, "addrphone", cusData.phoneNumber)
                    }

                    _setValue(addressSubrecord, "city", cusData.city)

                    customerRec.commitLine({
                        sublistId: 'addressbook'
                    });

                    customerRec.save({
                        enableSourcing: false,
                        ignoreMandatoryFields: true
                    })

                }

            }

            return searchResult.length === 0 ? _createCustomer(rec, currenciesIds) : searchResult[0].id;

        } catch (e) {

            avLog.error_message({
                logid: self.logId,
                msg: CONSTANTS.SHOP_ERRORS.EX,
                var1: JSON.stringify(e)
            })

            return;

        }
    }

    function _createCustomer(rec, currenciesIds) {
        var recId;
        try {

            var customerRec = record.create({
                type: record.Type.CUSTOMER,
                isDynamic: true
            })

            _setValue(customerRec, "isperson", "T")
            _setValue(customerRec, "firstname", rec.getValue("custrecord_av_sw_order_cx_fn"))
            _setValue(customerRec, "lastname", rec.getValue("custrecord_av_sw_order_cx_ln"))
            _setValue(customerRec, "companyname", rec.getValue("custrecord_av_sw_order_cx_cn"))
            _setValue(customerRec, "email", rec.getValue("custrecord_av_sw_order_cx_email"))
            _setValue(customerRec, "subsidiary", _getSubsidiary(rec.getText("custrecord_av_sw_channel")))

            customerRec = _setAddress(customerRec, rec)

            customerRec = _setCurrencies(currenciesIds, customerRec)

            recId = customerRec.save({
                enableSourcing: true,
                ignoreMandatoryFields: true
            })

            return recId

        } catch (e) {

            log.debug("Error creating customer 2", JSON.stringify(e))

            return;

        }
    }

    function _getSubsidiary(name) {
        if (!name) return;
        var subsidiary = '';
        var subsidiarySearch = search.create({
            type: 'customrecord_cseg_msg_sales_chan',
            filters: [
            ['name', search.Operator.IS, name]
            ],
            columns: [
            search.createColumn({ name: 'custrecord_msg_sc_subsidiary', label: 'Subsidiary' })
            ]
        }).run().getRange({
            start: 0,
            end: 1
        });
    
        if (subsidiarySearch && subsidiarySearch.length) {
            subsidiary = subsidiarySearch[0].getValue('custrecord_msg_sc_subsidiary');
        }
    
        return subsidiary;
    }

    /**
     * 
     * @param {Array} currenciesIds 
     */

    function _setCurrencies(currenciesIds, customerRec) {

        for (let i = 0; i < currenciesIds.length; i++) {

            customerRec.selectNewLine({
                sublistId: "currency"
            })

            customerRec.setCurrentSublistValue({
                sublistId: "currency",
                fieldId: "currency",
                value: currenciesIds[i]
            })

            customerRec.commitLine({
                sublistId: "currency",
                ignoreRecalc: false
            })

        }

        return customerRec
    }

    function _setAddress(customerRec, rec) {

        var addObj = JSON.parse(rec.getValue("custrecord_av_sw_order_ship_address"))
        var cusData = {
            "email": rec.getValue("custrecord_av_sw_order_cx_email"),
            "firstName": rec.getValue("custrecord_av_sw_order_cx_fn"),
            "lastName": rec.getValue("custrecord_av_sw_order_cx_ln"),
            "country": addObj.country && addObj.country.iso,
            "zip": addObj.zip,
            "street": addObj.street,
            "city": addObj.city,
            "swid": addObj.swid,
            "attention": addObj.attention || ""
        }

        try {

            customerRec.selectNewLine({
                sublistId: 'addressbook'
            });

            var addressSubrecord = customerRec.getCurrentSublistSubrecord({
                sublistId: 'addressbook',
                fieldId: 'addressbookaddress'
            });

            addressSubrecord.setValue({
                fieldId: 'addressee',
                text: cusData.firstName + ' ' + cusData.lastName,
                });

            addressSubrecord.setValue({
                fieldId: 'country',
                value: cusData.country,
            });

            addressSubrecord.setValue({
                fieldId: 'zip',
                value: cusData.zip,
            });

            addressSubrecord.setValue({
                fieldId: 'addr1',
                value: cusData.street,
            });

            if (String(cusData.phoneNumber).length > 7) {
                addressSubrecord.setValue({
                    fieldId: 'addrphone',
                    value: cusData.phoneNumber,
                });
            }

            addressSubrecord.setValue({
                fieldId: 'city',
                value: cusData.city,
            });

            addressSubrecord.setValue({
                fieldId: 'attention',
                value: cusData.attention || "",
            });

            customerRec.commitLine({
                sublistId: 'addressbook'
            });

        } catch (e) {

            log.debug("Error creating address", JSON.stringify(e))

        }

        return customerRec
    }

        /**
         * 
         * @param {SuiteScript Search Object} s 
         * @returns 
         */

        function _getAllResults(s) {
            var results = s.run();
            var searchResults = [];
            var searchid = 0;
            do {
                var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
                resultslice.forEach(function (slice) {
                    searchResults.push(slice);
                    searchid++;
                }
                );
            } while (resultslice.length >= 1000);
            return searchResults;
        }

        /**
         * 
         * @param {SuiteScript Record Object} rec 
         * @param {String} fld 
         * @param {String} val 
         * @returns 
         */

        function _setValue(rec, fld, val) {

            if (fld && val) {
                rec.setValue({
                    fieldId: fld,
                    value: val
                })
            }

            return rec
        }

        /**
         * 
         * @param {SuiteScript Record Object} rec 
         * @returns 
         */

        function _getShipCost(rec) {

            var sCost = rec.getValue("custrecord_av_sw_order_ship_amount");
            var sTax = rec.getValue("custrecord_av_sw_order_taxrate");

            if (!sCost || !sTax) return 0;

            return (sCost / (1 + (sTax / 100)))

        }

        /**
         * 
         * @param {String} dateRaw 
         * @returns 
         */

        function _formatDateFromNs(dateRaw) {

            if (!dateRaw) return null;

            var parsedDate = dateRaw ? new Date(format.parse({
                value: dateRaw,
                type: format.Type.DATE
            })) : new Date();

            return `${parsedDate.getFullYear()}-${_addZero(parsedDate.getMonth() + 1)}-${_addZero(parsedDate.getDate())} ${_addZero(parsedDate.getUTCHours())}:${_addZero(parsedDate.getMinutes())}:${_addZero(parsedDate.getSeconds())}`

        }

        /**
         * 
         * @param {Number} num 
         * @returns 
         */

        function _addZero(num) {
            if (num < 10) {
                num = "0" + num;
            }
            return num;
        }

        /**
         * 
         * @param {SuiteScript Record Object} rec 
         * @returns 
         */

        function _getItemComponents(rec, itemsById) {
            var itemSkus = [];

            if (rec.type != CONSTANTS.ITEM_TYPE.KIT_ITEM_TYPE) return itemSkus;

            var itemComponentsL = rec.getLineCount({ sublistId: "member" })

            for (var i = 0; i < itemComponentsL; i++) {
                let memberId = Number(rec.getSublistValue({
                    sublistId: "member",
                    fieldId: "item",
                    line: i
                }));
                if (itemsById.itemById[memberId])
                    itemSkus.push(itemsById.itemById[memberId]
                    )
            }

            return itemSkus
        }

        /**
         * 
         * @param {String} val 
         * @returns 
         */

        function _setNullAsDef(val) {
            return !val || (val && val.length == 0) ? null : val
        }

        /**
        * @descripription PIM is incosistent with nullable values, sometimes we have to send null as string
        * @param {String} val 
        * @returns 
        */

        function _setNullStringAsDef(val) {
            return !val ? "null" : val
        }


        /**
         * 
         * @param {String} isoCurr 
         * @returns 
         */

        function _lookUpCurrencybyIso(isoCurr) {
            let orderCurrencyId, currenciesIds = [];

            if (!isoCurr) return orderCurrencyId;

            _getAllResults(search.create({
                type: "currency",
                filters:
                    [],
                columns: ["name", "symbol"]
            })).every(function (result) {
                if (result.getValue("symbol") == isoCurr) {
                    orderCurrencyId = result.getValue("name")
                }
                currenciesIds.push(result.id)
                return true;
            });

            return {
                orderCurrencyId: orderCurrencyId,
                currenciesIds: currenciesIds
            }
        }

        /**
         * 
         * @param {SuiteScript Record Object} stagingRec 
         * @returns 
         */

        function _getItemsById() {

            var itemById = {};

            var itemSearchObj = _getAllResults(search.create({
                type: "item",
                filters: [],
                columns:
                    ["itemid", "parent", "type"]
            }));


            itemSearchObj.every(function (result) {

                if (result.getValue("parent")) {

                    itemById[result.id] = result.getValue("itemid").split(" ")[2];

                } else {
                    itemById[result.id] = result.getValue("itemid");
                }

                return true;
            });

            return {
                itemById: itemById
            }
        }

        return {
            onAction: onAction
        };
    })