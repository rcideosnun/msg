/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 ** @author: [Roberto Cideos]
 ** @dated: [23.7.2021]
 ** @Description: Integration between PIMCore and NetSuite
 *
 *  @NApiVersion 2.1
 *  @NScriptType WorkflowActionScript
 */

define(['./av_msg_lib_sv2',
    'N/https',
    './av_int_log.js',
    'N/runtime',
    'N/search',
    'N/record',
    'N/format'],
    function (settings,
        https,
        avLog,
        runtime,
        search,
        record,
        format) {

        var self = this;
        var deltaTime, startTime;

        var CONSTANTS = {
            INTERFACES: {
                "I01": {
                    onActionFunction: _sendItemToPim,
                    onActionHttpRequest: "POST",
                    connectionRequired: true
                },
                "I02": {
                    onActionFunction: _sendItemStockToPim,
                    onActionHttpRequest: "PUT", //PATCH
                    connectionRequired: true
                },
                "I09": {
                    onActionFunction: _sendLocationToPim,
                    onActionHttpRequest: "POST", //PATCH
                    connectionRequired: true
                },
                "I05": {
                    onActionFunction: _updateItemToPim,
                    onActionHttpRequest: "PUT",
                    connectionRequired: true
                },
                "I06": {
                    onActionFunction: _sendBrandToPim,
                    onActionHttpRequest: "POST",
                    connectionRequired: true
                },
                "S01": {
                    onActionFunction: _createSalesOrderFromStaging,
                    connectionRequired: false
                }
            },
            HTTP_CODE: {
                SUCCESS_UPDATE: 200,
                SUCCESS_CREATE: 201,
                ERROR: 401
            },
            INTERFACE_STATE: {
                NEW: 1,
                ERROR: 2,
                DONE: 3
            },
            TAX_SCHEDULES_MAPPING: {
                1: "standardRate",
                2: "free"
            },
            ERROR_TYPES: {
                DUPLICATED_ORDER: "SW1",
                ITEM_NOT_FOUND: "SW2",
                INTERNAL_ERROR: "SW3"
            },
            SHOPWARE_PRODUCT_TYPE: {
                PRODUCT: "product",
                PROMOTION: "promotion",
                DISCOUNT: "discount",
                GC_REDEMPTION: "xgx-coupon"
            },
            PRICE_TYPE: {
                BASE_PRICE: 1,
                CUSTOM_PRICE: -1
            },
            DEFAULT_SHIP_METHOD: {
                DHL: 411
            },
            ITEM_TYPE: {
                GIFT_CERTIFICATE_TYPE: "giftcertificateitem",
                GC_NS_TYPE: "GiftCert",
                KIT_ITEM_TYPE: "kititem"
            },
            SUBSIDIARY: {
                WEB_STORE_DEFAULT: 3
            },
            EXCHANGE_RATES: {
                "EUR": {
                    "EUR": 1,
                    "CHF": 1.067,
                    "Hungarian Forint": 401
                },
                "CHF": {
                    "EUR": 0.937207,
                    "CHF": 1,
                    "Hungarian Forint": 375.82
                },
                "Hungarian Forint": {
                    "EUR": 0.002493,
                    "CHF": 0.00266,
                    "Hungarian Forint": 1
                }
            },
            PROCESS_INVOICE: {
                BY_PAYMENT_METHOD: {
                    "Vorkasse": false
                }
            },
            ORDER_STATUS_ENTRY: {
                "Vorkasse": "A",
                "DEFAULT": "B"
            }
        }

        function onAction(context) {
            try {
                startTime = new Date().getTime()

                if (runtime.envType == "SANDBOX") {
                    //setup sandbox global variables
                    var scriptObj = runtime.getCurrentScript();
                    var CONFIG_ID = scriptObj.getParameter({ name: 'custscript_av_integration_id' }) || scriptObj.getParameter({ name: 'custscript_av_staging_config' }) || null;
                    var iface = self.iface = scriptObj.getParameter({ name: 'custscript_av_item_iface' }) || scriptObj.getParameter({ name: 'custscript_av_tran_iface' });
                    self.gcItem = scriptObj.getParameter({ name: 'custscript_av_msg_item_gc' })

                } else if (runtime.envType == "PRODUCTION") {
                    //setup production global variables
                    var scriptObj = runtime.getCurrentScript();
                    var CONFIG_ID = scriptObj.getParameter({ name: 'custscript_av_integration_id' }) || scriptObj.getParameter({ name: 'custscript_av_staging_config' }) || null;
                    var iface = self.iface = scriptObj.getParameter({ name: 'custscript_av_item_iface' }) || scriptObj.getParameter({ name: 'custscript_av_tran_iface' });
                    self.gcItem = scriptObj.getParameter({ name: 'custscript_av_msg_item_gc' })

                }
                log.debug("Debug", JSON.stringify(self))
                self.connectionObj = new settings(CONFIG_ID, context, CONSTANTS.INTERFACES[iface].connectionRequired);

                return CONSTANTS.INTERFACES[iface].onActionFunction(context)
            } catch (e) {
                log.debug("Error", JSON.stringify(e))
            }
        }

        function _sendItemToPim(context) {

            var payload = _getItemDataNew(context)

            var reqHeaders = {
                "Content-type": "application/json",
                "Authorization": `Bearer ${self.connectionObj.accessToken}`
            }

            var requestUrl = self.connectionObj.custrecord_av_integration_base_url.concat(self.connectionObj.custrecord_av_integration_product_post)

            var logId = avLog.create({
                iface: self.iface,
                send: payload,
                url: requestUrl,
                state: CONSTANTS.INTERFACE_STATE.NEW,
                item: context.newRecord.id
            })

            var request = https.request({
                method: CONSTANTS.INTERFACES[self.iface].onActionHttpRequest,
                url: requestUrl,
                body: payload,
                headers: reqHeaders
            })

            if (request.code == CONSTANTS.HTTP_CODE.SUCCESS_CREATE) {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.DONE,
                    receive: request.body
                })

                context.newRecord.setValue("custitem_av_item_sent_to_pim", true)
                context.newRecord.setValue("custitem_av_pim_product_id", JSON.parse(request.body).id)

            } else {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.ERROR,
                    receive: request.body
                })

                avLog.error_message({
                    logid: logId,
                    msg: CONSTANTS.ERROR_TYPES.INTERNAL_ERROR,
                    var1: request.body
                })
            }

            deltaTime = (new Date().getTime() - startTime) / 1000

            log.debug("Time Delta", `Send Item Time Usage: ${deltaTime}s`)
        }

        function _updateItemToPim(context) {

            var payload = _getItemUpdate(context)

            var reqHeaders = {
                "Content-type": "application/json",
                "Authorization": `Bearer ${self.connectionObj.accessToken}`
            }

            var requestUrl = self.connectionObj.custrecord_av_integration_base_url.concat(self.connectionObj.custrecord_av_integration_product_put)

            var logId = avLog.create({
                iface: self.iface,
                send: payload,
                url: requestUrl,
                state: CONSTANTS.INTERFACE_STATE.NEW,
                item: context.newRecord.id
            })

            var request = https.request({
                method: CONSTANTS.INTERFACES[self.iface].onActionHttpRequest,
                url: requestUrl,
                body: payload,
                headers: reqHeaders
            })

            if (request.code == CONSTANTS.HTTP_CODE.SUCCESS_UPDATE || request.code == CONSTANTS.HTTP_CODE.SUCCESS_CREATE) {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.DONE,
                    receive: request.body
                })

                context.newRecord.setValue("custitem_av_item_sent_to_pim", true)

            } else {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.ERROR,
                    receive: request.body
                })

                avLog.error_message({
                    logid: logId,
                    var1: request.body
                })
            }

            deltaTime = (new Date().getTime() - startTime) / 1000

            log.debug("Time Delta", `Update Item Time Usage: ${deltaTime}s`)

        }

        function _sendItemStockToPim(context) {

            var stockS = _getItemStock(context)

            var reqHeaders = {
                "Content-type": "application/json",
                "Authorization": `Bearer ${self.connectionObj.accessToken}`,
                //"X-HTTP-Method-Override": "PATCH"
            }

            for (var i = 0; i < stockS.length; i++) {

                let payload = stockS[i]

                var requestUrl = self.connectionObj.custrecord_av_integration_base_url.concat(self.connectionObj.custrecord_av_integration_stock_post)

                var logId = avLog.create({
                    iface: self.iface,
                    send: payload,
                    url: requestUrl,
                    state: CONSTANTS.INTERFACE_STATE.NEW,
                    item: context.newRecord.id
                })

                var request = https.request({
                    method: CONSTANTS.INTERFACES[self.iface].onActionHttpRequest,
                    url: requestUrl,
                    body: payload,
                    headers: reqHeaders
                })

                if (request.code == CONSTANTS.HTTP_CODE.SUCCESS_UPDATE) {
                    avLog.update({
                        id: logId,
                        responseCode: request.code,
                        state: CONSTANTS.INTERFACE_STATE.DONE,
                        receive: request.body
                    })

                    context.newRecord.setValue("custitem_av_item_sent_to_pim", true)

                } else {
                    avLog.update({
                        id: logId,
                        responseCode: request.code,
                        state: CONSTANTS.INTERFACE_STATE.ERROR,
                        receive: request.body
                    })

                    avLog.error_message({
                        logid: logId,
                        var1: request.body
                    })
                }
            }
        }

        function _createSalesOrderFromStaging(context) {

            try {

                var stagingRec = context.newRecord, anyItemNotFound = false, itemsNotFound = [], hasGc = false, gcCodesIds = [], gcCodesRedeemed = [], soId;
                var orderCurrency = stagingRec.getValue("custrecord_av_sw_order_currency");
                var giftCertsS = _getGiftCertificates();
                var isOrderNew = _checkIfOrderIsNew(stagingRec.getValue("custrecord_av_sw_order_number"));
                var paymentMethod = stagingRec.getText("custrecord_av_sw_order_payment_method");

                if (!isOrderNew) {
                    stagingRec.setValue("custrecord_av_sw_ns_transaction", self.oldSalesOrderId)
                    stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.DONE)

                    var logId = stagingRec.getValue("custrecord_av_sw_order_log")

                    avLog.error_message({
                        logid: logId,
                        staging: stagingRec.id,
                        msg: CONSTANTS.ERROR_TYPES.DUPLICATED_ORDER,
                        var1: self.oldSalesOrderId
                    })

                    return;
                }

                var entitiyId = stagingRec.getValue("custrecord_av_sw_order_cx_entity") ? stagingRec.getValue("custrecord_av_sw_order_cx_entity") : _findEntity(stagingRec)

                stagingRec.setValue("custrecord_av_sw_order_cx_entity", entitiyId)

                if (!entitiyId) return stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)

                var addressS = _getAddresses(entitiyId);
                var shipAddressId = _searchShipAddress(addressS, stagingRec, entitiyId);
                var billAddressId = _searchBillAddress(addressS, stagingRec, entitiyId);

                var soRec = record.transform({
                    fromType: record.Type.CUSTOMER,
                    fromId: entitiyId,
                    toType: record.Type.SALES_ORDER,
                    isDynamic: true
                });

                if (shipAddressId) {
                    soRec.setValue("shipaddresslist", shipAddressId);
                }
                if (billAddressId) {
                    soRec.setValue("billaddresslist", billAddressId);
                }
                soRec.setValue("custbody_av_sw_order_number", stagingRec.getValue("custrecord_av_sw_order_number"));
                soRec.setValue("custbody_av_payment_method", stagingRec.getValue("custrecord_av_sw_order_payment_method"));
                soRec.setText("custbody_av_msg_payment_method", stagingRec.getText("custrecord_av_sw_order_payment_method"));
                soRec.setValue("custbody_av_order_time", stagingRec.getValue("custrecord_av_sw_order_date_time"));
                soRec.setText("shipmethod", _pullShipMethod(stagingRec.getValue("custrecord_av_sw_order_ship_method")));
                soRec.setValue("shippingcost", _getShipCost(stagingRec));
                soRec.setText("currency", stagingRec.getValue("custrecord_av_sw_order_currency"));
                soRec.setValue("custbody_av_sales_channel_id", stagingRec.getValue("custrecord_av_sw_channel_id"));

                if (CONSTANTS.ORDER_STATUS_ENTRY[paymentMethod]) {
                    soRec.setValue("orderstatus", CONSTANTS.ORDER_STATUS_ENTRY[paymentMethod])
                } else {
                    soRec.setValue("orderstatus", CONSTANTS.ORDER_STATUS_ENTRY.DEFAULT)
                }

                var stagingLines = stagingRec.getLineCount({
                    sublistId: "recmachcustrecord_av_sw_order"
                })

                var itemS = _getItems(stagingRec);
                var itemIds = itemS.itemIds;
                var nsItemType = itemS.itemTypes;

                var itemLocS = _getLocationsByItem(itemIds)

                for (var i = 0; i < stagingLines; i++) {

                    try {

                        var itemId = itemIds[stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_item", i)];
                        var gcCode = stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_item", i);
                        var pimProdType = stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_type", i);
                        var nsProdType = nsItemType[stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_item", i)];
                        var itemTaxRate = Number(stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_taxrate", i));

                        if (itemId && pimProdType == CONSTANTS.SHOPWARE_PRODUCT_TYPE.PRODUCT && nsProdType != CONSTANTS.ITEM_TYPE.GC_NS_TYPE) {

                            var soLine = soRec.selectNewLine({
                                sublistId: "item"
                            })

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'item',
                                value: itemId
                            });

                            if (itemLocS[itemId]) {

                                soLine.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'location',
                                    value: itemLocS[itemId].locId
                                });

                            }

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'quantity',
                                value: stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_qty", i)
                            });

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'taxrate1',
                                value: itemTaxRate
                            });

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'grossamt',
                                value: stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_total_price", i)
                            });

                            if (soLine.getCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'itemtype'
                            }) == CONSTANTS.ITEM_TYPE.GC_NS_TYPE) {

                                var gcQ = parseInt(stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_qty", i))

                                soLine.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'giftcertfrom',
                                    value: stagingRec.getValue("custrecord_av_sw_order_cx_fn")
                                });

                                soLine.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'giftcertrecipientname',
                                    value: stagingRec.getValue("custrecord_av_sw_order_cx_fn")
                                });

                                soLine.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'giftcertrecipientemail',
                                    value: stagingRec.getValue("custrecord_av_sw_order_cx_email")
                                });

                            }

                            soLine.commitLine("item", false)

                        } else if (pimProdType == CONSTANTS.SHOPWARE_PRODUCT_TYPE.DISCOUNT || pimProdType == CONSTANTS.SHOPWARE_PRODUCT_TYPE.PROMOTION) {

                            var soLine = soRec.selectNewLine({
                                sublistId: "item"
                            })

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'item',
                                value: self.connectionObj.discountItem
                            });

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'price',
                                value: CONSTANTS.PRICE_TYPE.CUSTOM_PRICE
                            });

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'taxrate1',
                                value: itemTaxRate
                            });

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'grossamt',
                                value: stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_total_price", i)
                            });

                            soLine.commitLine("item", false)

                        } else if (itemId && pimProdType == CONSTANTS.SHOPWARE_PRODUCT_TYPE.PRODUCT && nsProdType == CONSTANTS.ITEM_TYPE.GC_NS_TYPE) {

                            var reqQ = Number(stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_qty", i))

                            for (var j = 0; j < reqQ; j++) {

                                var soLine = soRec.selectNewLine({
                                    sublistId: "item"
                                })

                                soLine.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'item',
                                    value: itemId
                                });

                                if (itemLocS[itemId]) {

                                    soLine.setCurrentSublistValue({
                                        sublistId: 'item',
                                        fieldId: 'location',
                                        value: itemLocS[itemId].locId
                                    });

                                }

                                soLine.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'quantity',
                                    value: stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_qty", i)
                                });

                                soLine.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'taxrate1',
                                    value: itemTaxRate
                                })

                                soLine.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'grossamt',
                                    value: stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_total_price", i)
                                });

                                soLine.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'giftcertfrom',
                                    value: stagingRec.getValue("custrecord_av_sw_order_cx_fn")
                                });

                                soLine.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'giftcertrecipientname',
                                    value: stagingRec.getValue("custrecord_av_sw_order_cx_fn")
                                });

                                soLine.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'giftcertrecipientemail',
                                    value: stagingRec.getValue("custrecord_av_sw_order_cx_email")
                                });

                                soLine.commitLine("item", false)
                            }

                            /*gcCodesRedeemed.push({
                                code: gcCode,
                                amtUsed: stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_total_price", i)
                            });
                            gcCodesIds.push(gcCode);*/

                            hasGc = true;

                        } else if (gcCode && pimProdType == CONSTANTS.SHOPWARE_PRODUCT_TYPE.GC_REDEMPTION) {
                            var soLine = soRec.selectNewLine({
                                sublistId: "item"
                            })

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'item',
                                value: self.gcItem
                            });

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'price',
                                value: CONSTANTS.PRICE_TYPE.CUSTOM_PRICE
                            });

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'description',
                                value: gcCode
                            });

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'grossamt',
                                value: -Number(stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_total_price", i))
                            });

                            soLine.commitLine("item", false);

                            /*var soLine = soRec.selectNewLine({
                                sublistId: "giftcertredemption"
                            })
   
                            soLine.setCurrentSublistText({
                                sublistId: 'giftcertredemption',
                                fieldId: 'authcode',
                                text: gcCode
                            });
   
                            soLine.setCurrentSublistValue({
                                sublistId: 'giftcertredemption',
                                fieldId: 'authcodeapplied',
                                value: stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_total_price", i)
                            });
   
                            soLine.commitLine("giftcertredemption", false);*/
                            gcCodesRedeemed.push({
                                code: gcCode,
                                amtUsed: stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_total_price", i)
                            });
                            gcCodesIds.push(gcCode);
                            hasGc = true;
                        } else {
                            itemsNotFound.push(
                                stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_item", i)
                            )
                            anyItemNotFound = true;
                            continue;
                        }

                        var itemLine = soRec.selectLine({
                            sublistId: "item",
                            line: i
                        })

                        itemLine.setCurrentSublistValue({
                            sublistId: 'item',
                            fieldId: 'taxrate1',
                            value: itemTaxRate
                        });

                        if (itemId && pimProdType == CONSTANTS.SHOPWARE_PRODUCT_TYPE.PRODUCT && nsProdType != CONSTANTS.ITEM_TYPE.GC_NS_TYPE) {
                            let itemNetAmt = Number(itemLine.getCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'amount'
                            })).toFixed(2);

                            let itemQty = Number(itemLine.getCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'quantity'
                            }))

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'price',
                                value: CONSTANTS.PRICE_TYPE.CUSTOM_PRICE
                            });

                            soLine.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'rate',
                                value: (itemNetAmt / itemQty).toFixed(2)
                            });
                        }
                        soLine.setCurrentSublistValue({
                            sublistId: 'item',
                            fieldId: 'grossamt',
                            value: stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_total_price", i)
                        });

                        itemLine.commitLine("item", false)

                    } catch (e) {
                        log.debug("Error", JSON.stringify(e))
                    }

                }

                if (anyItemNotFound) {

                    stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)

                    avLog.error_message({
                        staging: stagingRec.id,
                        msg: CONSTANTS.ERROR_TYPES.ITEM_NOT_FOUND,
                        var1: itemsNotFound,
                    })

                    return;
                }

                soRec.save.promise({
                    enableSourcing: true,
                    ignoreMandatoryFields: true
                }).then(function (recId) {

                    stagingRec.setValue("custrecord_av_sw_ns_transaction", recId)
                    stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.DONE)

                }, function (e) {
                    stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
                    log.debug("Error creating sales order", JSON.stringify(e))
                    avLog.error_message({
                        staging: stagingRec.id,
                        msg: CONSTANTS.ERROR_TYPES.INTERNAL_ERROR,
                        var1: JSON.stringify(e),
                    })
                })

                if (hasGc && soRec.id) {
                    soRec = record.load({
                        type: "salesorder",
                        id: soRec.id
                    })
                    let gcCodesIdsResave = [];

                    if (gcCodesIds.length) {
                        _updateGcCustom(gcCodesRedeemed, giftCertsS, orderCurrency);
                    }
                    var gcLines = soRec.getLineCount({
                        sublistId: "item"
                    })

                    for (var i = 0; i < gcLines; i++) {

                        gcCodesIdsResave.push({
                            id: soRec.getSublistValue({
                                sublistId: 'item',
                                fieldId: 'giftcertkey',
                                line: i
                            })
                        });

                    }

                    if (gcCodesIdsResave.length) {
                        _updateGcCrs(gcCodesIdsResave)
                    }

                    if (CONSTANTS.PROCESS_INVOICE.BY_PAYMENT_METHOD[paymentMethod] === false) return;

                    var invRec = record.transform({
                        fromType: "salesorder",
                        fromId: soRec.id,
                        toType: "invoice",
                        isDynamic: true
                    })

                    var invLineCount = invRec.getLineCount({
                        sublistId: "item"
                    })

                    for (var i = 0; i < invLineCount; i++) {

                        let hasGc = invRec.getSublistValue({
                            sublistId: 'item',
                            fieldId: 'giftcertkey',
                            line: i
                        });

                        if (!hasGc) {
                            invRec.removeLine({
                                sublistId: "item",
                                line: i
                            })
                        }

                    }

                    invRec.save.promise().then(function (response) {
                        log.debug("Invoice Id", JSON.stringify(response))
                    }, function (error) {
                        log.debug("Error creating invoice", JSON.stringify(error))
                    });

                }




            } catch (e) {
                stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
                avLog.error_message({
                    staging: stagingRec.id,
                    msg: CONSTANTS.ERROR_TYPES.INTERNAL_ERROR,
                    var1: JSON.stringify(e),
                })
                log.debug("Error", JSON.stringify(e))
            }

        }

        function _getAddresses(eid) {
            var addresses = {};

            _getAllResults(search.create({
                type: "customer",
                filters:
                    [
                        ["isinactive", "is", "F"],
                        "AND",
                        ["internalid", "anyof", eid]
                    ],
                columns:
                    [
                        search.createColumn({
                            name: "custrecord_av_shopware_address_id",
                            join: "Address",
                            label: "Shopware Address Id"
                        }),
                        search.createColumn({
                            name: "addressinternalid",
                            join: "Address",
                            label: "Address Internal ID"
                        })
                    ]
            })).every(function (result) {

                addresses[result.getValue({
                    name: "custrecord_av_shopware_address_id",
                    join: "Address"
                })] = {
                    nsId: result.getValue({
                        name: "addressinternalid",
                        join: "Address"
                    }),

                }

                return true
            });

            return addresses
        }

        function _searchShipAddress(adresses, stagingRec, eid) {

            if (adresses[stagingRec.getValue("custrecord_av_sw_order_ship_add_id")]) {
                if (adresses[stagingRec.getValue("custrecord_av_sw_order_ship_add_id")].nsId) {
                    return adresses[stagingRec.getValue("custrecord_av_sw_order_ship_add_id")].nsId
                }
            }

            var shipAddData = stagingRec.getValue("custrecord_av_sw_order_ship_address") ? JSON.parse(stagingRec.getValue("custrecord_av_sw_order_ship_address")) : null;

            if (!shipAddData) return;

            var customerRec = record.load({
                type: "customer",
                id: eid,
                isDynamic: true
            })

            customerRec.selectNewLine({
                sublistId: 'addressbook'
            });

            var addressSubrecord = customerRec.getCurrentSublistSubrecord({
                sublistId: 'addressbook',
                fieldId: 'addressbookaddress'
            });

            addressSubrecord.setValue({
                fieldId: 'addr1',
                value: shipAddData[0].street,
            });

            addressSubrecord.setValue({
                fieldId: 'country',
                value: shipAddData[0].country,
            });

            addressSubrecord.setValue({
                fieldId: 'zip',
                value: shipAddData[0].zip,
            });

            addressSubrecord.setValue({
                fieldId: 'addr1',
                value: shipAddData[0].street,
            });

            addressSubrecord.setValue({
                fieldId: 'city',
                value: shipAddData[0].city,
            });

            addressSubrecord.setValue({
                fieldId: 'custrecord_av_shopware_address_id',
                value: shipAddData[0].swid,
            });

            addressSubrecord.setValue({
                fieldId: 'attention',
                value: shipAddData[0].attention || "",
            });

            addressSubrecord.setValue({
                fieldId: 'defaultshipping',
                value: true
            });

            customerRec.commitLine({
                sublistId: 'addressbook'
            });

            customerRec.save();

            var addressS = _getAddresses(eid);

            if (addressS[stagingRec.getValue("custrecord_av_sw_order_ship_add_id")]) {
                if (addressS[stagingRec.getValue("custrecord_av_sw_order_ship_add_id")].nsId) {
                    return addressS[stagingRec.getValue("custrecord_av_sw_order_ship_add_id")].nsId
                }
            }

            return;
        }

        function _searchBillAddress(adresses, stagingRec, eid) {

            if (adresses[stagingRec.getValue("custrecord_av_sw_order_bill_add_id")]) {
                if (adresses[stagingRec.getValue("custrecord_av_sw_order_bill_add_id")].nsId) {
                    return adresses[stagingRec.getValue("custrecord_av_sw_order_bill_add_id")].nsId
                }
            }

            var shipAddData = stagingRec.getValue("custrecord_av_sw_order_bill_address") ? JSON.parse(stagingRec.getValue("custrecord_av_sw_order_bill_address")) : null;

            if (!shipAddData) return;

            var customerRec = record.load({
                type: "customer",
                id: eid,
                isDynamic: true
            })

            customerRec.selectNewLine({
                sublistId: 'addressbook'
            });

            var addressSubrecord = customerRec.getCurrentSublistSubrecord({
                sublistId: 'addressbook',
                fieldId: 'addressbookaddress'
            });

            addressSubrecord.setValue({
                fieldId: 'country',
                value: shipAddData[0].country,
            });

            addressSubrecord.setValue({
                fieldId: 'zip',
                value: shipAddData[0].zip,
            });

            addressSubrecord.setValue({
                fieldId: 'addr1',
                value: shipAddData[0].street,
            });

            addressSubrecord.setValue({
                fieldId: 'city',
                value: shipAddData[0].city,
            });

            addressSubrecord.setValue({
                fieldId: 'custrecord_av_shopware_address_id',
                value: shipAddData[0].swid,
            });

            addressSubrecord.setValue({
                fieldId: 'attention',
                value: shipAddData[0].attention || "",
            });

            addressSubrecord.setValue({
                fieldId: 'defaultbilling',
                value: true
            });

            customerRec.commitLine({
                sublistId: 'addressbook'
            });

            customerRec.save();

            var addressS = _getAddresses(eid);

            if (addressS[stagingRec.getValue("custrecord_av_sw_order_bill_add_id")]) {
                if (addressS[stagingRec.getValue("custrecord_av_sw_order_bill_add_id")].nsId) {
                    return addressS[stagingRec.getValue("custrecord_av_sw_order_bill_add_id")].nsId
                }
            }

            return;
        }

        function _getGiftCertificates() {
            let gcByCode = {};
            search.create({
                type: "customrecord_av_cs_gc",
                filters: [],
                columns: ["custrecord_av_cs_gc_code", "custrecord_av_cs_gc_rem_amt"]
            }).run().each(function (result) {

                gcByCode[String(result.getValue("custrecord_av_cs_gc_code"))] = {
                    id: result.id,
                    amt: result.getValue("custrecord_av_cs_gc_rem_amt")
                }

                return true
            })

            return gcByCode
        }

        function _updateGcCustom(gcCodes, giftCertsS, orderCurrency) {

            for (let i = 0; i < gcCodes.length; i++) {

                let cusGcRec = record.load({
                    type: "customrecord_av_cs_gc",
                    id: giftCertsS[gcCodes[i].code].id,
                    isDynamic: true
                })

                let baseCurrency = cusGcRec.getText("custrecord_av_cs_gc_curr");

                let remAmt = Number(cusGcRec.getValue("custrecord_av_cs_gc_rem_amt"));

                let amtUsedToBase = CONSTANTS.EXCHANGE_RATES[orderCurrency][baseCurrency] * gcCodes[i].amtUsed;

                cusGcRec.setValue({
                    fieldId: "custrecord_av_cs_gc_rem_amt",
                    value: remAmt + amtUsedToBase
                });

                let gcLines = cusGcRec.getLineCount({
                    sublistId: "recmachcustrecord_av_parent"
                })

                for (let j = 0; j < gcLines; j++) {

                    let lineCurrency = cusGcRec.getSublistText({
                        sublistId: "recmachcustrecord_av_parent",
                        fieldId: "custrecord_av_gc_currency",
                        line: j
                    });

                    let oldRemAmt = cusGcRec.getSublistValue({
                        sublistId: "recmachcustrecord_av_parent",
                        fieldId: "custrecord_av_gc_remaining_amount",
                        line: j
                    });//from lineCurrency to base

                    let newRemAmt = CONSTANTS.EXCHANGE_RATES[baseCurrency][lineCurrency] * Number(remAmt + amtUsedToBase);//

                    cusGcRec.selectLine({
                        sublistId: "recmachcustrecord_av_parent",
                        line: j
                    });

                    cusGcRec.setCurrentSublistValue({
                        sublistId: "recmachcustrecord_av_parent",
                        fieldId: "custrecord_av_gc_remaining_amount",
                        value: newRemAmt // TODO: Calculate the amount based on the currency used by the sales order
                    });

                    cusGcRec.commitLine({
                        sublistId: "recmachcustrecord_av_parent"
                    });
                }

                cusGcRec.save();
            }

        }

        function _updateGcCrs(gcCodes) {

            for (gcC in gcCodes) {

                if (gcCodes[gcC].id)
                    record.load({
                        type: "giftcertificate",
                        id: gcCodes[gcC].id
                    }).save()

            }

        }

        function _pullShipMethod(shipMethodVal) {

            if (!shipMethodVal) return;

            var shipArr = JSON.parse(shipMethodVal)

            if (!shipArr.length) return;

            return shipArr[0].name
        }

        function _sendBrandToPim(context) {

            var payload = _getBrand(context)

            var reqHeaders = {
                "Content-type": "application/json",
                "Authorization": `Bearer ${self.connectionObj.accessToken}`
            }

            var requestUrl = self.connectionObj.custrecord_av_integration_base_url.concat(self.connectionObj.custrecord_av_integration_brand_post)

            var logId = avLog.create({
                iface: self.iface,
                send: payload,
                url: requestUrl,
                state: CONSTANTS.INTERFACE_STATE.NEW,
                brand: context.newRecord.id
            })

            var request = https.request({
                method: CONSTANTS.INTERFACES[self.iface].onActionHttpRequest,
                url: requestUrl,
                body: payload,
                headers: reqHeaders
            })

            if (request.code == CONSTANTS.HTTP_CODE.SUCCESS_CREATE) {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.DONE,
                    receive: request.body
                })

                context.newRecord.setValue("custitem_av_vendor_sent_to_pim", true)

            } else {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.ERROR,
                    receive: request.body
                })

                avLog.error_message({
                    logid: logId,
                    var1: request.body
                })
            }

        }

        function _getBrand(context) {

            var currRec = context.newRecord

            var vId = currRec.id;
            var dN = currRec.getValue("name");

            var vendorObj = {
                "id": vId,
                "displayName": dN
            }

            return JSON.stringify(vendorObj)
        }

        function _getItemDataNew(context) {

            var currRec = context.newRecord;
            var itemType = currRec.type

            var itemNumber = currRec.getValue("itemid");
            var itemDisplayName = currRec.getValue("displayname");
            var itemEan = currRec.getValue("upccode");
            var itemIsActive = !currRec.getValue("custitem_av_pim_product_inactive");
            var itemPrefix = currRec.getText("custitem_av_pim_item_prefix");
            var itemProductTypeId = _getRecordFieldValue("customrecord_av_pim_product_type", currRec.getValue("custitem_av_pim_product_type_id"), "custrecord_av_pim_product_type_id");
            var itemDepartmentId = _getRecordFieldValue("customrecord_av_pim_department", currRec.getValue("custitem_av_pim_department_id"), "custrecord_av_pim_department_id");
            var itemTaxRate = currRec.getValue("taxschedule") ? CONSTANTS.TAX_SCHEDULES_MAPPING[currRec.getValue("taxschedule")] : "";
            var itemNetPrice = itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(_getPriceNet(currRec)) : _getPriceGiftCert(currRec);
            var itemNetPriceB2B = itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(_getPriceB2b(currRec)) : _getPriceGiftCert(currRec);
            var itemNetPriceUVP = itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(_getPriceUVP(currRec)) : _getPriceGiftCert(currRec);
            var itemNetPriceMinimal = itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(_getPriceMinimal(currRec)) : _getPriceGiftCert(currRec);
            var itemHeight = parseInt(currRec.getValue("custitem_msg_height"));
            var itemWidth = parseInt(currRec.getValue("custitem_msg_width"));
            var itemLength = parseInt(currRec.getValue("custitem_msg_length"));
            var itemWeight = parseFloat(currRec.getValue("custitem_av_weight"));
            var itemBrandId = parseInt(currRec.getValue("custitem_av_brand_id"));
            var itemPimParent = currRec.getValue("custitem_av_parent_pim_product_id");
            var itemDepartment = currRec.getText("custitem_av_pim_department_id");
            var itemProductType = currRec.getText("custitem_av_pim_product_type_id");
            var itemDeliveryTimes = currRec.getText("custitem_av_delivery_time");
            var itemBrandNumber = currRec.getValue("custitem_av_brand_number");
            var itemAvailability = currRec.getValue("custitem_av_item_availability");
            var itemErpCreatedAt = _formatDateFromNs(currRec.getValue("createddate"));
            var itemFirstStorageAt = _formatDateFromNs(currRec.getValue("custitem_av_item_firststorageat"));
            var itemLastStorageAt = _formatDateFromNs(currRec.getValue("custitem_av_item_laststorageat"));
            var itemLastStockAt = _formatDateFromNs(currRec.getValue("custitem_av_item_lastemptystockat"));
            var itemContributionMargin = currRec.getValue("custitem_av_item_contributionmargin");
            var itemTopCategory = currRec.getText("custitem_av_top_category");
            var itemMasseinheit = currRec.getText("custitem_av_massenheiten");
            var itemVerkaufseinheit = currRec.getValue("custitem_av_verkaufseinheit");
            var itemGrundeinheit = currRec.getValue("custitem_av_grundeinheit");
            var itemBackInStock = Boolean(currRec.getValue("custitem_av_item_backinstock"));
            var itemUNnumber = currRec.getText("custitem_msg_un_number");
            var itemParent = currRec.getText("parent") || currRec.getValue("custitem_av_parent_product_main");
            var itemComponents = _getItemComponents(currRec);

            var itemObj = {
                "number": itemNumber,
                "productNumber": itemNumber,
                "displayName": itemDisplayName || itemNumber,
                "ean": itemEan || "1",
                "active": itemIsActive,
                "prefix": _setNullAsDef(itemPrefix),
                "netBasePrice": itemNetPrice,
                "netPriceB2B": _setNullAsDef(itemNetPriceB2B),
                "netPriceUVP": _setNullAsDef(itemNetPriceUVP),
                "netPriceMinimal": _setNullAsDef(itemNetPriceMinimal),
                "taxRate": itemTaxRate,
                "productVersionKey": _setNullAsDef(itemProductType),
                "departmentKeys": [
                    itemDepartment
                ],
                "height": _setNullAsDef(itemHeight),
                "width": _setNullAsDef(itemWidth),
                "length": _setNullAsDef(itemLength),
                "weight": _setNullAsDef(itemWeight),
                "brandId": _setNullAsDef(itemBrandId),
                "topCategory": _setNullAsDef(itemTopCategory),
                "unitKey": _setNullAsDef(itemMasseinheit),
                "purchaseUnit": _setNullAsDef(itemVerkaufseinheit),
                "referenceUnit": _setNullAsDef(itemGrundeinheit),
                "itemParent": _setNullAsDef(itemPimParent),
                "erpCreatedAt": _setNullAsDef(itemErpCreatedAt),
                "contributionMargin": _setNullAsDef(itemContributionMargin),
                "brandNumber": _setNullAsDef(itemBrandNumber),
                "availability": _setNullAsDef(itemAvailability),
                "backInStock": _setNullAsDef(itemBackInStock),
                "deliveryTimes": _setNullAsDef(itemDeliveryTimes),
                "hazmatTypes": _setNullAsDef(itemUNnumber),
                "firstStorageAt": _setNullAsDef(itemFirstStorageAt),
                "lastStorageAt": _setNullAsDef(itemLastStorageAt),
                "lastEmptyStockAt": _setNullAsDef(itemLastStockAt),
                "parentProductNumber": _setNullAsDef(itemParent),
                "comboProductNumbers": _setNullAsDef(itemComponents)
            }

            return JSON.stringify(itemObj)
        }

        function _getItemUpdate(context) {

            var currRec = context.newRecord;
            var itemType = currRec.type

            var itemNumber = currRec.getValue("itemid");
            var pimItemId = Number(currRec.getValue("custitem_av_pim_product_id"));
            var itemDisplayName = currRec.getValue("displayname");
            var itemEan = currRec.getValue("upccode");
            var itemIsActive = !currRec.getValue("custitem_av_pim_product_inactive");
            var itemPrefix = currRec.getText("custitem_av_pim_item_prefix");
            var itemProductTypeId = _getRecordFieldValue("customrecord_av_pim_product_type", currRec.getValue("custitem_av_pim_product_type_id"), "custrecord_av_pim_product_type_id");
            var itemDepartmentId = _getRecordFieldValue("customrecord_av_pim_department", currRec.getValue("custitem_av_pim_department_id"), "custrecord_av_pim_department_id");
            var itemTaxRate = currRec.getValue("taxschedule") ? CONSTANTS.TAX_SCHEDULES_MAPPING[currRec.getValue("taxschedule")] : "";
            var itemNetPrice = itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(_getPriceNet(currRec)) : _getPriceGiftCert(currRec);
            var itemNetPriceB2B = itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(_getPriceB2b(currRec)) : _getPriceGiftCert(currRec);
            var itemNetPriceUVP = itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(_getPriceUVP(currRec)) : _getPriceGiftCert(currRec);
            var itemNetPriceMinimal = itemType != CONSTANTS.ITEM_TYPE.GIFT_CERTIFICATE_TYPE ? parseFloat(_getPriceMinimal(currRec)) : _getPriceGiftCert(currRec);
            var itemHeight = parseInt(currRec.getValue("custitem_msg_height"));
            var itemWidth = parseInt(currRec.getValue("custitem_msg_width"));
            var itemLength = parseInt(currRec.getValue("custitem_msg_length"));
            var itemWeight = parseFloat(currRec.getValue("custitem_av_weight"));
            var itemBrandId = parseInt(currRec.getValue("custitem_av_brand_id"));
            var itemPimParent = currRec.getValue("custitem_av_parent_pim_product_id");
            var itemDepartment = currRec.getText("custitem_av_pim_department_id");
            var itemProductType = currRec.getText("custitem_av_pim_product_type_id");
            var itemDeliveryTimes = currRec.getText("custitem_av_delivery_time");
            var itemBrandNumber = currRec.getValue("custitem_av_brand_number");
            var itemAvailability = currRec.getValue("custitem_av_item_availability");
            var itemErpCreatedAt = _formatDateFromNs(currRec.getValue("createddate"));
            var itemFirstStorageAt = _formatDateFromNs(currRec.getValue("custitem_av_item_firststorageat"));
            var itemLastStorageAt = _formatDateFromNs(currRec.getValue("custitem_av_item_laststorageat"));
            var itemLastStockAt = _formatDateFromNs(currRec.getValue("custitem_av_item_lastemptystockat"));
            var itemContributionMargin = currRec.getValue("custitem_av_item_contributionmargin");
            var itemTopCategory = currRec.getText("custitem_av_top_category");
            var itemMasseinheit = currRec.getText("custitem_av_massenheiten");
            var itemVerkaufseinheit = currRec.getValue("custitem_av_verkaufseinheit");
            var itemGrundeinheit = currRec.getValue("custitem_av_grundeinheit");
            var itemBackInStock = Boolean(currRec.getValue("custitem_av_item_backinstock"));
            var itemUNnumber = currRec.getText("custitem_msg_un_number");
            var itemParent = currRec.getText("parent") || currRec.getValue("custitem_av_parent_product_main");
            var itemComponents = _getItemComponents(currRec);


            var itemObj = {
                "productId": pimItemId,
                "productNumber": itemNumber,
                "displayName": itemDisplayName || itemNumber,
                "ean": itemEan || "1",
                "active": itemIsActive,
                "prefix": _setNullAsDef(itemPrefix),
                "netBasePrice": itemNetPrice,
                "netPriceB2B": _setNullAsDef(itemNetPriceB2B),
                "netPriceUVP": _setNullAsDef(itemNetPriceUVP),
                "netPriceMinimal": _setNullAsDef(itemNetPriceMinimal),
                "taxRate": itemTaxRate,
                "parentProductId": _setNullAsDef(itemPimParent),
                "productVersionKey": _setNullAsDef(itemProductType),
                "departmentKeys": [
                    itemDepartment
                ],
                "height": _setNullAsDef(itemHeight),
                "width": _setNullAsDef(itemWidth),
                "length": _setNullAsDef(itemLength),
                "weight": _setNullAsDef(itemWeight),
                "brandId": _setNullAsDef(itemBrandId),
                "topCategory": _setNullAsDef(itemTopCategory),
                "unitKey": _setNullAsDef(itemMasseinheit),
                "purchaseUnit": _setNullAsDef(itemVerkaufseinheit),
                "referenceUnit": _setNullAsDef(itemGrundeinheit),
                "itemParent": _setNullAsDef(itemPimParent),
                "erpCreatedAt": _setNullAsDef(itemErpCreatedAt),
                "contributionMargin": _setNullAsDef(itemContributionMargin),
                "brandNumber": _setNullAsDef(itemBrandNumber),
                "availability": _setNullAsDef(itemAvailability),
                "backInStock": _setNullAsDef(itemBackInStock),
                "deliveryTimes": _setNullAsDef(itemDeliveryTimes),
                "hazmatTypes": _setNullAsDef(itemUNnumber),
                "firstStorageAt": _setNullAsDef(itemFirstStorageAt),
                "lastStorageAt": _setNullAsDef(itemLastStorageAt),
                "lastEmptyStockAt": _setNullAsDef(itemLastStockAt),
                "parentProductNumber": _setNullAsDef(itemParent),
                "comboProductNumbers": _setNullAsDef(itemComponents)
            }

            return JSON.stringify(itemObj)
        }

        function _sendLocationToPim(context) {

            var payload = _getLocationData(context.newRecord)

            var reqHeaders = {
                "Content-type": "application/json",
                "Authorization": `Bearer ${self.connectionObj.accessToken}`
            }

            var requestUrl = self.connectionObj.custrecord_av_integration_base_url.concat(self.connectionObj.custrecord_av_integration_store_post)

            var logId = avLog.create({
                iface: self.iface,
                send: payload,
                url: requestUrl,
                state: CONSTANTS.INTERFACE_STATE.NEW
            })

            var request = https.request({
                method: CONSTANTS.INTERFACES[self.iface].onActionHttpRequest,
                url: requestUrl,
                body: payload,
                headers: reqHeaders
            })

            if (request.code == CONSTANTS.HTTP_CODE.SUCCESS_CREATE) {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.DONE,
                    receive: request.body
                })

                context.newRecord.setValue("custitem_av_item_sent_to_pim", true)
                context.newRecord.setValue("custitem_av_pim_product_id", JSON.parse(request.body).id)

            } else {
                avLog.update({
                    id: logId,
                    responseCode: request.code,
                    state: CONSTANTS.INTERFACE_STATE.ERROR,
                    receive: request.body
                })

                avLog.error_message({
                    logid: logId,
                    msg: CONSTANTS.ERROR_TYPES.INTERNAL_ERROR,
                    var1: request.body
                })
            }

            deltaTime = (new Date().getTime() - startTime) / 1000

            log.debug("Time Delta", `Send Item Time Usage: ${deltaTime}s`)
        }

        function _getLocationData(currRec) {
            var locData = {};
            _getAllResults(search.create({
                type: "location",
                filters:
                    ["internalid", "anyof", currRec.id],
                columns:
                    [
                        search.createColumn({
                            name: "name",
                            sort: search.Sort.ASC,
                            label: "Name"
                        }),
                        search.createColumn({ name: "city", label: "City" }),
                        search.createColumn({ name: "state", label: "State/Province" }),
                        search.createColumn({ name: "country", label: "Country" }),
                        search.createColumn({ name: "custrecord_av_inactive", label: "Inactive" }),
                        search.createColumn({ name: "address1", label: "Address 1" }),
                        search.createColumn({ name: "address2", label: "Address 2" }),
                        search.createColumn({ name: "zip", label: "Zip" }),
                        search.createColumn({ name: "latitude", label: "Latitude" }),
                        search.createColumn({ name: "longitude", label: "Longitude" })
                    ]
            })).every(function (result) {
                let streetS = result.getValue("address1").split(" ");
                let streetName = streetS[0];
                let streetNumber = streetS[streetS.length - 1];

                locData = {
                    "id": _setNullAsDef(currRec.id),
                    "displayName": _setNullAsDef(result.getValue("name")),
                    "street": _setNullAsDef(streetName),
                    "houseNumber": _setNullAsDef(streetNumber),
                    "zipCode": _setNullAsDef(result.getValue("zip")),
                    "city": _setNullAsDef(result.getValue("city")),
                    "countryIsoCode": _setNullAsDef(result.getValue("country")),
                    "latitude": parseFloat(result.getValue("latitude")),
                    "longitude": parseFloat(result.getValue("longitude")),
                    "active": !result.getValue("custrecord_av_inactive") ? true : false
                }

                return true;
            });

            return JSON.stringify(locData)
        }

        function _getPriceNet(currRec) {
            return currRec.getSublistValue({
                sublistId: "price1",
                fieldId: "price_1_",
                line: 0
            })
        }

        function _getPriceUVP(currRec) {
            return currRec.getSublistValue({
                sublistId: "price1",
                fieldId: "price_1_",
                line: 4
            })
        }

        function _getPriceMinimal(currRec) {
            return currRec.getSublistValue({
                sublistId: "price1",
                fieldId: "price_1_",
                line: 3
            })
        }

        function _getPriceB2b(currRec) {
            return currRec.getSublistValue({
                sublistId: "price1",
                fieldId: "price_1_",
                line: 2
            })
        }

        function _getPriceGiftCert(currRec) {
            return currRec.getSublistValue({
                sublistId: "price",
                fieldId: "price_1_",
                line: 0
            })
        }

        function _getRecordFieldValue(recType, recId, fieldName) {

            try {

                return Number(search.lookupFields({
                    type: recType,
                    id: recId,
                    columns: fieldName
                })[fieldName])

            } catch (e) {
                log.debug({
                    title: "Error",
                    details: JSON.stringify(e)
                })

                return null
            }

        }

        function _getItemStock(context) {

            var currRec = context.newRecord;
            var pimProductId = Number(currRec.getValue("custitem_av_pim_product_id"));
            var stockQty = _getStocks(currRec.id, pimProductId);

            return stockQty

        }

        function _getStocks(itemId, pimId) {
            var qtyAvailable = 0;
            var stockByLoc = [];
            var itemSearchObj = _getAllResults(search.create({
                type: "item",
                filters:
                    [
                        "internalid", "anyof", itemId
                    ],
                columns:
                    ["itemid",
                        "displayname",
                        "salesdescription",
                        "type",
                        "quantityavailable"
                    ]
            }));

            var stockS = _getAllResults(search.create({
                type: "item",
                filters:
                    [["internalid", "anyof", itemId],
                        "AND",
                    ["isinactive", "is", "F"]
                    ],
                columns:
                    [
                        search.createColumn({
                            name: "itemid",
                            sort: search.Sort.ASC,
                            label: "Name"
                        }),
                        search.createColumn({ name: "locationquantityavailable", label: "Location Available" }),
                        search.createColumn({
                            name: "name",
                            join: "inventoryLocation"
                        })
                    ]
            }))

            stockS.every(function (result) {
                stockByLoc.push({
                    productId: pimId,
                    warehouse: result.getValue({
                        name: "name",
                        join: "inventoryLocation"
                    }),
                    stock: result.getValue("locationquantityavailable") || 0,
                    buildToOrder: 0,
                    available: result.getValue("locationquantityavailable") ? true : false
                })

                return true;
            });

            return stockByLoc
        }

        function _getItems(stagingRec) {

            var itemSkus = [], filters = [], itemIds = {}, itemTypes = {};

            var stagingLines = stagingRec.getLineCount({
                sublistId: "recmachcustrecord_av_sw_order"
            })

            for (var i = 0; i < stagingLines; i++) {

                try {

                    var itemSku = stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_item", i)

                    if (itemSku) {
                        itemSkus.push(itemSku)
                    }

                } catch (e) {
                    log.debug("Error", JSON.stringify(e))
                }

            }

            for (var i = 0; i < itemSkus.length; i++) {
                if (i == 0) {
                    filters.push(["name", "is", itemSkus[i]])
                } else {
                    filters.push("OR")
                    filters.push(["name", "is", itemSkus[i]])
                }
            }

            var itemSearchObj = _getAllResults(search.create({
                type: "item",
                filters: filters,
                columns:
                    ["itemid", "parent", "type"]
            }));


            itemSearchObj.every(function (result) {

                if (result.getValue("parent")) {

                    itemIds[result.getValue("itemid").split(" ")[2]] = result.id;
                    itemTypes[result.getValue("itemid").split(" ")[2]] = result.getValue("type");

                } else {
                    itemIds[result.getValue("itemid")] = result.id;
                    itemTypes[result.getValue("itemid")] = result.getValue("type");
                }

                return true;
            });

            return {
                itemIds: itemIds,
                itemTypes: itemTypes
            }
        }

        function _getLocationsByItem(itemsByName) {

            var stockByItem = {};
            var itemInternalIds = Object.values(itemsByName)

            if (!itemInternalIds.length) return stockByItem

            var itemSearchObj = _getAllResults(search.create({
                type: "item",
                filters:
                    [
                        ["isinactive", "is", "F"],
                        "AND",
                        ["inventorylocation", "anyof", self.connectionObj.fulfillableLocations],
                        "AND",
                        ["internalid", "anyof", itemInternalIds]
                    ],
                columns:
                    ["itemid", "inventoryLocation", "locationquantityavailable"]
            }));

            itemSearchObj.every(function (result) {

                var itemId = result.id;
                var locId = result.getValue("inventoryLocation");
                var qtyAvailable = Number(result.getValue("locationquantityavailable")) || 0;

                if (!stockByItem[itemId]) {

                    stockByItem[itemId] = {
                        locId: locId,
                        qtyAvailable: qtyAvailable
                    }

                } else {

                    if (locId && (qtyAvailable > stockByItem[itemId].qtyAvailable)) {
                        stockByItem[itemId] = {
                            locId: locId,
                            qtyAvailable: qtyAvailable
                        }
                    }

                }

                return true;
            });

            return stockByItem
        }

        function _checkIfOrderIsNew(orderNumber) {

            self.oldSalesOrderId;

            var itemSearchObj = _getAllResults(search.create({
                type: "salesorder",
                filters:
                    [
                        "custbody_av_sw_order_number", "is", orderNumber
                    ],
                columns: []
            }));

            itemSearchObj.every(function (result) {

                if (result.id)
                    self.oldSalesOrderId = result.id

                return true;
            });

            if (self.oldSalesOrderId) {
                return false
            } else {
                return true
            }

        }

        function _findEntity(rec) {

            try {
                var addObj = JSON.parse(rec.getValue("custrecord_av_sw_order_ship_address"))
                var cusData = {
                    "email": rec.getValue("custrecord_av_sw_order_cx_email"),
                    "country": addObj[0].country,
                    "zip": addObj[0].zip,
                    "street": addObj[0].street,
                    "city": addObj[0].city
                }

                var searchResult = search.create({
                    type: record.Type.CUSTOMER,
                    filters: [
                        ['email', search.Operator.IS, cusData.email]
                    ],
                    columns: []
                }).run().getRange({
                    start: 0,
                    end: 1
                });

                if (searchResult.length && addObj.length) {
                    var entityIid = searchResult[0].id;
                    var orderShipCode = cusData.zip;
                    var orderStreet = cusData.street;
                    var isAddressInSys = false;

                    search.create({
                        type: record.Type.CUSTOMER,
                        filters: [
                            ['email', search.Operator.IS, cusData.email]
                        ],
                        columns: [
                            search.createColumn({
                                name: "zipcode",
                                join: "Address",
                                label: "Zip Code"
                            }),
                            search.createColumn({
                                name: "address1",
                                join: "Address",
                                label: "Address 1"
                            })]
                    }).run().each(function (result) {

                        var customerZipCode = result.getValue({
                            name: "zipcode",
                            join: "Address"
                        })

                        var customerStreet = result.getValue({
                            name: "address1",
                            join: "Address"
                        })

                        if ((customerZipCode == orderShipCode) && (customerStreet == orderStreet)) {
                            isAddressInSys = true;
                        }

                        return true
                    });

                    if (!isAddressInSys) {

                        var customerRec = record.load({
                            type: record.Type.CUSTOMER,
                            id: entityIid,
                            isDynamic: true
                        })

                        customerRec.selectNewLine({
                            sublistId: 'addressbook'
                        });

                        customerRec.setCurrentSublistValue({
                            sublistId: 'addressbook',
                            fieldId: 'defaultbilling',
                            value: true
                        })

                        customerRec.setCurrentSublistValue({
                            sublistId: 'addressbook',
                            fieldId: 'defaultshipping',
                            value: true
                        })

                        var addressSubrecord = customerRec.getCurrentSublistSubrecord({
                            sublistId: 'addressbook',
                            fieldId: 'addressbookaddress'
                        });

                        _setValue(addressSubrecord, "country", cusData.country)
                        _setValue(addressSubrecord, "zip", cusData.zip)
                        _setValue(addressSubrecord, "addr1", cusData.street)

                        if (String(cusData.phoneNumber).length > 7) {
                            _setValue(addressSubrecord, "addrphone", cusData.phoneNumber)
                        }

                        _setValue(addressSubrecord, "city", cusData.city)

                        customerRec.commitLine({
                            sublistId: 'addressbook'
                        });

                        customerRec.save({
                            enableSourcing: false,
                            ignoreMandatoryFields: true
                        })

                    }

                }

                return searchResult.length === 0 ? _createCustomer(rec) : searchResult[0].id;

            } catch (e) {

                avLog.error_message({
                    logid: self.logId,
                    msg: CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR,
                    var1: JSON.stringify(e)
                })

                return;

            }
        }

        function _createCustomer(rec) {
            var recId;
            try {

                var customerRec = record.create({
                    type: record.Type.CUSTOMER,
                    isDynamic: true
                })

                _setValue(customerRec, "isperson", "T")
                _setValue(customerRec, "firstname", rec.getValue("custrecord_av_sw_order_cx_fn"))
                _setValue(customerRec, "lastname", rec.getValue("custrecord_av_sw_order_cx_ln"))
                _setValue(customerRec, "companyname", rec.getValue("custrecord_av_sw_order_cx_cn"))
                _setValue(customerRec, "email", rec.getValue("custrecord_av_sw_order_cx_email"))
                _setValue(customerRec, "subsidiary", CONSTANTS.SUBSIDIARY.WEB_STORE_DEFAULT)

                customerRec = _setAddress(customerRec, rec)

                var recId = customerRec.save.promise({
                    enableSourcing: true,
                    ignoreMandatoryFields: true
                }).then(function (response) {

                    recId = response

                }, function (e) {

                    log.debug("Error creating customer 1", JSON.stringify(e))

                })

                return recId

            } catch (e) {

                log.debug("Error creating customer 2", JSON.stringify(e))

                return;

            }
        }
        function _setAddress(customerRec, rec) {

            var addObj = JSON.parse(rec.getValue("custrecord_av_sw_order_ship_address"))
            var cusData = {
                "email": rec.getValue("custrecord_av_sw_order_cx_email"),
                "country": addObj[0].country,
                "zip": addObj[0].zip,
                "street": addObj[0].street,
                "city": addObj[0].city,
                "swid": addObj[0].swid,
                "attention": addObj[0].attention || ""
            }

            try {

                for (var i = 0; i < addObj.length; i++) {

                    customerRec.selectNewLine({
                        sublistId: 'addressbook'
                    });

                    var addressSubrecord = customerRec.getCurrentSublistSubrecord({
                        sublistId: 'addressbook',
                        fieldId: 'addressbookaddress'
                    });

                    addressSubrecord.setValue({
                        fieldId: 'country',
                        value: cusData.country,
                    });

                    addressSubrecord.setValue({
                        fieldId: 'zip',
                        value: cusData.zip,
                    });

                    addressSubrecord.setValue({
                        fieldId: 'addr1',
                        value: cusData.street,
                    });

                    if (String(cusData.phoneNumber).length > 7) {
                        addressSubrecord.setValue({
                            fieldId: 'addrphone',
                            value: cusData.phoneNumber,
                        });
                    }

                    addressSubrecord.setValue({
                        fieldId: 'city',
                        value: cusData.city,
                    });

                    addressSubrecord.setValue({
                        fieldId: 'custrecord_av_shopware_address_id',
                        value: cusData.swid,
                    });

                    addressSubrecord.setValue({
                        fieldId: 'attention',
                        value: cusData.attention || "",
                    });

                    customerRec.commitLine({
                        sublistId: 'addressbook'
                    });

                }

            } catch (e) {

                log.debug("Error creating address", JSON.stringify(e))

            }

            return customerRec
        }

        function _getAllResults(s) {
            var results = s.run();
            var searchResults = [];
            var searchid = 0;
            do {
                var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
                resultslice.forEach(function (slice) {
                    searchResults.push(slice);
                    searchid++;
                }
                );
            } while (resultslice.length >= 1000);
            return searchResults;
        }

        function _setValue(rec, fld, val) {

            if (fld && val) {
                rec.setValue({
                    fieldId: fld,
                    value: val
                })
            }

            return rec
        }

        function _getShipCost(rec) {

            var sCost = rec.getValue("custrecord_av_sw_order_ship_amount");
            var sTax = rec.getValue("custrecord_av_sw_order_taxrate");

            if (!sCost || !sTax) return 0;

            return (sCost / (1 + (sTax / 100)))

        }

        function _formatDateFromNs(dateRaw) {

            if (!dateRaw) return null;

            var parsedDate = dateRaw ? new Date(format.parse({
                value: dateRaw,
                type: format.Type.DATE
            })) : new Date();

            return `${parsedDate.getFullYear()}-${_addZero(parsedDate.getMonth() + 1)}-${_addZero(parsedDate.getDate())} ${_addZero(parsedDate.getUTCHours())}:${_addZero(parsedDate.getMinutes())}:${_addZero(parsedDate.getSeconds())}`

        }

        function _addZero(num) {
            if (num < 10) {
                num = "0" + num;
            }
            return num;
        }

        function _getItemComponents(rec) {
            var itemSkus = [];

            if (rec.type != CONSTANTS.ITEM_TYPE.KIT_ITEM_TYPE) return itemSkus;

            var itemComponentsL = rec.getLineCount({ sublistId: "member" })

            for (var i = 0; i < itemComponentsL; i++) {
                itemSkus.push(
                    rec.getSublistValue({
                        sublistId: "member",
                        fieldId: "item_display",
                        line: i
                    })
                )
            }

            return itemSkus
        }

        function _setNullAsDef(val) {
            return !val ? null : val
        }

        return {
            onAction: onAction
        };
    })