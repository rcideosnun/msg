/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
** @version: 2.1
** @author: [Roberto Cideos]
** @dated: [20.9.2020]
** @Description: Integration between PIMCore and NetSuite 
*
*  @NApiVersion 2.1
*  @NScriptType Restlet
*/


define(['N/record', 'N/search', './av_int_log', 'N/runtime', './av_msg_pim_cfg_sv2', 'N/query'], function (record, search, avLog, runtime, settings, query) {

    var self = this;
    var { itemFields, itemUpdateMapping, itemUrlUpdateMapping } = settings;

    var CONSTANTS = {
        INTERFACES: {
            "customdeploy_interface_i08_post": {
                onActionFunction: _updateItemsUrl,
                iface: "P03",
                httpMethod: "GET"
            },
            "customdeploy_interface_i04_get": {
                onActionFunction: _getItems,
                iface: "I04",
                httpMethod: "GET"
            },
            "customdeploy_interface_i03_put": {
                onActionFunction: _updateItems,
                iface: "I03",
                httpMethod: "PUT"
            },
            "customdeploy_interface_s01_post": {
                onActionFunction: _createNewSalesOrder,
                iface: "S01",
                httpMethod: "POST"
            }
        },
        HTTP_CODE: {
            SUCCESS: 200,
            ERROR: 400
        },
        INTERFACE_STATE: {
            NEW: 1,
            ERROR: 2,
            DONE: 3
        },
        PARAMETERS: {
            pageResults: 1000
        },
        ERROR_MESSAGES: {
            DUPLICATED_ORDER: "SW1",
            MISSING_ITEMS: "SW2",
            INTERNAL_ERROR: "SW3",
            ADDRESS_ERROR: "SW4",
            MISSING_PAYMENT: "SW5",
            MISSING_EMAIL: "SW6",
            MISSING_CUSTOMER: "SW7",
            P03_ITEM_NOT_EXIST: 'PIM30001',
            P03_EX: 'PIM30002',
            P03_SUCCESS: 'PIM30003',
            P03_EMPTY_PRODUCT: 'PIM30004'
        },
        SUBSIDIARY: {
            WEB_STORE_DEFAULT: 3
        }
    }

    function _entryPoint(ctx) {
        self.errorMap = _getErrorMsgMap();
        self.logId = avLog.create({
            iface: CONSTANTS.INTERFACES[runtime.getCurrentScript().deploymentId].iface,
            state: CONSTANTS.INTERFACE_STATE.NEW
        })

        return CONSTANTS.INTERFACES[runtime.getCurrentScript().deploymentId].onActionFunction(ctx)
    }

    function _getErrorMsgMap() {
        let errors = {};
        search.create({
            type: "customrecord_av_error_messages",
            filters:
                [
                ],
            columns:
                ["name", "custrecord_av_err_code"]
        }).run().each(function (result) {

            errors[result.getValue("name")] = result.getValue("custrecord_av_err_code")

            return true
        })

        return errors
    }

    function _getItems(ctx) {
        try {
            var itemSearchResponse = [];
            var parameters = ctx;
            var taxRates = _getTaxRates();

            log.debug("-Payload logs-", JSON.stringify(ctx))
            var timestampParam = parameters.timestamp || 0
            var pageParam = parameters.page || 1
            var columns = Object.values(itemFields)
            var itemSearch = search.create({
                type: "item",
                filters: [
                    ["type", "anyof", "InvtPart"],
                    "AND",
                    ["custitem_av_update_timestamp", "greaterthanorequalto", timestampParam],
                    "AND",
                    ["custitem_av_pim_approved_item", "is", "T"],
                    "AND",
                    [["pricing.pricelevel", "anyof", "1"], "OR", ["pricing.pricelevel", "anyof", "@NONE@"]]
                ],
                columns: columns
            }).getAllResultsPaginated(pageParam)

            log.debug("Debug", `Items count: ${itemSearch.length}`)
            log.debug("Debug", `Items: ${JSON.stringify(itemSearch)}`)

            itemSearch.every(function (resultItem) {

                let itemObj = {}

                Object.keys(itemFields).map((el) => {

                    if (el == "deparmentIds") {
                        itemObj[el] = [resultItem.getValue(itemFields[el])]
                    } else if (el == "isActive") {
                        itemObj[el] = !resultItem.getValue(itemFields[el])
                    } else if (el == "prefix") {
                        itemObj[el] = resultItem.getText(itemFields[el])
                    } else {
                        itemObj[el] = resultItem.getValue(itemFields[el])
                    }
                })

                itemObj["taxRates"] = taxRates

                itemSearchResponse.push(itemObj)

                return true
            })

            itemSearchResponse = JSON.stringify(itemSearchResponse)

        } catch (e) {

            avLog.update({
                id: self.logId,
                responseCode: CONSTANTS.HTTP_CODE.ERROR,
                state: CONSTANTS.INTERFACE_STATE.ERROR,
                receive: ctx
            })

            return new Error(e)

        } finally {

            if (itemSearchResponse.length > 0) {

                avLog.update({
                    id: self.logId,
                    responseCode: CONSTANTS.HTTP_CODE.SUCCESS,
                    state: CONSTANTS.INTERFACE_STATE.DONE,
                    send: itemSearchResponse,
                    receive: ctx
                })

                return itemSearchResponse
            }
            else {

                avLog.update({
                    id: self.logId,
                    responseCode: CONSTANTS.HTTP_CODE.SUCCESS,
                    state: CONSTANTS.INTERFACE_STATE.DONE,
                    send: itemSearchResponse,
                    receive: ctx
                })

                return { Error: "No item was found" }
            }

        }
    }

    function _updateItems(ctx) {

        try {

            const body = ctx;
            const formLength = Object.keys(itemUpdateMapping).length;

            if (body) {

                var isDataIncomplete = Object.values(body).some((el) => !el)

                if (!isDataIncomplete && (Object.values(body).length == formLength)) {

                    return _handlePostSuccess(body, logId, itemUpdateMapping)

                } else {

                    return _handlePostIncomplete(body, logId, itemUpdateMapping)

                }
            }


        } catch (e) {

        }
    }

    function _updateItemsUrl(ctx) {
        try {

            const body = ctx;
            const formLength = Object.keys(itemUrlUpdateMapping).length;
            if (body) {

                if (body.productNumber) {

                    return _handlePostI07Success(body)

                } else {
                    var errorDetails = _getErrorMessage(CONSTANTS.ERROR_MESSAGES.P03_EMPTY_PRODUCT);
                    var message = errorDetails ? errorDetails.msg : '';
                    var send = JSON.stringify({
                        code: CONSTANTS.HTTP_CODE.ERROR,
                        message: message
                    })
                    avLog.update({
                        id: self.logId,
                        state: CONSTANTS.INTERFACE_STATE.ERROR,
                        send: send
                    })
                    avLog.error_message({
                        logid: self.logId,
                        var1: JSON.stringify(e),
                        msg: CONSTANTS.ERROR_MESSAGES.P03_EMPTY_PRODUCT
                    })
                    return send;
                }
            }


        } catch (e) {
            var errorDetails = _getErrorMessage(CONSTANTS.ERROR_MESSAGES.P03_EX);
            var message = errorDetails ? errorDetails.msg + ' "' + e.message + '"' : e.message;
            var send = JSON.stringify({
                code: CONSTANTS.HTTP_CODE.ERROR,
                message: message
            })
            avLog.update({
                id: self.logId,
                state: CONSTANTS.INTERFACE_STATE.ERROR,
                send: send
            })
            avLog.error_message({
                logid: self.logId,
                var1: JSON.stringify(e),
                msg: CONSTANTS.ERROR_MESSAGES.P03_EX
            })
            return send;
        }
    }

    function _getErrorMessage (name) {
        if (!name) return false;
        var errorMessageSearch = search.create({
            type: 'customrecord_av_error_messages',
            filters:
            [['name', search.Operator.IS, name]],
            columns: [
            search.createColumn({ name: 'custrecord_av_err_msg' }),
            ]
        }).run().getRange({
            start: 0,
            end: 1
        });

        return errorMessageSearch.length
                ? {
                    id: errorMessageSearch[0].id,
                    msg: errorMessageSearch[0].getValue('custrecord_av_err_msg')
                } 
                : null;
    }

    function _handlePostI07Success(body) {

        avLog.update({
            id: self.logId,
            receive: body
        })

        var itemS = _findItemByItemId(body)
        var itemId = itemS && itemS.itemId;
        var itemType = itemS && itemS.itemType;

        try {
            if (itemId && itemType) {
                var itemRecord = record.load({
                    type: itemType,
                    id: itemId,
                    isDynamic: true
                })

                itemRecord.setValue({
                    fieldId: "custitem_av_item_image_url",
                    value: body.imageUrl
                })
                var salesDescriptions = body.salesDescriptions;
                if (salesDescriptions && salesDescriptions.length) {
                    salesDescriptions.forEach(function (description) {
                        switch (description.language) {
                            case 'en_GB':
                                itemRecord.setValue({
                                    fieldId: "salesdescription",
                                    value: description.description
                                })
                                break;
                            case 'de_DE':
                                itemRecord.selectLine({
                                    sublistId: 'translations',
                                    line: 0
                                })

                                itemRecord.setCurrentSublistValue({
                                    sublistId: 'translations',
                                    fieldId: 'salesdescription',
                                    value: description.description
                                })

                                itemRecord.commitLine({
                                    sublistId: 'translations'
                                })
                                break;
                        }
                    });
                }

                itemRecord.save({
                    enableSourcing: false,
                    ignoreMandatoryFields: true
                })

                var errorDetails = _getErrorMessage(CONSTANTS.ERROR_MESSAGES.P03_SUCCESS);
                var message = errorDetails ? errorDetails.msg + ' ' + body.productNumber : '';
                var send = JSON.stringify({
                    code: CONSTANTS.HTTP_CODE.SUCCESS,
                    message: message
                })
                avLog.update({
                    id: self.logId,
                    state: CONSTANTS.INTERFACE_STATE.DONE,
                    item: itemId,
                    send: send
                })
                return send;

            } else {
                var errorDetails = _getErrorMessage(CONSTANTS.ERROR_MESSAGES.P03_ITEM_NOT_EXIST);
                var message = errorDetails ? errorDetails.msg : '';
                var send = JSON.stringify({
                    code: CONSTANTS.HTTP_CODE.ERROR,
                    message: message + ' : ' + body.productNumber
                })
                avLog.update({
                    id: self.logId,
                    state: CONSTANTS.INTERFACE_STATE.ERROR,
                    send: send
                })
                avLog.error_message({
                    logid: self.logId,
                    var1: send,
                    msg: CONSTANTS.ERROR_MESSAGES.P03_ITEM_NOT_EXIST
                })
                return send;
            }
        } catch (e) {
            var errorDetails = _getErrorMessage(CONSTANTS.ERROR_MESSAGES.P03_EX);
            var message = errorDetails ? errorDetails.msg + ' "' + e.message + '"' : e.message;
            var send = JSON.stringify({
                code: CONSTANTS.HTTP_CODE.ERROR,
                message: message
            })
            avLog.update({
                id: self.logId,
                state: CONSTANTS.INTERFACE_STATE.ERROR,
                send: send
            })
            avLog.error_message({
                logid: self.logId,
                var1: JSON.stringify(e),
                msg: CONSTANTS.ERROR_MESSAGES.P03_EX
            })
            return send;
        }
    }

    function _findItemByItemId(body) {

        var itemId, itemType;

        var itemSearch = search.create({
            type: "item",
            filters: [
                ["itemid", "is", body.productNumber],
                "AND",
                ["isinactive", "is", "F"]],
            columns: []
        }).getAllResultsFromSavedSearch()

        itemSearch.forEach(function (result) {

            itemId = result.id;
            itemType = result.recordType;
            return true;
        });

        if (!itemId) return null;

        return {
            itemId: itemId,
            itemType: itemType
        }
    }

    function _handlePostSuccess() {
        return JSON.stringify({
            message: "Success"
        })
    }

    function _handlePostIncomplete(body, logId, dataMapping) {

        var missingKeys, missingValues = [], msg;
        body = JSON.parse(body)
        var isAnyMissingKey = Object.keys(body).length < Object.keys(dataMapping).length
        var isAnyMissingValue = Object.values(body).some((el) => !el)
        log.debug("isAnyMissingKey", isAnyMissingKey)
        log.debug("isAnyMissingValue", isAnyMissingValue)

        if (isAnyMissingKey) {

            var mappingKeysCase = Object.keys(dataMapping)
            var mappingKeysBody = Object.keys(body)

            missingKeys = _compareArrays(mappingKeysBody, mappingKeysCase)

            msg = `Missing data keys: ${missingKeys}`

            avLog.update({
                id: logId,
                responseCode: CONSTANTS.HTTP_CODE.ERROR,
                state: CONSTANTS.INTERFACE_STATE.ERROR,
                send: msg,
                receive: body
            })

            return JSON.stringify({
                message: msg
            })
        } else if (isAnyMissingValue) {

            Object.keys(body).forEach(function (el) {
                if (!body[el] || body[el] == undefined || !body[el].length) {
                    missingValues.push(el)
                }
            })

            msg = `Missing data values for: ${missingValues}`

            avLog.update({
                id: logId,
                responseCode: CONSTANTS.HTTP_CODE.ERROR,
                state: CONSTANTS.INTERFACE_STATE.ERROR,
                send: msg,
                receive: body
            })

            return JSON.stringify({
                message: msg
            })
        } else {

            msg = {
                message: "Data is incorrect"
            }

            avLog.update({
                id: logId,
                responseCode: CONSTANTS.HTTP_CODE.ERROR,
                state: CONSTANTS.INTERFACE_STATE.ERROR,
                send: msg,
                receive: body
            })

            return JSON.stringify(msg)
        }
    }

    function _createNewSalesOrder(ctx) {
        try {

            const body = ctx;

            if (body) {

                avLog.update({
                    id: self.logId,
                    receive: body
                })

                return _handleCreateStaging(body)

            }


        } catch (e) {

            avLog.update({
                id: self.logId,
                responseCode: CONSTANTS.HTTP_CODE.ERROR,
                state: CONSTANTS.INTERFACE_STATE.ERROR
            })

            avLog.error_message({
                logid: self.logId,
                var1: e,
                msg: CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR
            })

            return JSON.stringify({
                code: self.errorMap[CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR],
                message: e,
                details: self.errorDetails || JSON.stringify(e)
            })
        }

    }

    function _handleCreateStaging(body) {

        try {
            log.debug("-SW Payload logs-", JSON.stringify(body))

            if (!body.data.length) return JSON.stringify({
                code: self.errorMap[CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR],
                message: "No order data found in the payload"
            });

            var countryIds = _getAllCountriesByAbb()
            var data = body.data[0];
            var priceObj = data.price;
            var orderCustomerObj = data.orderCustomer;
            var productSkus = [];

            if (!data.orderCustomer.email) {

                let msg = "Missing email on customer";

                avLog.update({
                    id: self.logId,
                    responseCode: self.errorMap[CONSTANTS.ERROR_MESSAGES.MISSING_EMAIL],
                    send: msg,
                    state: CONSTANTS.INTERFACE_STATE.ERROR
                })

                return JSON.stringify({
                    code: self.errorMap[CONSTANTS.ERROR_MESSAGES.MISSING_EMAIL],
                    message: msg
                });
            }

            if (!data.orderCustomer.firstName) {

                let msg = "Missing customer name";

                avLog.update({
                    id: self.logId,
                    responseCode: self.errorMap[CONSTANTS.ERROR_MESSAGES.MISSING_CUSTOMER],
                    send: msg,
                    state: CONSTANTS.INTERFACE_STATE.ERROR
                })

                return JSON.stringify({
                    code: self.errorMap[CONSTANTS.ERROR_MESSAGES.MISSING_CUSTOMER],
                    message: msg,
                    state: CONSTANTS.INTERFACE_STATE.ERROR
                });
            }

            if (!data.transactions) {

                let msg = "Missing payment type";

                avLog.update({
                    id: self.logId,
                    responseCode: self.errorMap[CONSTANTS.ERROR_MESSAGES.MISSING_PAYMENT],
                    msg: msg,
                    state: CONSTANTS.INTERFACE_STATE.ERROR
                })

                return JSON.stringify({
                    code: self.errorMap[CONSTANTS.ERROR_MESSAGES.MISSING_PAYMENT],
                    message: msg
                });
            }

            if (!data.transactions[0].paymentMethod.name) {

                let msg = "Missing payment type";

                avLog.update({
                    id: self.logId,
                    responseCode: self.errorMap[CONSTANTS.ERROR_MESSAGES.MISSING_PAYMENT],
                    msg: msg,
                    state: CONSTANTS.INTERFACE_STATE.ERROR
                })

                return JSON.stringify({
                    code: self.errorMap[CONSTANTS.ERROR_MESSAGES.MISSING_PAYMENT],
                    message: msg
                });
            }

            var newOrder = record.create({
                type: "customrecord_av_sw_order",
                isDynamic: true
            })

            var entityId = _findEntity(data, countryIds)
            log.debug("entity", JSON.stringify(entityId))
            _setValue(newOrder, "custrecord_av_sw_order_cx_entity", entityId)
            _setValue(newOrder, "custrecord_av_sw_order_number", data.orderNumber)
            _setValue(newOrder, "custrecord_av_sw_order_date", data.orderDate)
            _setValue(newOrder, "custrecord_av_sw_order_log", self.logId)
            _setValue(newOrder, "custrecord_av_sw_channel_id", data.salesChannelId)

            newOrder = _setPriceObjectFields(newOrder, priceObj)

            newOrder = _setOrderCustomerObjectFields(newOrder, orderCustomerObj)

            newOrder = _setOtherFields(newOrder, data)

            var orderLinesLength = data.lineItems.length

            for (var i = 0; i < orderLinesLength; i++) {

                let lineItem = data.lineItems[i]

                newOrder.selectNewLine({
                    sublistId: "recmachcustrecord_av_sw_order"
                })

                newOrder.setCurrentSublistValue({
                    sublistId: "recmachcustrecord_av_sw_order",
                    fieldId: "custrecord_av_sw_order_line_item",
                    value: lineItem.payload.productNumber || lineItem.payload.voucherCode
                })

                newOrder.setCurrentSublistValue({
                    sublistId: "recmachcustrecord_av_sw_order",
                    fieldId: "custrecord_av_sw_order_line_qty",
                    value: lineItem.quantity
                })

                newOrder.setCurrentSublistValue({
                    sublistId: "recmachcustrecord_av_sw_order",
                    fieldId: "custrecord_av_sw_order_line_type",
                    value: lineItem.type
                })

                newOrder.setCurrentSublistValue({
                    sublistId: "recmachcustrecord_av_sw_order",
                    fieldId: "custrecord_av_sw_order_line_unit_price",
                    value: lineItem.unitPrice
                })

                newOrder.setCurrentSublistValue({
                    sublistId: "recmachcustrecord_av_sw_order",
                    fieldId: "custrecord_av_sw_order_line_total_price",
                    value: lineItem.totalPrice
                })

                if (lineItem.price.taxRules.length) {
                    newOrder.setCurrentSublistValue({
                        sublistId: "recmachcustrecord_av_sw_order",
                        fieldId: "custrecord_av_sw_order_line_taxrate",
                        value: lineItem.price.taxRules[0].taxRate
                    })
                }

                newOrder.commitLine({
                    sublistId: "recmachcustrecord_av_sw_order"
                })

                if (lineItem.payload.productNumber)
                    productSkus.push(lineItem.payload.productNumber)
            }

            var allItemsExistObj = _checkIfAllItemsExist(productSkus);

            if (!allItemsExistObj.result) {
                avLog.update({
                    id: self.logId,
                    responseCode: CONSTANTS.HTTP_CODE.ERROR,
                    state: CONSTANTS.INTERFACE_STATE.ERROR
                });

                avLog.error_message({
                    logid: self.logId,
                    msg: CONSTANTS.ERROR_MESSAGES.MISSING_ITEMS,
                    var1: allItemsExistObj.details
                });

                self.errorDetails = allItemsExistObj.details;

                return JSON.stringify({
                    code: self.errorMap[CONSTANTS.ERROR_MESSAGES.MISSING_ITEMS],
                    message: "An item in the order cannot be found in NetSuite",
                    details: self.errorDetails
                })
            }

            var isOrderNewObj = _checkIfOrderIsNew(data.orderNumber);

            if (!isOrderNewObj.result) {

                avLog.update({
                    id: self.logId,
                    state: CONSTANTS.INTERFACE_STATE.DONE
                });

                _setValue(newOrder, "custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.DONE);

                _setValue(newOrder, "custrecord_av_sw_ns_transaction", self.oldSalesOrderId);

                return JSON.stringify({
                    code: self.errorMap[CONSTANTS.ERROR_MESSAGES.DUPLICATED_ORDER],
                    message: "The order is duplicated, another order with same number has already been received"
                })

            }

            newOrder.save.promise({
                enableSourcing: false,
                ignoreMandatoryFields: true
            }).then(function (response) {

                avLog.update({
                    id: self.logId,
                    responseCode: CONSTANTS.HTTP_CODE.SUCCESS,
                    state: CONSTANTS.INTERFACE_STATE.DONE,
                    staging: response
                })

                if (isOrderNewObj.result && allItemsExistObj.result) {

                    avLog.update({
                        id: self.logId,
                        state: CONSTANTS.INTERFACE_STATE.DONE
                    })

                }

            }, function (error) {

                avLog.update({
                    id: self.logId,
                    responseCode: CONSTANTS.HTTP_CODE.ERROR,
                    state: CONSTANTS.INTERFACE_STATE.ERROR
                })

                avLog.error_message({
                    logid: self.logId,
                    msg: CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR,
                    var1: JSON.stringify(error)
                })

                self.errorDetails = JSON.stringify(error);

            });

            return JSON.stringify({
                code: 200,
                message: `Order succesfully created with number ${data.orderNumber}`
            })

        } catch (e) {

            avLog.error_message({
                logid: self.logId,
                msg: CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR,
                var1: self.errorDetails || JSON.stringify(e)
            })


            self.errorDetails = JSON.stringify(JSON.stringify(e));

            return JSON.stringify({
                code: self.errorMap[CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR],
                message: "Order couldn't be created",
                details: self.errorDetails || JSON.stringify(e)
            })
        }
    }

    function _setPriceObjectFields(newOrder, priceObj) {

        _setValue(newOrder, "custrecord_av_sw_order_netprice", priceObj.netPrice)
        _setValue(newOrder, "custrecord_av_sw_order_totalprice", priceObj.totalPrice)
        _setValue(newOrder, "custrecord_av_sw_order_taxamt", priceObj.calculatedTaxes[0].tax)
        _setValue(newOrder, "custrecord_av_sw_order_taxrate", priceObj.calculatedTaxes[0].taxRate)

        return newOrder
    }

    function _setOrderCustomerObjectFields(newOrder, orderCustomerObj) {

        _setValue(newOrder, "custrecord_av_sw_order_cx_salutation", orderCustomerObj.salutation)
        _setValue(newOrder, "custrecord_av_sw_order_cx_fn", orderCustomerObj.firstName)
        _setValue(newOrder, "custrecord_av_sw_order_cx_ln", orderCustomerObj.lastName)
        _setValue(newOrder, "custrecord_av_sw_order_cx_email", orderCustomerObj.email)
        _setValue(newOrder, "custrecord_av_sw_order_cx_cn", orderCustomerObj.company)
        _setValue(newOrder, "custrecord_av_sw_order_id", orderCustomerObj.orderId)

        return newOrder
    }

    function _setOtherFields(newOrder, data) {

        _setValue(newOrder, "custrecord_av_sw_order_payment_method", data.transactions[0].paymentMethod.name)
        _setValue(newOrder, "custrecord_av_sw_order_ship_address", _getAddresses(data.addresses, data.deliveries[0].shippingOrderAddressId))
        _setValue(newOrder, "custrecord_av_sw_order_bill_address", _getAddresses(data.addresses, data.billingAddressId))
        _setValue(newOrder, "custrecord_av_sw_order_ship_add_id", data.deliveries[0].shippingOrderAddressId)
        _setValue(newOrder, "custrecord_av_sw_order_bill_add_id", data.billingAddressId)
        _setValue(newOrder, "custrecord_av_sw_order_ship_method", _getShipMethod(data.deliveries))
        _setValue(newOrder, "custrecord_av_sw_order_currency", data.currency.isoCode)
        _setValue(newOrder, "custrecord_av_sw_order_ship_amount", data.shippingTotal)
        _setValue(newOrder, "custrecord_av_sw_order_language", data.language.name)
        _setValue(newOrder, "custrecord_av_sw_order_date_time", _formatDateTime(data.orderDateTime))

        return newOrder
    }

    function _formatDateTime(dT) {
        //MSG WANTS THIS FORMAT: "2022.07.18 16:40"
        var formattedDate = new Date(dT);
        var year = formattedDate.getFullYear();
        var month = formattedDate.getMonth() + 1;
        var day = formattedDate.getDate();
        var hr = formattedDate.getHours();
        var min = formattedDate.getMinutes();
        var sec = formattedDate.getSeconds();

        return `${year}.${month}.${day} ${hr}:${min}:${sec}`
    }

    function _findEntity(body, countryIds) {

        try {
            var searchResult = search.create({
                type: record.Type.CUSTOMER,
                filters: [
                    ['email', search.Operator.IS, body.orderCustomer.email]
                ],
                columns: []
            }).run().getRange({
                start: 0,
                end: 1
            });

            if (searchResult.length && body.addresses.length) {
                var entityIid = searchResult[0].id;
                var orderShipCode = body.addresses[0].zipcode;
                var orderStreet = body.addresses[0].street;
                var isAddressInSys = false;

                search.create({
                    type: record.Type.CUSTOMER,
                    filters: [
                        ['email', search.Operator.IS, body.orderCustomer.email]
                    ],
                    columns: [
                        search.createColumn({
                            name: "zipcode",
                            join: "Address",
                            label: "Zip Code"
                        }),
                        search.createColumn({
                            name: "address1",
                            join: "Address",
                            label: "Address 1"
                        })]
                }).run().each(function (result) {

                    var customerZipCode = result.getValue({
                        name: "zipcode",
                        join: "Address"
                    })

                    var customerStreet = result.getValue({
                        name: "address1",
                        join: "Address"
                    })

                    if ((customerZipCode == orderShipCode) && (customerStreet == orderStreet)) {
                        isAddressInSys = true;
                    }

                    return true
                });

                if (!isAddressInSys) {

                    var customerRec = record.load({
                        type: record.Type.CUSTOMER,
                        id: entityIid,
                        isDynamic: true
                    })

                    customerRec.selectNewLine({
                        sublistId: 'addressbook'
                    });

                    customerRec.setCurrentSublistValue({
                        sublistId: 'addressbook',
                        fieldId: 'defaultbilling',
                        value: true
                    })

                    customerRec.setCurrentSublistValue({
                        sublistId: 'addressbook',
                        fieldId: 'defaultshipping',
                        value: true
                    })

                    var addressSubrecord = customerRec.getCurrentSublistSubrecord({
                        sublistId: 'addressbook',
                        fieldId: 'addressbookaddress'
                    });

                    _setValue(addressSubrecord, "country", body.addresses[0].country.iso)
                    _setValue(addressSubrecord, "zip", body.addresses[0].zipcode)
                    _setValue(addressSubrecord, "addr1", body.addresses[0].street)

                    if (String(body.addresses[i].phoneNumber).length > 7) {
                        _setValue(addressSubrecord, "addrphone", body.addresses[0].phoneNumber)
                    }

                    _setValue(addressSubrecord, "city", body.addresses[0].city)

                    customerRec.commitLine({
                        sublistId: 'addressbook'
                    });

                    customerRec.save({
                        enableSourcing: false,
                        ignoreMandatoryFields: true
                    })

                }

            }

            return searchResult.length === 0 ? _createCustomer(body, countryIds) : searchResult[0].id;
        } catch (e) {

            avLog.error_message({
                logid: self.logId,
                msg: CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR,
                var1: JSON.stringify(e)
            })

            return;

        }
    }

    function _createCustomer(body, countryIds) {
        var recId;
        try {

            var customerRec = record.create({
                type: record.Type.CUSTOMER,
                isDynamic: true
            })

            _setValue(customerRec, "isperson", "T")
            _setValue(customerRec, "firstname", body.orderCustomer.firstName)
            _setValue(customerRec, "lastname", body.orderCustomer.lastName)
            _setValue(customerRec, "companyname", body.orderCustomer.company)
            _setValue(customerRec, "email", body.orderCustomer.email)
            _setValue(customerRec, "subsidiary", CONSTANTS.SUBSIDIARY.WEB_STORE_DEFAULT)

            customerRec = _setAddress(customerRec, body, countryIds)

            var recId = customerRec.save.promise({
                enableSourcing: true,
                ignoreMandatoryFields: true
            }).then(function (response) {

                recId = response

            }, function (e) {

                avLog.update({
                    id: self.logId,
                    responseCode: CONSTANTS.HTTP_CODE.ERROR,
                    state: CONSTANTS.INTERFACE_STATE.ERROR
                })

                avLog.error_message({
                    logid: self.logId,
                    msg: CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR,
                    var1: JSON.stringify(e)
                })

            })

            return recId

        } catch (e) {

            avLog.error_message({
                logid: self.logId,
                msg: CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR,
                var1: JSON.stringify(e)
            })

            return;

        }
    }

    function _setAddress(customerRec, body, countryIds) {

        try {
            // Create a line in the Address sublist.

            if (!body.addresses) throw new Error()

            for (var i = 0; i < body.addresses.length; i++) {

                customerRec.selectNewLine({
                    sublistId: 'addressbook'
                });

                var addressSubrecord = customerRec.getCurrentSublistSubrecord({
                    sublistId: 'addressbook',
                    fieldId: 'addressbookaddress'
                });

                addressSubrecord.setText({
                    fieldId: 'country',
                    text: body.addresses[i].country.iso,
                });

                addressSubrecord.setValue({
                    fieldId: 'zip',
                    value: body.addresses[i].zipcode,
                });

                addressSubrecord.setValue({
                    fieldId: 'addr1',
                    value: body.addresses[i].street,
                });

                if (String(body.addresses[i].phoneNumber).length > 7) {
                    addressSubrecord.setValue({
                        fieldId: 'addrphone',
                        value: body.addresses[i].phoneNumber,
                    });
                }

                addressSubrecord.setValue({
                    fieldId: 'city',
                    value: body.addresses[i].city,
                });

                customerRec.commitLine({
                    sublistId: 'addressbook'
                });

            }

        } catch (e) {

            avLog.error_message({
                logid: self.logId,
                msg: e
            })

            return JSON.stringify({
                code: 406,
                message: "Customer couldn't be created",
                details: JSON.stringify(e)
            })
        }

        return customerRec
    }

    function _getAddresses(addresses, addressId) {

        var addressesArray = []

        for (var i = 0; i < addresses.length; i++) {

            if (addresses[i].id == addressId)
                addressesArray.push({
                    country: addresses[i].country.iso || null,
                    zip: addresses[i].zipcode || null,
                    street: addresses[i].street || null,
                    city: addresses[i].city || null,
                    swid: addresses[i].id || null,
                    attention: addresses[i].company && addresses[i].department ? `${addresses[i].company} ${addresses[i].department}` : ""
                })
        }

        return JSON.stringify(addressesArray)
    }

    function _getShipMethod(deliveries) {

        var deliveriesArray = [];

        for (var i = 0; i < deliveries.length; i++) {

            deliveriesArray.push({
                name: deliveries[i].shippingMethod ? deliveries[i].shippingMethod.name : null
            })
        }

        return JSON.stringify(deliveriesArray)
    }

    function _getTaxRates() {

        var taxRates = [];

        var salestaxitemSearchObj = search.create({
            type: "salestaxitem",
            filters:
                [
                    ["description", "contains", "standard"],
                    "OR",
                    ["description", "contains", "reduced"],
                    "OR",
                    ["description", "startswith", "zero"]
                ],
            columns:
                [
                    search.createColumn({
                        name: "name",
                        sort: search.Sort.ASC,
                        label: "Name"
                    }),
                    search.createColumn({ name: "rate", label: "Rate" })
                ]
        });

        salestaxitemSearchObj.run().each(function (result) {

            let currentTaxKeyPair = {}

            currentTaxKeyPair[result.getValue("name")] = parseFloat(result.getValue("rate"));

            taxRates.push(currentTaxKeyPair)

            return true;
        });

        return taxRates
    }

    Object.prototype.updateItem = function () {

        var updateObj = {};

        record.submitFields({
            type: "",
            id: "",
            values: ""
        })

    }

    function _methodNotImplemented(ctx) {
        return JSON.stringify({ message: 'This method has not been implemented' })
    }

    function _checkIfOrderIsNew(orderNumber) {

        self.oldSalesOrderId; //custbody_av_sw_order_number

        log.debug("orderNumber", JSON.stringify(orderNumber))

        var itemSearchObj = search.create({
            type: "salesorder",
            filters:
                [
                    "custbody_av_sw_order_number", "is", orderNumber
                ],
            columns: ["tranid"]
        }).getAllResultsFromSavedSearch()

        itemSearchObj.forEach(function (result) {
            self.oldSalesOrderId = result.id;
            self.oldSalesOrderNumber = result.getValue("tranid");
            return false;
        });

        log.debug("self.oldSalesOrderNumber", JSON.stringify(self.oldSalesOrderNumber))

        if (self.oldSalesOrderNumber) {
            return {
                details: `The order already exists in NetSuite with number: ${self.oldSalesOrderNumber}`,
                result: false
            }
        } else {
            return {
                details: null,
                result: true
            }
        }

    }

    function _checkIfAllItemsExist(skusFromShopware) {

        if (!skusFromShopware) return;

        var skusInNetSuite = [];
        var filters = [];
        //log.debug("skusFromShopware", JSON.stringify(skusFromShopware))
        for (var i = 0; i < skusFromShopware.length; i++) {
            let currItem = skusFromShopware[i];

            if (currItem) {
                if (i == 0) {
                    filters.push(["name", "is", currItem])
                } else {
                    filters.push("OR")
                    filters.push(["name", "is", currItem])
                }
            }
        }
        //log.debug("skusInNetSuite filters", JSON.stringify(filters))
        var itemSearch = search.create({
            type: "item",
            filters: filters,
            columns: "itemid"
        }).getAllResultsFromSavedSearch()

        itemSearch.forEach(function (result) {

            skusInNetSuite.push(result.getValue("itemid"))

            return true;
        });
        //log.debug("skusInNetSuite", JSON.stringify(skusInNetSuite))
        var allSkusExistObj = _areArraysEqual(skusInNetSuite, skusFromShopware)

        return allSkusExistObj

    }

    function _areArraysEqual(array1, array2) {

        var index;
        var arr = [];
        for (var i = 0; i < array2.length; i++) {
            index = array1.indexOf(array2[i]);
            if (index == -1) {
                arr.push(array2[i]);
            }
        }

        return {
            result: arr.length ? false : true,
            details: `The following sku's don't exist in NetSuite ${arr.join()}`
        }
    }

    function _setValue(rec, fld, val) {

        if (fld && val) {
            rec.setValue({
                fieldId: fld,
                value: val
            })
        }

        return rec
    }

    function _setText(rec, fld, txt) {
        if (fld && txt) {
            rec.setText({
                fieldId: fld,
                text: txt
            })
        }
        return rec
    }

    function _getAllCountriesByAbb() {

        return {
            "AT": 13,
            "DE": 57
        }

    }

    Object.prototype.getAllResultsPaginated = function (p) {

        p -= 1
        var si = parseInt(p) * parseInt(CONSTANTS.PARAMETERS.pageResults);
        var sr = this.run().getRange({ start: si, end: si + 1000 });

        return sr;
    }

    Object.prototype.getAllResultsFromSavedSearch = function (p) {

        var self = this;
        var results = self.run();
        var searchResults = [];
        var searchid = 0;
        do {
            var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
            resultslice.forEach(function (slice) {
                searchResults.push(slice);
                searchid++;
            }
            );
        } while (resultslice.length >= 1000);
        return searchResults;

    }

    return {
        get: _entryPoint,
        post: _entryPoint,
        put: _entryPoint,
        delete: _methodNotImplemented,
    };
});