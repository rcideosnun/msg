/**
** Copyright (c) 2020 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Roberto Cideos
* Date: 06.09.2022
* Send/Update Stock in PIM
* @NApiVersion 2.1
* @NScriptType MapReduceScript
*/

define(['N/search', 'N/workflow', 'N/runtime'],
    function (search, workflow, runtime) {

        const CONST = {
            WORKFLOW_CONFIG: {
                ID: "customworkflow_av_pim_send_items"
            }
        }

        function getInputData() {

            log.debug("DEBUG", `-Script Starting on ${new Date()}-`)

            return _getItems()
        }

        function map(context) {

            let itemObj = JSON.parse(context.value);
            let scriptObj = runtime.getCurrentScript();
            let wfActionId = scriptObj.getParameter({ name: "custscript_pim_task_sch_id" });
            let wfStateId = scriptObj.getParameter({ name: "custscript_pim_state_sch_id" });

            try {

                let instanceId = workflow.initiate({
                    recordType: itemObj.type,
                    recordId: itemObj.id,
                    workflowId: CONST.WORKFLOW_CONFIG.ID
                });

                workflow.trigger({
                    recordType: itemObj.type,
                    workflowId: CONST.WORKFLOW_CONFIG.ID,
                    recordId: itemObj.id,
                    workflowInstanceId: instanceId,
                    actionId: wfActionId,
                    stateId: wfStateId
                })


            } catch (e) {
                log.error('Unable to trigger WF to send items' + JSON.stringify(itemObj), JSON.stringify(e))
            }


        }

        function summarize() {
            log.debug("DEBUG", `-Script Finished on ${new Date()}-`)
        }

        function _getItems() {
            var itemsToBeProcessed = [];

            var filters = [
                [
                    ["isinactive", "is", "F"],
                    "AND",
                    ["custitem_av_pim_approved_item", "is", "T"],
                    "AND",
                    ["custitem_av_pim_item_prefix", "noneof", "@NONE@"],
                    "AND",
                    ["custitem_av_pim_product_inactive", "is", "F"],
                    "AND",
                    ["custitem_av_pim_department_id", "noneof", "@NONE@"],
                    "AND",
                    ["custitem_av_pim_product_type_id", "noneof", "@NONE@"],
                    "AND",
                    ["custitem_av_pim_product_id", "isnotempty", ""]
                ]
            ]


            _getAllResults(search.create({
                type: "item",
                filters: filters,
                columns: []
            })).every(function (result) {

                let itemObj = {
                    id: result.id,
                    type: result.recordType
                }

                itemsToBeProcessed.push(itemObj)

                return true;
            });

            log.debug("Results length sliced", JSON.stringify(itemsToBeProcessed.length))

            return itemsToBeProcessed
        }

        function _getAllResults(s) {
            var results = s.run();
            var searchResults = [];
            var searchid = 0;
            do {
                var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
                resultslice.forEach(function (slice) {
                    searchResults.push(slice);
                    searchid++;
                }
                );
            } while (resultslice.length >= 1000);
            return searchResults;
        }

        return {
            getInputData: getInputData,
            map: map,
            summarize: summarize
        };


    })