
/**
** Copyright (c) 2020 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Roberto Cideos
* Date: 05/07/2022
* Bill Shopware Orders
* @NApiVersion 2.1
* @NScriptType MapReduceScript
*/

define(['N/search', 'N/record'],
    function (search, record) {

        const CONSTANTS = {
            ORDER_STATUS: {
                BILLED : "SalesOrd:G"
            },
            LBS_TO_KGS: {
                CONVERSION_RATE: 0.453592
            }
        }

        function getInputData() {
            log.debug("DEBUG", `-Script Starting on ${new Date()}-`)

            return _getFulfillmentsToBill()
        }

        function map(context) {

            try {
                var ifObj = JSON.parse(context.value);

                log.debug("Processing element", JSON.stringify(ifObj))

                if (!ifObj.soId) return;

                var invRec = record.transform({
                    fromType: "salesorder",
                    fromId: ifObj.soId,
                    toType: "invoice"
                });
                
                invRec.setValue("entryformquerystring",`id=${ifObj.soId}&memdoc=0&transform=salesord&e=T&itemship=${ifObj.ifId}&xml=T`);

                var invId = invRec.save();

                log.debug("Invoice created with id", JSON.stringify(invId))

                record.submitFields({
                    type: "itemfulfillment",
                    id: ifObj.ifId,
                    values: {
                        custbody_av_msg_if_invoice: invId,
                        custbody_av_total_pkg_weight: ifObj.totalWeightinKgs
                    }
                })

            } catch (e) {
                log.error(e.type,e.message)
            }


        }

        function summarize() {
            log.debug("DEBUG", '-Script Finished-' + JSON.stringify(new Date()))
        }

        function _getFulfillmentsToBill() {
            var ifIds = [], uniqueIds = [];

            _getAllResults(search.create({
                type: "itemfulfillment",
                filters:
                [
                   ["type","anyof","ItemShip"], 
                   "AND", 
                   ["status","anyof","ItemShip:C"], 
                   "AND", 
                   ["createdfrom.custbody_av_sw_order_number","isnotempty",""], 
                   "AND", 
                   ["mainline","is","T"],
                   "AND", 
                   ["custbody_av_msg_if_invoice","anyof","@NONE@"],
                   "AND", 
                   ["createdfrom.status","noneof",CONSTANTS.ORDER_STATUS.BILLED]
                ],
                columns:
                ["entity", "tranid", "createdfrom", "totalpackageweightinpounds"]
             })).every(function (result) {
                if (uniqueIds.indexOf(result.id) == -1) {
                    ifIds.push({
                        soId: result.getValue("createdfrom"),
                        ifId: result.id,
                        totalWeightinKgs : result.getValue("totalpackageweightinpounds") ? _LbToKg(Number(result.getValue("totalpackageweightinpounds"))) : 0
                    });
                    uniqueIds.push(result.id)
                }
                return true;
            });

            log.debug("Fulfillment Ids to Bill", JSON.stringify(ifIds))

            return ifIds
        }

        function _getOrdersToBill() {
            var soIds = [], uniqueIds = [];

            _getAllResults(search.create({
                type: "customrecord_av_sw_order",
                filters:
                    [
                        ["created", "on", "today"],
                        "AND",
                        ["custrecord_av_sw_ns_inv", "anyof", "@NONE@"],
                        "AND",
                        ["custrecord_av_sw_ns_transaction", "noneof", "@NONE@"],
                        "AND",
                        ["custrecord_av_sw_ns_transaction.type", "anyof", "SalesOrd"]
                    ],
                columns: ["custrecord_av_sw_ns_transaction"]
            })).every(function (result) {
                if (uniqueIds.indexOf(result.getValue("custrecord_av_sw_ns_transaction")) == -1 &&
                    result.getValue("custrecord_av_sw_ns_transaction")) {
                    soIds.push({
                        id: result.getValue("custrecord_av_sw_ns_transaction"),
                        stagingId: result.id
                    });
                    uniqueIds.push(result.getValue("custrecord_av_sw_ns_transaction"))
                }
                return true;
            });

            log.debug("Order Ids to Bill", JSON.stringify(soIds))

            return soIds
        }

        /**
         * 
         * @param {Number} weightInLb 
         */

        function _LbToKg(weightInLb) {

            return weightInLb*CONSTANTS.LBS_TO_KGS.CONVERSION_RATE

        }

        function _getAllResults(s) {
            var results = s.run();
            var searchResults = [];
            var searchid = 0;
            do {
                var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
                resultslice.forEach(function (slice) {
                    searchResults.push(slice);
                    searchid++;
                }
                );
            } while (resultslice.length >= 1000);
            return searchResults;
        }

        return {
            getInputData: getInputData,
            map: map,
            summarize: summarize
        };

    })