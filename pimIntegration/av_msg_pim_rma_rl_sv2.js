/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
** @version: 2.1
** @author: [Roberto Cideos]
** @dated: [11.04.2022]
** @Description: Integration between PIMCore and NetSuite (RMA)
*
*  @NApiVersion 2.1
*  @NScriptType Restlet
*/


define(['N/record',
    'N/search',
    'N/format',
    './av_int_log',
    'N/runtime',
    './av_msg_pim_cfg_sv2'],
    function (
        record,
        search,
        format,
        avLog,
        runtime,
        settings) {

        var self = this;

        var CONSTANTS = {
            INTERFACES: {
                "customdeploy_interface_r01_post": {
                    onActionFunction: _getOrderDetails,
                    iface: "R01",
                    httpMethod: "POST"
                },
                "customdeploy_interface_r02_post": {
                    onActionFunction: _createNewRMA,
                    iface: "R02",
                    httpMethod: "POST"
                },
                "customdeploy_interface_r04_post": {
                    onActionFunction: _updateRMA,
                    iface: "R04",
                    httpMethod: "POST"
                }
            },
            HTTP_CODE: {
                SUCCESS: 200,
                ERROR: 400,
                INCOMPLETE: 407
            },
            INTERFACE_STATE: {
                NEW: 1,
                ERROR: 2,
                DONE: 3
            },
            ERROR_MESSAGES: {
                DUPLICATED_ORDER: "SW1",
                MISSING_ITEMS: "SW2",
                INTERNAL_ERROR: "RMA0009",
                RMA_NOT_CREATED: "RMA0010"
            },
            SUBSIDIARY: {
                WEB_STORE_DEFAULT: 3
            },
            RMA_VALIDITY_STATUS: {
                ISVALID: "valid_all_rma",
                ISNOTVALID: "valid_no_rma",
                UNAUTHORIZED: "invalid_authentication"
            },
            VALID_SHIP_COUNTRIES: ["DE", "AT"],
            RETURN_PERIOD_MS: "15552000000",
            RMA_STATUS: {
                DEFAULT: "open"
            }
        }

        /**
         * 
         * @param {SuiteScript Object} ctx 
         * @returns 
         */

        function _entryPoint(ctx) {
            self.iface = CONSTANTS.INTERFACES[runtime.getCurrentScript().deploymentId].iface;
            self.logId = avLog.create({
                iface: self.iface,
                state: CONSTANTS.INTERFACE_STATE.NEW
            })

            return CONSTANTS.INTERFACES[runtime.getCurrentScript().deploymentId].onActionFunction(ctx)
        }

        /**
         * 
         * @param {SuiteScript Object} ctx 
         * @returns 
         */

        function _updateRMA(ctx) {

            try {

                const body = ctx;

                if (body.orderNumber && body.email && body.rmaNumber) {

                    avLog.update({
                        id: self.logId,
                        receive: body
                    })

                    return _searchRMA(body)

                } else {

                    let res = `{
                        "code": ${CONSTANTS.HTTP_CODE.INCOMPLETE},
                        "data": "Payload is incomplete"
                    }`;

                    avLog.update({
                        id: self.logId,
                        receive: body,
                        state: CONSTANTS.INTERFACE_STATE.ERROR,
                        send: res
                    })

                    return res
                }


            } catch (e) {

                avLog.update({
                    id: self.logId,
                    responseCode: CONSTANTS.HTTP_CODE.ERROR,
                    state: CONSTANTS.INTERFACE_STATE.ERROR
                })

                avLog.error_message({
                    logid: self.logId,
                    var1: e.message ? e.message : e,
                    msg: CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR
                })

                return JSON.stringify({
                    code: 406,
                    message: e.message ? e.message : e
                })
            }

        }

        /**
         * 
         * @param {SuiteScript Object} ctx 
         * @returns 
         */

        function _searchRMA(ctx) {
            let rmaId;
            _getAllResults(search.create({
                type: "returnauthorization",
                filters:
                    [
                        ["tranid", "is", ctx.rmaNumber],
                        "AND",
                        ["taxline", "is", "F"],
                        "AND",
                        ["mainline", "is", "T"]
                    ],
                columns: []
            })).every(function (result) {
                rmaId = result.id;
                return true
            })

            if (!rmaId) {

                let res = `{
                    "code": ${CONSTANTS.HTTP_CODE.ERROR},
                    "data": "No RMA was found in NetSuite with the number ${ctx.rmaNumber}"
                }`;

                avLog.update({
                    id: self.logId,
                    receive: ctx,
                    transaction: rmaId,
                    state: CONSTANTS.INTERFACE_STATE.ERROR,
                    send: res
                })

                return res
            }

            let hasReq = ctx.labelRequest == "false" ? false : true;

            record.submitFields({
                type: "returnauthorization",
                id: rmaId,
                values: {
                    custbody_av_has_requested_label: hasReq
                }
            })

            let res = `{
                "code": ${CONSTANTS.HTTP_CODE.SUCCESS},
                "data": "RMA updated succesfully for the number ${ctx.rmaNumber}"
            }`;

            avLog.update({
                id: self.logId,
                receive: ctx,
                transaction: rmaId,
                state: CONSTANTS.INTERFACE_STATE.DONE,
                send: res
            })

            return res

        }

        /**
         * 
         * @param {SuiteScript Object} ctx 
         * @returns 
         */

        function _getOrderDetails(ctx) {

            try {

                const body = ctx;

                if (body.orderNumber && body.email) {

                    avLog.update({
                        id: self.logId,
                        receive: body
                    })

                    return _searchOrders(body)

                } else {

                    let res = `{
                        "total" : 0,
                            "status": "${CONSTANTS.RMA_VALIDITY_STATUS.UNAUTHORIZED}",
                            "data": []
                        }`;

                    avLog.update({
                        id: self.logId,
                        send: res,
                        state: CONSTANTS.INTERFACE_STATE.ERROR
                    })

                    return res
                }


            } catch (e) {

                avLog.update({
                    id: self.logId,
                    responseCode: CONSTANTS.HTTP_CODE.ERROR,
                    state: CONSTANTS.INTERFACE_STATE.ERROR
                })

                avLog.error_message({
                    logid: self.logId,
                    var1: e.message ? e.message : e,
                    msg: CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR
                })

                return JSON.stringify({
                    code: 406,
                    message: e.message ? e.message : e
                })
            }

        }

        /**
         * 
         * @param {SuiteScript Object} ctx 
         * @returns 
         */

        function _createNewRMA(ctx) {

            try {

                const body = ctx;

                if (body) {

                    avLog.update({
                        id: self.logId,
                        receive: body
                    })

                    return _handleCreateStaging(body)

                }


            } catch (e) {

                avLog.update({
                    id: self.logId,
                    responseCode: CONSTANTS.HTTP_CODE.ERROR,
                    state: CONSTANTS.INTERFACE_STATE.ERROR
                })

                avLog.error_message({
                    logid: self.logId,
                    var1: e,
                    msg: CONSTANTS.ERROR_MESSAGES.RMA_NOT_CREATED
                })

                return JSON.stringify({
                    code: 406,
                    message: e
                })
            }

        }

        /**
         * 
         * @param {Object} body 
         * @returns 
         */

        function _handleCreateStaging(body) {
            let productSkus = [];
            try {

                if (!body.rmaPosition || !body.orderNumber) return;

                var orderNumber = body.orderNumber;
                var orderEmail = body.email;

                var newRma = record.create({
                    type: "customrecord_av_rma_staging_record",
                    isDynamic: true
                })

                newRma.setValue("name", orderNumber);
                newRma.setValue("custrecord_av_rma_order_log", self.logId);
                newRma.setValue("custrecord_av_rma_email", orderEmail);
                newRma.setText("custrecord_av_rma_iface", self.iface);

                for (let rmaLine in body.rmaPosition) {

                    if (!body.rmaPosition[rmaLine].productNumber) continue;

                    newRma.selectNewLine({
                        sublistId: "recmachcustrecord_av_rma_parent"
                    })

                    newRma.setCurrentSublistValue({
                        sublistId: "recmachcustrecord_av_rma_parent",
                        fieldId: "custrecord_av_rma_l_item",
                        value: body.rmaPosition[rmaLine].productNumber
                    })

                    newRma.setCurrentSublistValue({
                        sublistId: "recmachcustrecord_av_rma_parent",
                        fieldId: "custrecord_av_rma_l_quantity",
                        value: body.rmaPosition[rmaLine].quantity
                    })

                    newRma.setCurrentSublistValue({
                        sublistId: "recmachcustrecord_av_rma_parent",
                        fieldId: "custrecord_av_rma_l_position",
                        value: rmaLine
                    })

                    newRma.setCurrentSublistValue({
                        sublistId: "recmachcustrecord_av_rma_parent",
                        fieldId: "custrecord_av_rma_l_ror",
                        value: body.rmaPosition[rmaLine].ror
                    })

                    newRma.setCurrentSublistValue({
                        sublistId: "recmachcustrecord_av_rma_parent",
                        fieldId: "custrecord_av_rma_l_reference_id",
                        value: rmaLine
                    })
                    newRma.commitLine({
                        sublistId: "recmachcustrecord_av_rma_parent"
                    })

                    if (body.rmaPosition[rmaLine].productNumber)
                        productSkus.push(body.rmaPosition[rmaLine].productNumber)
                }

                newRma.save.promise({
                    enableSourcing: false,
                    ignoreMandatoryFields: true
                }).then(function (response) {

                    avLog.update({
                        id: self.logId,
                        responseCode: CONSTANTS.HTTP_CODE.SUCCESS,
                        state: CONSTANTS.INTERFACE_STATE.DONE,
                        staging: response
                    })

                    var allItemsExistObj = _checkIfAllItemsExist(productSkus);

                    if (!allItemsExistObj.result) {
                        avLog.update({
                            id: self.logId,
                            responseCode: CONSTANTS.HTTP_CODE.ERROR,
                            state: CONSTANTS.INTERFACE_STATE.ERROR
                        })

                        avLog.error_message({
                            logid: self.logId,
                            msg: CONSTANTS.ERROR_MESSAGES.MISSING_ITEMS,
                            var1: allItemsExistObj.details,
                            staging: response
                        })
                    }

                    if (allItemsExistObj.result) {

                        avLog.update({
                            id: self.logId,
                            state: CONSTANTS.INTERFACE_STATE.DONE
                        })

                    }

                }, function (error) {

                    avLog.update({
                        id: self.logId,
                        responseCode: CONSTANTS.HTTP_CODE.ERROR,
                        state: CONSTANTS.INTERFACE_STATE.ERROR
                    })

                    avLog.error_message({
                        logid: self.logId,
                        msg: CONSTANTS.ERROR_MESSAGES.RMA_NOT_CREATED,
                        var1: JSON.stringify(error)
                    })

                });

                return _searchOrders(body)

            } catch (e) {

                avLog.error_message({
                    logid: self.logId,
                    msg: CONSTANTS.ERROR_MESSAGES.RMA_NOT_CREATED,
                    var1: JSON.stringify(e)
                })

                return JSON.stringify({
                    code: 406,
                    message: "Return couldn't be created",
                    details: JSON.stringify(e)
                })
            }
        }

        /**
         * 
         * @param {SuiteScript Object} ctx 
         * @returns 
         */

        function _methodNotImplemented(ctx) {
            return JSON.stringify({ message: 'This method has not been implemented' })
        }

        /**
         * 
         * @param {Object} body 
         * @returns 
         */

        function _searchOrders(body) {

            let res, rmaId, soItems = [], orderHasGc = false;
            self.rmaData = {};

            var soSearch = _getAllResults(search.create({
                type: "salesorder",
                filters:
                    [
                        ["custbody_av_sw_order_number", "is", body.orderNumber],
                        "AND",
                        ["email", "is", body.email],
                        "AND",
                        ["shiprecvstatus", "is", "F"],
                        "AND",
                        ["taxline", "is", "F"],
                        "AND",
                        ["shipping", "is", "F"]
                    ],
                columns: ["tranid", "trandate", "currency", "shipcountry", "item", "custcol_av_return_period", "line", "custcol_msg_display_name", "quantity", "rate", "grossamount",
                    search.createColumn({
                        name: "parent",
                        join: "item"
                    }),
                    search.createColumn({
                        name: "type",
                        join: "item"
                    }),
                    //"line.cseg_msg_brands",
                    "custcol_av_item_link",
                    "custcol_av_upc"
                ],
            }))

            soSearch.forEach(function (result) {

                self.oldSoId = result.id;
                self.oldSoDn = result.getValue("tranid");
                self.shipCountry = result.getValue("shipcountry");
                self.orderDate = _parseDateFromNetSuite(result.getValue("trandate"));
                self.currency = result.getText("currency");

                if (!self.items && result.getValue("item")) {
                    self.items = {};
                    self.items[result.getValue("item")] = result.getValue("custcol_av_return_period") ? _monthToMs(result.getValue("custcol_av_return_period")) : CONSTANTS.RETURN_PERIOD_MS
                }
                if (result.getValue("item") && Number(result.getValue("line"))) {
                    let sku = result.getValue({
                        name: "parent",
                        join: "item"
                    }) ? result.getText("item").replace(result.getText({
                        name: "parent",
                        join: "item"
                    }), "").replace(/ /g, "").replace(/:/g, "") : result.getText("item");
                    let itemType = result.getValue({
                        name: "type",
                        join: "item"
                    });
                    let skyByType = _getSku(sku, result, itemType);
                    soItems.push({
                        "id": result.getValue("line"),
                        "productId": skyByType,
                        "referenceId": skyByType,
                        "productNumber": skyByType,
                        "ean": result.getValue("custcol_av_upc"),
                        "label": result.getValue("custcol_msg_display_name"),
                        "hasExpired": _isItemExpired(result.getValue("custcol_av_return_period")),
                        "isExcluded": _isItemExcluded(itemType, result.getValue("custcol_av_return_period")),
                        "quantity": result.getValue("quantity"),
                        "unitPrice": result.getValue("rate") || result.getValue("grossamount"),
                        "totalPrice": result.getValue("grossamount"),
                        //"brandName": result.getText("line.cseg_msg_brands"),
                        "coverUrl": result.getValue("custcol_av_item_link"),
                        "reasonsOfReturn": [
                            {
                                "technicalName": "rorNotWorking"
                            },
                            {
                                "technicalName": "rorNotLiking"
                            },
                            {
                                "technicalName": "rorNotAccordingToDescription"
                            },
                            {
                                "technicalName": "rorWrongProduct"
                            },
                            {
                                "technicalName": "rorDifferentReason"
                            }
                        ]
                    })

                    if (_hasGc(itemType)) {
                        orderHasGc = true
                    }
                }

                return true;
            });

            var isNotValidRma = _isNotValidRma();

            if (!self.oldSoId) {
                let res = `{
                    "total" : 0,
                        "status": "${CONSTANTS.RMA_VALIDITY_STATUS.UNAUTHORIZED}",
                        "data": []
                    }`;

                avLog.update({
                    id: self.logId,
                    send: res,
                    state: CONSTANTS.INTERFACE_STATE.DONE
                })

                return res
            };

            if (isNotValidRma) {

                let res = `{
                    "total" : 0,
                        "status": "${CONSTANTS.RMA_VALIDITY_STATUS.ISNOTVALID}",
                        "data": []
                    }`;

                avLog.update({
                    id: self.logId,
                    send: res,
                    state: CONSTANTS.INTERFACE_STATE.DONE
                })

                return res
            };

            rmaId = _getRmaId();

            if (!rmaId) {
                return _responseNoRma(soItems, body, orderHasGc);
            } else {
                _getRmaData(rmaId);
            }

            if (!isNotValidRma) {

                if (!self.rmaNumber) {
                    let resStatus, resTotal;
                    let data = [{
                        "rma": "",
                        "lineItems": soItems,
                        "order": {
                            "orderNumber": self.oldSoDn,
                            "orderCustomer.email": body.email,
                        },
                        "reasonsOfReturn": [
                            {
                                "technicalName": "rorNotWorking"
                            },
                            {
                                "technicalName": "rorNotLiking"
                            },
                            {
                                "technicalName": "rorNotAccordingToDescription"
                            },
                            {
                                "technicalName": "rorWrongProduct"
                            },
                            {
                                "technicalName": "rorDifferentReason"
                            }
                        ]
                    }];

                    if (orderHasGc) {
                        resStatus = CONSTANTS.RMA_VALIDITY_STATUS.UNAUTHORIZED;
                        data = [];
                        resTotal = 0;
                    } else {
                        resStatus = CONSTANTS.RMA_VALIDITY_STATUS.ISVALID;
                        resTotal = 1;
                    }

                    res = `{
                        "total" : ${resTotal},
                            "status": "${resStatus}",
                            "data": ${JSON.stringify(data)}
                        }`

                } else {
                    let rmas = [], rmaLines = [];
                    var rmaList = Object.keys(self.rmaData);

                    for (var i = 0; i < rmaList.length; i++) {

                        let rmaEl = {
                            "id": self.rmaData[rmaList[i]].id,
                            "rmaNumber": self.rmaData[rmaList[i]].number,
                            "orderDateTime": self.rmaData[rmaList[i]].dateCreated,
                            "orderCurrencyIsoCode": self.rmaData[rmaList[i]].currency,
                            "orderCurrencySymbol": self.rmaData[rmaList[i]].currencySymbol,
                            "rmaState": {
                                "technicalName": self.rmaData[rmaList[i]].rmaState
                            },
                            "rmaReturnDocumentUrl": null, //where is the mapping? q 2 Lucky
                            "hasRequestedLabel": false, //where is the mapping? q 2 Lucky
                            "rmaCreatedAt": self.rmaData[rmaList[i]].dateCreated,
                            "rmaUpdatedAt": self.rmaData[rmaList[i]].lastUpdateDate, //where is the mapping? q 2 Lucky
                            /*"lineItems": self.rmaData[rmaList[i]].items,
                            "order": {
                                "orderNumber": self.oldSoDn,
                                "orderCustomer.email": body.email,
                            }*/
                        }
                        rmaLines.push(self.rmaData[rmaList[i]].items);
                        rmas.push(rmaEl);

                    };

                    let data = [{
                        "rma": rmas.length ? rmas[0] : "",
                        "lineItems": rmaLines.flat(),
                        "order": {
                            "orderNumber": self.oldSoDn,
                            "orderCustomer.email": body.email,
                        },
                        "reasonsOfReturn": [
                            {
                                "technicalName": "rorNotWorking"
                            },
                            {
                                "technicalName": "rorNotLiking"
                            },
                            {
                                "technicalName": "rorNotAccordingToDescription"
                            },
                            {
                                "technicalName": "rorWrongProduct"
                            },
                            {
                                "technicalName": "rorDifferentReason"
                            }
                        ]
                    }];

                    res = `{
                        "total" : ${rmaList.length},
                            "status": "${CONSTANTS.RMA_VALIDITY_STATUS.ISVALID}",
                            "data": ${JSON.stringify(data)}
                        }`
                }

            } else if (isNotValidRma) {

                res = `{
                    "total" : 0,
                        "status": "${CONSTANTS.RMA_VALIDITY_STATUS.ISNOTVALID}",
                        "data": []
                    }`

            } else {

                res = `{
                    "total" : 0,
                        "status": "${CONSTANTS.RMA_VALIDITY_STATUS.UNAUTHORIZED}",
                        "data": []
                    }`

            }

            avLog.update({
                id: self.logId,
                send: res,
                state: CONSTANTS.INTERFACE_STATE.DONE
            })

            return res

        }

        function _getRmaId() {
            let rmaId;
            _getAllResults(search.create({
                type: "returnauthorization",
                filters:
                    [
                        ["createdfrom.type", "anyof", "SalesOrd"],
                        "AND",
                        ["type", "anyof", "RtnAuth"],
                        "AND",
                        ["createdfrom", "anyof", self.oldSoId]
                    ]
            })).every(function (result) {

                rmaId = result.id;

                return true
            })

            return rmaId
        }

        /**
         * 
         * @param {Array} soItems 
         * @param {Object} body 
         * @returns 
         */

        function _responseNoRma(soItems, body, orderHasGc) {
            let resStatus, resTotal;
            let data = [{
                "rma": {
                    "rmaNumber": "",
                    "orderDateTime": self.orderDate,
                    "orderCurrencyIsoCode": self.currency,
                    "orderCurrencySymbol": "€",
                    "rmaState": {
                        "technicalName": CONSTANTS.RMA_STATUS.DEFAULT
                    },
                    "hasRequestedLabel": false,
                    "rmaReturnDocumentUrl": "",
                    "rmaCreatedAt": "",
                    "rmaUpdatedAt": ""
                },
                "lineItems": soItems,
                "order": {
                    "orderNumber": self.oldSoDn,
                    "orderCustomer.email": body.email,
                },
                "reasonsOfReturn": [
                    {
                        "technicalName": "rorNotWorking"
                    },
                    {
                        "technicalName": "rorNotLiking"
                    },
                    {
                        "technicalName": "rorNotAccordingToDescription"
                    },
                    {
                        "technicalName": "rorWrongProduct"
                    },
                    {
                        "technicalName": "rorDifferentReason"
                    }
                ]
            }];

            if (orderHasGc) {
                resStatus = CONSTANTS.RMA_VALIDITY_STATUS.UNAUTHORIZED;
                data = [];
                resTotal = 0;
            } else {
                resStatus = CONSTANTS.RMA_VALIDITY_STATUS.ISVALID;
                resTotal = 1;
            }

            return `{
                "total" : ${resTotal},
                    "status": "${resStatus}",
                    "data": ${JSON.stringify(data)}
                }`

        }

        /**
         * 
         * @param {String} rmaId 
         */

        function _getRmaData(rmaId) {

            _getAllResults(search.create({
                type: "returnauthorization",
                filters:
                    [
                        ["type", "anyof", "RtnAuth"],
                        "AND",
                        ["internalid", "anyof", rmaId],
                        "AND",
                        ["taxline", "is", "F"]
                    ],
                columns: [
                    "trandate",
                    "type",
                    "tranid",
                    "entity",
                    search.createColumn({
                        name: "itemid",
                        join: "item",
                        label: "Name"
                    }),
                    search.createColumn({
                        name: "displayname",
                        join: "item",
                        label: "Display Name"
                    }),
                    search.createColumn({
                        name: "type",
                        join: "item"
                    }),
                    "rate",
                    "quantity",
                    "netamount",
                    "currency",
                    "custbody_av_rma_status",
                    "line",
                    "custbody_av_rma_return_reason",
                    "custcol_av_item_brand",
                    "custcol_av_item_link",
                    "lastmodifieddate",
                    "custcol_av_return_of_reason",
                    "custcol_av_upc"
                ]
            })).forEach(function (result) {

                self.rmaNumber = result.getValue("tranid")

                if (!self.rmaData[self.rmaNumber] && result.getValue("line") == 0) {

                    self.rmaData[self.rmaNumber] = {
                        id: result.id,
                        number: self.rmaNumber,
                        dateCreated: new Date(result.getValue("trandate")),
                        lastUpdateDate: new Date(result.getValue("lastmodifieddate")),
                        currency: result.getText("currency"),
                        currencySymbol: "€",
                        rmaState: result.getText("custbody_av_rma_status"),
                        returnReasonId: result.getValue("custbody_av_rma_return_reason"),
                        returnReasonText: result.getText("custbody_av_rma_return_reason"),
                        items: []
                    }

                } else {

                    self.rmaData[self.rmaNumber].items.push({
                        id: result.getValue("line"),
                        productNumber: result.getValue({ name: "itemid", join: "item" }),
                        referenceId: result.getValue({ name: "itemid", join: "item" }),
                        ean: result.getValue("custcol_av_upc"),
                        label: result.getValue({ name: "displayname", join: "item" }),
                        hasExpired: _isItemExpired(result.getValue("custcol_av_return_period")),
                        isExcluded: _isItemExcluded(result.getValue({
                            name: "type",
                            join: "item"
                        }), result.getValue("custcol_av_return_period")),
                        quantity: Math.abs(result.getValue("quantity")),
                        unitPrice: Math.abs(result.getValue("rate")),
                        totalPrice: Math.abs(result.getValue("netamount")),
                        brandName: result.getText("custcol_av_item_brand"),
                        coverUrl: result.getValue("custcol_av_item_link"),
                        reasonsOfReturn:
                        {
                            "technicalName": [
                                {
                                    "id": result.getValue("custcol_av_return_of_reason"),
                                    "technicalName": result.getText("custcol_av_return_of_reason")
                                }
                            ]
                        }
                    })

                }

                return true;
            });

        }

        function _isNotValidRma() {

            try {

                var rP = _searchRetPeriod();

                if (CONSTANTS.VALID_SHIP_COUNTRIES.indexOf(self.shipCountry) == -1) return true;

                if ((new Date().getTime() - self.orderDate.getTime()) > rP) return true;

                return false

            } catch (e) {
                log.debug("Error RMA Validity", JSON.stringify(e))

                return false
            }

        }

        /**
         * 
         * @param {SuiteScript Object Search} body 
         * @returns 
         */

        function _getAllResults(s) {
            var results = s.run();
            var searchResults = [];
            var searchid = 0;
            do {
                var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
                resultslice.forEach(function (slice) {
                    searchResults.push(slice);
                    searchid++;
                }
                );
            } while (resultslice.length >= 1000);
            return searchResults;
        }

        /**
         * 
         * @param {String} dateNotFormatted 
         * @returns 
         */

        function _parseDateFromNetSuite(dateNotFormatted) {

            if (!dateNotFormatted) return null;

            var parsedDate = dateNotFormatted ? format.parse({
                value: dateNotFormatted,
                type: format.Type.DATE
            }) : null;

            return parsedDate

        }

        function _searchRetPeriod() {

            var subS = search.lookupFields({
                type: "subsidiary",
                id: CONSTANTS.SUBSIDIARY.WEB_STORE_DEFAULT,
                columns: "custrecord_av_return_period_global"
            }).custrecord_av_return_period_global;

            return subS ? _monthToMs(subS) : CONSTANTS.RETURN_PERIOD_MS
        }

        function _monthToMs(val) {
            if (!val) return CONSTANTS.RETURN_PERIOD_MS;
            return val * 24 * 60 * 60 * 1000
        }

        /**
         * 
         * @param {String} email 
         * @returns 
         */

        function _findEntity(email) {

            if (!email) return;

            var searchResult = search.create({
                type: record.Type.CUSTOMER,
                filters: [
                    ['email', search.Operator.IS, email]
                ],
                columns: []
            }).run().getRange({
                start: 0,
                end: 1
            });

            return searchResult.length ? searchResult[0].id : null;
        }

        /**
         * 
         * @param {Array} skusFromShopware 
         * @returns 
         */

        function _checkIfAllItemsExist(skusFromShopware) {

            if (!skusFromShopware) return;

            var skusInNetSuite = [];
            var filters = [];
            for (var i = 0; i < skusFromShopware.length; i++) {
                let currItem = skusFromShopware[i];

                if (currItem) {
                    if (i == 0) {
                        filters.push(["name", "is", currItem])
                    } else {
                        filters.push("OR")
                        filters.push(["name", "is", currItem])
                    }
                }
            }
            var itemSearch = _getAllResults(search.create({
                type: "item",
                filters: filters,
                columns: "itemid"
            }))

            itemSearch.forEach(function (result) {

                skusInNetSuite.push(result.getValue("itemid"))

                return true;
            });
            var allSkusExistObj = _areArraysEqual(skusInNetSuite, skusFromShopware)

            return allSkusExistObj

        }

        /**
         * 
         * @param {Array} array1 
         * @param {Array} array2
         * @returns 
         */

        function _areArraysEqual(array1, array2) {

            var index;
            var arr = [];
            for (var i = 0; i < array2.length; i++) {
                index = array1.indexOf(array2[i]);
                if (index == -1) {
                    arr.push(array2[i]);
                }
            }

            return {
                result: arr.length ? false : true,
                details: `The following sku's don't exist in NetSuite ${arr.join()}`
            }
        }

        /**
         * 
         * @param {String} rP 
         * @returns 
         */

        function _isItemExpired(rP) {
            if ((new Date().getTime() - self.orderDate.getTime()) > _monthToMs(rP)) {
                return true
            } else {
                return false
            }
        }

        /**
         * 
         * @param {String} itemType 
         * @param {String} rP 
         * @returns 
         */

        function _isItemExcluded(itemType, rP) {

            if ((new Date().getTime() - self.orderDate.getTime()) > _monthToMs(rP)) {
                return true
            }

            if (itemType == "GiftCert" || itemType == "Discount") {
                return true
            }

            return false
        }

        /**
         * 
         * @param {String} itemType 
         * @returns 
         */

        function _hasGc(itemType) {

            if (itemType == "GiftCert" || itemType == "Discount") {
                return true
            }

            return;
        }

        /**
         * 
         * @param {String} sku 
         * @param {SuiteScript Search Result Object} result 
         * @param {String} itemType 
         * @returns 
         */

        function _getSku(sku, result, itemType) {

            if (itemType == "GiftCert" || itemType == "Discount") {

                let rec = record.load({ type: "salesorder", id: result.id });

                let idx = rec.findSublistLineWithValue({
                    sublistId: "item",
                    fieldId: "line",
                    value: result.getValue("line")
                })

                return record.load({ type: "salesorder", id: result.id }).getSublistValue({
                    sublistId: "item",
                    fieldId: "description",
                    line: idx
                })
            } else {
                return sku
            }
        }

        return {
            get: _entryPoint,
            post: _entryPoint,
            put: _entryPoint,
            delete: _methodNotImplemented,
        };
    });