/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 ** @author: [Tuba Mohsin]
 ** @dated: [25.04.2023]
 ** @Description: workflow to calculate delivery time
 *
 *  @NApiVersion 2.1
 *  @NScriptType WorkflowActionScript
 */
define([
  "N/runtime",
  "./av_msg_pim_item_lib",
], function (
  runtime,
  lib
) {
  /**
   *
   * @param {Object} context
   * @returns
   */

  const onAction = (context) => {
    try {
      const itemId = context.newRecord.id;
      const itemType = context.newRecord.type;
      log.debug('Worflow Triggered', `Calculating Delivery time for item: ${itemId}`);
      lib.handleDeliveryTimeRequest([{ id: itemId, type: itemType || null }]);

    } catch (e) {
      log.debug("Error", e);
    }
  }

  return {
    onAction: onAction,
  };
});
