/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 ** @author: [Tuba Mohsin]
 ** @dated: [01.02.2023]
 ** @Description: resends the error request in the integration log
 *
 *  @NApiVersion 2.1
 *  @NScriptType WorkflowActionScript
 */
define([
  "./av_msg_pim_item_lib",
  "./av_msg_pim_item_update_rl",
  "./av_int_log.js",
  "./../shopware/av30_shop_order_sync_mr",
  "./../shopware/av30_shop_order_voucher_rl",
  "./../shopware/libraries/av30_shop_order_tracking_lib"
], function (
  lib,
  itemUpdateRL,
  avLog,
  shopOrder,
  shopVoucher,
  shopOrderTracking
) {

  let deltaTime, startTime;
  const outbound_interfaces = ['P01', 'P02', 'P04', 'S02'];
  /**
   *
   * @param {Object} context
   * @returns
   */
  const onAction = (context) => {
    try {
      startTime = new Date().getTime();

      const record = context.newRecord;
      const iface = record.getText('custrecord_av_integration_type');
      var logId = record.id;
      log.debug('Worflow Triggered', `Resending request for interface: ${iface} for log: ${logId}`);

      const payload =
        outbound_interfaces.includes(iface)
          ? parseJSON(record.getText("custrecord_av_integration_send"))
          : parseJSON(record.getText("custrecord_av_integration_received"));

      const requestURL = record.getText("custrecord_av_integration_url");

      resendLogs(iface, payload, requestURL, logId);

      deltaTime = (new Date().getTime() - startTime) / 1000;
      log.debug("Time Delta", `Time Usage: ${deltaTime}s`);

    } catch (e) {
      log.debug("Error when Resending", e);
      avLog.error_message({
        logid: logId,
        // msg: 'PIM00001', // do we need to create any specific type for this error?
        var1: `Error when Resending: ${e}`,
      });
    }
  }

  const resendLogs = (iface, payload, requestUrl, logId) => {
    switch (iface) {
      case 'P01':
      case 'P02':
      case 'P04':
        lib.sendData(iface, payload, logId);
        break;
      case 'P03':
        itemUpdateRL.updateItemDetails(payload, logId);
      case 'S01':
        shopOrder.handleResendLog(payload, iface, logId);
        break;
      case 'S02':
        payload ? shopOrderTracking.sendData(iface, payload, logId) :
          shopOrderTracking.sendUpdateStatusRequest(iface, requestUrl, logId, true)
        break;
      case 'S03':
        shopVoucher.handleGetRequest(payload, logId);
        break;
      default:
        log.audit('resend logs', `method not defined for interface: ${iface}`);
        break;
    }
  }

  const parseJSON = (payload) => {
    try {
      if (!payload) return;
      if (typeof payload == 'object') return payload;
      payload = payload.replace('<p>', '').replace('</p>', '')
      return JSON.parse(payload);
    } catch (e) {
      log.audit('error in parsing json', e);
      throw e;
    }
  }

  return {
    onAction: onAction,
  };
});
