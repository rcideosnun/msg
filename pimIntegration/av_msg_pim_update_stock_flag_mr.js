/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tuba Mohsin
* Date: 28.03.2023
* This script is a fallback to stock update UE script
* If a transaction has more item lines, M/R will be triggered.
* @NApiVersion 2.1
* @NScriptType MapReduceScript
*/

define([
    "N/search",
    "N/runtime",
    "N/record"
], function (search, runtime, record) {


    const getInputData = () => {
        log.audit("getInputData", `-Script Started on ${new Date()}-`);
        try {
            const currentRuntime = runtime.getCurrentScript();
            const itemIds = currentRuntime.getParameter({ name: "custscript_itemids" });
            log.audit('itemIds length', itemIds.length);
            return getItemsToFlag(itemIds);
        } catch (ex) {
            log.error({ title: "getInputData : ex", details: ex });
        }
    };

    const map = (context) => {
        try {
            const item = JSON.parse(context.value);
            log.debug('triggered map for =', item);
            if (item && item.id && item.type) {
                record.submitFields({
                    type: item.type,
                    id: item.id,
                    values: {
                        custitem_msg_pim_request_sync_stock: true,
                        custitem_msg_pim_calc_deliverytime: true,
                        custitem_av_pos_req_stock_sync: true
                    }
                })
            }
        } catch (e) {
            log.error('error in map', e);
        }
    };


    const summarize = () => {
        log.audit({ title: "summarize", details: "triggered" });
        log.audit("summarize", `-Script Finished on ${new Date()}-`);
    };

    const getItemsToFlag = (itemIds) => {
        if (itemIds) {
            itemIds = JSON.parse(itemIds);
        }
        const itemDetails = [];
        const searchObj = search.create({
            type: search.Type.ITEM,
            filters:
                [['internalid', 'anyof', itemIds]],
            columns: []
        });

        let myPagedData = searchObj.runPaged({ pageSize: 1000 });
        myPagedData.pageRanges.forEach(function (pageRange) {
            let myPage = myPagedData.fetch({ index: pageRange.index });
            myPage.data.forEach(function (result) {
                itemDetails.push({ id: result.id, type: result.recordType });
            });
        });
        log.audit('itemDetails length', itemDetails.length);
        return itemDetails;
    };

    return {
        getInputData: getInputData,
        map: map,
        summarize: summarize,
    };
});
