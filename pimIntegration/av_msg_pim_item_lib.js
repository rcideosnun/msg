/**
 ** Copyright (c) 2015 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 ** @author: Tuba Mohsin
 ** @dated: 26.01.2023
 ** @Description: This library contains functions related to PIM item interface
 *
 * *@NApiVersion 2.1
 */
define(["N/search", "./av_msg_config_lib", "./av_int_log.js", "N/https", "N/format", "N/record"], function (search, settings, avLog, https, format, record) {

  const CONSTANTS_LIB = {
    MSG_ITEM_STATUS: {
      EOL: '2',
      Keine_Lagerware: '4',
      Lagerabbau: '10',
    },
    DELIVERY_TEXT: {
      sofort_lieferbar: 1,
      lieferbar_3_4_Werktagen: 2,
      lieferbar_ab: 3,
      wird_bestellt: 4,
      nicht_online: 5,
      beim_Lieferanten_bestellt: 6
    },
    INTERFACE_STATE: {
      NEW: 1,
      ERROR: 2,
      DONE: 3,
    },
    ERROR_TYPES: {
      MISSING_DISPLAY_NAME: "PIM10001",
      MISSING_PARENT: "PIM10003",
      MISSING_PRICE: "PIM20003",
    },
    RECORD_TYPE: {
      InvtPart: 'inventoryitem',
      Kit: 'kititem',
      GiftCert: 'giftcertificateitem',
      Service: 'serviceitem',
      Assembly: 'assemblyitem',
      Discount: 'discountitem',
      SerialItem: 'serializedinventoryitem',
      NonInvtPart: 'noninventoryitem',
    },
    NON_INV_ITEM: ['giftcertificateitem', 'serviceitem', 'noninventoryitem'],
    KIT_ITEM: ['kititem'],
    PIM_CONFIG: '1', // TODO: hardcoding internal id for now to proceed with high priority task, need to change it to name
  };
  var self = this;

  /**
   * This fnc prepares the payload and send request to PIM
   * @param {Array} items - list of item {itemIds, itemType}
   * @param {String} iface - interface key (P01, P02 or P04)
   * @param {String} configId 
   * @returns 
   */
  const handleRequest = (item, iface) => {
    try {
      self.interface = getInterface(iface);
      const itemIds = item.map(i => i.id);
      if (!itemIds || !itemIds.length > 0) return;

      var logId = avLog.create({
        iface: iface,
        state: CONSTANTS_LIB.INTERFACE_STATE.NEW,
        item: itemIds[0]
      })

      var { payload, itemMap } = self.interface.getPayloadFunction(itemIds, self.interface, item, logId);
      log.audit(`payload to be sent to PIM for interface: ${iface}`, payload);

      if (self.interface['handleErrorFunction']) payload = self.interface.handleErrorFunction(payload, logId);
      if (!payload || !payload.length > 0) return;
      return sendData(iface, payload, logId, itemMap);

    } catch (ex) {
      avLog.update({
        id: logId,
        send: payload ? JSON.stringify(payload) : null,
        state: CONSTANTS_LIB.INTERFACE_STATE.ERROR,
      });
      avLog.error_message({
        logid: logId,
        msg: interface.technical_error,
        var1: ex,
      });
      log.error({ title: "handleRequest : ex", details: ex });
    }
  }

  /**
   * This fnc sends request to PIM
   * @param {String} iface - interface key (P01, P02 or P04)
   * @param {Array} payload - expected request body 
   * @param {String} logId  - Interface logId to update the log
   */
  const sendData = (iface, payload, logId, itemMap = null) => {
    let interface = self.interface ? self.interface : getInterface(iface);
    try {
      const requestObj = {};
      const connectionObj = new settings(CONSTANTS_LIB.PIM_CONFIG, false, interface.connectionRequired, logId);
      requestObj.url = connectionObj.custrecord_av_integration_base_url.concat(connectionObj[interface.request_url_fieldId]);
      requestObj.method = interface.onActionHttpRequest;
      const request = https.request({
        method: requestObj.method,
        url: requestObj.url,
        body: JSON.stringify(payload),
        headers: {
          "Content-type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${connectionObj.accessToken}`,
        },
      });
      interface.handleResponseFunction(payload, request, logId, interface, requestObj, itemMap);
    } catch (ex) {
      log.audit({ title: "sendData : ex", details: ex });
      avLog.update({
        id: logId,
        send: payload ? JSON.stringify(payload) : null,
        state: CONSTANTS_LIB.INTERFACE_STATE.ERROR,
      });
      avLog.error_message({
        logid: logId,
        msg: interface.technical_error,
        var1: ex,
      });

    }
  }

  /**
   * This fnc handles generice response handling for all interfaces
   */
  const genericResponseHandling = (payload, response, logId, interface, requestObj) => {
    const body = JSON.parse(response.body);
    log.audit('response body', body);
    let successOrFailure = { success: false, fail: false };
    if (body.success && (body.success?.length > 0 || Object.keys(body.success).length > 0)) {
      avLog.update({
        id: logId,
        url: requestObj.url,
        send: JSON.stringify(payload),
        receive: response.body,
        responseCode: response.code,
        state: CONSTANTS_LIB.INTERFACE_STATE.DONE,
      });
      successOrFailure.success = true;
    }
    if (body.failed && body.failed?.length > 0) {
      const message = interface.internal_error;
      body.failed.forEach((err) => {
        let concatenatedErrorMsg = `Error for product ${err?.productNumber} - `;
        if (err.errors && err.errors.length > 0) {
          concatenatedErrorMsg += err.errors.map((u) => `${u.property}: ${u.message}`).join(", ");
        }
        avLog.update({
          id: logId,
          url: requestObj.url,
          send: JSON.stringify(payload),
          responseCode: response.code,
          state: CONSTANTS_LIB.INTERFACE_STATE.ERROR,
          receive: response.body,
        });

        avLog.error_message({
          logid: logId,
          msg: message,
          var1: concatenatedErrorMsg,
        });
      });

      successOrFailure.fail = true;
    }
    // fallback error: if there's any error due to request method
    // there is no failed array
    if (!successOrFailure.fail && !successOrFailure.success) {
      avLog.update({
        id: logId,
        send: JSON.stringify(payload),
        responseCode: response.code,
        responseCode: response.code,
        url: requestObj.url,
        state: CONSTANTS_LIB.INTERFACE_STATE.ERROR,
        receive: response.body
      })
      avLog.error_message({
        logid: logId,
        msg: interface.internal_error,
        var1: response.body
      })
    }
    return successOrFailure;
  }

  /**
   * This fnc perform additional handling of the response for P01 interface
   */
  const handleResponseP01 = (payload, response, logId, interface, requestObj, itemIdMap) => {
    let res = genericResponseHandling(payload, response, logId, interface, requestObj);
    if (res.success) {
      const body = JSON.parse(response.body);
      let successItems = Object.keys(body.success);
      if (!itemIdMap) itemIdMap = getItemMap(successItems);
      successItems.forEach(item => {
        log.audit('success item', item);
        if (itemIdMap && itemIdMap[item]) updateTimestampAndFlag('custitem_msg_time_sent_pim', 'custitem_msg_pim_request_sync', item, itemIdMap);
      })
    }
    return res;
  }

  /**
   * This fnc perform additional handling of the response for P02 interface
   */
  const handleResponseP02 = (payload, response, logId, interface, requestObj, itemIdMap) => {
    let res = genericResponseHandling(payload, response, logId, interface, requestObj);
    if (res.success) {
      const body = JSON.parse(response.body);
      let successItems = body.success;
      if (!itemIdMap) itemIdMap = getItemMap(successItems);
      successItems.forEach(item => {
        log.audit('item', item);
        log.audit('item', itemIdMap);
        if (itemIdMap && itemIdMap[item]) updateTimestampAndFlag('custitem_msg_time_sent_pim_price', 'custitem_msg_pim_request_sync_price', item, itemIdMap);
      })
    }
    return res;
  }

  /**
   * This fnc perform additional handling of the response for P04 interface
   */
  const handleResponseP04 = (payload, response, logId, interface, requestObj, itemIdMap) => {
    let res = genericResponseHandling(payload, response, logId, interface, requestObj);
    if (res.success) {
      const body = JSON.parse(response.body);
      let successItems = body.success;
      if (!itemIdMap) itemIdMap = getItemMap(successItems);
      successItems.forEach(item => {
        if (itemIdMap && itemIdMap[item]) updateTimestampAndFlag('custitem_msg_time_sent_pim_stock', 'custitem_msg_pim_request_sync_stock', item, itemIdMap);
      })
    }
    return res;
  }

  /**
   * This function is only used when lib is triggered from resend interface workflow
   * This map is used to update the timestamp/flag of the successful items
   */
  const getItemMap = (itemNames) => {
    let filters = [];
    let itemIdMap = {};
    itemNames.forEach(name => {
      if (filters.length > 0) filters.push('OR');
      filters.push(["nameinternal", "is", name]);
    })
    const itemSearchObj = search.create({
      type: "item",
      filters,
      columns:
        [
          'type',
          'internalid',
          'itemid',
          'parent'
        ]
    });
    let myPagedData = itemSearchObj.runPaged({ pageSize: 1000 });
    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });
      myPage.data.forEach(function (result) {
        log.audit('result', result);
        let name = result.getValue({ name: "itemid" });
        if (result.getValue({ name: "parent" })) name = splitItemIds(name); //name.split(" ")[2]
        itemIdMap[name] = { id: result.getValue({ name: "internalid" }), type: result.recordType };
      });
    });

    log.audit('getItemMap', itemIdMap);
    return itemIdMap;
  }

  /**
   * This fnc updates the timestamp of success items
   */
  const updateTimestampAndFlag = (timestampId, flagId, itemId, itemMap) => {
    // This needs to be done because we compare the modified date to this date in order to sync the item.
    // Since we are updating the record by setting this value, it always changes the last modified date
    // which will always be higher than this date.
    // If you are reading this and have a better solution, please recommend it to me (Tehseen Ahmed)

    // calculating time seperately for each item in a list to ensure secs calculation is correct
    log.audit('updateTimestampAndFlag', `updating timestamp and flag for item: ${itemId}`);
    const today = new Date();
    const formattedDate = format.format({
      value: today,
      type: format.Type.DATETIME,
    });
    const lastIndex = formattedDate.lastIndexOf(":");
    const timeBeforeSecond = formattedDate.slice(0, lastIndex) + ":59";
    const parsedDate = format.parse({
      value: timeBeforeSecond,
      type: format.Type.DATETIME,
    });

    if (itemMap[itemId]) {
      record.submitFields({
        id: itemMap[itemId].id,
        type: itemMap[itemId].type,
        values: {
          [timestampId]: parsedDate,
          [flagId]: false
        },
      });
    }
  };

  /**
   * This fnc handles error for P01 interface
   */
  const handleErrorsP01 = (payloads, logId) => {
    const validatedPayload = [];
    log.audit('handleErrorsP01', 'Triggered')
    payloads.forEach((payload) => {
      if (payload && !payload['displayName']) {
        avLog.update({
          id: logId,
          send: JSON.stringify(payloads),
          state: CONSTANTS_LIB.INTERFACE_STATE.ERROR,
        });
        avLog.error_message({
          logid: logId,
          msg: CONSTANTS_LIB.ERROR_TYPES.MISSING_DISPLAY_NAME,
          var1: JSON.stringify(payloads),
        });

      } else if (payload && payload['itemType'] == "kititem" && !payload['parentProductNumber']) {
        avLog.update({
          id: logId,
          send: JSON.stringify(payloads),
          state: CONSTANTS_LIB.INTERFACE_STATE.ERROR,
        });

        avLog.error_message({
          logid: logId,
          msg: CONSTANTS_LIB.ERROR_TYPES.MISSING_PARENT,
          var1: JSON.stringify(payloads),
        });
      } else {
        delete payload.itemType; delete payload.lastSyncDate;
        validatedPayload.push(payload);
      }
    })
    log.audit('validatedPayload', validatedPayload);
    return validatedPayload
  }

  /**
   * This fnc handles error for P02 interface
   */
  const handleErrorsP02 = (payloads, logId) => {
    if (!payloads || !payloads.length > 0) {
      avLog.update({
        id: logId,
        send: JSON.stringify(payloads),
        state: CONSTANTS_LIB.INTERFACE_STATE.ERROR,
      });
      avLog.error_message({
        logid: logId,
        msg: CONSTANTS_LIB.ERROR_TYPES.MISSING_PRICE,
        var1: JSON.stringify(payloads),
      });
    }
    return payloads
  }

  const splitItemIds = (itemId) => {
    return itemId.indexOf(' : ') > -1 ? itemId.split(' : ').pop() : itemId;
  }
  /**
   * This fnc returns the payload for P01 interface
   */
  const getPayloadP01 = (itemIds) => {
    log.audit("getPayloadP01", "Triggered");
    let payload = [];
    const { data, itemMap } = getItemDetails(itemIds);

    Object.keys(data).forEach((key) => {
      let item = data?.[key]?.[0] || null;
      if (!item) return;
      let itemType = item.recordType;
      let subsidiaryKeys = item.subsidiary?.[0]?.text;
      subsidiaryKeys = subsidiaryKeys.indexOf(' : ') > -1 ? subsidiaryKeys.split(' : ').pop() : subsidiaryKeys;
      let hazmatTypeKeys = [];

      item?.custitem_msg_un_number.forEach(element => {
        hazmatTypeKeys.push(element.text);
      });

      let parent = splitItemIds(item['parent.itemid']);

      let msgMainProduct = splitItemIds(item['CUSTITEM_MSG_MAIN_PRODUCT_KIT.itemid']);
      let itemParent = parent ? parent : msgMainProduct;

      if (itemType == 'kititem') {
        itemParent = msgMainProduct;
      }

      payload.push({
        productNumber: item.itemid,
        displayName: item.displayname || item.itemid,
        ean: setNullAsDef(item.upccode),
        subsidiaryKeys: typeof subsidiaryKeys == "string" ? [subsidiaryKeys] : subsidiaryKeys,
        itemGroupType: item.cseg_msg_igt?.[0]?.text,
        brand: setNullAsDef(item.cseg_msg_brands?.[0]?.text),
        taxScheduleKey: item.taxschedule?.[0]?.text,
        isActive: item.custitem_av_is_shop_online,
        brandProductNumber: setNullAsDef(item.custitem_av_vendor_item_number || item.vendorname),
        hazmatTypeKeys: setNullAsDef(hazmatTypeKeys),
        height: setNullAsDef(parseInt(item.custitem_msg_height)),
        width: setNullAsDef(parseInt(item.custitem_msg_width)),
        length: setNullAsDef(parseInt(item.custitem_msg_length)),
        weight: setNullAsDef(item.weight ? parseFloat(item.weight) : null),
        refUnit: setNullAsDef(parseInt(item.custitem_av_ref_unit)),
        purchaseUnit: setNullAsDef(parseInt(item.custitem_av_purchase_unit)),
        unitKey: setNullAsDef(item.custitem_av_unit_key?.[0]?.text),
        productType: item.custitem_msg_product_type?.[0]?.text || "",
        parentProductNumber: setNullAsDef(itemParent),
        comboProductNumbers: setNullAsDef(getItemComponents(data?.[key])),
        lastSyncDate: setNullAsDef(item.custitem_msg_time_sent_pim),
        itemType: itemType
      });
    });

    return { payload, itemMap };
  }

  /**
  * This function perform search to get item Details
  */
  const getItemDetails = (items) => {
    try {
      const itemDetails = {};
      const itemIdMap = {};
      const searchObj = search.create({
        type: "item",
        filters: [["internalid", "anyof", items]],
        columns: [
          "itemid",
          "displayname",
          "subsidiary",
          "cseg_msg_igt",
          "cseg_msg_brands",
          "taxschedule",
          "custitem_av_is_shop_online",
          "vendorname",
          "custitem_av_vendor_item_number",
          "custitem_msg_un_number",
          "custitem_msg_height",
          "custitem_msg_width",
          "custitem_msg_length",
          "weight",
          "custitem_msg_product_type",
          "custitem_msg_time_sent_pim",
          "custitem_av_ref_unit",
          "custitem_av_purchase_unit",
          "custitem_av_unit_key",
          "upccode",
          search.createColumn({
            name: "itemid",
            join: "parent",
          }),
          search.createColumn({
            name: "itemid",
            join: "CUSTITEM_MSG_MAIN_PRODUCT_KIT",
          }),
          search.createColumn({
            name: "itemid",
            join: "memberItem",
          }),
          search.createColumn({
            name: "internalid",
            join: "memberItem",
          }),
          search.createColumn({
            name: "parent",
            join: "memberItem",
          }),
        ],
      });

      let myPagedData = searchObj.runPaged({ pageSize: 1000 });
      myPagedData.pageRanges.forEach(function (pageRange) {
        let myPage = myPagedData.fetch({ index: pageRange.index });
        myPage.data.forEach(function (result) {
          let values = result.toJSON().values;
          values.recordType = result.recordType;
          if (values['parent.itemid']) {
            values.itemid = splitItemIds(values.itemid);
          }
          if (itemDetails[result.id]) {
            itemDetails[result.id] = [...itemDetails[result.id], values];
          } else {
            itemDetails[result.id] = [values];
            itemIdMap[values.itemid] = { id: result.id, type: result.recordType };
          }
        });
      });
      log.audit("saved search itemDetails", itemDetails);
      return { data: itemDetails, itemMap: itemIdMap };
    } catch (ex) {
      log.error('error in getting item details', ex);
      throw ex;
    }

  }

  /**
   * This fnc returns the payload for P02 interface
   */
  const getPayloadP02 = (itemIds) => {
    log.audit("getPayloadP02", "Triggered");
    let payload = [];
    const { data, itemMap } = getItemPrice(itemIds);

    Object.keys(data).forEach((key) => {
      let item = data?.[key]?.[0] || null;
      if (!item) return;
      const avgCost = setNullAsDef(parseFloat(item['averagecost']));
      const cost = setNullAsDef(parseFloat(item['cost']));
      const basePrice = setNullAsDef(parseFloat(item['baseprice']));
      let contributionMargin = avgCost
        ? setRoundValue(basePrice - avgCost)
        : cost
          ? setRoundValue(basePrice - cost)
          : null;

      const taxschedule = item.taxschedule?.[0]?.text;
      const divFactor = taxschedule == 'Steuerfrei' ? 1 : 1.2;
      contributionMargin = contributionMargin < 0 ? 0 : contributionMargin;
      let baseprice = setNullAsDef(parseFloat(item['CUSTRECORD_MSG_IP_ITEM.custrecord_msg_ip_base']) / divFactor);
      let b2bprice = setNullAsDef(parseFloat(item['CUSTRECORD_MSG_IP_ITEM.custrecord_msg_ip_b2b']));
      payload.push({
        "productNumber": item.itemid,
        "netBasePrice": baseprice,
        "netB2BPrice": b2bprice ? b2bprice : Math.round(baseprice * 90) / 100,
        "netSRPPrice": setNullAsDef(parseFloat(item['CUSTRECORD_MSG_IP_ITEM.custrecord_msg_ip_srp']) / divFactor),
        "netMinimalPrice": setNullAsDef(parseFloat(item['CUSTRECORD_MSG_IP_ITEM.custrecord_msg_ip_minimal'])),
        "contributionMargin": contributionMargin

      });
    });
    log.audit("getPayloadP02", payload);
    return { payload, itemMap };
  }

  const setRoundValue = (val) => Math.round(val * 100) / 100;

  const getItemPrice = (items) => {
    let itemPrice = {};
    const itemIdMap = {};
    let searchObj = search.create({
      type: "item",
      filters: [
        ["internalid", "anyof", items],
        "AND",
        [
          "custrecord_msg_ip_item.custrecord_msg_ip_date_from",
          "onorbefore",
          "today",
        ],
        "AND",
        [
          ["custrecord_msg_ip_item.custrecord_msg_ip_date_to", "isempty", ""],
          "OR",
          [
            "custrecord_msg_ip_item.custrecord_msg_ip_date_to",
            "onorafter",
            "today",
          ],
        ],
      ],
      columns: [
        "itemid",
        "averagecost",
        "cost",
        "baseprice",
        "taxschedule",
        search.createColumn({
          name: "internalid",
          sort: search.Sort.ASC,
        }),
        search.createColumn({
          name: "itemid",
          join: "parent",
        }),
        search.createColumn({
          name: "custrecord_msg_ip_date_from",
          join: "CUSTRECORD_MSG_IP_ITEM",
          sort: search.Sort.DESC,
        }),
        search.createColumn({
          name: "custrecord_msg_ip_date_to",
          join: "CUSTRECORD_MSG_IP_ITEM",
        }),
        search.createColumn({
          name: "custrecord_msg_ip_base",
          join: "CUSTRECORD_MSG_IP_ITEM",
        }),
        search.createColumn({
          name: "custrecord_msg_ip_b2b",
          join: "CUSTRECORD_MSG_IP_ITEM",
        }),
        search.createColumn({
          name: "custrecord_msg_ip_srp",
          join: "CUSTRECORD_MSG_IP_ITEM",
        }),
        search.createColumn({
          name: "custrecord_msg_ip_minimal",
          join: "CUSTRECORD_MSG_IP_ITEM",
        }),
      ],
    });
    let myPagedData = searchObj.runPaged({ pageSize: 1000 });
    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });
      myPage.data.forEach(function (result) {
        let values = result.toJSON().values;
        values.recordType = result.recordType;
        if (values['parent.itemid']) {
          values.itemid = splitItemIds(values.itemid);
          // .split(" ")[2]
        }
        if (itemPrice[result.id]) {
          // just picking the pricing with later Date from. 
          // the search is sorted in desc
          // the highest date from of any particular item is always the first one.
          return;
          // itemPrice[result.id] = [ ...itemPrice[result.id], values];
        } else {
          itemPrice[result.id] = [values];
          itemIdMap[values.itemid] = { id: result.id, type: result.recordType };
        }
      });
    });
    log.audit("saved search itemPrice", itemPrice);
    return { data: itemPrice, itemMap: itemIdMap };
  }


  const setNullAsDef = (val) => {
    return !val || (val && val.length == 0) ? null : val
  }

  const getItemComponents = (itemMembers) => {
    let members = []
    itemMembers.forEach(function (member) {
      if (member['memberItem.itemid']) {
        if (member['memberItem.parent'] && member['memberItem.parent'].length > 0) {
          members.push(splitItemIds(member['memberItem.itemid']));
          // members.push(member['memberItem.itemid'].split(" ")[2])
        } else members.push(member['memberItem.itemid']);
      }
    })
    return members;
  }


  /**
   * This fnc returns the payload for P04 interface
   */
  const getPayloadP04 = (itemIds, interface, item, logId) => {
    log.audit("getPayloadP04", "Triggered");
    let payload = [];
    const closedOutStatusIdsForOther = [CONSTANTS_LIB.MSG_ITEM_STATUS.EOL, CONSTANTS_LIB.MSG_ITEM_STATUS.Keine_Lagerware, CONSTANTS_LIB.MSG_ITEM_STATUS.Lagerabbau]; //Ausgelaufen/EOL, Keine Lagerware, Lagerabbau
    const closedOutStatusForFocusItems = [CONSTANTS_LIB.MSG_ITEM_STATUS.EOL, CONSTANTS_LIB.MSG_ITEM_STATUS.Lagerabbau]; //Ausgelaufen/EOL, Lagerabbau
    const connectionObj = new settings(CONSTANTS_LIB.PIM_CONFIG, false, interface.connectionRequired, logId);

    nonInvItemsIds = item.filter(i => CONSTANTS_LIB.NON_INV_ITEM.includes(i.type)).map(x => x.id);
    InvItemIds = item.filter(i => !CONSTANTS_LIB.NON_INV_ITEM.includes(i.type)).map(x => x.id);
    kitItemIds = item.filter(i => CONSTANTS_LIB.KIT_ITEM.includes(i.type)).map(x => x.id);

    const salesLocations = getSalesLocations();
    let { data, itemMap } = getLocationStocksForInvItems(InvItemIds, salesLocations);
    let { nonInvData, nonInvItemMap } = getLocationStocksForNonInvItems(nonInvItemsIds, salesLocations, connectionObj['custrecord_av_integration_const_stock']);
    let { kitData, kitItemMap } = getLocationStocksForKitItems(kitItemIds, salesLocations);

    Object.keys(data).forEach((key) => {
      let salesLocation = data[key];
      let details = itemMap[key];
      let isClosedOut = false;
      if (details.isFocusItem) {
        isClosedOut = closedOutStatusForFocusItems.includes(details.msgItemStatus) ? true : false;
      } else {
        isClosedOut = closedOutStatusIdsForOther.includes(details.msgItemStatus) ? true : false;
      }
      payload.push({
        "productNumber": key,
        "salesLocations": salesLocation,
        "isPimInactive": details.isPimInactive,
        "isClosedOut": isClosedOut,
        "deliveryTime": {
          "text": details.deliveryText,
          "date": details.deliveryDate
        },
      });
    });

    Object.keys(nonInvData).forEach((key) => {
      let salesLocation = nonInvData[key];
      let details = nonInvItemMap[key];
      let isClosedOut = false;
      if (details.isFocusItem) {
        isClosedOut = closedOutStatusForFocusItems.includes(details.msgItemStatus) ? true : false;
      } else {
        isClosedOut = closedOutStatusIdsForOther.includes(details.msgItemStatus) ? true : false;
      }
      payload.push({
        "productNumber": key,
        "salesLocations": salesLocation,
        "isPimInactive": details.isPimInactive,
        "isClosedOut": isClosedOut,
        "deliveryTime": {
          "text": details.deliveryText,
          "date": details.deliveryDate
        },
      });
    });

    Object.keys(kitData).forEach((key) => {
      let salesLocation = kitData[key];
      let details = kitItemMap[key];
      let isClosedOut = false;
      if (details.isFocusItem) {
        isClosedOut = closedOutStatusForFocusItems.includes(details.msgItemStatus) ? true : false;
      } else {
        isClosedOut = closedOutStatusIdsForOther.includes(details.msgItemStatus) ? true : false;
      }

      payload.push({
        "productNumber": key,
        "salesLocations": salesLocation,
        "isPimInactive": details.isPimInactive,
        "isClosedOut": isClosedOut,
        "deliveryTime": {
          "text": details.deliveryText,
          "date": details.deliveryDate
        },
      });
    });

    if (nonInvItemMap) itemMap = { ...itemMap, ...nonInvItemMap };
    if (kitItemMap) itemMap = { ...itemMap, ...kitItemMap };

    return { payload, itemMap };
  }

  const getSalesLocations = () => {
    const locations = [];
    const locationSearch = search.create({
      type: 'customrecord_msg_sales_location',
      filters: [['isinactive', 'is', 'F']],
      columns: [
        'name',
        'custrecord_msg_sl_location',
        'custrecord_msg_sl_default_location'
      ]
    });
    locationSearch.run().each(function (result) {
      let location = result.getValue({ name: "custrecord_msg_sl_location" });
      location = location ? location.split(',') : location;

      locations.push({
        id: result.id,
        location: location,
        name: result.getValue({ name: "name" }),
        isDefault: result.getValue({ name: "custrecord_msg_sl_default_location" }),
      });
      return true;
    });
    return locations;
  }

  const formatDateinYYYYMMDD = (datestring) => {
    const date = new Date(datestring);
    if (date) {
      return `${date.getFullYear()}-${("0" + (date.getMonth() + 1)).slice(-2)}-${("0" + date.getDate()).slice(-2)}`;
    }
    return null;
  }
  const getLocationStocksForInvItems = (itemIds, salesLocations) => {
    const stockByLoc = {};
    const itemIdMap = {};

    if (!itemIds.length > 0) return { data: {}, itemMap: {} };

    salesLocations.forEach(location => {
      if (!location.location) return { data: {}, itemMap: {} };

      const searchObj = search.create({
        type: 'item',
        filters: [
          ['internalid', 'anyof', itemIds],
          'AND',
          ['inventorylocation', 'anyof', location.location],
          'AND',
          ['isinactive', 'is', false]
        ],
        columns: [
          search.createColumn({
            name: 'internalid',
            summary: 'GROUP'
          }),
          search.createColumn({
            name: 'type',
            summary: 'GROUP',
          }),
          search.createColumn({
            name: 'itemid',
            summary: 'GROUP',
          }),
          search.createColumn({
            name: 'formulanumeric',
            summary: 'SUM',
            formula: 'case when {locationquantityavailable} is null then 0 else {locationquantityavailable} end',
          }),
          search.createColumn({
            name: "isserialitem",
            summary: "GROUP",
          }),
          search.createColumn({
            name: "custitem_msg_deliverytext",
            summary: "GROUP",
          }),
          search.createColumn({
            name: "custitem_msg_deliverydate",
            summary: "GROUP",
          }),
          search.createColumn({
            name: "custitem_av_pim_product_inactive",
            summary: "GROUP",
          }),
          search.createColumn({
            name: "custitem_msg_item_status",
            summary: "GROUP",
          }),
          search.createColumn({
            name: "custitem_msg_is_focus_item",
            summary: "GROUP",
          }),
        ]
      })

      let myPagedData = searchObj.runPaged({ pageSize: 1000 });

      myPagedData.pageRanges.forEach(function (pageRange) {
        let myPage = myPagedData.fetch({ index: pageRange.index });
        myPage.data.forEach(function (result) {
          log.audit('stock result', result);
          const id = result.getValue({ name: "internalid", summary: "GROUP" });
          const isSerialItem = result.getValue({ name: "isserialitem", summary: "GROUP" });
          const type = isSerialItem ? CONSTANTS_LIB.RECORD_TYPE.SerialItem :
            CONSTANTS_LIB.RECORD_TYPE[result.getValue({ name: "type", summary: "GROUP" })];
          let deliveryText = result.getText({ name: "custitem_msg_deliverytext", summary: "GROUP" });
          deliveryText = deliveryText == '- None -' ? '' : deliveryText;
          let deliveryDate = setNullAsDef(result.getValue({ name: "custitem_msg_deliverydate", summary: "GROUP" }));
          deliveryDate = deliveryDate ? formatDateinYYYYMMDD(formatDate(deliveryDate)) : null;
          const isPimInactive = result.getValue({ name: "custitem_av_pim_product_inactive", summary: "GROUP" });
          const msgItemStatus = result.getValue({ name: "custitem_msg_item_status", summary: "GROUP" });
          const isFocusItem = result.getValue({ name: "custitem_msg_is_focus_item", summary: "GROUP" });

          let itemId = result.getValue({ name: "itemid", summary: "GROUP" });
          itemId = splitItemIds(itemId);
          const stock = {
            location: location.name,
            quantity: Number(result.getValue({ name: "formulanumeric", summary: "SUM" })) || 0,
          };
          if (stockByLoc[itemId]) {
            stockByLoc[itemId] = [...stockByLoc[itemId], stock];
          } else {
            stockByLoc[itemId] = [stock];
            itemIdMap[itemId] = { id, type, deliveryText, deliveryDate, isPimInactive, msgItemStatus, isFocusItem };
          }
        });
      });

    });

    return { data: stockByLoc, itemMap: itemIdMap };
  }

  const getLocationStocksForNonInvItems = (itemIds, salesLocations, constantStock) => {
    const stockByLoc = {};
    const itemIdMap = {};

    if (!itemIds.length > 0) return { nonInvData: {}, nonInvItemMap: {} };

    const defaultLocation = salesLocations.find(x => x.isDefault);
    if (!defaultLocation) return { nonInvData: {}, nonInvItemMap: {} };

    const searchObj = search.create({
      type: 'item',
      filters: [
        ['internalid', 'anyof', itemIds],
        'AND',
        ['isinactive', 'is', false]
      ],
      columns: [
        'itemid',
        'custitem_msg_deliverytext',
        'custitem_msg_deliverydate',
        'custitem_av_pim_product_inactive',
        'custitem_msg_item_status',
        'custitem_msg_is_focus_item'
      ]
    })
    let myPagedData = searchObj.runPaged({ pageSize: 1000 });

    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });
      myPage.data.forEach(function (result) {
        log.audit('stock result', result);
        const id = result.id;
        const type = result.recordType;
        let itemId = result.getValue({ name: "itemid" });
        itemId = splitItemIds(itemId);

        let deliveryText = result.getText({ name: "custitem_msg_deliverytime" });
        deliveryText = deliveryText == '- None -' ? '' : deliveryText;
        let deliveryDate = setNullAsDef(result.getValue({ name: "custitem_msg_deliverydate" }));
        deliveryDate = deliveryDate ? formatDateinYYYYMMDD(formatDate(deliveryDate)) : null;
        const isPimInactive = result.getValue({ name: "custitem_av_pim_product_inactive" });
        const msgItemStatus = result.getValue({ name: "custitem_msg_item_status" });
        const isFocusItem = result.getValue({ name: "custitem_msg_is_focus_item" });

        const stock = {
          location: defaultLocation.name,
          quantity: Number(constantStock) || 0,
        };
        log.audit('stock', stock);
        if (stockByLoc[itemId]) {
          stockByLoc[itemId] = [...stockByLoc[itemId], stock];
        } else {
          stockByLoc[itemId] = [stock];
          itemIdMap[itemId] = { id, type, deliveryText, deliveryDate, isPimInactive, msgItemStatus, isFocusItem };
        }
      });
    });

    return { nonInvData: stockByLoc, nonInvItemMap: itemIdMap };
  }

  const getLocationStocksForKitItems = (itemIds, salesLocations) => {
    const stockByLoc = {};
    const itemIdMap = {};
    if (!itemIds.length > 0) return { kitData: {}, kitItemMap: {} };

    salesLocations.forEach(location => {
      if (!location.location) return { kitData: {}, kitItemMap: {} };

      const searchObj = search.create({
        type: 'item',
        filters: [
          ["type", "anyof", "Kit"],
          'AND',
          ['internalid', 'anyof', itemIds],
          'AND',
          ['memberitem.inventorylocation', 'anyof', location.location],
          'AND',
          ['isinactive', 'is', false]
        ],
        columns: [
          search.createColumn({
            name: 'internalid',
            summary: 'GROUP'
          }),
          search.createColumn({
            name: "memberitem",
            summary: "GROUP",
          }),
          search.createColumn({
            name: 'type',
            summary: 'GROUP',
          }),
          search.createColumn({
            name: 'itemid',
            summary: 'GROUP',
          }),
          search.createColumn({
            name: "formulanumeric",
            summary: "SUM",
            formula: "TRUNC( (case when {memberitem.locationquantityavailable} is null then 0 else {memberitem.locationquantityavailable} end) / {memberquantity}, 0)",
          }),
          search.createColumn({
            name: "memberquantity",
            summary: "GROUP",
          }),
          search.createColumn({
            name: "custitem_msg_deliverytext",
            summary: "GROUP",
          }),
          search.createColumn({
            name: "custitem_msg_deliverydate",
            summary: "GROUP",
          }),
          search.createColumn({
            name: "custitem_av_pim_product_inactive",
            summary: "GROUP",
          }),
          search.createColumn({
            name: "custitem_msg_item_status",
            summary: "GROUP",
          }),
          search.createColumn({
            name: "custitem_msg_is_focus_item",
            summary: "GROUP",
          }),
        ]
      })

      let myPagedData = searchObj.runPaged({ pageSize: 1000 });

      myPagedData.pageRanges.forEach(function (pageRange) {
        let myPage = myPagedData.fetch({ index: pageRange.index });
        myPage.data.forEach(function (result) {

          const id = result.getValue({ name: "internalid", summary: "GROUP" });
          const type = CONSTANTS_LIB.RECORD_TYPE[result.getValue({ name: "type", summary: "GROUP" })];
          let deliveryText = result.getText({ name: "custitem_msg_deliverytext", summary: "GROUP" });
          deliveryText = deliveryText == '- None -' ? '' : deliveryText;
          let deliveryDate = setNullAsDef(result.getValue({ name: "custitem_msg_deliverydate", summary: "GROUP" }));
          deliveryDate = deliveryDate ? formatDateinYYYYMMDD(formatDate(deliveryDate)) : null;
          const isPimInactive = result.getValue({ name: "custitem_av_pim_product_inactive", summary: "GROUP" });
          const msgItemStatus = result.getValue({ name: "custitem_msg_item_status", summary: "GROUP" });
          const isFocusItem = result.getValue({ name: "custitem_msg_is_focus_item", summary: "GROUP" });

          let itemId = result.getValue({ name: "itemid", summary: "GROUP" });
          itemId = splitItemIds(itemId);
          const stock = {
            location: location.name,
            quantity: Number(result.getValue({ name: "formulanumeric", summary: "SUM" })) || 0,
          };

          if (stockByLoc[itemId]) {
            const stockLocIndex = stockByLoc[itemId].findIndex(x => x.location == location.name);
            if (stockLocIndex != '-1' && stock.quantity < stockByLoc[itemId][stockLocIndex].quantity) {
              stockByLoc[itemId][stockLocIndex] = stock;
            } else if (stockLocIndex == '-1') {
              stockByLoc[itemId] = [...stockByLoc[itemId], stock];
            }
          } else {
            stockByLoc[itemId] = [stock];
            itemIdMap[itemId] = { id, type, deliveryText, deliveryDate, isPimInactive, msgItemStatus, isFocusItem };
          }
        });
      });

    });
    return { kitData: stockByLoc, kitItemMap: itemIdMap }
  }

  /**
  * This fnc returns interface configuration
  */
  const getInterface = (iface) => {
    log.audit('getInterface', 'getting interface details.')
    switch (iface) {
      case "P01":
        return {
          id: iface,
          onActionHttpRequest: "POST",
          connectionRequired: true,
          internal_error: "PIM10002",
          technical_error: "PIM00001",
          request_url_fieldId: 'custrecord_av_integration_product_post',
          getPayloadFunction: getPayloadP01,
          handleErrorFunction: handleErrorsP01,
          handleResponseFunction: handleResponseP01,
        };
      case "P02":
        return {
          id: iface,
          onActionHttpRequest: "PUT",
          connectionRequired: true,
          internal_error: "PIM20002",
          technical_error: "PIM00001",
          request_url_fieldId: 'custrecord_av_integration_product_put',
          getPayloadFunction: getPayloadP02,
          handleErrorFunction: handleErrorsP02,
          handleResponseFunction: handleResponseP02,
        };
      case "P04":
        return {
          id: iface,
          onActionHttpRequest: "PUT",
          connectionRequired: true,
          internal_error: "PIM40002",
          technical_error: "PIM00001",
          request_url_fieldId: 'custrecord_av_integration_stock_patch',
          getPayloadFunction: getPayloadP04,
          handleResponseFunction: handleResponseP04,
        };
    }
  }


  /** Delivery time calculation functions */

  /**
   * 
   * Calculates delivery time based on item stock
   */
  const handleDeliveryTimeRequest = (itemsObj) => {
    try {
      const items = itemsObj.filter(x => x.type != 'kititem').map(i => i.id);
      const kitItems = itemsObj.filter(x => x.type == 'kititem').map(i => i.id);
      let itemsStockDetailsforDTCal = {};
      let = itemsPurchaseDetails = {};
      const salesLocations = getSalesLocations();
      let salesLocationIds = salesLocations.map(i => i.location) || [];
      salesLocationIds = [...new Set(salesLocationIds.flat(1))]

      if (items.length > 0) {
        itemsStockDetailsforDTCal = getItemStockDetailsForDTCalculation(items, salesLocationIds);
        itemsPurchaseDetails = getItemPurchaseDetails(items);
      }

      if (Object.keys(itemsStockDetailsforDTCal).length == 0 || kitItems.length > 0) {
        const kitComponents = getKitComponents(kitItems);
        handleKitItems(kitComponents);
      }

      Object.keys(itemsStockDetailsforDTCal).forEach((key) => {
        log.debug('Calculating delivery time for item =====', key);
        const itemData = itemsStockDetailsforDTCal[key];
        const itemPurchaseData = itemsPurchaseDetails[key];
        const fieldsToBeUpdated = {};
        if (itemData) {
          const { deliveryText, deliveryDate, pimInactive } = calculateDeliveryTime(itemData, itemPurchaseData);
          if (!deliveryText) return; // if delivery text not calculated
          if (!(deliveryText == itemData.deliveryText && deliveryDate == itemData.deliveryDate && pimInactive == itemData.pimInactive)) {
            fieldsToBeUpdated['custitem_msg_deliverytext'] = deliveryText;
            fieldsToBeUpdated['custitem_msg_deliverydate'] = deliveryDate;
            if (!itemData.shopPrepMode) fieldsToBeUpdated['custitem_av_pim_product_inactive'] = pimInactive;
            fieldsToBeUpdated['custitem_msg_pim_request_sync_stock'] = true;
            fieldsToBeUpdated['custitem_msg_pim_calc_deliverytime'] = false;
          } // if delivery text & date is changed

          if (itemData.calDeliverytime) {
            fieldsToBeUpdated['custitem_msg_pim_calc_deliverytime'] = false;
          }
          // fieldsToBeUpdated['custitem_msg_pim_calc_deliverytime'] = false;

          if (Object.keys(fieldsToBeUpdated).length > 0) {
            record.submitFields({
              id: key,
              type: itemData.type,
              values: fieldsToBeUpdated,
            });
          }

        };
      })
    } catch (ex) {
      log.error('Error while calculating delivery time', ex);
    }
  };

  const calculateDeliveryTime = (itemData, itemPurchaseData) => {
    // const itemData = itemDataList[0];
    const itemHasPO = itemPurchaseData && itemPurchaseData.length > 0 ? true : false;
    const itemPOWithReceiptDate = itemHasPO ? itemPurchaseData.filter(x => x.expectedreceiptdate) : [];
    let deliveryText = null;
    let deliveryDate = null;
    let pimInactive = false;
    if (itemData.availableQty > 0) {
      log.audit('product is in stock **************');
      deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.sofort_lieferbar;
    }
    else if (itemData.producerStockAvailability > 0) {
      log.audit('product has producer stock available qty **************');
      deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.lieferbar_3_4_Werktagen;

      if (itemData.producerStockQty > 0) {
        if ((itemData.producerStockQty - itemData.backOrderedQty) > 0) {
          deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.lieferbar_3_4_Werktagen;
        } else {
          log.audit('product has producer stock available qty only enough to fulfill back orders  **************');
          deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.wird_bestellt;
        }
      }
    }
    else if (!itemData.isFocusItem && itemData.msgItemStatus && itemData.msgItemStatus == CONSTANTS_LIB.MSG_ITEM_STATUS.EOL) {
      deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.nicht_online;
      pimInactive = true;
    }
    else if (itemHasPO) {
      let isFulfilled = false;
      if (itemPOWithReceiptDate.length > 0) {
        log.audit("product is in supply chain **************");
        let calBackOrderQty = itemData.backOrderedQty;

        for (let x = 0; x < itemPOWithReceiptDate.length; x++) {
          let po = itemPOWithReceiptDate[x];
          // results are already sorted, so the first po in list is the one with nearest expectedreceiptdate
          calBackOrderQty = calBackOrderQty - po.purchasedQty;
          let expectedreceiptdate = formatDate(po.expectedreceiptdate);
          let now = new Date();
          now.setHours(0, 0, 0, 0);

          if (calBackOrderQty < 0 && expectedreceiptdate >= now) {
            log.audit("purchased product quantity is sufficient **************");
            deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.lieferbar_ab;
            deliveryDate = formatDate(po.expectedreceiptdate);
            isFulfilled = true;
            break;
          } else if (calBackOrderQty < 0 && expectedreceiptdate < now) {
            log.audit("po expectedreceiptdate is outdated");
            deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.beim_Lieferanten_bestellt;
            deliveryDate = null;
            isFulfilled = true;
            break;
          }
        }
      }
      if (!isFulfilled) {
        log.audit("product is purchased, but qty is less or has no expected Receipt Date **************");
        deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.wird_bestellt;
      }
    }
    else if (!itemData.isFocusItem && (itemData.safetystocklevel == '' || itemData.safetystocklevel == 0)) {
      log.audit('product is not focus item & safetystocklevel is 0 **************');
      deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.nicht_online;
      pimInactive = true;
    }
    else {
      deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.wird_bestellt;
    }

    // condiions for FOCUS ITEMS
    if (itemData.isFocusItem) {
      // product is focus item
      log.audit("product is focus item **************");
      let now = new Date();
      now.setHours(0, 0, 0, 0);

      if (itemData.focusDeliveryDate < now) return { deliveryText, deliveryDate, pimInactive };

      if (deliveryDate && deliveryDate > itemData.focusDeliveryDate) {
        deliveryDate = itemData.focusDeliveryDate;
      } else if (deliveryText == CONSTANTS_LIB.DELIVERY_TEXT.lieferbar_3_4_Werktagen && itemData.focusDeliveryDate) {
        //delivery text is lieferbar in 3-4 Tagen && confirm if focusDeliveryDate is within 4 days
        if (itemData.focusDeliveryDate <= new Date(new Date().setDate(now.getDate() + 3))) {
          deliveryDate = itemData.focusDeliveryDate;
          deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.lieferbar_ab;
        }
      } else if (deliveryText == CONSTANTS_LIB.DELIVERY_TEXT.wird_bestellt && itemData.focusDeliveryDate) {
        deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.lieferbar_ab;
        deliveryDate = itemData.focusDeliveryDate;
      }
    }

    return { deliveryText, deliveryDate, pimInactive };
  }

  const formatDate = (dateParam) => {
    if (dateParam && dateParam != "") {
      try {
        return format.parse({ value: dateParam, type: format.Type.DATE });
        // return format.format({ value: dateParam, type: format.Type.DATE });
      } catch (error) {
        log.error('formatDate', error);
        return null;
      }
    } else return null;
  };

  const getItemPurchaseDetails = (items) => {
    // items = ['4107', '11603', '23583', '49604'];
    const itemPurchaseDetails = {};
    const searchObj = search.create({
      type: "item",
      filters: [
        ["internalid", "anyof", items],
        "AND",
        ["transaction.recordtype", "is", "purchaseorder"],
        "AND",
        ["isinactive", "is", false],
        "AND",
        ["transaction.status", "anyof", "PurchOrd:B", "PurchOrd:E", "PurchOrd:D"]
      ],
      columns: [
        search.createColumn({
          name: "internalid",
          summary: "GROUP",
          label: "Internal ID",
        }),
        search.createColumn({
          name: "internalid",
          join: "transaction",
          summary: "GROUP",
          label: "Related Transaction Internal ID",
        }),
        search.createColumn({
          name: "expectedreceiptdate",
          join: "transaction",
          summary: "GROUP",
          sort: search.Sort.DESC,
          label: "Expected Receipt Date",
        }),
        search.createColumn({
          name: "recordtype",
          join: "transaction",
          summary: "GROUP",
          label: "Related Transaction",
        }),
        search.createColumn({
          name: "quantity",
          join: "transaction",
          summary: "SUM",
          label: "Related Transaction Item Qty"
        }),
      ],
    });

    let myPagedData = searchObj.runPaged({ pageSize: 1000 });

    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });

      myPage.data.forEach(function (result) {

        const internalid = result.getValue({ name: "internalid", summary: "GROUP" });
        const record = {
          purchasedOrderId: result.getValue({ name: "internalid", join: "transaction", summary: "GROUP" }),
          purchasedQty: Number(result.getValue({ name: "quantity", join: "transaction", summary: "SUM" })) || 0,
          expectedreceiptdate: result.getValue({ name: "expectedreceiptdate", join: "transaction", summary: "GROUP" })
        }

        if (itemPurchaseDetails[internalid]) {
          itemPurchaseDetails[internalid] = [...itemPurchaseDetails[internalid], record]
        } else {
          itemPurchaseDetails[internalid] = [record]
        }
      });
    });

    return itemPurchaseDetails;
  }

  const getItemStockDetailsForDTCalculation = (items, salesLocationIds) => {
    const itemDetailForDT = {};
    const searchObj = search.create({
      type: "item",
      filters: [
        ["internalid", "anyof", items],
        "AND",
        ["inventorylocation", "anyof", salesLocationIds],
        "AND",
        ["isinactive", "is", false],
      ],
      columns: [
        search.createColumn({
          name: "internalid",
          summary: "GROUP",
          sort: search.Sort.ASC,
          label: "Internal ID",
        }),
        search.createColumn({
          name: "custitem_av_pim_product_inactive",
          summary: "GROUP",
        }),
        search.createColumn({
          name: "inventorylocation",
          summary: "GROUP",
          label: "Inventory Location"
        }),
        search.createColumn({
          name: "locationquantitybackordered",
          summary: "SUM",
          label: "Location Back Ordered",
        }),
        search.createColumn({
          name: "locationquantityavailable",
          summary: "SUM",
          label: "Location Available",
        }),
        search.createColumn({
          name: "custrecord_msg_vsa_quantity",
          join: "CUSTRECORD_MSG_VSA_ITEM",
          summary: "SUM",
          label: "Quantity",
        }),
        search.createColumn({
          name: "formulanumeric",
          formula: "CASE WHEN {custrecord_msg_vsa_item.custrecord_msg_vsa_is_available} = 'T' THEN 1 ELSE 0 END",
          summary: "SUM",
          label: "Formula (Numeric)"
        }),
        // search.createColumn({
        //   name: "custrecord_msg_vsa_is_available",
        //   join: "CUSTRECORD_MSG_VSA_ITEM",
        //   summary: "COUNT",
        //   label: "is available",
        // }),
        search.createColumn({
          name: "custitem_msg_is_focus_item",
          summary: "GROUP",
          label: "Is Focus Item",
        }),
        search.createColumn({
          name: "safetystocklevel",
          summary: "GROUP",
          label: "Safety Stock Level"
        }),
        search.createColumn({
          name: "custitem_msg_focus_deliverydate",
          summary: "GROUP",
          label: "Focus Delivery Date"
        }),
        search.createColumn({
          name: "custitem_msg_deliverydate",
          summary: "GROUP",
          label: "Delivery Date"
        }),
        search.createColumn({
          name: "custitem_msg_deliverytext",
          summary: "GROUP",
          label: "Delivery Text"
        }),
        search.createColumn({
          name: "type",
          summary: "GROUP",
          label: "Type"
        }),
        search.createColumn({
          name: "isserialitem",
          summary: "GROUP",
        }),
        search.createColumn({
          name: "custitem_av_shop_prep_mode",
          summary: "GROUP",
        }),
        search.createColumn({
          name: "custitem_msg_pim_calc_deliverytime",
          summary: "GROUP",
        }),
        search.createColumn({
          name: "custitem_msg_item_status",
          summary: "GROUP",
        })
      ],
    });

    let myPagedData = searchObj.runPaged({ pageSize: 1000 });

    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });

      myPage.data.forEach(function (result) {
        const internalid = result.getValue({ name: "internalid", summary: "GROUP" });
        const isserialitem = result.getValue({ name: "isserialitem", summary: "GROUP" });


        if (itemDetailForDT[internalid]) {
          // sum the available qty and backorder for all locations. 
          // the result is grouped by locations.
          const itemDetailsByLoc = itemDetailForDT[internalid];
          itemDetailsByLoc.availableQty = itemDetailsByLoc.availableQty + (Number(result.getValue({ name: "locationquantityavailable", summary: "SUM" })) || 0)
          itemDetailsByLoc.backOrderedQty = itemDetailsByLoc.backOrderedQty + (Number(result.getValue({ name: "locationquantitybackordered", summary: "SUM" })) || 0)
          itemDetailForDT[internalid] = itemDetailsByLoc;
        } else {
          itemDetailForDT[internalid] = {
            pimInactive: result.getValue({ name: "custitem_av_pim_product_inactive", summary: "GROUP" }),
            shopPrepMode: result.getValue({ name: "custitem_av_shop_prep_mode", summary: "GROUP" }),
            calDeliverytime: result.getValue({ name: "custitem_msg_pim_calc_deliverytime", summary: "GROUP" }),
            msgItemStatus: result.getValue({ name: "custitem_msg_item_status", summary: "GROUP" }),
            type: isserialitem ? CONSTANTS_LIB.RECORD_TYPE.SerialItem : CONSTANTS_LIB.RECORD_TYPE[result.getValue({ name: "type", summary: "GROUP" })],
            isFocusItem: result.getValue({ name: "custitem_msg_is_focus_item", summary: "GROUP" }),
            safetystocklevel: Number(result.getValue({ name: "safetystocklevel", summary: "GROUP" })) || 0,
            focusDeliveryDate: formatDate(result.getValue({ name: "custitem_msg_focus_deliverydate", summary: "GROUP" })),
            deliveryDate: formatDate(result.getValue({ name: "custitem_msg_deliverydate", summary: "GROUP" })),
            deliveryText: result.getValue({ name: "custitem_msg_deliverytext", summary: "GROUP" }),
            backOrderedQty: Number(result.getValue({ name: "locationquantitybackordered", summary: "SUM" })) || 0,
            availableQty: Number(result.getValue({ name: "locationquantityavailable", summary: "SUM" })) || 0,
            producerStockAvailability: result.getValue({ name: "formulanumeric", summary: "SUM" }),
            producerStockQty: Number(result.getValue({ name: "custrecord_msg_vsa_quantity", join: "CUSTRECORD_MSG_VSA_ITEM", summary: "SUM" })),
          }
        }
      });
    });

    return itemDetailForDT;

  }

  const handleKitItems = (kitComponents) => {

    Object.keys(kitComponents).forEach((kitItem) => {
      log.debug('Calculating delivery time for kit item =====', kitItem);
      const fieldsToBeUpdated = {};
      const { deliveryText, deliveryDate, pimInactive } = calculateKitDelivery(kitComponents[kitItem]);

      if (!deliveryText) return; // if delivery text not calculated

      if (!(deliveryText == kitComponents[kitItem].deliveryText && deliveryDate == kitComponents[kitItem].deliveryDate && pimInactive == kitComponents[kitItem].pimInactive)) {
        fieldsToBeUpdated['custitem_msg_deliverytext'] = deliveryText;
        fieldsToBeUpdated['custitem_msg_deliverydate'] = deliveryDate;
        if (!kitComponents[kitItem].shopPrepMode) fieldsToBeUpdated['custitem_av_pim_product_inactive'] = pimInactive;
        fieldsToBeUpdated['custitem_msg_pim_request_sync_stock'] = true;
        fieldsToBeUpdated['custitem_msg_pim_calc_deliverytime'] = false;
      } // if delivery text & date is changed

      if (kitComponents[kitItem].calDeliverytime) {
        fieldsToBeUpdated['custitem_msg_pim_calc_deliverytime'] = false;
      }

      log.audit('fieldsToBeUpdated', fieldsToBeUpdated);
      log.audit('keys', Object.keys(fieldsToBeUpdated).length);
      if (Object.keys(fieldsToBeUpdated).length > 0) {
        record.submitFields({
          id: kitItem,
          type: 'kititem',
          values: fieldsToBeUpdated,
        });
      }
    });

  }

  const calculateKitDelivery = (kitItem) => {
    const components = kitItem.components;
    let deliveryText = null;
    let deliveryDate = null;
    let pimInactive = false;

    const nichtOnline = components.filter(x => x.deliveryText == CONSTANTS_LIB.DELIVERY_TEXT.nicht_online);
    if (nichtOnline.length > 0) {
      deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.nicht_online;
      pimInactive = true;
      return { deliveryText, deliveryDate, pimInactive };
    }

    const wirdBestellt = components.filter(x => x.deliveryText == CONSTANTS_LIB.DELIVERY_TEXT.wird_bestellt);
    if (wirdBestellt.length > 0) {
      deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.wird_bestellt;
      return { deliveryText, deliveryDate, pimInactive };
    }

    const lieferbarAb = components.filter(x => x.deliveryText == CONSTANTS_LIB.DELIVERY_TEXT.lieferbar_ab);
    if (lieferbarAb.length > 0) {
      deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.lieferbar_ab;
      deliveryDate = lieferbarAb[0].deliveryDate;
      for (let i = 1; i < lieferbarAb.length; i++) {
        if (deliveryDate < lieferbarAb[i].deliveryDate) {
          deliveryDate = lieferbarAb[i].deliveryDate;
        }
      }
      return { deliveryText, deliveryDate, pimInactive };
    }

    const lieferbarIn3Days = components.filter(x => x.deliveryText == CONSTANTS_LIB.DELIVERY_TEXT.lieferbar_3_4_Werktagen);
    if (lieferbarIn3Days.length > 0) {
      deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.lieferbar_3_4_Werktagen;
      return { deliveryText, deliveryDate, pimInactive };
    }

    const sofortLieferbar = components.filter(x => x.deliveryText == CONSTANTS_LIB.DELIVERY_TEXT.sofort_lieferbar);
    if (sofortLieferbar.length > 0) {
      deliveryText = CONSTANTS_LIB.DELIVERY_TEXT.sofort_lieferbar;
      return { deliveryText, deliveryDate, pimInactive };
    }
  }

  const getKitComponents = (items) => {
    const kitComponents = {};
    if (!items.length > 0) return {};
    const kititemSearchObj = search.create({
      type: "kititem",
      filters:
        [
          ["type", "anyof", "Kit"],
          "AND",
          ["internalid", "anyof", items]
        ],
      columns:
        [
          search.createColumn({
            name: "itemid",
            join: "memberItem",
            label: "Name"
          }),
          search.createColumn({
            name: "custitem_msg_deliverytext",
            join: "memberItem",
            label: "Delivery Text"
          }),
          search.createColumn({
            name: "custitem_msg_deliverydate",
            join: "memberItem",
            label: "Delivery Date"
          }),
          search.createColumn({ name: "internalid" }),
          search.createColumn({ name: "custitem_msg_deliverydate", label: "Delivery Date" }),
          search.createColumn({ name: "custitem_msg_deliverytext", label: "Delivery Text" }),
          search.createColumn({ name: "custitem_av_pim_product_inactive", label: "PIM Product Inactive" }),
          search.createColumn({ name: "custitem_av_shop_prep_mode" }),
          search.createColumn({ name: "custitem_msg_pim_calc_deliverytime" })
        ]
    });

    let myPagedData = kititemSearchObj.runPaged({ pageSize: 1000 });

    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });

      myPage.data.forEach(function (result) {
        const internalid = result.getValue({ name: "internalid" });

        if (kitComponents[internalid]) {
          kitComponents[internalid].components.push({
            name: result.getValue({ name: "itemid", join: "memberItem" }),
            deliveryText: result.getValue({ name: "custitem_msg_deliverytext", join: "memberItem" }),
            deliveryDate: formatDate(result.getValue({ name: "custitem_msg_deliverydate", join: "memberItem" }))
          });
        } else {
          kitComponents[internalid] = {
            deliveryText: result.getValue({ name: "custitem_msg_deliverytext" }),
            deliveryDate: formatDate(result.getValue({ name: "custitem_msg_deliverydate" })),
            pimInactive: result.getValue({ name: "custitem_av_pim_product_inactive" }),
            shopPrepMode: result.getValue({ name: "custitem_av_shop_prep_mode" }),
            calDeliverytime: result.getValue({ name: "custitem_msg_pim_calc_deliverytime" }),
            components: [{
              name: result.getValue({ name: "itemid", join: "memberItem" }),
              deliveryText: result.getValue({ name: "custitem_msg_deliverytext", join: "memberItem" }),
              deliveryDate: formatDate(result.getValue({ name: "custitem_msg_deliverydate", join: "memberItem" }))
            }]
          }
        }
      });
    });
    return kitComponents;
  }


  return {
    handleRequest: handleRequest,
    getInterface: getInterface,
    sendData: sendData,
    handleDeliveryTimeRequest: handleDeliveryTimeRequest
  };
});
