/**
 ** Copyright (c) 2022 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 *
 * Author: Tuba Mohsin
 * Date: 28.01.2023
 * Add and update items to PIM
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */

define([
  "N/search",
  "N/task",
  "N/runtime",
  "./av_msg_pim_item_lib.js",
  "./av_int_log.js",
], function (search, task, runtime, lib) {


  const getInputData = () => {
    log.audit("getInputData", `-Script Started on ${new Date()}-`);
    try {
      let index = 0;
      return getItemsToSync(index);
    } catch (ex) {
      log.error({ title: "getInputData : ex", details: ex });
    }
  };

  const map = (context) => {
    log.audit({ title: "map", details: 'triggered' });
    const Item = JSON.parse(context.value);
    const currentRuntime = runtime.getCurrentScript();
    const iface = currentRuntime.getParameter({ name: "custscript_av_pim_item_sync_iface" });
    lib.handleRequest(Item, iface);

  };


  const summarize = () => {
    log.audit({ title: "summarize", details: "triggered" });
    try {
      const currentRuntime = runtime.getCurrentScript();
      let index = 0;
      index = parseInt(index) + 1000;
      log.audit({ title: "summarize: index", details: index });

      let itemsToSync = [];
      //getItemsToSync(index);
      if (itemsToSync.length > 0) {
        //reschedule the map reduce script
        let mrTask = task.create({
          taskType: task.TaskType.MAP_REDUCE,
          scriptId: currentRuntime.id,
          deploymentId: currentRuntime.deploymentId,
        });
        const mrTaskId = mrTask.submit();
        log.audit({ title: "summarize", details: `mrTaskId: ${mrTaskId}` });
      }
    } catch (ex) {
      log.error({ title: "summarize : ex", details: ex });
    }
    log.audit("summarize", `-Script Finished on ${new Date()}-`);
  };

  const getItemsToSync = (index) => {
    const currentRuntime = runtime.getCurrentScript();
    let results = [];

    let itemSearch = search.load({
      id: currentRuntime.getParameter({ name: "custscript_av_pim_item_sync_search" }),
    });

    // let currentRange = itemSearch.run().getRange({
    //   start: index,
    //   end: index + 1000,
    // });

    // for (let i = 0; !!currentRange && i < currentRange.length; i++) {
    //   results.push(currentRange[i].id)
    // }

    let myPagedData = itemSearch.runPaged({ pageSize: 1000 });

    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });
      myPage.data.forEach(function (result) {
        let internalId = result.id || result.getValue('internalid') || result.getValue({ name: "internalid", summary: "GROUP" });
        // log.audit('internalid', internalId);
        results.push({ id: internalId, type: result.recordType || null })
      });
    });


    let resultGroups = [];
    const size = currentRuntime.getParameter({ name: "custscript_av_pim_item_sync_size" }) || 100;

    for (let i = 0; i < results.length; i += size) {
      resultGroups.push(results.slice(i, i + size));
    }
    log.audit('resultGroups', resultGroups);
    return resultGroups;
  };

  return {
    getInputData: getInputData,
    map: map,
    summarize: summarize,
  };
});
