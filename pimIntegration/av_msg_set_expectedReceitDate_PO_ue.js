/** 
 * Copyright (c) 2023 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @author: Tuba Mohsin
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @description: Set expected delivery date to be one week after the date of the PO, only if the current value is the same as the date of the PO.
 */


define(['N/format', 'N/search'], function (format, search) {

    function beforeSubmit(context) {
        const currRecord = context.newRecord;
        if (context.type == 'create' || context.type == 'edit') {
            const vendorDays = getDaysFromVendor(currRecord.getValue('entity'));
            checkLineItems(currRecord, vendorDays);
        }
    }

    function checkLineItems(currRecord, vendorDays) {
        let trandate = currRecord.getValue('trandate');
        let formattedTranDate = trandate ? format.format({ value: trandate, type: format.Type.DATE }) : null;
        const itemLineCount = currRecord.getLineCount({ sublistId: 'item' });

        for (var i = 0; i < itemLineCount; i++) {
            let item = currRecord.getSublistValue('item', 'item', i);
            if (item > 0) {
                let expectedReceiptDate = currRecord.getSublistValue('item', 'expectedreceiptdate', i);
                let formattedExpectedReceiptDate = expectedReceiptDate ? format.format({ value: expectedReceiptDate, type: format.Type.DATE }) : null;

                if (formattedExpectedReceiptDate == formattedTranDate || !formattedExpectedReceiptDate) {
                    log.audit('updating expected receipt date for PO = ', currRecord.id);
                    let oneWeekAfterReceiptDate = new Date(trandate);
                    let noOfDays = (vendorDays && vendorDays > 0) ? vendorDays : 7;
                    oneWeekAfterReceiptDate = new Date(oneWeekAfterReceiptDate.setDate(trandate.getDate() + noOfDays));
                    currRecord.setSublistValue('item', 'expectedreceiptdate', i, formatDate(oneWeekAfterReceiptDate));
                    log.audit('formatted oneWeekAfterReceiptDate', oneWeekAfterReceiptDate);
                }
            }

        };

    }


    const getDaysFromVendor = (vendorId) => {
        const vendorLookup = search.lookupFields({
            type: 'vendor',
            id: vendorId,
            columns: ['custentity_msg_delivery_time']
        });
        return Number(vendorLookup?.custentity_msg_delivery_time) || 0;
    }
    const formatDate = (dateParam) => {
        if (dateParam && dateParam != "") {
            try {
                return format.parse({ value: dateParam, type: format.Type.DATE });
            } catch (error) {
                log.error('formatDate', error);
                return null;
            }
        } else return null;
    };

    return {
        beforeSubmit: beforeSubmit,
    };
});