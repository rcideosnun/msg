/**
 ** Copyright (c) 2015 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.x
 ** @author: Roberto Cideos
 ** @dated: 6/9/2021
 ** @Description: This library is used to load configuration object
 * *@NApiVersion 2.x
 */

define(["N/format", "N/record", "N/search", "N/runtime", "N/https", "./av_int_log.js"],
    function (format, record, search, runtime, https, avLog) {
        var CONST = {
            CODE: {
                SUCCESS: 200,
                ERROR: 401
            },
            TIME_BUFFER: 0 //minutes
        }

        /**
         * 
         * @param {String} configId 
         * @param {Object} context 
         * @param {Boolean} connectionRequired 
         * @returns 
         */

        function _getConfigObj(configId, context, connectionRequired, logId) {
            try {
                if (!configId) return null

                var configFields = ["custrecord_av_integration_base_url",
                    "custrecord_av_integration_auth_url",
                    "custrecord_av_integration_refresh_url",
                    "custrecord_av_integration_username",
                    "custrecord_av_integration_password",
                    "custrecord_av_integration_auth_token",
                    "custrecord_av_integration_refresh_token",
                    "custrecord_av_integration_token_exp",
                    "custrecord_av_integration_product_get",
                    "custrecord_av_integration_product_post",
                    "custrecord_av_integration_product_put",
                    "custrecord_av_integration_dpmt_get",
                    "custrecord_av_integration_stock_patch",
                    "custrecord_av_integration_fulfill_locs",
                    "custrecord_av_integration_disc_item",
                    "custrecord_av_integration_brand_post",
                    "custrecord_av_integration_sftp_out",
                    "custrecord_av_integration_so_post",
                    "custrecord_av_integration_stock_post",
                    "custrecord_av_integration_store_post",
                    "custrecord_av_integration_const_stock"]

                var configObj = search.lookupFields({
                    type: "customrecord_av_integration_config",
                    id: configId,
                    columns: configFields
                });

                configObj.fulfillableLocations = _getLocationIds(configObj.custrecord_av_integration_fulfill_locs);
                configObj.discountItem = _getDiscItems(configObj.custrecord_av_integration_disc_item);
                configObj.configId = configId;

                if (connectionRequired) {

                    if (configObj.custrecord_av_integration_auth_token &&
                        configObj.custrecord_av_integration_refresh_token &&
                        configObj.custrecord_av_integration_token_exp) {

                        return _checkTokenValidity(configObj)

                    } else {
                        return _generateNewToken(configObj)
                    }
                } else {
                    return configObj
                }

            } catch (e) {
                if(logId) {
                    avLog.update({
                        id: logId,
                        receive: e?.errorDetails || e || "Credentials are invalid"
                    })
                } else {
                    avLog.create({
                        receive: e?.errorDetails || e || "Credentials are invalid"
                    })
                }
                throw new Error(JSON.stringify(e));
            }
        }

        /**
         * 
         * @param {Object} configObj 
         * @returns 
         */

        function _checkTokenValidity(configObj) {

            var timeNow = new Date().getTime()
            var expirationTokenTime = configObj.custrecord_av_integration_token_exp

            var expirationTokenTimeParsed = format.parse({
                value: expirationTokenTime,
                type: format.Type.DATETIME
            })

            if (expirationTokenTimeParsed.getTime() + CONST.TIME_BUFFER * 60 * 1000 > timeNow) {
                log.debug("Old Token", `${expirationTokenTimeParsed.getTime() + CONST.TIME_BUFFER * 60 * 1000}> ${timeNow}`)
                configObj.accessToken = configObj.custrecord_av_integration_auth_token
                return configObj
            } else {
                log.debug("New Token")
                return _generateNewToken(configObj)
            }

        }

        function _refreshToken() {

            var reqBody = JSON.stringify({
                "refreshToken": configObj.custrecord_av_integration_refresh_token
            })

            var reqHeaders = {
                "Content-type": "application/json"
            }

            var reqUrl = configObj.custrecord_av_integration_base_url.concat(configObj.custrecord_av_integration_refresh_url)

            var response = https.request({
                headers: reqHeaders,
                method: https.Method.POST,
                url: reqUrl,
                body: reqBody
            })

            return _handleTokenResponse(response, configObj)
        }

        /**
         * 
         * @param {Object} configObj 
         * @returns 
         */

        function _generateNewToken(configObj) {

            var reqBody = JSON.stringify({
                "username": configObj.custrecord_av_integration_username,
                "password": configObj.custrecord_av_integration_password
            })

            var reqHeaders = {
                "Content-type": "application/json"
            }

            var reqUrl = configObj.custrecord_av_integration_base_url.concat(configObj.custrecord_av_integration_auth_url)

            var response = https.request({
                headers: reqHeaders,
                method: https.Method.POST,
                url: reqUrl,
                body: reqBody
            })
        
            return _handleTokenResponse(response, configObj)

        }

        /**
         * 
         * @param {Object} response 
         * @param {Object} configObj 
         * @returns 
         */

        function _handleTokenResponse(response, configObj) {
            if (response.code == CONST.CODE.SUCCESS) {
                var resBody = JSON.parse(response.body)
                var expirationTime = new Date();
                expirationTime = new Date(expirationTime.setHours(new Date().getHours() + 1))
                expirationTime = format.format({
                    value: expirationTime,
                    type: format.Type.DATETIME
                })

                var authorizationData = {
                    "custrecord_av_integration_auth_token": resBody.token,
                    "custrecord_av_integration_refresh_token": resBody.refreshToken,
                    "custrecord_av_integration_token_exp": expirationTime
                }

                record.submitFields({
                    type: "customrecord_av_integration_config",
                    id: configObj.configId,
                    values: authorizationData
                })

                configObj.accessToken = resBody.token

                return configObj
            } else {
                throw {
                    errorCode: response.code,
                    errorDetails: response.body
                }
            }
        }

        /**
         * 
         * @param {Array} s 
         * @returns 
         */

        function _getLocationIds(s) {

            var locIds = [];

            for (var i = 0; i < s.length; i++) {
                locIds.push(s[i].value)
            }

            return locIds

        }

        /**
         * 
         * @param {Array} di 
         * @returns 
         */

        function _getDiscItems(di) {

            var discountItemId = "";

            if (!di.length) return discountItemId

            return di[0].value

        }

        return _getConfigObj

    })