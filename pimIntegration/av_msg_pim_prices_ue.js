/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tuba Mohsin
* Date: 3/1/2023
* Description: Updates item record when price is changed in custom record
* @NApiVersion 2.1
* @NScriptType UserEventScript
*/

define(['N/record', 'N/search'], function (record, search) {
  const afterSubmit = (context) => {
    try {
      // if (context.type === context.UserEventType.CREATE) {
      //  updateItemFlag(context.newRecord);
      // }

      if (context.type === context.UserEventType.EDIT ||
        context.type === context.UserEventType.XEDIT) {
        if (checkIfChanged(context.oldRecord, context.newRecord, context.type)) {

          context.type === context.UserEventType.XEDIT ?
            updateItemFlag(context.oldRecord) : updateItemFlag(context.newRecord);
        }
      }
    } catch (ex) {
      log.error({ title: 'afterSubmit : ex', details: ex });
    }
  }

  const updateItemFlag = (nsRecord) => {
    const itemId = nsRecord.getValue({
      fieldId: 'custrecord_msg_ip_item'
    });

    record.submitFields({
      type: getRecordType(itemId),
      id: itemId,
      values: {
        custitem_msg_pim_request_sync_price: true
      }
    });
  }

  const getRecordType = (itemId) => {
    const itemSearch = search.create({
      type: search.Type.ITEM,
      filters:
        [['internalid', search.Operator.ANYOF, itemId]],
      columns: []
    }).run().getRange({
      start: 0,
      end: 1
    });

    return itemSearch.length ? itemSearch[0].recordType : null;
  }

  const checkIfChanged = (oldRecord, newRecord, contextType) => {
    if (contextType != 'xedit' && newRecord.getValue('custrecord_msg_ip_item') !== oldRecord.getValue('custrecord_msg_ip_item')) {
      // if item is changed update flag on old record and new record.
      updateItemFlag(oldRecord)
      return true;
    }
    if (newRecord.getText('custrecord_msg_ip_date_from') !== oldRecord.getText('custrecord_msg_ip_date_from')) {
      return true;
    }
    if (newRecord.getText('custrecord_msg_ip_date_to') !== oldRecord.getText('custrecord_msg_ip_date_to')) {
      return true;
    }
    if (newRecord.getValue('custrecord_msg_ip_base') !== oldRecord.getValue('custrecord_msg_ip_base')) {
      return true;
    }
    if (newRecord.getValue('custrecord_msg_ip_b2b') !== oldRecord.getValue('custrecord_msg_ip_b2b')) {
      return true;
    }
    if (newRecord.getValue('custrecord_msg_ip_srp') !== oldRecord.getValue('custrecord_msg_ip_srp')) {
      return true;
    }
    if (newRecord.getValue('isinactive') !== oldRecord.getValue('isinactive')) {
      return true;
    }
    if (newRecord.getValue('custrecord_msg_ip_minimal') !== oldRecord.getValue('custrecord_msg_ip_minimal')) {
      return true;
    }
    return false;
  }

  return {
    afterSubmit: afterSubmit,
  }
})