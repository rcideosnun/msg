/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tehseen Ahmed
* Date: 2/8/2022
* Description: Updates item record when price is changed in custom record
* @NApiVersion 2.1
* @NScriptType ClientScript
*/

define(['N/record'], function (record) {
  const validateLine = (context) => {
    let currentRecord = context.currentRecord;
    const sublistName = context.sublistId;
    if (sublistName === 'recmachcustrecord_msg_ip_item') {
      currentRecord.setValue({
        fieldId: 'custitem_av_pim_item_price_updated',
        value: true
      });
    }
    return true;
  }
  return {
    validateLine: validateLine
  }
})