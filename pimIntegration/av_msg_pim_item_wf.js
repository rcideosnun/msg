/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 ** @author: [Tuba Mohsin]
 ** @dated: [26.01.2023]
 ** @Description: Integration between PIMCore and NetSuite
 *
 *  @NApiVersion 2.1
 *  @NScriptType WorkflowActionScript
 */
define([
  "N/runtime",
  "./av_msg_pim_item_lib",
], function (
  runtime,
  lib
) {

  var deltaTime, startTime;
  /**
   *
   * @param {Object} context
   * @returns
   */

  const onAction = (context) => {
    try {
      startTime = new Date().getTime();
      const scriptObj = runtime.getCurrentScript();
      const iface =  scriptObj.getParameter({ name: "custscript_av_item_iface" }) || null;
      const itemId = context.newRecord.id;
      const itemType = context.newRecord.type;
      if(itemId) {
        log.debug('Worflow Triggered', `Processing interface: ${iface} for item: ${itemId}`);
        lib.handleRequest([{id: itemId, type: itemType || null}], iface);
        deltaTime = (new Date().getTime() - startTime) / 1000;
      }
      log.debug("Time Delta", `Time Usage: ${deltaTime}s`);
      
    } catch (e) {
      log.debug("Error", e);
    }
  }

return {
    onAction: onAction,
  };
});
