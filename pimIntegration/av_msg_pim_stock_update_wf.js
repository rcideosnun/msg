/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 ** @author: [Tuba Mohsin]
 ** @dated: [26.01.2023]
 ** @Description: update stock when transaction is Closed since the Userevent is not triggered on closed action
 *
 *  @NApiVersion 2.1
 *  @NScriptType WorkflowActionScript
 */
define(['N/record', 'N/search', 'N/task'], function (record, search, task) {
  /**
   *
   * @param {Object} context
   * @returns
   */

  const onAction = (context) => {
    try {
      const newRecord = context.newRecord;
      // todo: only trigger if items are changed in case of edit.
      // items changed, qty changed.

      const items = getItems(newRecord);
      const itemDetails = getRecordType(items);
      log.audit("items sublist length ", items.length);
      log.audit("unique item length", itemDetails.length);
      if (itemDetails.length > 95) {
        triggerMapReduceScript(items);
        return;
      }

      itemDetails.forEach((item) => {
        if (item) {
          record.submitFields({
            type: item.type,
            id: item.id,
            values: {
              custitem_msg_pim_calc_deliverytime: true,
            },
          });

          record.submitFields({
            type: item.type,
            id: item.id,
            values: {
              custitem_msg_pim_request_sync_stock: true,
              custitem_av_pos_req_stock_sync: true
            },
          });
        }
      });
    } catch (e) {
      log.debug("Error", e);
    }
  };

  const getItems = (tranRecord) => {
    const tranRecordType = tranRecord.type;
    const sublistId =
      tranRecordType == "inventoryadjustment" ? "inventory" : "item";
    const items = [];
    const itemLineCount = tranRecord.getLineCount({
      sublistId: sublistId,
    });
    for (var i = 0; i < itemLineCount; i++) {
      const id = tranRecord.getSublistValue({
        sublistId: sublistId,
        fieldId: "item",
        line: i,
      });
      items.push(id);
    }
    return items;
  };

  const getRecordType = (itemIds) => {
    const itemDetails = [];
    const searchObj = search.create({
      type: search.Type.ITEM,
      filters: [["internalid", search.Operator.ANYOF, itemIds]],
      columns: [],
    });

    let myPagedData = searchObj.runPaged({ pageSize: 1000 });
    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });
      myPage.data.forEach(function (result) {
        itemDetails.push({ id: result.id, type: result.recordType });
      });
    });

    return itemDetails;
  };

  const triggerMapReduceScript = (itemIds) => {
    const mapReduceTask = task.create({
      taskType: task.TaskType.MAP_REDUCE,
      scriptId: "customscript_av_pim_update_stock_flag_mr",
      params: { custscript_itemids: itemIds },
    });
    mapReduceTask.submit();
  };

  return {
    onAction: onAction,
  };
});
