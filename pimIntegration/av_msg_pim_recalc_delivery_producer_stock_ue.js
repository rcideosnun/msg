/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tuba Mohsin
* Date: 3/1/2023
* Description: flags item for Delivery time recalculation when producer stock is updated
* @NApiVersion 2.1
* @NScriptType UserEventScript
*/

define(['N/record', 'N/search'], function (record, search) {
  const afterSubmit = (context) => {
    try {
      if (context.type === context.UserEventType.CREATE || context.type === context.UserEventType.DELETE) {
        recalculateDeliveryTime(context.newRecord);
      }
      else if (context.type === context.UserEventType.EDIT || context.type === context.UserEventType.XEDIT) {
        if (checkIfChanged(context.oldRecord, context.newRecord, context.type)) {
          context.type === context.UserEventType.XEDIT ?
            recalculateDeliveryTime(context.oldRecord) : recalculateDeliveryTime(context.newRecord);
        }
      }
    } catch (ex) {
      log.error({ title: 'afterSubmit : ex', details: ex });
    }
  }

  const recalculateDeliveryTime = (nsRecord) => {
    const itemId = nsRecord.getValue({
      fieldId: 'custrecord_msg_vsa_item'
    });

    record.submitFields({
      type: getRecordType(itemId),
      id: itemId,
      values: {
        custitem_msg_pim_calc_deliverytime: true
      }
    });
  }

  const getRecordType = (itemId) => {
    const itemSearch = search.create({
      type: search.Type.ITEM,
      filters:
        [['internalid', search.Operator.ANYOF, itemId]],
      columns: []
    }).run().getRange({
      start: 0,
      end: 1
    });

    return itemSearch.length ? itemSearch[0].recordType : null;
  }

  const checkIfChanged = (oldRecord, newRecord, contextType) => {
    if (contextType != 'xedit' && newRecord.getValue('custrecord_msg_vsa_item') !== oldRecord.getValue('custrecord_msg_vsa_item')) {
      // if item is changed update flag on old record and new record.
      recalculateDeliveryTime(oldRecord)
      return true;
    }
    if (newRecord.getValue('custrecord_msg_vsa_vendor') !== oldRecord.getValue('custrecord_msg_vsa_vendor')) {
      return true;
    }
    if (newRecord.getValue('custrecord_msg_vsa_availability') !== oldRecord.getValue('custrecord_msg_vsa_availability')) {
      return true;
    }
    if (newRecord.getValue('custrecord_msg_vsa_quantity') !== oldRecord.getValue('custrecord_msg_vsa_quantity')) {
      return true;
    }
    if (newRecord.getValue('isinactive') !== oldRecord.getValue('isinactive')) {
      return true;
    }
    if (newRecord.getValue('custrecord_msg_vsa_is_available') !== oldRecord.getValue('custrecord_msg_vsa_is_available')) {
      return true;
    }
    return false;
  }

  return {
    afterSubmit: afterSubmit,
  }
})