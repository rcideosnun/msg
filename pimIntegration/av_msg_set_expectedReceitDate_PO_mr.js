/**
 ** Copyright (c) 2022 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 *
 * Author: Tuba Mohsin
 * Date: 19.06.2023
 *  Set expected delivery date to be one week after the date of the PO, only if the current value is the same as the date of the PO.
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */

define([
  "N/search",
  "N/runtime",
  "N/record",
  "N/format"
], function (search, runtime, record, format) {


  const getInputData = () => {
    log.audit("getInputData", `-Script Started on ${new Date()}-`);
    try {
      return getPOtoSetDate();
    } catch (ex) {
      log.error({ title: "getInputData : ex", details: ex });
    }
  };

  const map = (context) => {
    try {
      log.audit({ title: "map", details: 'triggered' });
      const po = JSON.parse(context.value);
      vendorDays = getDaysFromVendor(po.entity);
      checkLineItems(po.id, vendorDays);
    } catch (ex) {
      log.error({ title: "map stage : ex", details: ex });
    }

  };


  const summarize = () => {
    log.audit("summarize", `-Script Finished on ${new Date()}-`);
  };

  const getPOtoSetDate = () => {
    const currentRuntime = runtime.getCurrentScript();
    let results = [];

    let itemSearch = search.load({
      id: currentRuntime.getParameter({ name: "custscript_av_saved_search" }),
    });

    let myPagedData = itemSearch.runPaged({ pageSize: 1000 });

    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });
      myPage.data.forEach(function (result) {
        let internalId = result.id || result.getValue('internalid') || result.getValue({ name: "internalid", summary: "GROUP" });
        let entity = result.getValue({ name: 'internalid', join: 'vendor', summary: 'GROUP' }) || result.getValue({ name: 'internalid', join: 'vendor' }) || null;
        results.push({ id: internalId, type: result.recordType || null, entity: entity })
      });
    });

    return results;
  };

  const checkLineItems = (poId, vendorDays) => {
    log.audit('poId', poId);
    const currRecord = record.load({
      type: record.Type.PURCHASE_ORDER,
      id: poId
    })
    let trandate = currRecord.getValue('trandate');
    let formattedTranDate = trandate ? format.format({ value: trandate, type: format.Type.DATE }) : null;
    const itemLineCount = currRecord.getLineCount({ sublistId: 'item' });

    for (var i = 0; i < itemLineCount; i++) {
      let item = currRecord.getSublistValue('item', 'item', i);
      if (item > 0) {
        let expectedReceiptDate = currRecord.getSublistValue('item', 'expectedreceiptdate', i);
        let formattedExpectedReceiptDate = expectedReceiptDate ? format.format({ value: expectedReceiptDate, type: format.Type.DATE }) : null;
        if (formattedExpectedReceiptDate == formattedTranDate || !formattedExpectedReceiptDate) {
          log.audit('updating expected receipt date for PO = ', poId);
          let oneWeekAfterReceiptDate = new Date(trandate);
          let noOfDays = (vendorDays && vendorDays > 0) ? vendorDays : 7;
          oneWeekAfterReceiptDate = new Date(oneWeekAfterReceiptDate.setDate(trandate.getDate() + noOfDays));
          currRecord.setSublistValue('item', 'expectedreceiptdate', i, formatDate(oneWeekAfterReceiptDate));
        }
      }
    };

    currRecord.save();
  }

  const getDaysFromVendor = (vendorId) => {
    if (!vendorId) return null;
    const vendorLookup = search.lookupFields({
      type: 'vendor',
      id: vendorId,
      columns: ['custentity_msg_delivery_time']
    });
    return Number(vendorLookup?.custentity_msg_delivery_time) || 0;
  }

  const formatDate = (dateParam) => {
    if (dateParam && dateParam != "") {
      try {
        return format.parse({ value: dateParam, type: format.Type.DATE });
      } catch (error) {
        log.error('formatDate', error);
        return null;
      }
    } else return null;
  };

  return {
    getInputData: getInputData,
    map: map,
    summarize: summarize,
  };
});
