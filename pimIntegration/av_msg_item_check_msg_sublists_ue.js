/** 
 * Copyright (c) 2023 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @author: Tuba Mohsin
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @description: Check if MSGs sublist (price or producer stock) is updated in Item
 */


define([], function () {
    const MSG_ITEM_STATUS = {
        EOL: '2',
    }

    function beforeSubmit(context) {
        if (context.type == 'create') {
            context.newRecord.setValue({
                fieldId: 'custitem_msg_pim_calc_deliverytime',
                value: true
            });

            if (context.newRecord.getLineCount('recmachcustrecord_msg_ip_item') > 0) {
                context.newRecord.setValue({
                    fieldId: 'custitem_msg_pim_request_sync_price',
                    value: true
                });
            }

            if (context.newRecord.getLineCount('recmachcustrecord_av_item') > 0) {
                context.newRecord.setValue({
                    fieldId: 'custitem_msg_update_vendor_price',
                    value: true
                });
            }
        }
        if (context.type == 'edit') {
            try {
                checkPriceSublist(context);
                checkProducerStockSublist(context);
                checkFocusItemFields(context);
                checkMSGItemStatus(context);
                checkVendorPriceSublist(context);

            } catch (e) {
                log.error('Error', e);
            }
        }
    }

    function checkPriceSublist(context) {
        var oldArr = extractPriceValues(context.oldRecord);
        var newArr = extractPriceValues(context.newRecord);
        if (comparation(oldArr.flat(1), newArr.flat(1))) {
            log.audit('price updated for item =>', context.newRecord.id);
            context.newRecord.setValue({
                fieldId: 'custitem_msg_pim_request_sync_price',
                value: true
            });
        }
    }

    function checkVendorPriceSublist(context) {
        var oldArr = extractVendorPriceValues(context.oldRecord);
        var newArr = extractVendorPriceValues(context.newRecord);
        if (comparation(oldArr.flat(1), newArr.flat(1))) {
            log.audit('vendor price updated for item =>', context.newRecord.id);
            context.newRecord.setValue({
                fieldId: 'custitem_msg_update_vendor_price',
                value: true
            });
        }
    }

    function checkMSGItemStatus(context) {
        const oldVal = context.oldRecord.getValue('custitem_msg_item_status');
        const newVal = context.newRecord.getValue('custitem_msg_item_status');

        if (oldVal != newVal && (newVal == MSG_ITEM_STATUS.EOL || oldVal == MSG_ITEM_STATUS.EOL)) {
            context.newRecord.setValue({
                fieldId: 'custitem_msg_pim_calc_deliverytime',
                value: true
            });
        }
    }

    function checkFocusItemFields(context) {
        const oldVals = [];
        const newVals = [];

        oldVals.push(context.oldRecord.getValue('custitem_msg_is_focus_item'));
        oldVals.push(context.oldRecord.getValue('custitem_msg_focus_deliverydate'));
        oldVals.push(context.oldRecord.getValue('safetystocklevel'));

        newVals.push(context.newRecord.getValue('custitem_msg_is_focus_item'));
        newVals.push(context.newRecord.getValue('custitem_msg_focus_deliverydate'));
        newVals.push(context.newRecord.getValue('safetystocklevel'));

        if (comparation(oldVals.flat(1), newVals.flat(1))) {
            log.audit('focus fields updated for item for item =>', context.newRecord.id);
            context.newRecord.setValue({
                fieldId: 'custitem_msg_pim_calc_deliverytime',
                value: true
            });
        }

    }

    function extractPriceValues(rec) {
        var arr = [];
        const sublistCount = rec.getLineCount('recmachcustrecord_msg_ip_item');
        for (let i = 0; i < sublistCount; i++) {
            arr.push(rec.getSublistValue('recmachcustrecord_msg_ip_item', 'custrecord_msg_ip_b2b', i))
            arr.push(rec.getSublistValue('recmachcustrecord_msg_ip_item', 'custrecord_msg_ip_base', i))
            arr.push(rec.getSublistText('recmachcustrecord_msg_ip_item', 'custrecord_msg_ip_date_from', i))
            arr.push(rec.getSublistValue('recmachcustrecord_msg_ip_item', 'custrecord_msg_ip_srp', i))
            arr.push(rec.getSublistText('recmachcustrecord_msg_ip_item', 'custrecord_msg_ip_date_to', i))
            arr.push(rec.getSublistValue('recmachcustrecord_msg_ip_item', 'custrecord_msg_ip_minimal', i))

        }
        return arr;
    }

    function extractVendorPriceValues(rec) {
        var arr = [];
        const sublistCount = rec.getLineCount('recmachcustrecord_av_item');
        for (let i = 0; i < sublistCount; i++) {
            arr.push(rec.getSublistValue('recmachcustrecord_av_item', 'custrecord_av_vendor', i))
            arr.push(rec.getSublistValue('recmachcustrecord_av_item', 'custrecord_av_ek', i))
            arr.push(rec.getSublistText('recmachcustrecord_av_item', 'custrecord_av_valid_from', i))
            arr.push(rec.getSublistText('recmachcustrecord_av_item', 'custrecord_av_valid_to', i))
        }
        return arr;
    }

    function checkProducerStockSublist(context) {
        var oldArr = extractProducerStockValues(context.oldRecord);
        var newArr = extractProducerStockValues(context.newRecord);
        if (comparation(oldArr.flat(1), newArr.flat(1))) {
            log.audit('Producer Stock updated for item =>', context.newRecord.id);
            context.newRecord.setValue({
                fieldId: 'custitem_msg_pim_calc_deliverytime',
                value: true
            });
        }
    }

    function extractProducerStockValues(rec) {
        var arr = [];
        const sublistCount = rec.getLineCount('recmachcustrecord_msg_vsa_item');
        for (let i = 0; i < sublistCount; i++) {
            arr.push(rec.getSublistValue('recmachcustrecord_msg_vsa_item', 'custrecord_msg_vsa_availability', i));
            arr.push(rec.getSublistValue('recmachcustrecord_msg_vsa_item', 'custrecord_msg_vsa_is_available', i));
            arr.push(rec.getSublistValue('recmachcustrecord_msg_vsa_item', 'custrecord_msg_vsa_vendor', i));
            arr.push(rec.getSublistValue('recmachcustrecord_msg_vsa_item', 'custrecord_msg_vsa_quantity', i));
        }
        return arr;
    }

    function comparation(a, b) {

        if (a.length != b.length) {
            return true;
        } else {
            for (var i = 0; i < a.length; i++) {
                if (a[i] != b[i]) {
                    return true;
                };
            }
        }
        return false;
    }

    return {
        beforeSubmit: beforeSubmit,
    };
});