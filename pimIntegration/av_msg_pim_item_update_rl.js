/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 ** @author: [Tuba Mohsin]
 ** @dated: [18.01.2023]
 ** @Description: Integration between PIMCore and NetSuite
 *
 *  @NApiVersion 2.1
 *  @NScriptType Restlet
 */
define([
  "N/record",
  "N/search",
  "./av_int_log.js",
  "N/runtime"
], function (record, search, avLog, runtime) {
  const self = this;

  const CONSTANTS = {
    INTERFACES: {
      // set deployment id as key
      customdeploy_interface_p03_post: {
        onActionFunction: updateItemDetails,
        iface: "P03",
        httpMethod: "POST",
      },
    },
    HTTP_CODE: {
      SUCCESS: 200,
      ERROR: 400,
    },
    INTERFACE_STATE: {
      NEW: 1,
      ERROR: 2,
      DONE: 3,
    },
    ERROR_MESSAGES: {
      P03_ITEM_NOT_EXIST: "PIM30001",
      P03_EX: "PIM30002",
      P03_SUCCESS: "PIM30003",
      P03_EMPTY_PRODUCT: "PIM30004",
    }
  };

  function entryPoint(ctx) {
    self.logId = avLog.create({
      iface: CONSTANTS.INTERFACES[runtime.getCurrentScript().deploymentId].iface,
      state: CONSTANTS.INTERFACE_STATE.NEW,
    });
    return CONSTANTS.INTERFACES[runtime.getCurrentScript().deploymentId].onActionFunction(ctx, self.logId);
  }

  function updateItemDetails(ctx, logId) {
    try {
      const body = ctx;
      if (body) {
        if (body.productNumber) {
          return handlePostP03Success(body, logId);
        } else {
          const errorDetails = getErrorMessage(CONSTANTS.ERROR_MESSAGES.P03_EMPTY_PRODUCT);
          const message = errorDetails ? errorDetails.msg : "";
          const send = JSON.stringify({
            code: CONSTANTS.HTTP_CODE.ERROR,
            message: message,
          });
          avLog.update({
            id: logId || self.logId,
            state: CONSTANTS.INTERFACE_STATE.ERROR,
            send: send,
            receive: body
          });
          avLog.error_message({
            logid: logId || self.logId,
            var1: JSON.stringify(errorDetails),
            msg: CONSTANTS.ERROR_MESSAGES.P03_EMPTY_PRODUCT,
          });
          return send;
        }
      }
    } catch (e) {
      log.debug('error in updateItemDetails', e)
      const errorDetails = getErrorMessage(CONSTANTS.ERROR_MESSAGES.P03_EX);
      const message = errorDetails
        ? errorDetails.msg + ' "' + e.message + '"'
        : e.message;

      const send = JSON.stringify({
        code: CONSTANTS.HTTP_CODE.ERROR,
        message: message,
      });

      avLog.update({
        id: logId || self.logId,
        state: CONSTANTS.INTERFACE_STATE.ERROR,
        send: send,
      });
      avLog.error_message({
        logid: logId || self.logId,
        var1: e,
        msg: CONSTANTS.ERROR_MESSAGES.P03_EX,
      });

      return send;
    }
  }

  function getErrorMessage(name) {
    if (!name) return false;
    const errorMessageSearch = search
      .create({
        type: "customrecord_av_error_messages",
        filters: [["name", search.Operator.IS, name]],
        columns: [search.createColumn({ name: "custrecord_av_err_msg" })],
      })
      .run()
      .getRange({
        start: 0,
        end: 1,
      });

    return errorMessageSearch.length
      ? {
        id: errorMessageSearch[0].id,
        msg: errorMessageSearch[0].getValue("custrecord_av_err_msg"),
      }
      : null;
  }

  function handlePostP03Success(body, logId) {
    avLog.update({
      id: logId || self.logId,
      receive: body,
    });

    const itemS = findItemByItemId(body);
    const itemId = itemS && itemS.itemId;
    const itemType = itemS && itemS.itemType;

    try {
      if (itemId && itemType) {
        const itemRecord = record.load({
          type: itemType,
          id: itemId,
          // isDynamic: true,
        });

        itemRecord.setValue({
          fieldId: "custitem_av_item_image_url",
          value: body.imageUrl,
        });
        const salesDescriptions = body.salesDescriptions;
        if (salesDescriptions && salesDescriptions.length) {
          const en_description = salesDescriptions.find(x => x.language == 'en_GB');
          if (en_description) {
            itemRecord.setValue({
              fieldId: "salesdescription",
              value: en_description.description,
            });
          }
          const count = itemRecord.getLineCount('translations');
          for (let x = 0; x < count; x++) {
            const locale = itemRecord.getSublistValue('translations', 'locale', x);
            const locale_description = salesDescriptions.find(x => x.language == locale);
            itemRecord.setSublistValue('translations', 'salesdescription', x, locale_description.description);
          }

          // switch (description.language) {
          //   case "en_GB":
          //     itemRecord.setValue({
          //       fieldId: "salesdescription",
          //       value: description.description,
          //     });
          //     break;
          //   case "de_DE":
          //     itemRecord.selectLine({
          //       sublistId: "translations",
          //       line: 0,
          //     });

          //     itemRecord.setCurrentSublistValue({
          //       sublistId: "translations",
          //       fieldId: "salesdescription",
          //       value: description.description,
          //     });

          //     itemRecord.commitLine({
          //       sublistId: "translations",
          //     });
          //     break;
          // }
          // });
        }

        itemRecord.save({
          enableSourcing: false,
          ignoreMandatoryFields: true,
        });

        const errorDetails = getErrorMessage(
          CONSTANTS.ERROR_MESSAGES.P03_SUCCESS
        );
        const message = errorDetails
          ? errorDetails.msg + " " + body.productNumber
          : "";
        const send = JSON.stringify({
          code: CONSTANTS.HTTP_CODE.SUCCESS,
          message: message,
        });
        avLog.update({
          id: logId || self.logId,
          state: CONSTANTS.INTERFACE_STATE.DONE,
          item: itemId,
          send: send,
        });
        return send;
      } else {
        const errorDetails = getErrorMessage(
          CONSTANTS.ERROR_MESSAGES.P03_ITEM_NOT_EXIST
        );
        const message = errorDetails ? errorDetails.msg : "";
        const send = JSON.stringify({
          code: CONSTANTS.HTTP_CODE.ERROR,
          message: message + " : " + body.productNumber,
        });
        avLog.update({
          id: logId || self.logId,
          state: CONSTANTS.INTERFACE_STATE.ERROR,
          send: send,
        });
        avLog.error_message({
          logid: logId || self.logId,
          var1: send,
          msg: CONSTANTS.ERROR_MESSAGES.P03_ITEM_NOT_EXIST,
        });
        return send;
      }
    } catch (e) {
      const errorDetails = getErrorMessage(CONSTANTS.ERROR_MESSAGES.P03_EX);
      const message = errorDetails
        ? errorDetails.msg + ' "' + e.message + '"'
        : e.message;
      const send = JSON.stringify({
        code: CONSTANTS.HTTP_CODE.ERROR,
        message: message,
      });
      avLog.update({
        id: logId || self.logId,
        state: CONSTANTS.INTERFACE_STATE.ERROR,
        send: send,
      });
      avLog.error_message({
        logid: logId || self.logId,
        var1: JSON.stringify(e),
        msg: CONSTANTS.ERROR_MESSAGES.P03_EX,
      });
      log.error('error', e);
      return send;
    }
  }

  function findItemByItemId(body) {
    let itemId, itemType;

    const itemSearch = search
      .create({
        type: "item",
        filters: [
          ["itemid", "is", body.productNumber],
          "AND",
          ["isinactive", "is", "F"],
        ],
        columns: [],
      })
      .getAllResultsFromSavedSearch();

    itemSearch.forEach(function (result) {
      itemId = result.id;
      itemType = result.recordType;
      return true;
    });

    if (!itemId) return null;

    return {
      itemId: itemId,
      itemType: itemType,
    };
  }

  function methodNotImplemented() {
    return JSON.stringify({ message: "This method has not been implemented" });
  }

  Object.prototype.getAllResultsFromSavedSearch = function () {
    const self = this;
    const results = self.run();
    const searchResults = [];
    let searchid = 0;
    do {
      var resultslice = results.getRange({
        start: searchid,
        end: searchid + 1000,
      });
      resultslice.forEach(function (slice) {
        searchResults.push(slice);
        searchid++;
      });
    } while (resultslice.length >= 1000);
    return searchResults;
  };

  return {
    get: methodNotImplemented,
    post: entryPoint,
    put: methodNotImplemented,
    delete: methodNotImplemented,
    updateItemDetails: updateItemDetails,
  };
});
