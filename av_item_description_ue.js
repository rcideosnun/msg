/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tehseen Ahmed
* Date: 16/8/2022
* Updates purchase and sales description on item
* @NApiVersion 2.1
* @NScriptType UserEventScript
*/

define(['N/record'], function (record) {
  function beforeSubmit(context) {
    try {
      if (context.type === context.UserEventType.CREATE
        || context.type === context.UserEventType.EDIT) {
        var newRecord = context.newRecord;

        var displayName = newRecord.getValue({
          fieldId: 'displayname'
        });

        newRecord.setValue({
          fieldId: 'purchasedescription',
          value: displayName
        });

        newRecord.setValue({
          fieldId: 'salesdescription',
          value: displayName
        });
        
      }
    } catch (ex) {
      log.error({ title: 'beforeSubmit : ex', details: ex });
    }
  }

  return {
    beforeSubmit: beforeSubmit
  }
})