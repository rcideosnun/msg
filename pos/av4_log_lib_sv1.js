/*AV4 Interface Log Model:

	logIface = {
		send: string,
		receive: string,
		responseCode: number,
		state: string
		stagingRec: internalid,
		url: string
	}
*/

function logIface(log) {

	if (log.id) {
		var rec = nlapiLoadRecord('customrecord_av_integration_log', log.id);
	} else {
		var rec = nlapiCreateRecord('customrecord_av_integration_log');
	}
	if (log.iface) {
        setText(rec, 'custrecord_av_integration_type', log.iface)
	}
	rec = setLongtext(rec, 'custrecord_av_integration_send', log.send);
	rec = setLongtext(rec, 'custrecord_av_integration_received', log.receive);
	rec = setValue(rec, 'custrecord_av_integration_code', log.responseCode);
	rec = setValue(rec, 'custrecord_av_integration_state', log.state);
	rec = setValue(rec, 'custrecord_av_integration_url', log.url);

	return nlapiSubmitRecord(rec, true);
}

function setValue(rec, fieldId, value) {
	if (rec && fieldId && value) {
		rec.setFieldValue(fieldId, value);
	}
	return rec;
}

function setText(rec, fieldId, value) {
	if (rec && fieldId && value) {
		rec.setFieldText(fieldId, value);
	}
	return rec;
}

function setLongtext(rec, fieldId, value) {
	if (value) {
		if ( typeof value == 'object') {
			value = JSON.stringify(value);
		}
		value = value.substr(0, 100000);
		rec.setFieldValue(fieldId, value);
	}
	return rec;
}

/*AV4 Interface Error Model:

	error_message = {
		msg: string,
		logid: string,
		var1: number,
		var2: string
		tran: internalid,
		entity: internalid
	}
*/
    function error_message(error) {
        
        if (error && error.var1) {
            var em = nlapiCreateRecord('customrecord_av_interface_errormessage');
            
            if (error.logid) {
                setValue(em, 'custrecord_av_if_er_log', error.logid)
            }
            if (error.iface) {
                setValue(em, 'custrecord_av_if_er_type', error.iface)
            }
            if (error.msg) {
                setText(em, 'custrecord_av_if_er_msg', error.msg.toString().substr(0, 300))
            }
            if(error.var1) {
                setValue(em, 'custrecord_av_if_er_var1', error.var1.toString().substr(0, 300))
            }
            var eid = nlapiSubmitRecord(em);
        }

        return {
            error: error.msg,
            msg: error.msg,
            var2: error.var2
        };

    }

function log_error(error) {

	var answer = error_message(error);

	var logid = logIface({
		id : error.logid,
		transaction : error.tran,
		send : answer,
		state : 3
	});

	
	return answer;

}