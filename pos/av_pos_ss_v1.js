/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 1.0
 ** @author: [Roberto Cideos]
 ** @dated: [04.28.2021]
 ** @Description: POS interfaces
 ************************************************************************************** */

var self = this;
var context = nlapiGetContext(),
	pos_config_id = context.getSetting('SCRIPT', 'custscript_av4_pos_config_record') || context.getSetting('SCRIPT', 'custscript_av4_pos_config_record_wf'),
	interface_type = context.getSetting('SCRIPT', 'custscript_av4_pos_if_type') || context.getSetting('SCRIPT', 'custscript_av4_pos_if_type_wf');
dateAfterQuery = context.getSetting('SCRIPT', 'custscript_av4_pos_date_after') || 1;

if (nlapiGetContext().getEnvironment() == 'SANDBOX' || 'PRODUCTION') {

	/*DEFINE CONSTANTS*//*DEFINE CONSTANTS*//*DEFINE CONSTANTS*//*DEFINE CONSTANTS*//*DEFINE CONSTANTS*//*DEFINE CONSTANTS*//*DEFINE CONSTANTS*//*DEFINE CONSTANTS*/

	var POS_CONFIG = nlapiLookupField('customrecord_av4_pos_configuration', pos_config_id, pos_fields);
	var CREDENTIALS = {
		username: POS_CONFIG.custrecord_av4_pos_username,
		password: POS_CONFIG.custrecord_av4_pos_password
	};

	var API_BASEURL = {
		auth: POS_CONFIG.custrecord_av4_pos_authurl,
		base: POS_CONFIG.custrecord_av4_pos_baseurl
	};

	var API_ENDPOINTS = {
		API_GET: {
			item: POS_CONFIG.custrecord_av4_pos_items_get,
			customer: POS_CONFIG.custrecord_av4_pos_customers_get,
			document: POS_CONFIG.custrecord_av4_pos_documents_get
		},
		API_POST: {
			item: POS_CONFIG.custrecord_av4_pos_items_post,
			customer: POS_CONFIG.custrecord_av4_pos_customers_post,
			document: POS_CONFIG.custrecord_av4_pos_documents_post,
			addItemsToOrder: POS_CONFIG.custrecord_av4_pos_additem_post
		}
	};

	var INVOICE_DOCUMENT_TYPE = 'A'; //Auftrag
	var CUSTOMER_FLOUR_ID = POS_CONFIG.custrecord_av4_pos_customer_id;

	var BASE_PRICE_LIST = POS_CONFIG.custrecord_av4_pos_pricelist;

	var PRICE_MATRIX = {
		'USD': 'price4',
		'GBP': 'price2',
		'CAD': 'price3',
		'EUR': 'price1',
		'CHF': 'price5'
	};

	var LOG_STATUS = {
		NEW: 1,
		ERROR: 2,
		DONE: 3
	};

	var ERROR_MESSAGE = {
		POS: 'POS1'
	};

	var HTTP_CODES = {
		SUCCESS: 200,
		ERROR: 401
	};
	var DEFAULT_ANZAHLUNG_ITEM_FLOUR = 'ARTANZ'; //'10001';
	var DEFAULT_ANZAHLUNG_ITEM_NS = "Anzahlung" || context.getSetting('SCRIPT', 'custscript_av4_pos_anzahlung_item');
}

function execute() {

	var if_type = self.ifaceType = nlapiLookupField('customrecord_av_interface_type', interface_type, 'name');

	nlapiLogExecution('DEBUG', 'inbound called function', if_type);
	self.token = self._generateToken();

	switch (if_type) {
		case 'POS1':
			return call_I01(if_type);
		case 'POS3':
			return call_I03(if_type);
		case 'POS5':
			return call_I05(if_type);
		default:
			return;
	}
}

function call_I01(if_type) {
	var item = nlapiGetNewRecord();
	var itemNumber = item.getFieldValue('itemid');
	var itemDisplay = item.getFieldValue('displayname');
	var subsidiary = item.getFieldValue('subsidiary');
	var itemEan = item.getFieldValue("upccode");
	var itemHersteller = item.getFieldText("custitem_av_item_brand");
	var itemVendorN = item.getFieldValue("vendorname");

	self._getPosSettings(subsidiary);

	if (!itemNumber || !self.posSettings.accountId || !subsidiary) return true;

	var currency = nlapiLookupField('subsidiary', subsidiary, 'currency');

	var priceCurrencyList = PRICE_MATRIX[nlapiLookupField('currency', currency, 'name')];

	var itemPrice = item.getLineItemValue(priceCurrencyList, 'price_1_', 1);

	var item_obj = {
		number: String(itemNumber),
		title: String(itemDisplay),
		ean: String(itemEan),
		hersteller: String(itemHersteller),
		herstellernummer: String(itemVendorN),
		taxassignment: String(self.posSettings.accountId),
		sale: [{
			pricelist: BASE_PRICE_LIST,
			price: itemPrice
		}]
	};

	var headers = {
		'Content-type': 'application/json',
		'Authorization': 'JWT ' + self.token
	};

	self._generateLog(API_BASEURL.base.concat(API_ENDPOINTS.API_POST.item),
		item_obj,
		headers,
		null,
		null,
		if_type,
		item.id);

	var response_item_creation = nlapiRequestURL(API_BASEURL.base.concat(API_ENDPOINTS.API_POST.item), JSON.stringify(item_obj), headers);
	var response_item_creation_body = JSON.parse(response_item_creation.getBody());

	self._updateLog(API_BASEURL.base.concat(API_ENDPOINTS.API_POST.item),
		item_obj,
		headers,
		response_item_creation.code,
		JSON.stringify(response_item_creation_body),
		LOG_STATUS.DONE);
}

function call_I03(ifaceType) {

	var salesOrderRec = self.salesOrderRec = nlapiGetNewRecord();
	var invId = salesOrderRec.getId();
	var invNumber = salesOrderRec.getFieldValue('tranid');

	var headers = {
		'Content-type': 'application/json',
		'Authorization': 'JWT '.concat(String(self.token))
	};

	var anzahlungMetadata = JSON.stringify({
		isAnzahlung: true,
		documentNumber: invNumber,
		documentId: self.salesOrderRec.id
	});

	var payment_obj = {
		type: INVOICE_DOCUMENT_TYPE, // A=WA-Auftrag 
		businesspartner: CUSTOMER_FLOUR_ID,
		metadata: anzahlungMetadata
	};

	var reqUrl = API_BASEURL.base.concat(API_ENDPOINTS.API_POST.document);
	var prepaymentRes = nlapiRequestURL(reqUrl, JSON.stringify(payment_obj), headers);
	var prepaymentResBody = JSON.parse(prepaymentRes.getBody());

	self._generateLog(reqUrl,
		payment_obj,
		headers,
		prepaymentRes.code,
		JSON.parse(prepaymentRes.getBody()),
		ifaceType,
		null,
		invId);

	self._addAnzahlungItem(prepaymentResBody._id);
}

function call_I05(if_type) {

	var pos_staging_orders = self._searchExistingOrders();

	var headers = {
		'Content-type': 'application/json',
		'Authorization': 'JWT '.concat(String(self.token))
	};

	var dateAfter = nlapiAddDays(new Date(), -dateAfterQuery);
	var dateToISO8601 = String(dateAfter.getFullYear()).concat('-').concat(String(dateAfter.getMonth() + 1)).concat('-').concat(String(dateAfter.getDate()));
	var reqUrl = API_BASEURL.base.concat(API_ENDPOINTS.API_GET.document).concat('?dateAfter=').concat(dateToISO8601);
	var response_document_creation = nlapiRequestURL(reqUrl, null, headers);

	self._generateLog(reqUrl,
		null,
		headers,
		response_document_creation.code,
		JSON.parse(response_document_creation.getBody()),
		interface_type);

	var response_document_creation_body = JSON.parse(response_document_creation.getBody());

	nlapiLogExecution('DEBUG', 'Get worked', JSON.stringify(response_document_creation_body));

	if (response_document_creation.code == 200 && response_document_creation_body.length > 0) {
		for (var i = 0; i < response_document_creation_body.length; i++) {
			if (pos_staging_orders.indexOf(response_document_creation_body[i]._id) == -1) {
				var staging_record = nlapiCreateRecord('customrecord_av4_pos_staging');
				for (var j = 0; j < Object.keys(staging_record_mapping).length; j++) {

					if (Object.keys(staging_record_mapping)[j] == 'custrecord_av4_pos_staging_sa' || Object.keys(staging_record_mapping)[j] == 'custrecord_av4_pos_staging_ba') {
						var address = JSON.stringify({ address: response_document_creation_body[i][staging_record_mapping[Object.keys(staging_record_mapping)[j]]] });
						staging_record.setFieldValue(Object.keys(staging_record_mapping)[j], address);
					} else if (Object.keys(staging_record_mapping)[j] == 'custrecord_av4_pos_staging_cs') {
						staging_record.setFieldText(Object.keys(staging_record_mapping)[j], response_document_creation_body[i][staging_record_mapping[Object.keys(staging_record_mapping)[j]]].iso);
					} else if (Object.keys(staging_record_mapping)[j] == 'custrecord_av4_pos_store_settings') {
						self._getCashPointId(response_document_creation_body[i].pos[staging_record_mapping[Object.keys(staging_record_mapping)[j]]]);
						self.cashPointId ? staging_record.setFieldValue(Object.keys(staging_record_mapping)[j], self.cashPointId) : null;
					} else if (Object.keys(staging_record_mapping)[j] == 'custrecord_av4_pos_staging_pm') {
						self.curentPaymentMethod = response_document_creation_body[i].payments.length ? response_document_creation_body[i].payments[0].ref : null;
					} else if (Object.keys(staging_record_mapping)[j] == 'custrecord_av4_pos_staging_tc') {
						var rate = response_document_creation_body[i][staging_record_mapping[Object.keys(staging_record_mapping)[j]]].length ? response_document_creation_body[i][staging_record_mapping[Object.keys(staging_record_mapping)[j]]][0]['rate'] : null;
						staging_record.setFieldValue(Object.keys(staging_record_mapping)[j], rate);
					} else {
						staging_record.setFieldValue(Object.keys(staging_record_mapping)[j], response_document_creation_body[i][staging_record_mapping[Object.keys(staging_record_mapping)[j]]]);
					}

				}
				for (var k = 0; k < response_document_creation_body[i].items.length; k++) {
					var metadata = response_document_creation_body[i].items[k].metadata ? JSON.parse(response_document_creation_body[i].items[k].metadata) : '';
					if (!metadata) {
						staging_record.selectNewLineItem('recmachcustrecord_av4_pos_staging_line_record');
						for (var l = 0; l < Object.keys(staging_record_lines_mapping).length; l++) {
							if (Object.keys(staging_record_lines_mapping)[l] == 'custrecord_av4_pos_staging_line_item') {
								var itemId = _getItemByUPC(response_document_creation_body[i].items[k][staging_record_lines_mapping[Object.keys(staging_record_lines_mapping)[l]]]);
								if (itemId) {
									staging_record.setCurrentLineItemValue('recmachcustrecord_av4_pos_staging_line_record', Object.keys(staging_record_lines_mapping)[l], itemId);
								} else {
									break;
								}
							} else {
								staging_record.setCurrentLineItemValue('recmachcustrecord_av4_pos_staging_line_record', Object.keys(staging_record_lines_mapping)[l], response_document_creation_body[i].items[k][staging_record_lines_mapping[Object.keys(staging_record_lines_mapping)[l]]]);
							}
						}
						if (itemId) staging_record.commitLineItem('recmachcustrecord_av4_pos_staging_line_record');
					}
				}

				if (self.cashPointObj && self.cashPointId && self.curentPaymentMethod && self.cashPointDefaultCustomer) {
					staging_record.setFieldValue('custrecord_av4_pos_staging_pm', self.cashPointObj[self.cashPointId][self.curentPaymentMethod]);
					staging_record.setFieldValue('custrecord_av4_pos_staging_customer', self.cashPointDefaultCustomer);
				}

				if (self.logid) {
					staging_record.setFieldValue('custrecord_av4_pos_staging_log', self.logid);
				}
				try {
					if (self.curentPaymentMethod) {
						var recId = nlapiSubmitRecord(staging_record);
						nlapiTriggerWorkflow('customrecord_av4_pos_staging', recId, 'customworkflow_av4_pos_create_order', 'workflowaction88230');
					}
				} catch (e) {
					self._generateError(null, null, JSON.stringify(e));
				}
			}
		}
	} else if (response_document_creation.code == 200 && !response_document_creation_body.length) {
		self._updateLog(reqUrl,
			null,
			headers,
			response_document_creation.code,
			JSON.parse(response_document_creation.getBody()),
			LOG_STATUS.DONE);
	}
}

function call_I06(if_type) {

}
function call_I07(if_type) {



}

function _generateToken() {
	var headers = {
		'Content-type': 'application/json'
	};

	var payload = {
		username: CREDENTIALS.username,
		password: CREDENTIALS.password
	};

	var token_response = nlapiRequestURL(API_BASEURL.base.concat(API_BASEURL.auth), JSON.stringify(payload), headers);
	var token_response_body = JSON.parse(token_response.getBody());
	try {
		nlapiSubmitField('customrecord_av4_pos_configuration', pos_config_id, 'custrecord_av4_pos_token', token_response_body.token);
		return token_response_body.token;
	} catch (e) {
		nlapiLogExecution('DEBUG', 'Token couldn\'t be saved successfully');
		return null;
	}
}

function _generateLog(url, body, headers, responsecode, responsebody, iface, item, transaction) {
	var logid = logIface({
		send: body,
		iface: iface,
		receive: responsebody,
		responseCode: responsecode,
		state: LOG_STATUS.NEW,
		url: url,
		item: item || null,
		transaction: transaction || null
	});

	self.logid = logid;
}

function _updateLog(url, body, headers, responsecode, responsebody, status) {

	if (!self.logid) return;

	var logid = logIface({
		id: self.logid,
		send: body,
		receive: responsebody,
		responseCode: responsecode,
		state: status,
		url: url
	});

	self.logid = logid;
}

function _generateError(tran, entity, message) {

	error_message({
		msg: ERROR_MESSAGE.POS,
		logid: self.logid,
		var1: message
	});

	self._updateLog(null, null, null, null, null, LOG_STATUS.ERROR);

}

function _searchExistingOrders() {
	var pos_orders_array = [];
	var filters = [];

	var columns = [
		new nlobjSearchColumn('custrecord_av4_pos_staging_on')
	];

	var pos_staging_orders = nlapiSearchRecord('customrecord_av4_pos_staging', null, filters, columns) || [];

	for (var i = 0; i < pos_staging_orders.length; i++) {
		pos_orders_array.push(pos_staging_orders[i].getValue('custrecord_av4_pos_staging_on'));
	}

	return pos_orders_array;
}

function _getPosSettings(subsidiary) {
	self.posSettings = {};

	if (!subsidiary) return true;

	var filters = [
		['custrecord_av4_s_subsidiary', 'anyof', subsidiary],
		'AND',
		['custrecord_av4_pos_settings.custrecord_av4_default', 'is', 'T']
	];

	var columns = [
		new nlobjSearchColumn('name').setSort(false),
		new nlobjSearchColumn('custrecord_av4_s_pos_variance_account'),
		new nlobjSearchColumn('custrecord_av4_s_default_customer'),
		new nlobjSearchColumn('custrecord_av4_taxrate', 'CUSTRECORD_AV4_POS_SETTINGS', null),
		new nlobjSearchColumn('custrecord_av4_flour_accountid', 'CUSTRECORD_AV4_POS_SETTINGS', null)
	];

	var customrecord_av4_settingsSearch = nlapiSearchRecord('customrecord_av4_settings', null, filters, columns);

	for (var i = 0; customrecord_av4_settingsSearch && i < customrecord_av4_settingsSearch.length; i++) {

		self.posSettings = {
			id: customrecord_av4_settingsSearch[i].getId(),
			accountId: customrecord_av4_settingsSearch[i].getValue('custrecord_av4_flour_accountid', 'CUSTRECORD_AV4_POS_SETTINGS')
		};

	}
}

function _getItemByUPC(upcCode) {
	var itemId;

	var filters = [
		['upccode', 'is', upcCode]
	];

	var columns = [new nlobjSearchColumn('itemid')];

	var item_search = nlapiSearchRecord('item', null, filters, columns);

	for (var i = 0; item_search && i < item_search.length; i++) {

		itemId = item_search[i].getValue('itemid');

	}

	if (itemId) {
		return itemId;
	} else {
		return false;
	}

}


function _getCashPointId(cashPointExtId) {
	var cashPoint = {},
		cashPointId,
		cashPointDefaultCustomer;

	var filters = [
		['custrecord_av4_posid_cr', 'is', cashPointExtId]
	];
	var columns = [
		new nlobjSearchColumn('name').setSort(false),
		new nlobjSearchColumn('scriptid'),
		new nlobjSearchColumn('custrecord_av4_cr_store'),
		new nlobjSearchColumn('custrecord_av4_cr_default_customer'),
		new nlobjSearchColumn('custrecord_av4_cr_description'),
		new nlobjSearchColumn('custrecord_av4_posid_cr'),
		new nlobjSearchColumn('custrecord_av4_flour_paymentmethod', 'CUSTRECORD_AV4_PM_CASHPOINT', null),
		new nlobjSearchColumn('custrecord_av4_pm_id', 'CUSTRECORD_AV4_PM_CASHPOINT', null),
		new nlobjSearchColumn('internalid', 'CUSTRECORD_AV4_PM_CASHPOINT', null)
	];
	var cash_points_search = nlapiSearchRecord('customrecord_av4_cashregister', null, filters, columns);

	for (var i = 0; cash_points_search && (i < cash_points_search.length); i++) {
		if (!cashPoint[cash_points_search[i].getId()]) cashPoint[cash_points_search[i].getId()] = {};
		cashPoint[cash_points_search[i].getId()][cash_points_search[i].getValue('custrecord_av4_pm_id', 'CUSTRECORD_AV4_PM_CASHPOINT')] =
			cash_points_search[i].getValue('internalid', 'CUSTRECORD_AV4_PM_CASHPOINT');
		if (!cashPointId && cash_points_search[i].getId())
			cashPointId = cash_points_search[i].getId();
		if (!cashPointDefaultCustomer && cash_points_search[i].getValue('custrecord_av4_cr_default_customer'))
			cashPointDefaultCustomer = cash_points_search[i].getValue('custrecord_av4_cr_default_customer');

	}

	self.cashPointId = cashPointId;
	self.cashPointObj = cashPoint;
	self.cashPointDefaultCustomer = cashPointDefaultCustomer;
}

function _addAnzahlungItem(posDocumentId) {

	var invTotal = self.salesOrderRec.getFieldValue('origtotal');
	var invId = self.salesOrderRec.getId();

	var headers = {
		'Content-type': 'application/json',
		'Authorization': 'JWT '.concat(String(self.token))
	};

	var anzahlungMetadata = JSON.stringify({
		isAnzahlung: true,
		documentNumber: self.salesOrderRec.getFieldValue('tranid'),
		documentId: self.salesOrderRec.id
	});

	var item_obj = {
		'articles': [{ 'number': DEFAULT_ANZAHLUNG_ITEM_FLOUR, 'amount': 1, 'price': invTotal, 'notice': null, 'metadata': anzahlungMetadata }]
	};

	var reqUrl = API_BASEURL.base.concat(API_ENDPOINTS.API_POST.addItemsToOrder).replace('documentId', posDocumentId);
	var prepaymentRes = nlapiRequestURL(reqUrl, JSON.stringify(item_obj), headers);
	var prepaymentResBody = prepaymentRes.getBody();

	self._generateLog(reqUrl,
		item_obj,
		headers,
		prepaymentRes.code,
		JSON.parse(prepaymentResBody),
		self.ifaceType,
		null,
		invId);

	if (prepaymentRes.code == HTTP_CODES.SUCCESS) {
		self.salesOrderRec.setFieldValue('custbody_alta_prepaymentapplied', 'T');
	}
}