/**
 ** Copyright (c) 2015 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
** @version: 2.0
** @author: Roberto Cideos
** @dated: 19/05/2022
** @Description: Create GC Custom record on creation of native GC
*
*@NApiVersion 2.1
*@NScriptType UserEventScript
*
*/

define(["N/runtime",
    "N/record",
    "N/search"],
    function (runtime,
        record,
        search) {

        var self = this;

        const CONSTANTS = {
            GC_CONFIGURATION: {
                "EUR": {
                    "currencyId": "1",
                    "origamt": _getOrigAmt,
                    "remamt": _getRemAmt,
                    "exchrate": 1
                },
                "CHF": {
                    "currencyId": "5",
                    "origamt": _getOrigAmt,
                    "remamt": _getRemAmt,
                    "exchrate": 1.067
                },
                "Hungarian Forint": {
                    "currencyId": "4",
                    "origamt": _getOrigAmt,
                    "remamt": _getRemAmt,
                    "exchrate": 401
                }
            },
            GC_LINES_REC_ID: "recmachcustrecord_av_parent"
        };

        /**
         * 
         * @param {Suitescript Object} context 
         * @returns 
         */

        function afterSubmit(context) {
            try {
                log.debug("context", JSON.stringify(context))
                if (runtime.envType == "SANDBOX") {
                    //setup sandbox global variables, GC are not listed as available records in the List/Record setup
                    self.CONST = {
                        GC_URL: `https://${runtime.accountId}.com/app/accounting/transactions/giftcertificaterecord.nl?id=${context.newRecord.id}`
                    }

                } else if (runtime.envType == "PRODUCTION") {
                    //setup production global variables, GC are not listed as available records in the List/Record setup

                    self.CONST = {
                        GC_URL: `https://${runtime.accountId}.com/app/accounting/transactions/giftcertificaterecord.nl?id=${context.newRecord.id}`
                    }
                }

                let eventRouter = {
                    "create": _handleAsEvent,
                    "edit": _handleAsEvent
                }

                if (typeof eventRouter[context.type] !== "function") {
                    return;
                }

                eventRouter[context.type](context);
            } catch (e) {
                log.debug("Error", JSON.stringify(e))
            }
        }

        /**
         * 
         * @param {Suitescript Object} context 
         * @returns 
         */

        function _handleAsEvent(context) {

            let newRec = context.newRecord
            let recType = newRec.type
            let gcRec = record.load({ //you have to load this record, otherwise the code wont show up
                type: recType,
                id: newRec.id
            })

            let eventRouterTransaction = {
                "giftcertificate": _handleGC
            }

            if (typeof eventRouterTransaction[recType] != "function") {
                return
            }

            eventRouterTransaction[recType](gcRec)
        }

        /**
         * 
         * @param {Suitescript Object} context 
         * @returns 
         */

        function _handleGC(rec) {
            let gcId = _getGcId(rec.getValue("giftcertcode"))
            if (gcId) {
                _updateGc(rec, gcId);
                return;
            };

            let fieldVals = _getGcMapping(rec);

            let cgcRec = record.create({
                type: "customrecord_av_cs_gc",
                isDynamic: true
            })

            for (fld in fieldVals) {
                if (fieldVals[fld]) {
                    cgcRec.setValue({
                        fieldId: fld,
                        value: fieldVals[fld]
                    })
                }
            }

            let lineFlds = _getGcLinesMapping(rec);

            for (curr in CONSTANTS.GC_CONFIGURATION) {

                cgcRec.selectNewLine({
                    sublistId: CONSTANTS.GC_LINES_REC_ID
                })

                for (fld in CONSTANTS.GC_CONFIGURATION[curr]) {

                    if (typeof CONSTANTS.GC_CONFIGURATION[curr][fld] == "function") {
                        cgcRec.setCurrentSublistValue({
                            sublistId: CONSTANTS.GC_LINES_REC_ID,
                            fieldId: lineFlds[fld],
                            value: CONSTANTS.GC_CONFIGURATION[curr][fld](rec, curr)
                        })
                    } else {
                        cgcRec.setCurrentSublistValue({
                            sublistId: CONSTANTS.GC_LINES_REC_ID,
                            fieldId: lineFlds[fld],
                            value: CONSTANTS.GC_CONFIGURATION[curr][fld]
                        })
                    }
                }

                cgcRec.commitLine({
                    sublistId: CONSTANTS.GC_LINES_REC_ID
                })
            }

            cgcRec.save.promise().then(function (response) {
                log.debug("GC Custom created", `id: ${response}`)
            }, function (error) {
                log.debug("Error", JSON.stringify(error.message))
            });
        }

        /**
         * 
         * @param {String} gcCode 
         * @returns 
         */

        function _getGcId(gcCode) {
            if (!gcCode) return true
            let gcId;
            let gcSrch = search.create({
                type: "customrecord_av_cs_gc",
                filters:
                    [
                        ["custrecord_av_cs_gc_code", "is", gcCode]
                    ],
                columns: []
            });

            gcSrch.run().each(function (result) {
                gcId = result.id
                return true;
            });

            return gcId ? gcId : false
        }

        /**
         * 
         * @param {SuiteScript Object Record} rec 
         * @param {String} gcId 
         */

        function _updateGc(rec, gcId) {

            let cgcRec = record.load({ //you have to load this record, otherwise the code wont show up
                type: "customrecord_av_cs_gc",
                id: gcId,
                isDynamic: true
            })

            let fieldVals = _getGcMapping(rec);

            log.debug("fieldVals", JSON.stringify(fieldVals))

            for (fld in fieldVals) {
                cgcRec.setValue({
                    fieldId: fld,
                    value: fieldVals[fld]
                })
            }

            cgcRec = _cleanSublist(cgcRec);

            let lineFlds = _getGcLinesMapping(cgcRec);

            for (curr in CONSTANTS.GC_CONFIGURATION) {

                cgcRec.selectNewLine({
                    sublistId: CONSTANTS.GC_LINES_REC_ID
                })

                for (fld in CONSTANTS.GC_CONFIGURATION[curr]) {

                    if (typeof CONSTANTS.GC_CONFIGURATION[curr][fld] == "function") {
                        cgcRec.setCurrentSublistValue({
                            sublistId: CONSTANTS.GC_LINES_REC_ID,
                            fieldId: lineFlds[fld],
                            value: CONSTANTS.GC_CONFIGURATION[curr][fld](rec, curr)
                        })
                    } else {
                        cgcRec.setCurrentSublistValue({
                            sublistId: CONSTANTS.GC_LINES_REC_ID,
                            fieldId: lineFlds[fld],
                            value: CONSTANTS.GC_CONFIGURATION[curr][fld]
                        })
                    }
                }

                cgcRec.commitLine({
                    sublistId: CONSTANTS.GC_LINES_REC_ID
                })
            }

            cgcRec.save.promise().then(function (response) {
                log.debug("GC Custom updated", `id: ${response}`)
            }, function (error) {
                log.debug("Error", JSON.stringify(error.message))
            });

        }

        /**
         * 
         * @param {SuiteScript Object Record} rec 
         * @returns 
         */

        function _getGcMapping(rec) {

            return {
                "custrecord_av_cs_gc_code": rec.getValue("giftcertcode"),
                "custrecord_av_cs_gc_from": rec.getValue("sender"),
                "custrecord_av_cs_gc_rec": rec.getValue("name"),
                "custrecord_av_cs_gc_email": rec.getValue("email"),
                "custrecord_av_cs_gc_msg": rec.getValue("message"),
                "custrecord_av_cs_gc_exp": rec.getValue("expirationdate"),
                "custrecord_av_cs_gc_or_amt": rec.getValue("originalamount"),
                "custrecord_av_cs_gc_curr": rec.getValue("currency"),
                "custrecord_av_cs_gc_rem_amt": rec.getValue("amountremaining"),
                "custrecord_av_cs_gc_parent": `<a href=${self.CONST.GC_URL}>${rec.getValue("giftcertcode")}</a>`
            }
        }

        /**
         * 
         * @param {SuiteScript Object Record} rec 
         * @returns 
         */

        function _getGcLinesMapping() {
            return {
                "currencyId": "custrecord_av_gc_currency",
                "origamt": "custrecord_av_gc_amount",
                "remamt": "custrecord_av_gc_remaining_amount",
                "exchrate": "custrecord_av_exchange_rate"
            }
        }

        /**
         * 
         * @param {SuiteScript Object Record} rec 
         * @param {String} curr
         */

        function _getOrigAmt(rec, curr) {

            return Number(rec.getValue("originalamount")) * CONSTANTS.GC_CONFIGURATION[curr].exchrate

        }

        /**
         * 
         * @param {SuiteScript Object Record} rec 
         * @param {String} curr
         */

        function _getRemAmt(rec, curr) {

            return Number(rec.getValue("amountremaining")) * CONSTANTS.GC_CONFIGURATION[curr].exchrate

        }

        /**
         * 
         * @param {SuiteScript Object Record} gcRec 
         * @returns 
         */

        function _cleanSublist(gcRec) {

            var gcLen = gcRec.getLineCount(CONSTANTS.GC_LINES_REC_ID);

            for (var i = gcLen - 1; i >= 0; i--) {

                gcRec.removeLine({
                    sublistId: CONSTANTS.GC_LINES_REC_ID,
                    line: i,
                    ignoreRecalc: true
                });

            }

            return gcRec
        }

        return {
            afterSubmit: afterSubmit
        };

    })