/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 1.0
 ** @author: [Roberto Cideos]
 ** @dated: [04.28.2021]
 ** @Description: POS interfaces - Fields Mapping
 ************************************************************************************** */

var pos_fields = ["custrecord_av4_pos_username",
    "custrecord_av4_pos_password",
    "custrecord_av4_pos_baseurl",
    "custrecord_av4_pos_authurl",
    "custrecord_av4_pos_items_get",
    "custrecord_av4_pos_items_post",
    "custrecord_av4_pos_customers_get",
    "custrecord_av4_pos_customers_post",
    "custrecord_av4_pos_documents_get",
    "custrecord_av4_pos_documents_post",
    "custrecord_av4_pos_pricelist",
    "custrecord_av4_pos_customer_id",
    "custrecord_av4_pos_additem_post"]


var staging_record_mapping = {
    "custrecord_av4_pos_staging_on": "_id",
    "custrecord_av4_pos_staging_customer": "businesspartner",
    "custrecord_av4_pos_staging_pm": "paymentMethod",
    "custrecord_av4_pos_staging_om": "number",
    "custrecord_av4_pos_staging_op": "paid",
    "custrecord_av4_pos_staging_cd": "date",
    "custrecord_av4_pos_staging_cs": "currency",
    "custrecord_av4_pos_staging_tt": "taxType",
    "custrecord_av4_pos_staging_tc": "tax",
    "custrecord_av4_pos_staging_sa": "shippingAddress",
    "custrecord_av4_pos_staging_ba": "billingAddress",
    "custrecord_av4_pos_store_settings": "pointofsale"
}

var staging_record_lines_mapping = {
    "custrecord_av4_pos_staging_line_item": "businesspartner",
    "custrecord_av4_pos_staging_line_amount": "price",
    "custrecord_av4_pos_staging_line_qty": "amount",
    "custrecord_av4_pos_staging_line_taxrate": "taxRate",
    "custrecord_av4_pos_staging_line_item": "number"
}