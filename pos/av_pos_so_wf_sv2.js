/**
 ** Copyright (c) 2015 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
** @version: 2.0
** @author: Roberto Cideos
** @dated: 28/07/2022
** @Description: Create GC Custom record on creation of native GC - gc deployment on create doesn't works
*
*@NApiVersion 2.1
*@NScriptType WorkflowActionScript
*
*/

define(["N/record",
    "N/search"],
    function (record,
        search) {

        const CONSTANTS = {
            ITEM_TYPE: {
                GC: "GiftCert",
                GC_CUSTOM: "customrecord_av_cs_gc"
            }
        }

        function onAction(context) {
            try {
                log.debug("context", JSON.stringify(context))

                var eventRouter = {
                    "create": _handleAsEvent
                }

                if (typeof eventRouter[context.type] !== "function") {
                    return;
                }

                eventRouter[context.type](context);
            } catch (e) {
                log.debug("Error", JSON.stringify(e))
            }
        }

        function _handleAsEvent(context) {

            var newRec = context.newRecord
            var recType = newRec.type
            var gcRec = record.load({ //you have to load this record, otherwise the code wont show up
                type: recType,
                id: newRec.id
            })

            var eventRouterTransaction = {
                "salesorder": _handleSo
            }

            if (typeof eventRouterTransaction[recType] != "function") {
                return
            }

            eventRouterTransaction[recType](gcRec)
        }

        function _handleSo(rec) {
            var gcS = _getGcs();
            var lineCount = rec.getLineCount({
                sublistId: "item"
            })

            for(var i = 0; i < lineCount; i++) {
                let itemType = rec.getSublistValue({
                    sublistId: "item",
                    fieldId: "itemtype",
                    line: i
                });

                if(itemType == CONSTANTS.ITEM_TYPE.GC){
                    let gcId = gcS[rec.getSublistValue({
                        sublistId: "item",
                        fieldId: "giftcertnumber",
                        line: i
                    })];

                    if(!gcId) continue;

                    record.load({
                        type: "giftcertificate",
                        id: gcId
                    }).save()
                }
            }

        }

        function _getGcs() {
            var gcs = {};
            var gcSrch = search.create({
                type: "giftcertificate",
                filters: [],
                columns: ["giftcertcode"]
            });

            gcSrch.run().each(function (result) {
                gcs[result.getValue("giftcertcode")] = result.id;
                return true;
            });

            return gcs
        }

        return {
            onAction: onAction
        };

    })
    