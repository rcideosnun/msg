/**
 * Copyright (c) 2019 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @version: 2.x
 * @author: [Roberto Cideos]
 * @dated: [5.20.2021]
 * @Description:
 * WF Script to create orders from POS staging record
 *
 *@NApiVersion 2.1
 *@NScriptType WorkflowActionScript
 */

define(['N/record', 'N/search', 'N/runtime', 'N/workflow'], function (record, search, runtime, workflow) {

    var self = this;

    function onAction(scriptContext) {
        var contextRec = scriptContext.newRecord;

        if (runtime.envType == "SANDBOX") {
            var dF = _getOrderDefaultValues(contextRec)

            var anonCustomerId = dF.defaultCustomerId,
                locationId = dF.locationId,
                accountId = dF.accountId,
                paymentId = dF.paymentId,
                cashPointId = contextRec.getValue("custrecord_av4_pos_store_settings");

        } else if (runtime.envType == "PRODUCTION") {

            var dF = _getOrderDefaultValues(contextRec)

            var anonCustomerId = dF.defaultCustomerId,
                locationId = dF.locationId,
                accountId = dF.accountId,
                taxItemId = dF.taxItemId,
                paymentId = dF.paymentId,
                cashPointId = contextRec.getValue("custrecord_av4_pos_store_settings");
        }

        if (!anonCustomerId || !accountId || !locationId || !paymentId) return true

        try {

            var order = record.create({
                type: record.Type.CASH_SALE,
                isDynamic: true
            })

            order.setValue({
                fieldId: "entity",
                value: anonCustomerId,
                ignoreFieldChange: false,
                forceSyncSourcing: true

            })

            order.setValue({
                fieldId: "location",
                value: locationId,
                ignoreFieldChange: false

            })

            order.setValue({
                fieldId: "custbody_av4_cashregister",
                value: cashPointId,
                ignoreFieldChange: false

            })

            order.setValue({
                fieldId: "account",
                value: accountId,
                ignoreFieldChange: false

            })

            order.setValue({
                fieldId: "paymentmethod",
                value: paymentId,
                ignoreFieldChange: false

            })

            order.setValue({
                fieldId: "shippingcarrier",
                value: "",
                ignoreFieldChange: false

            })

            order.setValue({
                fieldId: "custbody_av4_pos_order",
                value: contextRec.id,
                ignoreFieldChange: false

            })

            var orderLinesLength = contextRec.getLineCount({
                sublistId: "recmachcustrecord_av4_pos_staging_line_record"
            })

            for (var i = 0; i < orderLinesLength; i++) {

                order.selectNewLine({
                    sublistId: "item"
                })

                order.setCurrentSublistText({
                    sublistId: "item",
                    fieldId: "item",
                    text: contextRec.getSublistValue({
                        sublistId: "recmachcustrecord_av4_pos_staging_line_record",
                        fieldId: "custrecord_av4_pos_staging_line_item",
                        line: i
                    })
                })

                order.setCurrentSublistValue({
                    sublistId: "item",
                    fieldId: "quantity",
                    value: contextRec.getSublistValue({
                        sublistId: "recmachcustrecord_av4_pos_staging_line_record",
                        fieldId: "custrecord_av4_pos_staging_line_qty",
                        line: i
                    })
                })

                var amount = parseFloat(contextRec.getSublistValue({
                    sublistId: "recmachcustrecord_av4_pos_staging_line_record",
                    fieldId: "custrecord_av4_pos_staging_line_amount",
                    line: i
                }))

                order.setCurrentSublistValue({
                    sublistId: "item",
                    fieldId: "grossamt",
                    value: amount * parseInt(contextRec.getSublistValue({
                        sublistId: "recmachcustrecord_av4_pos_staging_line_record",
                        fieldId: "custrecord_av4_pos_staging_line_qty",
                        line: i
                    }))
                })

                order.commitLine({
                    sublistId: "item"
                })
            }

            try {
                var tranId = order.save({
                    enableSourcing: true,
                    ignoreMandatoryFields: true
                })

                //custbody_av4_pos_order


            } catch (e) {
                log.error("ERROR", "Cannot create order from PO staging record:" + contextRec.id)
            }

            contextRec.setValue({
                fieldId: "custrecord_av4_pos_staging_transaction",
                value: tranId
            })


        } catch (e) {
            log.error("Error", JSON.stringify(e))
        }

    }

    function _getOrderDefaultValues(contextRec) {
        var cashPointId = contextRec.getValue("custrecord_av4_pos_store_settings")
        var paymentId = contextRec.getValue("custrecord_av4_pos_staging_pm")
        var taxRate = parseFloat(contextRec.getValue("custrecord_av4_pos_staging_tc"))

        var settingsSearch = search.lookupFields({
            type: "customrecord_av4_cashregister",
            id: cashPointId,
            columns: ["custrecord_av4_cr_default_customer", "custrecord_av4_cr_store"]
        })

        if (!settingsSearch) return true

        var defaultCustomerId = settingsSearch.custrecord_av4_cr_default_customer.length > 0 ? settingsSearch.custrecord_av4_cr_default_customer[0].value : null
        var locationId = settingsSearch.custrecord_av4_cr_store.length > 0 ? settingsSearch.custrecord_av4_cr_store[0].value : null

        var paymentSearch = search.lookupFields({
            type: "customrecord_av4_paymentmethods",
            id: paymentId,
            columns: ["custrecord_av4_pm_account", "custrecord_av4_ns_paymentmetthod"]
        })

        if (!paymentSearch) return true

        var accountId = paymentSearch.custrecord_av4_pm_account.length > 0 ? paymentSearch.custrecord_av4_pm_account[0].value : null
        var paymentId = paymentSearch.custrecord_av4_ns_paymentmetthod.length > 0 ? paymentSearch.custrecord_av4_ns_paymentmetthod[0].value : null

        var posSettings = search.lookupFields({
            type: "customrecord_av4_cashregister",
            id: cashPointId,
            columns: ["custrecord_av4_cr_settings"]
        })

        var posSettingsId = posSettings.custrecord_av4_cr_settings.length > 0 ? posSettings.custrecord_av4_cr_settings[0].value : null

        if (!posSettingsId && !taxRate) return true

        var taxItem = [];
        var customrecord_av4_tax_ratesSearchObj = search.create({
            type: "customrecord_av4_tax_rates",
            filters:
                [
                    ["custrecord_av4_pos_settings", "anyof", posSettingsId],
                    "AND",
                    ["formulatext: {custrecord_av4_taxrate}", "contains", String(taxRate)]
                ],
            columns:
                [
                    search.createColumn({
                        name: "scriptid",
                        sort: search.Sort.ASC,
                        label: "Script ID"
                    }),
                    search.createColumn({ name: "custrecord_av4_tax_type", label: "Tax Type" }),
                    search.createColumn({ name: "custrecord_av4_taxrate", label: "Tax Rate" }),
                    search.createColumn({ name: "custrecord_av4_ns_taxcodes", label: "NS Tax Code" }),
                    search.createColumn({ name: "custrecord_av4_pos_settings", label: "AV4 PoS Settings" }),
                    search.createColumn({ name: "custrecord_av4_default", label: "Default" }),
                    search.createColumn({ name: "custrecord_av4_flour_accountid", label: "flour account ID" }),
                    search.createColumn({ name: "custrecord_av4_flour_accountname", label: "Flour Account Name" })
                ]
        });

        try {
            customrecord_av4_tax_ratesSearchObj.run().each(function (result) {

                taxItem.push(result.getValue("custrecord_av4_ns_taxcodes"))

                return true;
            });
        } catch (e) {
            log.debug("Can't create search", JSON.stringify(e))
        }

        if (taxItem.length > 0) {
            var taxItemId = taxItem[0]
        } else {
            return true
        }

        return {
            defaultCustomerId: defaultCustomerId,
            locationId: locationId,
            accountId: accountId,
            taxItemId: taxItemId,
            paymentId: paymentId
        }
    }

    return {
        onAction: onAction
    };

})