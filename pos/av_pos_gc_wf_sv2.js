/**
 * Copyright (c) 2019 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @version: 1.0
 * @author: [Roberto Cideos]
 * @dated: [17/05/2022]
 * @Description:
 * WF Script to push gift certificates to POS-Flour
 *
 *@NApiVersion 2.1
 *@NScriptType WorkflowActionScript
 */

define(['N/record',
    'N/search',
    'N/runtime',
    "SuiteScripts/pimCoreIntegration/av_int_log.js",
    "N/https"],
    function (
        record,
        search,
        runtime,
        avLog,
        https) {

        var self = this;
        const INTERFACE_STATE = {
            ERROR: 2,
            DONE: 3
        }

        function onAction(scriptContext) {

            self.rec = scriptContext.newRecord;

            if (runtime.envType == "SANDBOX") {

                self.posId = runtime.getCurrentScript().getParameter({ name: 'custscript_av4_pos_config_record' })

            } else if (runtime.envType == "PRODUCTION") {

                self.posId = runtime.getCurrentScript().getParameter({ name: 'custscript_av4_pos_config_record' })
            }

            return _sendGC(_generateToken())
        }

        function _generateToken() {

            try {

                self.posConfig = search.lookupFields({
                    type: "customrecord_av4_pos_configuration",
                    id: self.posId,
                    columns: ["custrecord_av4_pos_baseurl",
                        "custrecord_av4_pos_voucher",
                        "custrecord_av4_pos_username",
                        "custrecord_av4_pos_password",
                        "custrecord_av4_pos_authurl"]
                })

                var res = https.post({
                    body: JSON.stringify({
                        username: self.posConfig.custrecord_av4_pos_username,
                        password: self.posConfig.custrecord_av4_pos_password
                    }),
                    url: `${self.posConfig.custrecord_av4_pos_baseurl}${self.posConfig.custrecord_av4_pos_authurl}`,
                    headers: {
                        'Content-type': 'application/json'
                    }
                })

                var token = JSON.parse(res.body).token

                record.submitFields({
                    type: "customrecord_av4_pos_configuration",
                    id: self.posId,
                    values: {
                        custrecord_av4_pos_token: token
                    }

                })

                return token

            } catch (e) {

                log.debug("Error generating POS token", JSON.stringify(e))
                return;
            }
        }

        function _sendGC(jwtT) {

            if (!jwtT) return;

            var payload = JSON.stringify({
                name: self.rec.getValue("custrecord_av_cs_gc_rec"),
                code: self.rec.getValue("custrecord_av_cs_gc_code"),
                value: self.rec.getValue("custrecord_av_cs_gc_or_amt"),
                valid: self.rec.getValue("custrecord_av_cs_gc_valid") ? true : false,
                taxassignment: runtime.getCurrentScript().getParameter({ name: "custscript_av_msg_pos_tax_id" }) || null
            })

            var logId = avLog.create({
                iface: self.iface,
                send: payload,
                url: `${self.posConfig.custrecord_av4_pos_baseurl}${self.posConfig.custrecord_av4_pos_voucher}`,
                gc: self.rec.id
            })

            var res = https.post({
                body: payload,
                url: `${self.posConfig.custrecord_av4_pos_baseurl}${self.posConfig.custrecord_av4_pos_voucher}`,
                headers: {
                    'Content-type': 'application/json',
                    'Authorization': 'JWT ' + jwtT
                }
            })

            avLog.update({
                id: logId,
                responseCode: res.code,
                state: INTERFACE_STATE.DONE,
                receive: res.body
            })

        }

        return {
            onAction: onAction
        };

    })