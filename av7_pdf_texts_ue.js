/* **************************************************************************************
 ** Copyright (c) 2018 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 **
 ** @bundle: AV7 Set Tax Texts
 ** @type: Library
 ** @version: 1.0
 ** @author: Martin Geist
 ** @dated: 18.09.2018
 ** @Description: User Event Script to set Tax Texts
 **
 ************************************************************************************** */

function BeforeSubmit(type) {

    if (type == 'edit' || type == 'create') {

        var record = nlapiGetNewRecord();

        var defLanguageId = nlapiGetContext().getSetting('SCRIPT', 'custscript_av7_company_def_language');
        if (!defLanguageId) {
            defLanguageId = 1;
        }

        var has_multilanguage = nlapiLoadConfiguration('companyfeatures').getFieldValue('multilanguage');
        if (has_multilanguage == 'T' && record.getFieldValue('entity')) {
            try {
                var l = nlapiLookupField('entity', record.getFieldValue('entity'), ['language', 'custentity_av_vendor_language']);
                var language = l.language;
                var languageVendor = l.custentity_av_vendor_language;
            } catch (e){
            }
        }

        if (language) {
            var languageId = GetLanguageId(language);
        } else {
            var languageId = languageVendor ? languageVendor : defLanguageId;
        }

        //tax
        var taxCode = record.getLineItemValue('item', 'taxcode', 1);
        if (taxCode) {
            try {
                var filters = [];
                filters[0] = new nlobjSearchFilter('custrecord_av7_tt_language', null, 'anyof', languageId);
                filters[1] = new nlobjSearchFilter('custrecord_av7_tt_tax_code', null, 'anyof', taxCode);
                filters[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
                var columns = [];
                columns[0] = new nlobjSearchColumn('custrecord_av7_tt_text');
                var results = nlapiSearchRecord('customrecord_av7_tax_texts', null, filters, columns);
                if (!results && defLanguageId && languageId != defLanguageId) {
                    filters[0] = new nlobjSearchFilter('custrecord_av7_tt_language', null, 'anyof', defLanguageId);
                    results = nlapiSearchRecord('customrecord_av7_tax_texts', null, filters, columns);
                }
                if (results && (results.length == 1)) {
                    record.setFieldValue('custbody_av7_tax_text', results[0].getValue(columns[0]));
                }
            } catch (e) {

            }

        }

        //tax
        var term = record.getFieldValue('terms');
        if (term) {
            try {
                var filters = [];
                filters[0] = new nlobjSearchFilter('custrecord_av7_termt_language', null, 'anyof', languageId);
                filters[1] = new nlobjSearchFilter('custrecord_av7_termt_term', null, 'anyof', term);
                filters[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
                var columns = [];
                columns[0] = new nlobjSearchColumn('custrecord_av7_termt_text');
                var results = nlapiSearchRecord('customrecord_av7_term_texts', null, filters, columns);
                if (!results && defLanguageId && languageId != defLanguageId) {
                    filters[0] = new nlobjSearchFilter('custrecord_av7_termt_language', null, 'anyof', defLanguageId);
                    results = nlapiSearchRecord('customrecord_av7_term_texts', null, filters, columns);
                }

                if (results && (results.length == 1)) {
                    record.setFieldValue('custbody_av7_term_text', results[0].getValue(columns[0]));
                } else {
                    record.setFieldValue('custbody_av7_term_text', record.getFieldText('terms'));
                }
            } catch (e) {

            }
        }

        //PDF texts
        var abb = nlapiGetContext().getSetting('SCRIPT', 'custscript_av7_company_abbreviation');
        if (abb) {
            try {
                var filters = [];
                filters[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
                filters[1] = new nlobjSearchFilter('custrecord_' + abb + '_pdf_texts_language', null, 'anyof', languageId);
                if (nlapiGetContext().getSetting('SCRIPT', 'custscript_av7_subsidary_seperated') == 'T') {
                    filters[2] = new nlobjSearchFilter('custrecord_' + abb + '_pdf_texts_subsidiary', null, 'anyof', record.getFieldValue('subsidiary'));
                }
                var results = nlapiSearchRecord('customrecord_' + abb + '_pdf_texts', null, filters, null);
                if (!results && defLanguageId && languageId != defLanguageId) {
                    filters[1] = new nlobjSearchFilter('custrecord_' + abb + '_pdf_texts_language', null, 'anyof', defLanguageId);
                    results = nlapiSearchRecord('customrecord_' + abb + '_pdf_texts', null, filters, null);
                }
                if (results) {
                    record.setFieldValue('custbody_' + abb + '_pdf_texts', results[0].getId());
                }
            } catch (e) {
                nlapiLogExecution("DEBUG", "Error", JSON.stringify(e))
            }   

        }
    }
}

function GetLanguageId(language) {

    switch (language) {
        case 'en_US':
            return 1;
        case 'en_EN':
        case 'en':
            return 13;
        case 'fi_FI':
            return 15;
        case 'fr_FR':
            return 16;
        case 'de_DE':
            return 18;
        case 'it_IT':
            return 26;
        case 'es_ES':
            return 36;
        default:
            return 1;
    }
}