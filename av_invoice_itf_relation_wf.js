/* **************************************************************************************
** Copyright (c) 2021 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
** @version: 1.0
**
** @author: Damian Krolikowski
** @dated: 7.08.2023
** @Description: ITF find feature. Before Submit.
** @Dependencies: Custom Transaction Body field: 	Label: Related ITF	 	Id: custbody_av_related_itf
**
**
************************************************************************************** */

/**
 *@NApiVersion 2.x
 *@NScriptType WorkflowActionScript
 */
define(['N/search'], function (search) {

    var LOGS = {};
    function onAction(scriptContext) {
        var contextRec = scriptContext.newRecord;
        LOGS.contextRecId = contextRec.id;

        try {
            var itfFoundResult = findItf(contextRec);
            LOGS.itfFoundResult = itfFoundResult;

            var updateResult = setFields(contextRec, itfFoundResult);
            LOGS.updateResult = updateResult;
        } catch (err) {
            log.error('Try Catch Error: ' + contextRec.id, err.message);
            LOGS.error = err;
        }

        return buildReturn(LOGS)
    }

    function findItf(contextRec) {
        var finalResult = '';

        var createdFrom = contextRec.getValue('createdfrom') || contextRec.getValue('custbody_av_related_so');
        if (!createdFrom) {
            return finalResult;
        }

        var itemfulfillmentSearchObj = search.create({
            type: "itemfulfillment",
            filters:
                [
                    ["type", "anyof", "ItemShip"],
                    "AND",
                    ["createdfrom", "anyof", createdFrom],
                    "AND",
                    ["mainline", "is", "T"]
                ],
            columns:
                [
                    "tranid",
                    search.createColumn({
                        name: "datecreated",
                        sort: search.Sort.DESC
                    })
                ]
        });

        itemfulfillmentSearchObj.run().each(function (result) {
            finalResult = result.id;
        });

        return finalResult;
    }

    function setFields(contextRec, itfFoundResult) {
        var finalResult = itfFoundResult || '';
        contextRec.setValue('custbody_av_related_itf', finalResult);

        return finalResult;
    }

    function buildReturn(input) {
        log.debug('LOGS', input);
        return JSON.stringify(input);
    }

    return {
        onAction: onAction
    }
});
