 /** 
 * Copyright (c) 2019 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @author: [Polina Melnykova]
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 * @description :
 * Spec: RMA PDF for vendor returns
 */

define(['N/url', 'N/currentRecord'], function(url, currentRecord) {
  
    function pageInit(context) {
      // TODO
    }
  
    function onButtonClick() {
      var suiteletUrl = url.resolveScript({
        scriptId: 'customscript_av_vendor_rma_pdf_sl',
        deploymentId: 'customdeploy1',
        returnExternalUrl: false,
        params: {
            custscript_record_id: currentRecord.get().id,
        },
      });
  
      window.open(suiteletUrl);
    }
  
    return {
        pageInit: pageInit,     
        onButtonClick: onButtonClick

    };
  });