/**
  * Copyright (c) 2022 Alta Via Consulting GmbH
  * All Rights Reserved.
  *
  * This software is the confidential and proprietary information of
  * Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
  * such Confidential Information and shall use it only in accordance
  * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
  *
  * @version: 2.x
  * @author: Tehseen Ahmed
  * @NApiVersion 2.1
  * @NScriptType MapReduceScript
  * Date: 16/1/2023
  * Description: Order sync interface for Shopware
*/

define(['N/search', 'N/record', 'N/workflow', 'N/runtime', 'N/https', 'N/format', './libraries/av30_shop_connection.js', './libraries/av30_constants.js', '../pimCoreIntegration/av_int_log.js'],
  function (search, record, workflow, runtime, https, format, settings, constants, logLib) {
    const getInputData = () => {
      log.audit({ title: 'getInputData', details: `-Script Started on ${new Date()}-` });

      const currentRuntime = runtime.getCurrentScript();
      const dateAfterQuery = currentRuntime.getParameter({
        name: constants.ORDER_SYNC_PARAMS.START_DATE
      });
      const ifaceType = currentRuntime.getParameter({
        name: constants.ORDER_SYNC_PARAMS.IFACE
      });
      const configId = currentRuntime.getParameter({
        name: constants.ORDER_SYNC_PARAMS.CONFIG
      });

      if (!configId) return;
      try {
        const connection = settings(configId, false, true);
        return getShopOrders(connection, ifaceType);

      } catch (ex) {
        log.error({ title: 'getInputData: ex', details: ex });
      }
    }

    const map = (context) => {
      const currentRuntime = runtime.getCurrentScript();
      const ifaceType = currentRuntime.getParameter({
        name: constants.ORDER_SYNC_PARAMS.IFACE
      });
      const shopOrder = JSON.parse(context.value);
      log.debug({ title: 'map: shopOrder', details: shopOrder });

      if (!isOrderExist(shopOrder.orderNumber, ifaceType)) {
        const logId = logLib.create({
          iface: ifaceType,
          state: constants.LOG_STATE.NEW,
          receive: shopOrder
        })

        try {
          const stagingRecordId = createStagingRecord(shopOrder, ifaceType, logId);
          if (stagingRecordId) {
            logLib.update({
              id: logId,
              state: constants.LOG_STATE.DONE
            });
            const date = new Date();
            context.write({
              key: shopOrder.id,
              value: date.getTime()
            });

            //   workflow.trigger({
            //     recordId: stagingRecordId,
            //     recordType: constants[ifaceType].STAGING_RECORD.TYPE,
            //     workflowId: constants[ifaceType].WF_CONFIG.WORKFLOW_ID,
            //     actionId: constants[ifaceType].WF_CONFIG.WORKFLOW_ACTION
            //   });
          }

        } catch (ex) {
          log.error({ title: 'map : ex', details: ex });
          logLib.update({
            id: logId,
            state: constants.LOG_STATE.ERROR
          });
          logLib.error_message({
            msg: constants[ifaceType].STAGING_RECORD.ERRORS.EX,
            logid: logId,
            var1: JSON.stringify(ex)
          });
        }
      }
    }

    const getShopOrders = (connection, ifaceType) => {
      const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': `Bearer ${connection.accessToken}`
      }

      const requestUrl = connection.custrecord_av_integration_base_url.concat(connection.custrecord_av_integration_product_post)
      const payload = connection.payload ? JSON.parse(connection.payload) : connection.payload;

      const logId = logLib.create({
        iface: ifaceType,
        send: JSON.stringify(payload),
        url: requestUrl,
        state: constants.LOG_STATE.NEW,
      })

      const response = https.request({
        method: 'POST',
        url: requestUrl,
        body: JSON.stringify(payload),
        headers: headers
      });

      const responseBody = JSON.parse(response.body);

      if (response.code == 200) {
        logLib.update({
          id: logId,
          receive: responseBody,
          headers: response.headers,
          responseCode: response.code,
          state: constants.LOG_STATE.DONE
        });
        return responseBody.data;
      } else {
        logLib.update({
          id: logId,
          receive: responseBody,
          headers: response.headers,
          responseCode: response.code,
          state: constants.LOG_STATE.ERROR
        });
      }
    }

    const createStagingRecord = (shopOrder, ifaceType, logId) => {
      try {
        let newOrder = record.create({
          type: constants[ifaceType].STAGING_RECORD.TYPE,
          isDynamic: true
        })
        const fields = constants[ifaceType].STAGING_RECORD.FIELDS;

        newOrder.setValue({
          fieldId: fields.LOG,
          value: logId
        });

        const shopOrderFields = Object.keys(constants.SHOP_ORDER_FIELDS);
        for (let j = 0; j < shopOrderFields.length; j++) {
          if (shopOrderFields[j] == fields.SALES_CHANNEL) {
            const salesChannel = shopOrder[constants.SHOP_ORDER_FIELDS[shopOrderFields[j]]];
            newOrder.setValue({
              fieldId: shopOrderFields[j],
              value: (salesChannel && salesChannel.shortName) ? salesChannel.shortName : ''
            });
          } else if (shopOrderFields[j] == fields.CURRENCY) {
            const currency = shopOrder[constants.SHOP_ORDER_FIELDS[shopOrderFields[j]]];
            newOrder.setValue({
              fieldId: shopOrderFields[j],
              value: (currency && currency.isoCode) ? currency.isoCode : ''
            });
          } else if (shopOrderFields[j] == fields.LANGUAGE) {
            const language = shopOrder[constants.SHOP_ORDER_FIELDS[shopOrderFields[j]]];
            newOrder.setValue({
              fieldId: shopOrderFields[j],
              value: (language && language.name) ? language.name : ''
            });
          } else if (shopOrderFields[j] == fields.PAYMENT) {
            const transactions = shopOrder[constants.SHOP_ORDER_FIELDS[shopOrderFields[j]]];
            newOrder.setValue({
              fieldId: shopOrderFields[j],
              value: (transactions && transactions.length) ? transactions[0].paymentMethod.name : ''
            });
          } else if (shopOrderFields[j] == fields.SHIP_ADDRESS) {
            let deliveries = shopOrder[constants.SHOP_ORDER_FIELDS[shopOrderFields[j]]];
            newOrder.setValue({
              fieldId: shopOrderFields[j],
              value: (deliveries && deliveries.length) ? JSON.stringify(deliveries[0].shippingOrderAddress) : ''
            });
          } else if (shopOrderFields[j] == fields.BILLING) {
            let billingAddress = shopOrder[constants.SHOP_ORDER_FIELDS[shopOrderFields[j]]];
            newOrder.setValue({
              fieldId: shopOrderFields[j],
              value: billingAddress ? JSON.stringify(billingAddress) : ''
            });
          } else if (shopOrderFields[j] == fields.SHIP_METHOD) {
            let deliveries = shopOrder[constants.SHOP_ORDER_FIELDS[shopOrderFields[j]]];
            newOrder.setValue({
              fieldId: shopOrderFields[j],
              value: (deliveries && deliveries.length) ? _getShipMethod(deliveries) : ''
            });
          } else if (shopOrderFields[j] == fields.DELIVERY_ID) {
            let deliveries = shopOrder[constants.SHOP_ORDER_FIELDS[shopOrderFields[j]]];
            newOrder.setValue({
              fieldId: shopOrderFields[j],
              value: (deliveries && deliveries.length) ? deliveries[0].id : ''
            });
          } else if (shopOrderFields[j] == fields.TAX_RATE) {
            let price = shopOrder[constants.SHOP_ORDER_FIELDS[shopOrderFields[j]]];
            newOrder.setValue({
              fieldId: shopOrderFields[j],
              value: (price && price.calculatedTaxes.length) ? price.calculatedTaxes[0].taxRate : ''
            });
          } else if (shopOrderFields[j] == fields.TAX) {
            let price = shopOrder[constants.SHOP_ORDER_FIELDS[shopOrderFields[j]]];
            newOrder.setValue({
              fieldId: shopOrderFields[j],
              value: (price && price.calculatedTaxes.length) ? price.calculatedTaxes[0].tax : ''
            });
          } else if (shopOrderFields[j] == fields.ORDER_DATE) {
            let orderDate = shopOrder[constants.SHOP_ORDER_FIELDS[shopOrderFields[j]]];
            // NetSuite is changing the date to 1 day before the actual date in the new Date() object.
            // Need to find out why.
            orderDate = orderDate.split('T').shift();
            var orderDay = Number(orderDate.split('-').pop());
            var dateObject = new Date(orderDate);
            var day = dateObject.getDate();
            if (day < orderDay) {
              dateObject.setDate(day + 1);
            }
            var parsedDate = format.parse({
              value: dateObject,
              type: format.Type.DATE
            })

            newOrder.setValue({
              fieldId: shopOrderFields[j],
              value: parsedDate
            });
          } else if (shopOrderFields[j] == fields.ORDER_TIME) {
            let dateTime = shopOrder[constants.SHOP_ORDER_FIELDS[shopOrderFields[j]]];
            newOrder.setValue({
              fieldId: shopOrderFields[j],
              value: new Date(dateTime)
            });
          } else if (shopOrderFields[j] == fields.GROUP) {
            let salesChannel = shopOrder[constants.SHOP_ORDER_FIELDS[shopOrderFields[j]]];
            newOrder.setValue({
              fieldId: shopOrderFields[j],
              value: salesChannel.customerGroup && salesChannel.customerGroup.name
            });
          } else {
            newOrder.setValue({
              fieldId: shopOrderFields[j],
              value: shopOrder[constants.SHOP_ORDER_FIELDS[shopOrderFields[j]]]
            });
          }
        }

        if (shopOrder.orderCustomer) {
          newOrder = _setCustomerFields(newOrder, shopOrder.orderCustomer, constants[ifaceType].STAGING_RECORD.CUSTOMER_FIELDS)
        }

        if (shopOrder.lineItems && shopOrder.lineItems.length) {
          const sublistFields = constants[ifaceType].STAGING_RECORD.SUBLIST_FIELDS;
          const orderLineFields = Object.keys(constants.SHOP_ORDER_LINE_FIELDS);
          for (let k = 0; k < shopOrder.lineItems.length; k++) {
            newOrder.selectNewLine({
              sublistId: constants[ifaceType].STAGING_RECORD.SUBLIST_ID
            });
            for (let l = 0; l < orderLineFields.length; l++) {
              if (orderLineFields[l] == sublistFields.TAX_RATE) {
                let price = shopOrder.lineItems[k][constants.SHOP_ORDER_LINE_FIELDS[orderLineFields[l]]];
                newOrder.setCurrentSublistValue({
                  sublistId: constants[ifaceType].STAGING_RECORD.SUBLIST_ID,
                  fieldId: orderLineFields[l],
                  value: (price && price.calculatedTaxes && price.calculatedTaxes.length) ? price.calculatedTaxes[0].taxRate : ''
                });
              } else if (orderLineFields[l] == sublistFields.TAX_AMOUNT) {
                let price = shopOrder.lineItems[k][constants.SHOP_ORDER_LINE_FIELDS[orderLineFields[l]]];
                newOrder.setCurrentSublistValue({
                  sublistId: constants[ifaceType].STAGING_RECORD.SUBLIST_ID,
                  fieldId: orderLineFields[l],
                  value: (price && price.calculatedTaxes && price.calculatedTaxes.length) ? price.calculatedTaxes[0].tax : ''
                });
              } else if (orderLineFields[l] == sublistFields.ITEM) {
                let product = shopOrder.lineItems[k][constants.SHOP_ORDER_LINE_FIELDS[orderLineFields[l]]];
                newOrder.setCurrentSublistValue({
                  sublistId: constants[ifaceType].STAGING_RECORD.SUBLIST_ID,
                  fieldId: orderLineFields[l],
                  value: (product && product.productNumber) ? product.productNumber : ''
                });
              } else if (orderLineFields[l] == sublistFields.PAYLOAD) {
                let payload = shopOrder.lineItems[k][constants.SHOP_ORDER_LINE_FIELDS[orderLineFields[l]]];
                newOrder.setCurrentSublistValue({
                  sublistId: constants[ifaceType].STAGING_RECORD.SUBLIST_ID,
                  fieldId: orderLineFields[l],
                  value: payload ? JSON.stringify(payload) : payload
                });
              } else if (orderLineFields[l] == sublistFields.DISC_SCOPE) {
                let payload = shopOrder.lineItems[k][constants.SHOP_ORDER_LINE_FIELDS[orderLineFields[l]]];
                newOrder.setCurrentSublistValue({
                  sublistId: constants[ifaceType].STAGING_RECORD.SUBLIST_ID,
                  fieldId: orderLineFields[l],
                  value: payload.discountScope
                });
              } else {
                newOrder.setCurrentSublistValue({
                  sublistId: constants[ifaceType].STAGING_RECORD.SUBLIST_ID,
                  fieldId: orderLineFields[l],
                  value: shopOrder.lineItems[k][constants.SHOP_ORDER_LINE_FIELDS[orderLineFields[l]]]
                });
              }
            }
            newOrder.commitLine({
              sublistId: constants[ifaceType].STAGING_RECORD.SUBLIST_ID
            });
          }
        }

        return newOrder.save({
          enableSourcing: false,
          ignoreMandatoryFields: true,
        });

      } catch (e) {

        logLib.error_message({
          logid: logId,
          msg: constants[ifaceType].ERRORS.EX,
          var1: JSON.stringify(e)
        })
      }
    }

    const _setCustomerFields = (newOrder, values, fields) => {
      const customerFields = Object.keys(constants.SHOP_CUSTOMER_FIELDS);
      for (let j = 0; j < customerFields.length; j++) {
        if (customerFields[j] == fields.VAT_ID) {
          const vatIds = values[constants.SHOP_CUSTOMER_FIELDS[customerFields[j]]] || [];
          newOrder.setValue({
            fieldId: customerFields[j],
            value: JSON.stringify(vatIds)
          });
        } else {
          newOrder.setValue({
            fieldId: customerFields[j],
            value: values[constants.SHOP_CUSTOMER_FIELDS[customerFields[j]]]
          });
        }
      }
      return newOrder;
    }

    const _getShipMethod = (deliveries) => {
      let deliveriesArray = [];
      for (let i = 0; i < deliveries.length; i++) {
        deliveriesArray.push({
          name: deliveries[i].shippingMethod ? deliveries[i].shippingMethod.name : null
        })
      }
      return JSON.stringify(deliveriesArray)
    }

    const isOrderExist = (orderId, ifaceType) => {
      if (!orderId) return;
      let shopOrderSearch = search.create({
        type: constants[ifaceType].STAGING_RECORD.TYPE,
        filters: [
          search.createFilter({
            name: 'isinactive',
            join: null,
            operator: search.Operator.IS,
            values: false
          }),
          search.createFilter({
            name: 'custrecord_av_sw_order_number',
            join: null,
            operator: search.Operator.IS,
            values: orderId
          })
        ],
        columns: []
      });

      return shopOrderSearch.runPaged().count;
    }

    const summarize = (summary) => {
      const currentRuntime = runtime.getCurrentScript();
      const ifaceType = currentRuntime.getParameter({
        name: constants.ORDER_SYNC_PARAMS.IFACE
      });
      const configId = currentRuntime.getParameter({
        name: constants.ORDER_SYNC_PARAMS.CONFIG
      });

      if (!configId) return;
      try {
        let orders = [];
        summary.output.iterator().each(function (key, value) {
          orders.push({
            id: key,
            customFields: {
              exportedToNetsuiteAt: value
            }
          });
          return true;
        });

        log.debug({ title: 'summarize: orders', details: orders });
        if (orders.length) {
          const connection = settings(configId, false, true);
          updateShopTimeStamp(connection, ifaceType, orders);
        }

      } catch (ex) {
        log.error({ title: 'summarize: ex', details: ex });
      }

      log.audit({ title: 'summarize', details: `-Script Finished on ${new Date()}-` });
    }

    const handleResendLog = (payload, iface, logId) => {
      payload?.data?.forEach(order => {
        createStagingRecord(order, iface, logId);
      });
    }
    
    const updateShopTimeStamp = (connection, ifaceType, payload) => {

      const ordersToSync = [{
        'action': 'upsert',
        'entity': 'order',
        'payload': payload
      }]

      const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': `Bearer ${connection.accessToken}`
      }

      const requestUrl = connection.custrecord_av_integration_base_url.concat(connection.custrecord_av_integration_tracking_post);

      const logId = logLib.create({
        iface: ifaceType,
        send: JSON.stringify(ordersToSync),
        url: requestUrl,
        state: constants.LOG_STATE.NEW
      })

      const response = https.request({
        method: 'POST',
        url: requestUrl,
        body: JSON.stringify(ordersToSync),
        headers: headers
      });

      const responseBody = JSON.parse(response.body);

      if (response.code == 200) {
        logLib.update({
          id: logId,
          receive: responseBody,
          headers: response.headers,
          responseCode: response.code,
          state: constants.LOG_STATE.DONE
        });
      } else {
        logLib.update({
          id: logId,
          receive: responseBody,
          headers: response.headers,
          responseCode: response.code,
          state: constants.LOG_STATE.ERROR
        });
        var message = (responseBody.errors && responseBody.errors.length) ? responseBody.errors[0].detail : JSON.stringify(responseBody);
        logLib.error_message({
          msg: constants[ifaceType].ERRORS.EX,
          logid: logId,
          var1: message
        });
      }
    }

    return {
      getInputData: getInputData,
      map: map,
      summarize: summarize,
      handleResendLog,
    }
  }
)