/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tuba Mohsin
* Date: 06/02/2023
* Sends tracking details to Shop
* @NApiVersion 2.1
* @NScriptType UserEventScript
*/

define(['./libraries/av30_shop_order_tracking_lib.js', 'N/runtime'], function (orderUpdateLib, runtime) {

  function afterSubmit(context) {
    try {
      let scriptObj = runtime.getCurrentScript();
      const iface = scriptObj.getParameter({ name: "custscript_so_iface" });
      // TODO: confirm the context type
      if (context.type === context.UserEventType.CREATE
        || context.type === context.UserEventType.EDIT
        || context.type === 'ship') {

        const shopId = context.newRecord.getValue('custbody_av_sw_order_id');
        if (!shopId) return;

        const packageLine = context.newRecord.getLineCount('package');
        if (!packageLine > 0) return;

        const soId = [context.newRecord.getValue('createdfrom')];
        const fulfillmentId = [context.newRecord.id];
        log.debug('Processing request for ==>', `iface: ${iface}, soID: ${soId}`);
        orderUpdateLib.handleTrackingRequest(soId, iface, fulfillmentId);
      }
    } catch (ex) {
      log.debug({ title: 'afterSubmit : ex', details: ex });
    }
  }

  return {
    afterSubmit: afterSubmit
  }
})