/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
** @version: 2.1
** @author: Tehseen Ahmed
** @dated: 27.1.2023
** @Description: Integration between Shopware and NetSuite (Voucher)
*
*  @NApiVersion 2.1
*  @NScriptType Restlet
*/


define(['N/search', 'N/record', '../pimCoreIntegration/av_int_log.js'],
  function (search, record, avLog) {
    let self = this;
    const CONSTANTS = {
      INTERFACE_STATE: {
        NEW: 1,
        ERROR: 2,
        DONE: 3
      }
    }


    function getEntryPoint(context) {
      self.logId = avLog.create({
        iface: 'S03',
        receive: context,
        state: CONSTANTS.INTERFACE_STATE.NEW
      });
      return handleGetRequest(context, self.logId);
    }


    const handleGetRequest = (context, logId) => {
      try {
        self.logId = self.logId || logId;
        if (context.voucherCode) {
          return getVoucher(context.voucherCode);
        } else {
          const errorDetails = getErrorMessage('S30002');
          const response = {
            code: 500,
            message: errorDetails.msg
          }
          avLog.update({
            id: self.logId,
            send: JSON.stringify(response),
            state: CONSTANTS.INTERFACE_STATE.ERROR
          });
          // Added by Tuba
          avLog.error_message({
            msg: 'S30002',
            logid: self.logId,
            var1: errorDetails.msg
          });
          return JSON.stringify(response);
        }

      } catch (ex) {
        log.error({ title: 'get: ex', details: ex });
        const response = {
          code: 500,
          message: ex
        }
        avLog.update({
          id: self.logId,
          responseCode: 400,
          send: JSON.stringify(response),
          state: CONSTANTS.INTERFACE_STATE.ERROR
        })
        // Added by Tuba
        // msg 'S30002' should be replaced by some other technical error
        avLog.error_message({
          msg: 'S30002', 
          logid: self.logId,
          var1: ex
        });
        return JSON.stringify(response);
      }
    }

    const getVoucher = (voucherCode) => {
      const gcSearch = search.create({
        type: search.Type.GIFT_CERTIFICATE,
        filters: [
          ['gccode', search.Operator.IS, voucherCode]
        ],
        columns: [
          search.createColumn({ name: 'name', label: 'To (Name)' }),
          search.createColumn({ name: 'email', label: 'To (Email)' }),
          search.createColumn({ name: 'giftcertcode', label: 'Gift Certificate Code' }),
          search.createColumn({ name: 'originalamount', label: 'Amount' }),
          search.createColumn({ name: 'amountremaining', label: 'Amount Available' })
        ]
      });

      const currentRange = gcSearch.run().getRange({
        start: 0,
        end: 1
      });

      if (currentRange.length && isValidGc(currentRange[0], voucherCode)) {
        // Have to load the record because currency is not available in search columns
        const gcRecord = record.load({
          type: record.Type.GIFT_CERTIFICATE,
          id: currentRange[0].id,
          isDynamic: true,
        });
        let voucher = {
          code: currentRange[0].getValue('giftcertcode'),
          customer: currentRange[0].getValue('name'),
          email: currentRange[0].getValue('email'),
          dataAmount: [{
            originalAmount: parseFloat(currentRange[0].getValue('originalamount')),
            remainingAmount: parseFloat(currentRange[0].getValue('amountremaining')),
            currency: gcRecord.getText('currency')
          }]
        }
        avLog.update({
          id: self.logId,
          send: JSON.stringify(voucher),
          state: CONSTANTS.INTERFACE_STATE.DONE
        })
        return JSON.stringify(voucher);
      } else {
        const errorDetails = getErrorMessage('S30001');
        const response = {
          code: 500,
          message: errorDetails.msg
        }
        avLog.update({
          id: self.logId,
          send: JSON.stringify(response),
          state: CONSTANTS.INTERFACE_STATE.ERROR
        });
        // Added by Tuba
        avLog.error_message({
          msg: 'S30001', 
          logid: self.logId,
          var1: errorDetails.msg
        });
        return JSON.stringify(response);
      }
    }

    const isValidGc = (searchResult, voucher) => {
      const regex = new RegExp(searchResult.getValue('giftcertcode'));
      return regex.test(voucher);
    }

    const getErrorMessage = (name) => {
      if (!name) return false;
      const errorMessageSearch = search.create({
        type: 'customrecord_av_error_messages',
        filters: [['name', search.Operator.IS, name]],
        columns: [search.createColumn({ name: 'custrecord_av_err_msg' })],
      }).run().getRange({
        start: 0,
        end: 1,
      });

      return errorMessageSearch.length ?
        {
          id: errorMessageSearch[0].id,
          msg: errorMessageSearch[0].getValue('custrecord_av_err_msg'),
        } : null;
    }

    const methodNotImplemented = (context) => {
      return JSON.stringify({ code: 502, message: 'This method has not been implemented' })
    }

    return {
      get: getEntryPoint,
      post: methodNotImplemented,
      put: methodNotImplemented,
      delete: methodNotImplemented,
      handleGetRequest,
    };
  });