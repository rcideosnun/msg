/**
 * Copyright (c) 2019 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @version: 2.x
 * @author: [Tehseen Ahmed]
 * @dated: [12.06.2022]
 * @Description: Library functions for Shopware order update
 *
 */

define([
  "N/search",
  "N/https",
  "./av30_constants.js",
  "../../pimCoreIntegration/av_int_log.js",
  "./av30_shop_connection.js",
], function (search, https, constants, logLib, settings) {
  var self = this;
  const CONSTANTS = {
    SHOPWARE_CONFIG: '102',
  } // this can be moved to script params

  const NS_SHOP_STATUS_MAP = {
    'pendingBillingPartFulfilled': {
      'delivery': 'ship_partially',
      'order_state': { 'paid': 'process', 'authorized': 'process' }
    },
    'partiallyFulfilled': {
      'delivery': 'ship_partially',
      'order_state': { 'paid': 'process', 'authorized': 'process' }
    },
    'pendingBilling': {
      'delivery': 'ship',
      'order_state': { 'paid': 'complete', 'authorized': 'process' }
    },
    'fullyBilled': {
      'delivery': 'ship',
      'order_state': { 'paid': 'complete', 'authorized': 'process' }
    },
    'cancelled': {
      'delivery': 'cancel',
      'order_state': { 'paid': 'complete', 'authorized': 'cancel' }
    },
    'closed': {
      'order_state': { 'paid': 'cancel', 'authorized': 'cancel' }
    },
    'pendingFulfillment': {
      'order_state': { 'paid': 'process', 'authorized': 'process' }
    }
  };

  /**
   * This function handles the request that comes from UE and WF
   * prepares the payload and send request to shopware
   */
  const handleTrackingRequest = (soIds, iface, fulfillmentId = null) => {
    try {
      if (!soIds || !soIds.length > 0) return;
      self.interface = getInterface(iface);
      var logId = logLib.create({
        iface: iface,
        state: constants.LOG_STATE.NEW,
        transaction: fulfillmentId ? fulfillmentId?.[0] : soIds?.[0],
      });

      var { payload, orderMap } = self.interface.getPayloadFunction(soIds);
      log.audit(`payload to be sent to Shopware for interface: ${iface}`, payload);

      if (!payload || !payload.length > 0) {
        logLib.update({
          id: logId,
          state: constants.LOG_STATE.ERROR,
        });
        logLib.error_message({
          logid: logId,
          msg: self.interface.missing_tracking_details,
          var1: JSON.stringify(payload),
        });
        return;
      }

      return sendData(iface, payload, logId, orderMap);
    } catch (ex) {

      logLib.update({
        id: logId,
        send: payload ? JSON.stringify(payload) : null,
        state: constants.LOG_STATE.ERROR,
      });
      logLib.error_message({
        logid: logId,
        msg: self.interface.technical_error,
        var1: ex,
      });
      log.error({ title: "handleRequest : ex", details: ex });
    }
  };

  /**
   * This function prepare tracking payload for shopware
   */
  const getTrackingPayload = (soIds) => {
    let trackingInfo = [];
    let payload = []
    const { data, orderMap } = getTrackingNumbers(soIds);
    Object.keys(data).forEach((key) => {
      let tracking = data[key];
      trackingInfo.push({
        id: tracking.id,
        trackingCodes: tracking.code,
      });
    });

    if (!trackingInfo || !trackingInfo.length > 0) return { payload, orderMap };

    payload = [
      {
        action: "upsert",
        entity: "order_delivery",
        payload: trackingInfo,
      },
    ];
    return { payload, orderMap };
  };

  /**
   * This function create search to get tracking info
   */
  const getTrackingNumbers = (soIds) => {
    let trackingNumbers = {};
    let orderMap = {};
    let itemfulfillmentSearchObj = search.create({
      type: "itemfulfillment",
      filters: [
        ["type", "anyof", "ItemShip"],
        "AND",
        ["createdfrom", "anyof", soIds],
        "AND",
        ["mainline", "is", "T"],
        "AND",
        ["status", "anyof", "ItemShip:B", "ItemShip:C"]
      ],
      columns: [
        "createdfrom",
        "custbody_av_sw_order_id",
        "custbody_av_delivery_id",
        search.createColumn({
          name: "trackingnumber",
          join: "shipmentPackage",
        }),
        search.createColumn({
          name: "statusref",
          join: "createdFrom",
        }),
      ],
    });

    let myPagedData = itemfulfillmentSearchObj.runPaged({ pageSize: 1000 });
    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });
      myPage.data.forEach(function (result) {
        const values = result.toJSON().values;
        const soId = values?.createdfrom?.[0].value;
        let code = values["shipmentPackage.trackingnumber"];
        const deliveryId = values["custbody_av_delivery_id"];
        const orderId = values["custbody_av_sw_order_id"];
        const status = values["createdFrom.statusref"]?.[0]?.value;
        if (!deliveryId) return;
        if (trackingNumbers[soId] && code) {
          trackingNumbers[soId]["code"] = [
            ...trackingNumbers[soId]["code"],
            code,
          ];
        } else {
          code = code ? [code] : [];
          trackingNumbers[soId] = { id: deliveryId, code: code };
          orderMap[orderId] = { deliveryId, status };
        }
      });
    });
    log.audit("trackingNumbers", trackingNumbers);
    return { data: trackingNumbers, orderMap };
  };

  /**
   * This function is only used when lib is triggered from resend interface workflow
   * This map is used to update to send statuses to shopware
   */
  const getOrderMap = (orderIds) => {
    let filters = [];
    let orderMap = {};
    log.audit('get ordermap orderIds', orderIds);
    orderIds.forEach((id) => {
      if (filters.length > 0) filters.push("OR");
      filters.push(["custbody_av_sw_order_id", "is", id]);
    });
    filters.push("AND", ["type", "anyof", "SalesOrd"], "AND", ["mainline", "is", "T"]);
    log.audit('filters', filters);
    const searchObj = search.create({
      type: "salesorder",
      filters,
      columns: [
        "statusref",
        "custbody_av_delivery_id",
        "custbody_av_sw_order_id",
      ],
    });
    let myPagedData = searchObj.runPaged({ pageSize: 1000 });
    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });
      myPage.data.forEach(function (result) {
        let orderId = result.getValue({ name: "custbody_av_sw_order_id" });
        orderMap[orderId] = {
          deliveryId: result.getValue({ name: "custbody_av_delivery_id" }),
          status: result.getValue({ name: "statusref" }),
        };
      });
    });

    log.audit("orderMap", orderMap);
    return orderMap;
  };

  /**
   * This function sends the request to shopware.
   * This method is also used by resend interface log
   */
  const sendData = (iface, payload, logId, orderMap = null) => {
    const interface = self.interface ? self.interface : getInterface(iface);
    let requestObj = {};
    try {
      let connection = settings(CONSTANTS.SHOPWARE_CONFIG, false, interface.connectionRequired)
      requestObj.requestUrl = connection.custrecord_av_integration_base_url.concat(connection[interface.request_url_fieldId]);
      requestObj.method = interface.onActionHttpRequest;
      requestObj.headers = {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${connection.accessToken}`,
      };
      const response = https.request({
        method: requestObj.method,
        url: requestObj.requestUrl,
        body: JSON.stringify(payload),
        headers: requestObj.headers,
      });
      log.audit('sendTrackingInfo ==> response', `Code: ${response.code} | Body: ${response.body}`);
      interface.handleResponseFunction(response, logId, requestObj, payload, iface, orderMap, connection, interface)
    } catch (ex) {

      log.audit("sendData : ex", ex);
      logLib.update({
        id: logId,
        send: payload ? JSON.stringify(payload) : null,
        state: constants.LOG_STATE.ERROR,
      });
      logLib.error_message({
        logid: logId,
        msg: interface.technical_error,
        var1: ex,
      });
    }
  };

  /**
   * This function handles the response received by shopware
   */
  const handleTrackingResponse = (response, logId, requestObj, payload, iface, orderMap, connection, interface) => {
    const responseBody = JSON.parse(response.body);
    if (response.code == 200) {
      logLib.update({
        id: logId,
        url: requestObj.requestUrl,
        send: JSON.stringify(payload),
        receive: responseBody,
        headers: requestObj.headers,
        responseCode: response.code,
        state: constants.LOG_STATE.DONE,
      });
      //TODO: multiple ids?
      const data = responseBody.data?.[0]?.result?.[0].entities || null;
      if (data && data.order) {
        if (!orderMap) orderMap = getOrderMap(data.order);
        data.order.forEach(orderId => {
          const DeliveryStatusResult = updateDeliveryStatus(logId, orderId, interface, connection, orderMap);
          if (DeliveryStatusResult) updateShopOrderStatus(iface, logId, orderId, connection, orderMap)

        })
      }
    } else {
      logLib.update({
        id: logId,
        url: requestObj.requestUrl,
        send: JSON.stringify(payload),
        receive: responseBody,
        headers: requestObj.headers,
        responseCode: response.code,
        state: constants.LOG_STATE.ERROR,
      });

      const message = responseBody.errors && responseBody.errors.length ? responseBody.errors[0].detail : JSON.stringify(responseBody);
      logLib.error_message({
        msg: constants[iface].ERRORS.EX,
        logid: logId,
        var1: message,
      });
    }

  }

  /**
   * Updates the delivery status in shopware
   */
  const updateDeliveryStatus = (logId, orderId, interface, connection, orderMap) => {
    try {
      let success = false;
      const orderStatus = orderMap[orderId]?.status || null;
      var deliveryId = orderMap[orderId]?.deliveryId || null;
      log.audit('NetSuite Order Status', orderStatus);
      if (orderStatus && deliveryId && orderStatus != 'pendingFulfillment') {

        var requestUrl = connection.custrecord_av_integration_base_url.concat(
          connection[interface.update_delivery_status_endpoint]
        );
        let transitionValue = NS_SHOP_STATUS_MAP[orderStatus]?.delivery || null;
        //TODO: test
        //TODO: no trasition value for pending fulfillment.
        //TODO: add request url in error msg
        if (!transitionValue) throw `No transition value defined for status: ${orderStatus}`;

        requestUrl = requestUrl.replace("{orderDeliveryId}", deliveryId);
        requestUrl = requestUrl.replace("{transitionValue}", transitionValue);
        const method = "POST";
        const headers = {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${connection.accessToken}`,
        };
        const response = https.request({
          method: method,
          url: requestUrl,
          headers: headers,
        });
        log.audit('updateDeliveryStatus ==> response', `Code: ${response.code} | Body: ${response.body}`);
        if (response.code == 200) return true;


        logLib.update({
          id: logId,
          state: constants.LOG_STATE.ERROR,
        });
        const responseBody = response.body ? JSON.parse(response.body) : null;
        const message = responseBody.errors && responseBody.errors.length ? responseBody.errors[0].detail : JSON.stringify(responseBody);
        logLib.error_message({
          msg: interface.updating_order_delivery_status,
          logid: logId,
          var1: `Error while updating status for Delivery: ${deliveryId}. \nDetails: ${message}`,
          var2: requestUrl
        });
      }
      return success;
    } catch (ex) {
      log.audit('Error while updating status for Delivery', ex);
      logLib.update({
        id: logId,
        state: constants.LOG_STATE.ERROR,
      });
      logLib.error_message({
        msg: interface.updating_order_delivery_status,
        logid: logId,
        var1: `Error while updating status for Delivery: ${deliveryId} \nDetails: ${ex}.`,
        var2: requestUrl
      });
    }
  }

  /**
   * Fetches the order transaction state from shopware
   */
  const getOrderStatus = (logId, orderId, interface, connection) => {
    let status = null;
    // const orderId = orderMap[deliveryId]?.orderId || null;  
    try {
      var requestUrl = connection.custrecord_av_integration_base_url.concat(
        connection[interface.get_order_status]
      );
      const method = "POST";
      let payload = getStatusPayload();
      payload = payload.replace('{orderId}', `"${orderId}"`);
      const headers = {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${connection.accessToken}`,
      };
      const response = https.request({
        method: method,
        url: requestUrl,
        body: payload,
        headers: headers,
      });

      log.audit('getOrderStatus ==> response', `Code: ${response.code} | Body: ${response.body}`);

      if (response.code == 200) {
        const body = JSON.parse(response.body);
        status = body.data?.[0]?.transactions?.[0]?.stateMachineState.technicalName || null;
        return status;
      }

      logLib.update({
        id: logId,
        state: constants.LOG_STATE.ERROR,
      });
      const responseBody = JSON.parse(response.body);
      const message = responseBody.errors && responseBody.errors.length ? responseBody.errors[0].detail : JSON.stringify(responseBody);
      logLib.error_message({
        msg: interface.fetching_trans_status_error,
        logid: logId,
        var1: `Error while fetching transaction status for Order Id: ${orderId}. \nDetails: ${message}`,
        var2: requestUrl
      });
      return status;
    } catch (ex) {
      log.audit('Error while fetching transaction status for Order Id', ex);
      logLib.update({
        id: logId,
        state: constants.LOG_STATE.ERROR,
      });
      logLib.error_message({
        msg: interface.fetching_trans_status_error,
        logid: logId,
        var1: `Error while fetching transaction status for Order Id: ${orderId}. \nDetails: ${message}`,
        var2: requestUrl
      });
    }
  }

  /**
   * Updates the order state in shopware
   */
  const updateShopOrderStatus = (iface, logId, orderId, connection, orderMap, updateLog = false) => {
    log.audit('orderMap', orderMap);
    // const orderId = orderMap[deliveryId]?.orderId || null;
    const nsOrderStatus = orderMap[orderId]?.status || null;
    const soId = orderMap[orderId]?.soId;
    const shipmethod = orderMap[orderId]?.shipmethod || null;
    const interface = self.interface ? self.interface : getInterface(iface);
    try {
      const orderStatus = getOrderStatus(logId, orderId, interface, connection, orderMap);
      log.audit('NetSuite Order Status', nsOrderStatus);
      if (orderStatus && orderId) {
        var requestUrl = connection.custrecord_av_integration_base_url.concat(connection[interface.update_order_status]);
        let transitionValue = NS_SHOP_STATUS_MAP[nsOrderStatus]?.['order_state']?.[orderStatus];
        log.audit('shipMethod', shipmethod);
        log.audit('connection.custrecord_av_integration_shipmethod', connection.custrecord_av_integration_shipmethod?.[0]?.value);
        isDigitalDelivery = shipmethod == connection.custrecord_av_integration_shipmethod?.[0]?.value ? true : false;

        if (isDigitalDelivery) {
          handleDigitalDeliveryStatus(iface, requestUrl, orderId, soId, logId, updateLog, connection);

        } else {
          if (!transitionValue) throw `No transition value defined for netsuite order status: ${nsOrderStatus} and Shopware Transaction State: ${orderStatus}`;
          requestUrl = requestUrl.replace('{orderId}', orderId);
          requestUrl = requestUrl.replace('{transitionValue}', transitionValue);
          if (updateLog) logLib.update({ id: logId, url: requestUrl });

          return sendUpdateStatusRequest(iface, requestUrl, logId, updateLog, orderId, connection);
        }

      }
    } catch (ex) {
      log.audit('Error while updating status for order', ex);
      logLib.update({
        id: logId,
        state: constants.LOG_STATE.ERROR,
      });
      logLib.error_message({
        msg: interface.updating_order_status_error,
        logid: logId,
        var1: `Error while updating status for order: ${orderId}. \nDetails: ${ex}`,
        var2: requestUrl
      });
    }
  }

  const handleDigitalDeliveryStatus = (iface, requestUrl, orderId, soId, logId, updateLog, connection) => {
    log.audit('handling digital delivery status');
    // first send 'process' state 
    requestUrl = requestUrl.replace('{orderId}', orderId);

    processStateURL = requestUrl;
    processStateURL = processStateURL.replace('{transitionValue}', 'process');
    logLib.update({ id: logId, url: processStateURL });
    log.audit('processStateURL', processStateURL);
    sendUpdateStatusRequest(iface, processStateURL, logId, updateLog, orderId, connection);

    // second send 'complete' state 
    completeStateURL = requestUrl;
    completeStateURL = completeStateURL.replace('{transitionValue}', 'complete');
    log.audit('completeStateURL', completeStateURL);
    logLib.update({ id: logId, url: completeStateURL });
    sendUpdateStatusRequest(iface, completeStateURL, logId, updateLog, orderId, connection);
  }

  /**
   * The function returns the standard payload that is used by GetOrderStatus endpoint
   */
  const getStatusPayload = () => {
    return `{
      "page": 1,
      "limit": 1,
      "filter": [
        {
          "type": "equals",
          "field": "id",
          "value": {orderId}
        }
      ],
      "associations": {
        "transactions": {
          "sort": [
            {
              "field": "createdAt",
              "order": "DESC",
              "naturalSorting": false
            }
          ],
          "associations": {
            "stateMachineState": {}
          },
          "total-count-mode": 1
        }
      },
      "includes": {
        "order": [
          "transactions"
        ],
        "order_transaction": [
          "stateMachineState"
        ],
        "state_machine_state": [
          "technicalName",
          "createdAt",
          "updatedAt"
        ]
      }
    }`
  }

  const getSOStatus = (soIds) => {
    if (!soIds || soIds.length == 0) return {};
    const soStatusMap = {};
    const salesorderSearchObj = search.create({
      type: "salesorder",
      filters:
        [
          ["type", "anyof", "SalesOrd"],
          "AND",
          ["internalid", "anyof", soIds],
          "AND",
          ["mainline", "is", "T"],
          "AND",
          ["custbody_av_sw_order_id", "isnotempty", ""]
        ],
      columns:
        [
          'statusref',
          'custbody_av_sw_order_id',
          'shipmethod'
        ]
    });

    let myPagedData = salesorderSearchObj.runPaged({ pageSize: 1000 });
    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });
      myPage.data.forEach(function (result) {
        const values = result.toJSON().values;
        const soId = result.id;
        const shopOrderId = values["custbody_av_sw_order_id"];
        const status = values["statusref"]?.[0]?.value;
        const shipmethod = values["shipmethod"]?.[0]?.value;
        // log.audit('status', status)
        // log.audit(' Object.keys(statusMap)',  Object.keys(statusMap));
        // log.audit(' Object.keys(statusMap).includes(status)',  Object.keys(statusMap).includes(status));
        if (Object.keys(NS_SHOP_STATUS_MAP).includes(status)) soStatusMap[shopOrderId] = { status, soId, shipmethod };
      });
    });
    return soStatusMap;
  }

  /**
   * Handles the request to send updated status to shopware
   */
  const handleStatusRequest = (soIds, iface) => {
    self.interface = getInterface(iface);
    try {
      if (!soIds || soIds.length == 0) return;
      if (!soIds[0]) return;

      var logId = logLib.create({
        iface: iface,
        state: constants.LOG_STATE.NEW,
        transaction: soIds?.[0],
      });

      let connection = settings(CONSTANTS.SHOPWARE_CONFIG, false, self.interface.connectionRequired);
      const soStatusMap = getSOStatus(soIds);
      if (!Object.keys(soStatusMap).length > 0) return;



      Object.keys(soStatusMap).forEach(orderId => {
        updateShopOrderStatus(iface, logId, orderId, connection, soStatusMap, true);
      });
    } catch (ex) {
      logLib.update({
        id: logId,
        state: constants.LOG_STATE.ERROR,
      });
      logLib.error_message({
        logid: logId,
        msg: self.interface.technical_error,
        var1: ex,
      });
      log.error({ title: "handleRequest : ex", details: ex });
    }
  };

  /**
   * This function sends the request to update status to shopware
   * This function is also used by resend interface
   */
  const sendUpdateStatusRequest = (iface, requestUrl, logId, updateLog, orderId = '', connect = null) => {
    let success = false;
    const interface = self.interface ? self.interface : getInterface(iface);
    const connection = connect ? connect : settings(CONSTANTS.SHOPWARE_CONFIG, false, interface.connectionRequired);
    const method = "POST";
    const headers = {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${connection.accessToken}`,
    };
    const response = https.request({
      method: method,
      url: requestUrl,
      headers: headers,
    });
    log.audit('updateShopOrderStatus ==> response', `Code: ${response.code} | Body: ${response.body}`);
    if (response.code == 200) {
      if (updateLog) logLib.update({ id: logId, state: constants.LOG_STATE.DONE, receive: JSON.parse(response.body) });
      return true;
    }

    const responseBody = JSON.parse(response.body);
    const message = responseBody.errors && responseBody.errors.length ? responseBody.errors[0].detail : JSON.stringify(responseBody);
    log.audit('message', message);

    if (message && message.includes('Possible transitions are: reopen')) {
      logLib.update({
        id: logId,
        state: constants.LOG_STATE.DONE,
        receive: JSON.parse(response.body)
      });
      return true;
    }

    logLib.update({
      id: logId,
      state: constants.LOG_STATE.ERROR,
      receive: JSON.parse(response.body)
    });

    logLib.error_message({
      msg: interface.updating_order_status_error,
      logid: logId,
      var1: `Error while updating status for order ${orderId}. \nDetails: ${message}`,
      var2: requestUrl
    });
    return success;
  }

  /**
   * This fnc returns interface configuration
   */
  const getInterface = (iface) => {
    log.audit("getInterface", "getting interface details.");
    switch (iface) {
      case "S02":
        return {
          id: iface,
          onActionHttpRequest: "POST",
          connectionRequired: true,
          tracking_error: "S20001",
          updating_order_status_error: "S20004",
          fetching_trans_status_error: "S20003",
          updating_order_delivery_status: "S20002",
          missing_tracking_details: "S20005",
          technical_error: "S00001",
          update_delivery_status_endpoint: 'custrecord_av_integration_delivry_status',
          request_url_fieldId: "custrecord_av_integration_tracking_post",
          get_order_status: "custrecord_av_integration_status_get",
          update_order_status: "custrecord_av_integration_so_post",
          getPayloadFunction: getTrackingPayload,
          // handleErrorFunction: handleErrorsP01,
          handleResponseFunction: handleTrackingResponse,
        };
    }
  };

  return {
    handleTrackingRequest,
    handleStatusRequest,
    sendData,
    sendUpdateStatusRequest
  };
});
