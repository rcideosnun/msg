/**
 ** Copyright (c) 2015 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.x
 ** @author: Tehseen Ahmed
 ** @dated: 16/1/2023
 ** @Description: This library is used to load configuration object
 * *@NApiVersion 2.1
 */

define(["N/format", "N/record", "N/search", "N/runtime", "N/https", 'N/encode', './av30_constants.js'],
  function (format, record, search, runtime, https, encode, constants) {
    const CONST = {
      CODE: {
        SUCCESS: 200,
        ERROR: 401
      },
      TIME_BUFFER: 0 //minutes
    }

    const _getConfigObj = (configId, context, connectionRequired) => {
      try {
        if (!configId) return null

        var configFields = ["custrecord_av_integration_base_url",
          "custrecord_av_integration_auth_url",
          "custrecord_av_integration_refresh_url",
          "custrecord_av_integration_username",
          "custrecord_av_integration_password",
          "custrecord_av_integration_auth_token",
          "custrecord_av_integration_refresh_token",
          "custrecord_av_integration_token_exp",
          "custrecord_av_integration_product_get",
          "custrecord_av_integration_product_post",
          "custrecord_av_integration_product_put",
          "custrecord_av_integration_dpmt_get",
          "custrecord_av_integration_stock_patch",
          "custrecord_av_integration_fulfill_locs",
          "custrecord_av_integration_disc_item",
          "custrecord_av_integration_brand_post",
          "custrecord_av_integration_sftp_out",
          "custrecord_av_integration_so_post",
          "custrecord_av_integration_stock_post",
          "custrecord_av_integration_store_post",
          "custrecord_av_integration_store_payload",
          "custrecord_av_integration_tracking_post",
          "custrecord_av_integration_status_get",
          "custrecord_av_integration_delivry_status",
          "custrecord_av_integration_shipmethod",
        ]

        var configObj = search.lookupFields({
          type: "customrecord_av_integration_config",
          id: configId,
          columns: configFields
        });

        configObj.fulfillableLocations = _getLocationIds(configObj.custrecord_av_integration_fulfill_locs);
        configObj.discountItem = _getDiscItems(configObj.custrecord_av_integration_disc_item);
        configObj.configId = configId;
        configObj.payload = configObj.custrecord_av_integration_store_payload;

        if (connectionRequired) {

          if (configObj.custrecord_av_integration_auth_token &&
            configObj.custrecord_av_integration_refresh_token &&
            configObj.custrecord_av_integration_token_exp) {
            return _checkTokenValidity(configObj)

          } else {
            return _generateNewToken(configObj)
          }
        } else {
          return configObj
        }

      } catch (e) {

        log.error('Error', e);
        throw e

        // JSON.stringify doesn't show error message
        // throw new Error(JSON.stringify({
        //   errorDetails: JSON.stringify(e)
        // }))
      }
    }

    const _checkTokenValidity = (configObj) => {

      var timeNow = new Date().getTime()
      var expirationTokenTime = configObj.custrecord_av_integration_token_exp

      var expirationTokenTimeParsed = format.parse({
        value: expirationTokenTime,
        type: format.Type.DATETIME
      })

      if (expirationTokenTimeParsed.getTime() + CONST.TIME_BUFFER * 60 * 1000 > timeNow) {
        log.debug("Old Token", `${expirationTokenTimeParsed.getTime() + CONST.TIME_BUFFER * 60 * 1000}> ${timeNow}`)
        configObj.accessToken = configObj.custrecord_av_integration_auth_token
        return configObj
      } else {
        log.debug("New Token")
        return _generateNewToken(configObj)
      }

    }

    const _generateNewToken = (configObj) => {

      var username = configObj.custrecord_av_integration_username;
      var password = configObj.custrecord_av_integration_password;

      var base64 = encode.convert({
        string: username + ':' + password,
        inputEncoding: encode.Encoding.UTF_8,
        outputEncoding: encode.Encoding.BASE_64
      });

      var reqHeaders = {
        "Content-type": "application/json",
        "Authorization": "Basic " + base64,
      }

      var reqUrl = configObj.custrecord_av_integration_base_url.concat(configObj.custrecord_av_integration_auth_url)

      var response = https.request({
        headers: reqHeaders,
        method: https.Method.POST,
        url: reqUrl,
        body: JSON.stringify({
          "grant_type": "client_credentials"
        })
      })

      return _handleTokenResponse(response, configObj)

    }

    const _handleTokenResponse = (response, configObj) => {
      if (response.code == CONST.CODE.SUCCESS) {
        var resBody = JSON.parse(response.body)
        var expirationTime = new Date();
        expirationTime = new Date(expirationTime.setHours(new Date().getHours() + 1))
        expirationTime = format.format({
          value: expirationTime,
          type: format.Type.DATETIME
        })

        var authorizationData = {
          "custrecord_av_integration_auth_token": resBody.access_token,
          "custrecord_av_integration_refresh_token": resBody.refreshToken,
          "custrecord_av_integration_token_exp": expirationTime
        }

        record.submitFields({
          type: "customrecord_av_integration_config",
          id: runtime.getCurrentScript().getParameter({ name: constants.ORDER_SYNC_PARAMS.CONFIG }) || configObj.configId,
          values: authorizationData
        })

        configObj.accessToken = resBody.access_token

        return configObj
      } else {
        throw new Error(JSON.stringify({
          errorCode: response.code,
          errorDetails: JSON.stringify(response.body)
        }))
      }
    }

    const _getLocationIds = (s) => {

      var locIds = [];

      for (var i = 0; i < s.length; i++) {
        locIds.push(s[i].value)
      }

      return locIds

    }

    const _getDiscItems = (di) => {

      var discountItemId = "";

      if (!di.length) return discountItemId

      return di[0].value

    }

    return _getConfigObj

  })