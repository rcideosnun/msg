/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tehseen Ahmed
* Date: 16/1/2023
* Constants for Shopware interface
*/

define([], function () {
  return {
    HTTP_CODES: {
      SUCCESS: 200,
      ERROR: 401
    },

    SHOP_ORDER_FIELDS: {
      "name": "orderNumber",
      "custrecord_av_sw_order_number": "orderNumber",
      "custrecord_av_sw_order_date": "orderDate",
      "custrecord_av_sw_order_date_time": "orderDateTime",
      "custrecord_av_sw_order_update_date": "updatedAt",
      "custrecord_av_sw_order_id": "id",
      "custrecord_av_sw_order_ship_amount": "shippingTotal",
      "custrecord_av_sw_order_totalprice": "amountTotal",
      "custrecord_av_sw_order_netprice": "amountNet",
      "custrecord_av_sw_order_bill_address": "billingAddress",
      "custrecord_av_sw_order_ship_address": "deliveries",
      "custrecord_av_sw_order_language": "language",
      "custrecord_av_sw_order_currency": "currency",
      "custrecord_av_sw_channel": "salesChannel",
      "custrecord_av_sw_order_ship_method": "deliveries",
      "custrecord_av_sw_order_payment_method": "transactions",
      "custrecord_av_sw_order_taxamt": "price",
      "custrecord_av_sw_order_taxrate": "price",
      "custrecord_av_sw_delivery_id": "deliveries",
      "custrecord_av_sw_order_customer_group": "salesChannel",
      "custrecord_av_sw_order_tax_status": "taxStatus"

    },

    SHOP_CUSTOMER_FIELDS: {
      "custrecord_av_sw_customer_number": "customerNumber",
      "custrecord_av_sw_customer_id": "customerId",
      "custrecord_av_sw_order_cx_salutation": "salutation",
      "custrecord_av_sw_order_cx_fn": "firstName",
      "custrecord_av_sw_order_cx_ln": "lastName",
      "custrecord_av_sw_order_cx_cn": "company",
      "custrecord_av_sw_order_cx_email": "email",
      "custrecord_av_sw_order_vat_ids": "vatIds"
    },

    SHOP_ORDER_LINE_FIELDS: {
      "custrecord_av_sw_order_line_item": "product",
      "custrecord_av_sw_order_line_qty": "quantity",
      "custrecord_av_sw_order_line_unit_price": "unitPrice",
      "custrecord_av_sw_order_line_total_price": "totalPrice",
      "custrecord_av_sw_order_line_taxrate": "price",
      "custrecord_av_sw_order_line_taxamt": "price",
      'custrecord_av_sw_order_line_type': 'type',
      'custrecord_av_sw_order_line_label': 'label',
      'custrecord_av_sw_order_line_payload': 'payload',
      'custrecord_av_sw_order_line_ref_id': 'referencedId',
      'custrecord_av_sw_order_line_disc_scope': 'payload'
    },

    S01: {
      STAGING_RECORD: {
        TYPE: 'customrecord_av_sw_order',
        FIELDS: {
          NAME: 'name',
          IFACE: 'custrecord_av_sw_order_iface',
          LOG: 'custrecord_av_sw_order_log',
          ENTITY: 'custrecord_av_sw_order_cx_entity',
          ORDER_DATE: 'custrecord_av_sw_order_date',
          ORDER_TIME: 'custrecord_av_sw_order_date_time',
          STATUS: 'custrecord_av_sw_order_state',
          ORDER_NUMBER: 'custrecord_av_sw_order_number',
          UPDATED_AT: 'custrecord_av_sw_order_update_date',
          SHIP_AMOUNT: 'custrecord_av_sw_order_ship_amount',
          TOTAL_PRICE: 'custrecord_av_sw_order_totalprice',
          NET_PRICE: 'custrecord_av_sw_order_netprice',
          BILLING: 'custrecord_av_sw_order_bill_address',
          SHIP_ADDRESS: 'custrecord_av_sw_order_ship_address',
          LANGUAGE: 'custrecord_av_sw_order_language',
          CURRENCY: 'custrecord_av_sw_order_currency',
          SALES_CHANNEL: 'custrecord_av_sw_channel',
          SHIP_METHOD: 'custrecord_av_sw_order_ship_method',
          PAYMENT: 'custrecord_av_sw_order_payment_method',
          TAX: 'custrecord_av_sw_order_taxamt',
          TAX_RATE: 'custrecord_av_sw_order_taxrate',
          DELIVERY_ID: 'custrecord_av_sw_delivery_id',
          GROUP: 'custrecord_av_sw_order_customer_group',
          TAX_STATUS: 'custrecord_av_sw_order_tax_status'
        },
        CUSTOMER_FIELDS: {
          NUMBER: 'custrecord_av_sw_customer_number',
          ID: 'custrecord_av_sw_customer_id',
          SALUTATION: 'custrecord_av_sw_order_cx_salutation',
          FIRST_NAME: 'custrecord_av_sw_order_cx_fn',
          LAST_NAME: 'custrecord_av_sw_order_cx_ln',
          COMPANY: 'custrecord_av_sw_order_cx_cn',
          EMAIL: 'custrecord_av_sw_order_cx_email',
          VAT_ID: 'custrecord_av_sw_order_vat_ids'
        },
        SUBLIST_ID: 'recmachcustrecord_av_sw_order',
        SUBLIST_FIELDS: {
          ITEM: 'custrecord_av_sw_order_line_item',
          QUANTITY: 'custrecord_av_sw_order_line_qty',
          UNIT_PRICE: 'custrecord_av_sw_order_line_unit_price',
          TOTAL_PRICE: 'custrecord_av_sw_order_line_total_price',
          TAX_RATE: 'custrecord_av_sw_order_line_taxrate',
          TAX_AMOUNT: 'custrecord_av_sw_order_line_taxamt',
          TYPE: 'custrecord_av_sw_order_line_type',
          LABEL: 'custrecord_av_sw_order_line_label',
          PAYLOAD: 'custrecord_av_sw_order_line_payload',
          REF_ID: 'custrecord_av_sw_order_line_ref_id',
          DISC_SCOPE: 'custrecord_av_sw_order_line_disc_scope'
        }
      },
      ERRORS: {
        ITEM_NOT_EXIST: 'S10001',
        PM_NOT_EXIST: 'S10002',
        CUSTOMER_INACTIVE: 'S10003',
        CURR_NOT_ASSOCIATED: 'S10004',
        MISSING_EMAIL: 'S10005',
        MISSING_NAME: 'S10006',
        MISSING_PAYMENT_TYPE: 'S10007',
        EX: 'S10008',
      },
      STATUS: {
        NEW: 1,
        ERROR: 2,
        DONE: 3
      },
      WF_CONFIG: {
        WORKFLOW_ACTION: '',
        WORKFLOW_ID: ''
      }
    },

    S02: {
      ERRORS: {
        EX: 'S20001'
      },
      STATUS: {
        NEW: 1,
        ERROR: 2,
        DONE: 3
      }
    },

    ORDER_SYNC_PARAMS: {
      IFACE: 'custscript_av30_shop_order_iface',
      START_DATE: 'custscript_av30_shop_order_from_day',
      CONFIG: 'custscript_av30_shop_order_config'
    },

    LOG_STATE: {
      NEW: 1,
      ERROR: 2,
      DONE: 3
    }
  };
});