/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tuba Mohsin
* Date: 06/02/2023
* Send updated order status to Shopware
* @NApiVersion 2.1
* @NScriptType UserEventScript
*/

define([ './libraries/av30_shop_order_tracking_lib.js','N/runtime', 'N/record'], function (orderUpdateLib, runtime, record) {

    function afterSubmit(context) {
      try {
        // if (context.type === context.UserEventType.CREATE) {
   
          if(context.type !== context.UserEventType.CREATE && 
            context.oldRecord.getValue('statusRef') === context.newRecord.getValue('statusRef') ) return;

          let scriptObj = runtime.getCurrentScript();
          const iface = scriptObj.getParameter({ name: "custscript_av_so_iface" });
          let soId = [context.newRecord.getValue('createdfrom')];
         
          if(context.newRecord.type == 'salesorder') {
            soId = [context.newRecord.id];
            const shopId = context.newRecord.getValue('custbody_av_sw_order_id');
            if(!shopId) return;

            const statusRef = context.newRecord.getValue('statusRef');
            const isShopOrderClosed = context.newRecord.getValue('custbody_av_sw_order_closed');

            if(statusRef !== 'closed' && isShopOrderClosed) {
              record.submitFields({
                id: context.newRecord.id,
                type: context.newRecord.type,
                values: {
                    'custbody_av_sw_order_closed': false,
                }
              })
            }
          }
          log.debug('Processing request for ==>', `iface: ${iface}, soID: ${soId}`);
          orderUpdateLib.handleStatusRequest(soId, iface);
        // }
      } catch (ex) {
        log.error({ title: 'beforeSubmit : ex', details: ex });
      }
    }
  
    return {
      afterSubmit: afterSubmit
    }
  })