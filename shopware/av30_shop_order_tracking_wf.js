/**
 * Copyright (c) 2019 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @version: 2.x
 * @author: [Tehseen Ahmed]
 * @dated: [23.01.2023]
 * @Description: WF Script to update orders in shopware
 *
 *@NApiVersion 2.1
 *@NScriptType WorkflowActionScript
 */

define(['N/runtime', './libraries/av30_shop_order_tracking_lib.js'],
  function (runtime, orderUpdateLib) {
    const onAction = (context) => {
      const currentRuntime = runtime.getCurrentScript();
      const ifaceType = currentRuntime.getParameter({
        name: 'custscript_av30_shop_tracking_iface'
      });
      // const configId = currentRuntime.getParameter({
      //   name: 'custscript_av30_shop_tracking_config'
      // });

      // if (!configId) return;
      try {
        const shopId = context.newRecord.getValue('custbody_av_sw_order_id');
        log.audit('shopId', shopId);
        if (!shopId) return;

        const soId = [context.newRecord.getValue('createdfrom')];
        const fulfillmentId = [context.newRecord.id];
        log.debug('Processing request for ==>', `iface: ${ifaceType}, soID: ${soId}`);
        orderUpdateLib.handleTrackingRequest(soId, ifaceType, fulfillmentId);

      } catch (ex) {
        log.error({ title: 'onAction: ex', details: ex });
      }
    }

    return {
      onAction: onAction
    };
  })