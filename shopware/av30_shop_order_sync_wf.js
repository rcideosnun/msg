/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
** @version: 2.1
** @author: Tehseen Ahmed
** @dated: 26.1.2023
** @Description: Create orders from shopware staging record
*
*  @NApiVersion 2.1
*  @NScriptType WorkflowActionScript
*/

define(['../pimCoreIntegration/av_int_log.js', 'N/runtime', 'N/search', 'N/record', 'N/format'],
  function (avLog, runtime, search, record, format) {
    var self = this;
    var CONSTANTS = {
      HTTP_CODE: {
        SUCCESS_UPDATE: 200,
        SUCCESS_CREATE: 201,
        ERROR: 401
      },
      INTERFACE_STATE: {
        NEW: 1,
        ERROR: 2,
        DONE: 3
      },
      SHOP_ERRORS: {
        ITEM_NOT_EXIST: 'S10001',
        PM_NOT_EXIST: 'S10002',
        CUSTOMER_INACTIVE: 'S10003',
        CURR_NOT_ASSOCIATED: 'S10004',
        MISSING_EMAIL: 'S10005',
        MISSING_NAME: 'S10006',
        MISSING_PAYMENT_TYPE: 'S10007',
        EX: 'S10008',
        DUPLICATED_ORDER: "S10009",
        TOTAL_NOT_EQUAL: "S10010",
        GC_NOT_FOUND: "S10011",
        MISSING_VAT_ID: "S10012"
      },
      SHOPWARE_PRODUCT_TYPE: {
        PRODUCT: "product",
        PROMOTION: "promotion",
        DISCOUNT: "discount",
        GC_REDEMPTION: "xgx-coupon"
      },
      PRICE_TYPE: {
        BASE_PRICE: 1,
        CUSTOM_PRICE: -1
      },
      DEFAULT_SHIP_METHOD: {
        DHL: 411
      },
      ITEM_TYPE: {
        GIFT_CERTIFICATE_TYPE: "giftcertificateitem",
        GC_NS_TYPE: "GiftCert",
        KIT_ITEM_TYPE: "kititem",
        SERVICE_TYPE: "serviceitem"
      },
      PROCESS_INVOICE: {
        BY_PAYMENT_METHOD: {
          "Vorkasse": false
        }
      },
      ORDER_STATUS_ENTRY: {
        "Vorkasse": "A",
        "DEFAULT": "B"
      }
    }

    function onAction(context) {
      var stagingRec = context.newRecord;
      self.iface = stagingRec.getValue("custrecord_av_sw_order_iface");
      self.logId = stagingRec.getValue("custrecord_av_sw_order_log");

      var scriptObj = runtime.getCurrentScript();
      self.cartDiscount = scriptObj.getParameter({ name: 'custscript_av30_shop_order_cart_disc' });
      self.cartDiscountDesc = scriptObj.getParameter({ name: 'custscript_av30_shop_order_cart_disc_des' });
      self.itemDiscount = scriptObj.getParameter({ name: 'custscript_av30_shop_order_item_disc' });
      self.groupDiscount = scriptObj.getParameter({ name: 'custscript_av30_shop_order_group_disc' });
      self.partialDiscount = scriptObj.getParameter({ name: 'custscript_av30_shop_order_partial_disc' });
      self.stockDiscount = scriptObj.getParameter({ name: 'custscript_av30_shop_order_stock_disc' });
      self.shippingDiscount = scriptObj.getParameter({ name: 'custscript_av30_shop_order_shipping_disc' });

      try {
        return _createSalesOrderFromStaging(stagingRec);

      } catch (ex) {
        log.error({ title: 'onAction: ex', details: ex });
        stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
        avLog.error_message({
          msg: CONSTANTS.SHOP_ERRORS.EX,
          staging: stagingRec.id,
          iface: self.iface,
          logid: self.logId,
          var1: ex.message ? ex.message : JSON.stringify(ex)
        });
      }
    }

    function _createSalesOrderFromStaging(stagingRec) {
      try {
        var anyItemNotFound = false, itemsNotFound = [], hasGc = false, soId;
        if (!stagingRec.getValue("custrecord_av_sw_order_cx_email")) {
          stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
          avLog.error_message({
            msg: CONSTANTS.SHOP_ERRORS.MISSING_EMAIL,
            staging: stagingRec.id,
            iface: self.iface,
            logid: self.logId,
          });
          return;
        }

        if (!stagingRec.getValue("custrecord_av_sw_order_cx_fn")) {
          stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
          avLog.error_message({
            msg: CONSTANTS.SHOP_ERRORS.MISSING_NAME,
            staging: stagingRec.id,
            iface: self.iface,
            logid: self.logId,
          });
          return;
        }

        if (!stagingRec.getValue("custrecord_av_sw_order_payment_method")) {
          stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
          avLog.error_message({
            msg: CONSTANTS.SHOP_ERRORS.MISSING_PAYMENT_TYPE,
            staging: stagingRec.id,
            iface: self.iface,
            logid: self.logId,
          });
          return;
        }

        var isOrderNew = _checkIfOrderIsNew(stagingRec.getValue("custrecord_av_sw_order_number"));
        var paymentMethod = stagingRec.getText("custrecord_av_sw_order_payment_method");
        var currencyS = _lookUpCurrencybyIso(stagingRec.getValue("custrecord_av_sw_order_currency"));
        if (!isOrderNew) {
          stagingRec.setValue("custrecord_av_sw_ns_transaction", self.oldSalesOrderId)
          stagingRec.setValue("custrecord_av_sw_order_cx_entity", self.entitiyId)
          stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.DONE)

          avLog.error_message({
            iface: self.iface,
            logid: self.logId,
            staging: stagingRec.id,
            msg: CONSTANTS.SHOP_ERRORS.DUPLICATED_ORDER,
            var1: self.oldSalesOrderId
          })

          return;
        }

        var custGroup = stagingRec.getValue("custrecord_av_sw_order_customer_group");
        var vatId = JSON.parse(stagingRec.getValue("custrecord_av_sw_order_vat_ids"));
        vatId = vatId[0];
        if (custGroup && custGroup.indexOf('B2B') > -1 && !vatId) {
          stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
          avLog.error_message({
            msg: CONSTANTS.SHOP_ERRORS.MISSING_VAT_ID,
            staging: stagingRec.id,
            iface: self.iface,
            logid: self.logId,
            var1: stagingRec.getValue("custrecord_av_sw_order_vat_ids")
          })
          return;
        }

        var entitiyId = stagingRec.getValue("custrecord_av_sw_order_cx_entity") ? stagingRec.getValue("custrecord_av_sw_order_cx_entity") : _findEntity(stagingRec, currencyS.currenciesIds)
        stagingRec.setValue("custrecord_av_sw_order_cx_entity", entitiyId)
        if (!entitiyId) return stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
        if (vatId) {
          updateVatId(entitiyId, vatId);
        }
        var addressS = _getAddresses(entitiyId);
        var shipAddressId = _getAddressId(addressS, stagingRec, entitiyId, 'custrecord_av_sw_order_ship_address');
        // update required because a new address maybe create in the above function.
        addressS = _getAddresses(entitiyId);
        var billAddressId = _getAddressId(addressS, stagingRec, entitiyId, 'custrecord_av_sw_order_bill_address');

        var soRec = record.transform({
          fromType: record.Type.CUSTOMER,
          fromId: entitiyId,
          toType: record.Type.SALES_ORDER,
          isDynamic: true
        });

        if (custGroup && custGroup.indexOf('B2B') > -1) {
          soRec.setValue("custbody_slx_is_b2b_order", true);
        }
        if (shipAddressId) {
          soRec.setValue("shipaddresslist", shipAddressId);
        }
        if (billAddressId) {
          soRec.setValue("billaddresslist", billAddressId);
        }

        soRec.setValue("shipcomplete", true);

        soRec = _setMainFields(soRec, stagingRec, currencyS);

        if (CONSTANTS.ORDER_STATUS_ENTRY[paymentMethod]) {
          soRec.setValue("orderstatus", CONSTANTS.ORDER_STATUS_ENTRY[paymentMethod])
        } else {
          soRec.setValue("orderstatus", CONSTANTS.ORDER_STATUS_ENTRY.DEFAULT)
        }

        var stagingLines = stagingRec.getLineCount({
          sublistId: "recmachcustrecord_av_sw_order"
        })

        var salesLocations = _getSalesLocations();
        var itemS = _getItems(stagingRec);
        var itemIds = itemS.itemIds || {};
        var itemTypes = itemS.itemTypes || {};
        var taxStatus = stagingRec.getValue("custrecord_av_sw_order_tax_status");
        //var discountItem = runtime.getCurrentScript().getParameter({ name: 'custscript_av_sw_order_disc_item' });
        var shipMethod = _pullShipMethod(stagingRec.getValue("custrecord_av_sw_order_ship_method"));
        var qtyByLocation = _getLocationsByItem(itemIds, salesLocations);
        var isFulfillable = isOrderFulfillable(stagingRec, qtyByLocation, stagingLines, itemIds);
        var defaultLocation = Object.keys(salesLocations).find(key => salesLocations[key] === true);
        var isValid = true;
        var orderLocation = '';
        if (shipMethod == 'Click & Collect') {
          if (isFulfillable) {
            orderLocation = runtime.getCurrentScript().getParameter({ name: 'custscript_av_sw_order_f_location' });
          } else {
            orderLocation = defaultLocation
          }
        } else {
          orderLocation = defaultLocation
        }
        soRec.setValue("location", orderLocation);



        // handle line items with promotions starts

        var productLines = [];
        var promotionLines = [];
        var giftLines = [];
        for (var i = 0; i < stagingLines; i++) {
          var line = {
            itemId: '',
            item: stagingRec.getSublistValue('recmachcustrecord_av_sw_order', 'custrecord_av_sw_order_line_item', i),
            label: stagingRec.getSublistValue('recmachcustrecord_av_sw_order', 'custrecord_av_sw_order_line_label', i),
            taxrate: Number(stagingRec.getSublistValue('recmachcustrecord_av_sw_order', 'custrecord_av_sw_order_line_taxrate', i)),
            gross: Number(stagingRec.getSublistValue('recmachcustrecord_av_sw_order', 'custrecord_av_sw_order_line_total_price', i)),
            rate: Number(stagingRec.getSublistValue('recmachcustrecord_av_sw_order', 'custrecord_av_sw_order_line_unit_price', i)),
            quantity: Number(stagingRec.getSublistValue('recmachcustrecord_av_sw_order', 'custrecord_av_sw_order_line_qty', i)),
            reference: stagingRec.getSublistValue('recmachcustrecord_av_sw_order', 'custrecord_av_sw_order_line_ref_id', i),
            payload: JSON.parse(stagingRec.getSublistValue('recmachcustrecord_av_sw_order', 'custrecord_av_sw_order_line_payload', i))
          }

          var type = stagingRec.getSublistValue('recmachcustrecord_av_sw_order', 'custrecord_av_sw_order_line_type', i);
          switch (type) {
            case 'product':
              var itemId = itemIds[line.item];
              if (itemId) {
                line.itemId = itemId;
                line.type = itemTypes[line.item];
                var lineQty = line.quantity;
                if (line.type == CONSTANTS.ITEM_TYPE.GC_NS_TYPE && lineQty > 1) {
                  var lineGross = line.gross / lineQty;
                  line.gross = lineGross;
                  for (let j = 0; j < lineQty; j++) {
                    line.quantity = 1;
                    productLines.push(line);
                  }
                } else {
                  productLines.push(line);
                }
              } else {
                itemsNotFound.push(line.item)
                anyItemNotFound = true;
              }
              break;
            case 'promotion':
              promotionLines.push(line);
              break;
            case 'xgx-coupon':
              giftLines.push(line);
              break;
          }
        }

        if (anyItemNotFound) {
          stagingRec.setValue('custrecord_av_sw_order_state', CONSTANTS.INTERFACE_STATE.ERROR)
          avLog.error_message({
            staging: stagingRec.id,
            msg: CONSTANTS.SHOP_ERRORS.ITEM_NOT_EXIST,
            var1: itemsNotFound,
            iface: stagingRec.getValue('custrecord_av_sw_order_iface'),
            logid: stagingRec.getValue('custrecord_av_sw_order_log')
          })
          return;
        }

        // Add product lines
        if (!promotionLines.length) {
          productLines.forEach(function (line) {
            addLineItem(soRec, line, false, stagingRec, orderLocation)
            return true;
          })

          // Add gift redemption lines
          giftLines.forEach(function (line) {
            addGcItem(soRec, line, stagingRec)
            return true;
          })
        } else {
          // Add promotions/discounts
          var promotionsByScope = {};
          promotionLines.forEach(function (line) {
            var discountScope = line.payload.discountScope;
            if (!promotionsByScope[discountScope]) {
              promotionsByScope[discountScope] = [line]
            } else {
              promotionsByScope[discountScope].push(line)
            }
          })

          if (promotionsByScope['cart'] && promotionsByScope['cart'].length > 1) {
            stagingRec.setValue('custrecord_av_sw_order_state', CONSTANTS.INTERFACE_STATE.ERROR)
            avLog.error_message({
              staging: stagingRec.id,
              msg: 'S10013',
              var1: promotionsByScope['cart'].length,
              iface: stagingRec.getValue('custrecord_av_sw_order_iface'),
              logid: stagingRec.getValue('custrecord_av_sw_order_log')
            })
            return;
          }

          if (promotionsByScope['cart'] && promotionsByScope['cart'].length > 0
            && (promotionsByScope['set'] && promotionsByScope['set'].length > 0 || promotionsByScope['setgroup'] && promotionsByScope['setgroup'].length > 0)) {
            stagingRec.setValue('custrecord_av_sw_order_state', CONSTANTS.INTERFACE_STATE.ERROR)
            avLog.error_message({
              staging: stagingRec.id,
              msg: 'S10014',
              var1: 'cart promotions: ' + promotionsByScope['cart'].length + '. group promotions' + promotionsByScope['set'].length || promotionsByScope['setgroup'].length,
              iface: stagingRec.getValue('custrecord_av_sw_order_iface'),
              logid: stagingRec.getValue('custrecord_av_sw_order_log')
            })
            return;
          }

          if (promotionsByScope['cart'] && promotionsByScope['cart'].length == 1) {
            var nonInvLines = [];
            productLines.forEach(function (line) {
              if (line.type == 'InvtPart') {
                addLineItem(soRec, line, false, stagingRec, orderLocation)
              } else {
                nonInvLines.push(line);
              }
              return true;
            })
            if (!nonInvLines.length) {
              var discountLine = promotionsByScope['cart'][0];
              var discountRate = discountLine.gross;
              // if (discountLine.payload.discountType == 'percentage') {
              //   discountRate = discountLine.payload.value;
              // }
              if (discountLine.taxrate) {
                discountRate = (discountRate / (1 + discountLine.taxrate / 100) * 100) / 100;
              }

              // TODO: Need to see how can we solve the rounding issue. There is no separate tax field to manipulate discount tax
              discountRate = String(discountRate);
              if (discountRate.includes('.')) {
                  if (discountRate.split('.')[1].length > 2); {
                    discountRate = parseFloat(discountRate);
                    discountRate = discountRate.toFixed(3);
                  }
              };
              discountRate = discountRate.indexOf('.') > -1 ? discountRate.replace('.', ',') : discountRate;
              // discountRate = '-' + discountRate;
              // if (discountLine.payload.discountType == 'percentage') {
              //   discountRate = '-' + discountLine.payload.value + '%';
              // }
              
              soRec.setValue({fieldId:'discountitem',value: self.cartDiscount});
              soRec.setText({fieldId:'discountrate',text: discountRate});

              soLine = soRec.selectNewLine({
                sublistId: 'item'
              })
      
              soLine.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'item',
                value: self.cartDiscountDesc
              });
              
              soLine.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'description',
                value: discountLine.payload.code ? discountLine.label + '. Promotion Code: ' + discountLine.payload.code : discountLine.label
              });
        
              soLine.commitLine("item")

              if (giftLines.length) {
                giftLines.forEach(function (line) {
                  addGcItem(soRec, line, stagingRec)
                  return true;
                })
              }
            }

            if (nonInvLines.length) {
              // Add subtotal
              var soLine = soRec.selectNewLine({
                sublistId: 'item'
              })
              soLine.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'item',
                value: '-2'
              });
              soLine.commitLine("item")
              addLineItem(soRec, promotionsByScope['cart'][0], self.stockDiscount, stagingRec, orderLocation);
              nonInvLines.forEach(function (line) {
                addLineItem(soRec, line, false, stagingRec, orderLocation)
              })
              giftLines.forEach(function (line) {
                addGcItem(soRec, line, stagingRec)
                return true;
              })
            }
          }

          if (promotionsByScope['set'] && promotionsByScope['set'].length > 0) {
            productLines.forEach(function (line) {
              addLineItem(soRec, line, false, stagingRec, orderLocation)
              return true;
            })

            // Add gift redemption lines
            giftLines.forEach(function (line) {
              addGcItem(soRec, line, stagingRec)
              return true;
            })
            promotionsByScope['set'].forEach(function (promotion) {
              var refId = promotion.payload.composition[0].id;
              var productLine = productLines.filter(function (line) {
                return line.reference == refId;
              }) || []
              if (productLine.length > 1) {
                stagingRec.setValue('custrecord_av_sw_order_state', CONSTANTS.INTERFACE_STATE.ERROR)
                avLog.error_message({
                  staging: stagingRec.id,
                  msg: 'S10015',
                  var1: refId,
                  iface: stagingRec.getValue('custrecord_av_sw_order_iface'),
                  logid: stagingRec.getValue('custrecord_av_sw_order_log')
                })
                isValid = false;
                return false;
              } else if (productLine[0]) {
                var idx = soRec.findSublistLineWithValue({
                  sublistId: 'item',
                  fieldId: 'item',
                  value: productLine[0].itemId
                })
                var compQty = promotion.payload.composition[0].quantity;
                var discItem = self.itemDiscount;
                if (compQty != productLine[0].quantity) {
                  discItem = self.partialDiscount
                }
                addLineItem(soRec, promotion, discItem, stagingRec, orderLocation, idx + 1)
                return true;
              }
            })
          }

          if (promotionsByScope['setgroup'] && promotionsByScope['setgroup'].length > 0) {
            promotionsByScope['setgroup'].forEach(function (promotion) {
              var discItem = self.groupDiscount;
              var filteredLines = [];
              promotion.payload.composition.forEach(function (comp) {
                var product = productLines.find(function (line) {
                  return line.reference == comp.id;
                })
                if (product) {
                  if (product.quantity != comp.quantity) {
                    discItem = self.partialDiscount;
                  }
                  filteredLines.push(product);
                }
              });
              filteredLines.forEach(function (line) {
                addLineItem(soRec, line, false, stagingRec, orderLocation)
                return true;
              })
              // Add subtotal
              var soLine = soRec.selectNewLine({
                sublistId: 'item'
              })
              soLine.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'item',
                value: '-2'
              });
              soLine.commitLine('item')
              addLineItem(soRec, promotion, discItem, stagingRec, orderLocation, false, true);
              return true;
            })

            // Add remaining products
            productLines.forEach(function (line) {
              var idx = soRec.findSublistLineWithValue({
                sublistId: 'item',
                fieldId: 'item',
                value: line.itemId
              })
              if (idx == -1) {
                addLineItem(soRec, line, false, stagingRec, orderLocation)
              }
              return true;
            })
            // Add gift redemption lines
            giftLines.forEach(function (line) {
              addGcItem(soRec, line, stagingRec)
              return true;
            })
          }

          if (promotionsByScope['delivery'] && promotionsByScope['delivery'].length > 0) {
            promotionsByScope['delivery'].forEach(function (promotion) {
              addLineItem(soRec, promotion, self.shippingDiscount, stagingRec, orderLocation)
              return true;
            })
          }
        }

        if (!isValid) return;

        // handle line items with promotions ends

        soRec = setShipping(stagingRec, soRec);
        var orderTotal = parseFloat(soRec.getValue('total'));
        var stagingTotal = parseFloat(stagingRec.getValue('custrecord_av_sw_order_totalprice'));
        
        if (orderTotal !== stagingTotal) {  
          stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)

          avLog.error_message({
            staging: stagingRec.id,
            msg: CONSTANTS.SHOP_ERRORS.TOTAL_NOT_EQUAL,
            var1: 'Order total: ' + orderTotal,
            iface: stagingRec.getValue("custrecord_av_sw_order_iface"),
            logid: stagingRec.getValue("custrecord_av_sw_order_log")
          })
          return;
        }

        soId = soRec.save({
          enableSourcing: true,
          ignoreMandatoryFields: true
        });

        stagingRec.setValue("custrecord_av_sw_ns_transaction", soId)
        stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.DONE)

        if (hasGc && soId) {

          soRec = record.load({
            type: "salesorder",
            id: soId
          })
          let gcCodesIdsResave = [];

          var gcLines = soRec.getLineCount({
            sublistId: "item"
          })

          for (var i = 0; i < gcLines; i++) {

            gcCodesIdsResave.push({
              id: soRec.getSublistValue({
                sublistId: 'item',
                fieldId: 'giftcertkey',
                line: i
              })
            });

            soRec.setSublistValue({
              sublistId: 'item',
              fieldId: 'description',
              value: soRec.getSublistValue({
                sublistId: 'item',
                fieldId: 'giftcertnumber',
                line: i
              }),
              line: i
            })

            //giftcertnumber

          }

          if (gcCodesIdsResave.length) {
            _updateGcCrs(gcCodesIdsResave)
          }

          // if (CONSTANTS.PROCESS_INVOICE.BY_PAYMENT_METHOD[paymentMethod] === false) return;

          // soRec.save({
          //   enableSourcing: false,
          //   ignoreMandatoryFields: true
          // })
        }

        // if (soId) {
        //   try {
        //     var invRec = record.transform({
        //       fromType: "salesorder",
        //       fromId: soId,
        //       toType: "invoice",
        //       isDynamic: true
        //     })

        //     var invoiceId = invRec.save({
        //       enableSourcing: true,
        //       ignoreMandatoryFields: true
        //     });

        //     stagingRec.setValue("custrecord_av_sw_ns_inv", invoiceId)
        //   } catch (ex) {
        //     log.error({ title: 'create invoice: ex', details: ex });
        //     stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
        //     avLog.error_message({
        //       msg: CONSTANTS.SHOP_ERRORS.EX,
        //       staging: stagingRec.id,
        //       iface: self.iface,
        //       logid: self.logId,
        //       var1: ex.message ? ex.message : JSON.stringify(ex)
        //     });
        //   }
        // }

      } catch (ex) {
        log.error({ title: '_createSalesOrderFromStaging: ex', details: ex });
        stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
        avLog.error_message({
          msg: CONSTANTS.SHOP_ERRORS.EX,
          staging: stagingRec.id,
          iface: self.iface,
          logid: self.logId,
          var1: ex.message ? ex.message : JSON.stringify(ex)
        });
      }

    }

    function addLineItem(soRec, line, promotionItem, stagingRec, orderLocation, lineNo, isGroup) {
      var soLine = '';
      if (lineNo) {
        soLine = soRec.insertLine({
          sublistId: 'item',
          line: lineNo
        })
      } else {
        soLine = soRec.selectNewLine({
          sublistId: 'item'
        })
      }

      soLine.setCurrentSublistValue({
        sublistId: 'item',
        fieldId: 'item',
        value: promotionItem ? promotionItem : line.itemId
      });

      if (promotionItem) {
        soLine.setCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'description',
          value: line.payload.code ? 'Promotion Code: ' + line.payload.code : line.label
        });
      }

      soLine.setCurrentSublistValue({
        sublistId: 'item',
        fieldId: 'location',
        value: orderLocation
      });

      soLine.setCurrentSublistValue({
        sublistId: 'item',
        fieldId: 'price',
        value: CONSTANTS.PRICE_TYPE.CUSTOM_PRICE
      });

      if (!promotionItem) {
        soLine.setCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'quantity',
          value: line.quantity
        });

        soLine.setCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'rate',
          value: line.rate
        });
  
        soLine.setCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'taxrate1',
          value: line.taxrate
        });
      }

      if (soLine.getCurrentSublistValue({
        sublistId: 'item',
        fieldId: 'itemtype'
      }) == CONSTANTS.ITEM_TYPE.GC_NS_TYPE) {

        soLine.setCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'giftcertfrom',
          value: stagingRec.getValue('custrecord_av_sw_order_cx_fn')
        });

        soLine.setCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'giftcertrecipientname',
          value: stagingRec.getValue('custrecord_av_sw_order_cx_fn')
        });

        soLine.setCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'giftcertrecipientemail',
          value: stagingRec.getValue('custrecord_av_sw_order_cx_email')
        });

      }

      // if (promotionItem && isGroup) {
      //   if (line.payload.discountType == 'percentage') {
      //     var rate = '-' + line.payload.value + '%';
      //     soLine.setCurrentSublistText({
      //       sublistId: 'item',
      //       fieldId: 'rate',
      //       text: rate
      //     });
      //   }
      // }

      if (stagingRec.getValue("custrecord_av_sw_order_tax_status") == 'net') {
        soLine.setCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'amount',
          value: line.gross
        });
      } else {
        soLine.setCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'grossamt',
          value: line.gross
        });
      }

      soLine.commitLine({
        sublistId: 'item'
      })
    }

    function addGcItem(soRec, line, stagingRec) {
      //log.audit('line', line);
      var soLine = soRec.selectNewLine({
        sublistId: 'giftcertredemption'
      })
      try {
        //log.audit('line.label', line.label);
        //log.audit('line.label', line.label.trim());
  
        soLine.setCurrentSublistText({
          sublistId: 'giftcertredemption',
          fieldId: 'authcode',
          text: line.label
        });
      } catch (ex) {
        //log.audit('ex', ex);
        stagingRec.setValue('custrecord_av_sw_order_state', CONSTANTS.INTERFACE_STATE.ERROR)
        avLog.error_message({
          msg: CONSTANTS.SHOP_ERRORS.GC_NOT_FOUND,
          staging: stagingRec.id,
          iface: self.iface,
          logid: self.logId,
          var1: ex.message ? ex.message : JSON.stringify(ex)
        });
        isValid = false;
        return;
      }

      soLine.setCurrentSublistText({
        sublistId: 'giftcertredemption',
        fieldId: 'authcodeapplied',
        text: Math.abs(line.gross)
      });
      log.audit('soLine', soLine);
      soLine.commitLine('giftcertredemption')
    }

    function _setMainFields(soRec, stagingRec, currencyS) {
      var date = new Date(stagingRec.getValue("custrecord_av_sw_order_date_time"));
      var parsedDate = format.parse({
        value: date,
        type: format.Type.DATE
      })

      soRec.setValue("trandate", parsedDate);
      var shipMethod = _pullShipMethod(stagingRec.getValue("custrecord_av_sw_order_ship_method"));
      soRec.setValue("custbody_av_sw_order_number", stagingRec.getValue("custrecord_av_sw_order_number"));
      soRec.setText("custbody_av_payment_method", stagingRec.getValue("custrecord_av_sw_order_payment_method"));
      if (shipMethod) {
        soRec.setText("shipmethod", shipMethod);
      }
      soRec.setText("currency", currencyS.orderCurrencyId);
      soRec.setValue("custbody_av_sw_order_id", stagingRec.getValue("custrecord_av_sw_order_id"));
      soRec.setValue("custbody_av_delivery_id", stagingRec.getValue("custrecord_av_sw_delivery_id"));
      var salesChannel = stagingRec.getValue("custrecord_av_sw_channel");
      if (salesChannel) {
        soRec.setText("cseg_msg_sales_chan", salesChannel);
      }
      return soRec
    }

    function setShipping(stagingRec, soRec) {
      var taxStatus = stagingRec.getValue("custrecord_av_sw_order_tax_status");
      var grossShippingCost = stagingRec.getValue("custrecord_av_sw_order_ship_amount");
      var taxRate = stagingRec.getValue("custrecord_av_sw_order_taxrate");
      if (taxStatus == 'gross') {
        var new_shipnetamt = Math.round(grossShippingCost / (1 + taxRate / 100) * 100) / 100;
        if (new_shipnetamt != soRec.getValue('shippingcost')) {
          soRec.setValue({
            fieldId: 'shippingcost',
            value: new_shipnetamt
          });
        }
        // update rate if not current
        if (taxRate != soRec.getValue('shippingtax1rate')) {
          soRec.setValue({
            fieldId: 'shippingtax1rate',
            value: taxRate
          });
          soRec.setValue({
            fieldId: 'shippingcost',
            value: new_shipnetamt
          });
        }
        var shipgrossControl = Math.round(new_shipnetamt * (Math.round((100 + taxRate) * 100) / 100)) / 100;
        if (shipgrossControl != grossShippingCost) {
          var new_shiptaxrate = grossShippingCost / new_shipnetamt * 100 - 100;
          soRec.setValue({
            fieldId: 'shippingtax1rate',
            value: new_shiptaxrate
          });
          soRec.setValue({
            fieldId: 'shippingcost',
            value: new_shipnetamt
          });
        }
      } else {
        soRec.setValue({
          fieldId: 'shippingtax1rate',
          value: taxRate
        });
        soRec.setValue({
          fieldId: 'shippingcost',
          value: grossShippingCost
        });
      }

      return soRec;
    }

    function _getAddresses(eid) {
      var addresses = [];

      _getAllResults(search.create({
        type: "customer",
        filters:
          [
            ["isinactive", "is", "F"],
            "AND",
            ["internalid", "anyof", eid]
          ],
        columns:
          [
            search.createColumn({
              name: "zipcode",
              join: "Address",
              label: "Zip Code"
            }),
            search.createColumn({
              name: "address1",
              join: "Address",
              label: "Address Line 1"
            }),
            search.createColumn({
              name: "addressinternalid",
              join: "Address",
              label: "Address Internal ID"
            })
          ]
      })).every(function (result) {
        addresses.push({
          id: result.getValue({
            name: "addressinternalid",
            join: "Address"
          }),
          zipcode: result.getValue({
            name: "zipcode",
            join: "Address"
          }),
          address1: result.getValue({
            name: "address1",
            join: "Address"
          }),
        });

        return true
      });
      return addresses
    }

    function _getAddressId(adresses, stagingRec, eid, fieldId) {
      var orderAddress = stagingRec.getValue(fieldId) ? JSON.parse(stagingRec.getValue(fieldId)) : null;
      var addressId = '';
      if (adresses.length) {
        adresses.forEach(address => {
          if (address.zipcode == orderAddress.zipcode.trim() && address.address1 == orderAddress.street.trim()) {
            addressId = address.id;
            return false;
          }
        });
      }

      if (addressId) return addressId;

      var customerRec = record.load({
        type: "customer",
        id: eid,
        isDynamic: true
      })

      if (orderAddress.country && orderAddress.country.iso) {
        customerRec = _setAddress(customerRec, stagingRec, orderAddress, fieldId)
      }

      customerRec.save();

      var updatedAddresses = _getAddresses(eid);

      updatedAddresses.forEach(address => {
        if (address.zipcode == orderAddress.zipcode && address.address1 == orderAddress.street) {
          addressId = address.id;
        }
      });

      return addressId;
    }

    function _updateGcCrs(gcCodes) {

      for (gcC in gcCodes) {

        if (gcCodes[gcC].id)
          record.load({
            type: "giftcertificate",
            id: gcCodes[gcC].id
          }).save()

      }

    }

    function _pullShipMethod(shipMethodVal) {

      if (!shipMethodVal) return;
      if (shipMethodVal.indexOf('[') < 0) return shipMethodVal;
      var shipArr = JSON.parse(shipMethodVal)

      if (!shipArr.length) return;

      return shipArr[0].name
    }

    function _getItems(stagingRec) {

      var itemSkus = [], filters = [], itemIds = {}, itemTypes = {}, itemById = {};

      var stagingLines = stagingRec.getLineCount({
        sublistId: "recmachcustrecord_av_sw_order"
      })

      for (var i = 0; i < stagingLines; i++) {

        try {

          var itemSku = stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_item", i)

          if (itemSku) {
            itemSkus.push(itemSku)
          }

        } catch (ex) {
          log.error({ title: '_getItems: ex', details: ex });
          stagingRec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
          avLog.error_message({
            msg: CONSTANTS.SHOP_ERRORS.EX,
            staging: stagingRec.id,
            iface: self.iface,
            logid: self.logId,
            var1: ex.message ? ex.message : JSON.stringify(ex)
          });
        }

      }

      if (itemSkus.length) {
        for (var i = 0; i < itemSkus.length; i++) {
          if (i == 0) {
            filters.push(["name", "is", itemSkus[i]])
          } else {
            filters.push("OR")
            filters.push(["name", "is", itemSkus[i]])
          }
        }

        var itemSearchObj = _getAllResults(search.create({
          type: "item",
          filters: filters,
          columns:
            ["itemid", "parent", "type"]
        }));


        itemSearchObj.every(function (result) {

          if (result.getValue("parent")) {

            itemIds[result.getValue("itemid").split(" ")[2]] = result.id;
            itemTypes[result.getValue("itemid").split(" ")[2]] = result.getValue("type");
            itemById[result.id] = result.getValue("itemid").split(" ")[2];

          } else {
            itemIds[result.getValue("itemid")] = result.id;
            itemTypes[result.getValue("itemid")] = result.getValue("type");
            itemById[result.id] = result.getValue("itemid");
          }

          return true;
        });
      }


      return {
        itemIds: itemIds,
        itemTypes: itemTypes,
        itemById: itemById
      }
    }

    function _getSalesLocations() {
      var locations = {};
      var locationSearch = search.create({
        type: 'customrecord_msg_sales_location',
        filters: [['isinactive', 'is', false]],
        columns: [
          search.createColumn({ name: 'name' }),
          search.createColumn({ name: 'custrecord_msg_sl_location' }),
          search.createColumn({ name: 'custrecord_msg_sl_default_location' })
        ]
      });

      locationSearch.run().each(function (result) {
        var location = result.getValue({ name: 'custrecord_msg_sl_location' });
        location = location.indexOf(',') > -1 ? location.split(',') : [location];
        location.forEach(loc => {
          locations[loc] = result.getValue({ name: 'custrecord_msg_sl_default_location' })
        });
        return true;
      });
      return locations;
    }

    function _getLocationsByItem(items, salesLocations) {
      var itemsStock = {};
      if (!Object.keys(items).length) return itemsStock;
      var itemSearchObj = _getAllResults(search.create({
        type: "item",
        filters:
          [
            ["isinactive", "is", "F"],
            "AND",
            ["inventorylocation", "anyof", Object.keys(salesLocations)],
            "AND",
            ["internalid", "anyof", Object.entries(items)[0]]
          ],
        columns:
          ["itemid", "inventoryLocation", "locationquantityavailable"]
      }));

      itemSearchObj.every(function (result) {
        if (itemsStock[result.id]) {
          itemsStock[result.id].push({
            locId: result.getValue("inventoryLocation"),
            qtyAvailable: Number(result.getValue("locationquantityavailable")) || 0
          });
        } else {
          itemsStock[result.id] = [{
            locId: result.getValue("inventoryLocation"),
            qtyAvailable: Number(result.getValue("locationquantityavailable")) || 0
          }]
        }
        return true;
      });
      return itemsStock;
    }

    function isOrderFulfillable(stagingRec, qtyByLocation, stagingLines, itemIds) {
      var isFulfillable = true;
      for (var i = 0; i < stagingLines; i++) {
        var itemId = itemIds[stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_item", i)];
        var type = stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_type", i);
        var orderQty = parseFloat(stagingRec.getSublistValue("recmachcustrecord_av_sw_order", "custrecord_av_sw_order_line_qty", i));
        if (type == 'product' && itemId) {
          var qtyLocations = qtyByLocation[itemId] || [];
          if (!qtyLocations.length) {
            isFulfillable = false;
          }
          qtyLocations.forEach(function (qtyLocation) {
            var locId = qtyLocation.locId;
            var qtyAvailable = qtyLocation.qtyAvailable;
            if (isFulfillable && locId == '1' && qtyAvailable < orderQty) {
              isFulfillable = false;
            }
            return true;
          })
          if (!isFulfillable) break;
        }
      }
      return isFulfillable;
    }

    function _checkIfOrderIsNew(orderNumber) {

      self.oldSalesOrderId;

      var itemSearchObj = _getAllResults(search.create({
        type: "salesorder",
        filters: [
          ["custbody_av_sw_order_number", "is", orderNumber],
          "AND",
          ["mainline", "is", "T"]
        ],
        columns: [
          search.createColumn({ name: "entity", label: "Customer" })
        ]
      }));

      itemSearchObj.every(function (result) {

        if (result.id)
          self.oldSalesOrderId = result.id
        self.entitiyId = result.getValue('entity')

        return true;
      });

      if (self.oldSalesOrderId) {
        return false
      } else {
        return true
      }

    }

    function _findEntity(rec, currenciesIds) {
      try {
        var address = JSON.parse(rec.getValue("custrecord_av_sw_order_ship_address"))
        var subsidiary = _getSubsidiary(rec.getValue("custrecord_av_sw_channel"));
        var searchResult = search.create({
          type: record.Type.CUSTOMER,
          filters: [
            ['email', search.Operator.IS, rec.getValue("custrecord_av_sw_order_cx_email")],
            'AND',
            ['subsidiary', search.Operator.ANYOF, subsidiary]
          ],
          columns: []
        }).run().getRange({
          start: 0,
          end: 1
        });
        if (searchResult.length && address) {
          var entityIid = searchResult[0].id;
          var orderShipCode = address.zipcode;
          var orderStreet = address.street;
          var isAddressInSys = false;

          search.create({
            type: record.Type.CUSTOMER,
            filters: [
              ['email', search.Operator.IS, rec.getValue("custrecord_av_sw_order_cx_email")]
            ],
            columns: [
              search.createColumn({
                name: "zipcode",
                join: "Address",
                label: "Zip Code"
              }),
              search.createColumn({
                name: "address1",
                join: "Address",
                label: "Address 1"
              })]
          }).run().each(function (result) {
            var customerZipCode = result.getValue({
              name: "zipcode",
              join: "Address"
            })

            var customerStreet = result.getValue({
              name: "address1",
              join: "Address"
            })

            if ((customerZipCode == orderShipCode) && (customerStreet == orderStreet)) {
              isAddressInSys = true;
            }

            return true
          });

          if (!isAddressInSys) {

            var customerRec = record.load({
              type: record.Type.CUSTOMER,
              id: entityIid,
              isDynamic: true
            })

            if (address.country && address.country.iso) {
              customerRec = _setAddress(customerRec, rec, address)
            }

            customerRec.save({
              enableSourcing: false,
              ignoreMandatoryFields: true
            })

          }

        }

        return searchResult.length === 0 ? _createCustomer(rec, currenciesIds, subsidiary) : searchResult[0].id;

      } catch (e) {

        avLog.error_message({
          logid: self.logId,
          msg: CONSTANTS.SHOP_ERRORS.EX,
          var1: JSON.stringify(e)
        })

        return;

      }
    }

    function _createCustomer(rec, currenciesIds, subsidiary) {
      var recId;
      try {

        var customerRec = record.create({
          type: record.Type.CUSTOMER,
          isDynamic: true
        })

        _setValue(customerRec, "isperson", "T")
        _setValue(customerRec, "firstname", rec.getValue("custrecord_av_sw_order_cx_fn"))
        _setValue(customerRec, "lastname", rec.getValue("custrecord_av_sw_order_cx_ln"))
        _setValue(customerRec, "companyname", rec.getValue("custrecord_av_sw_order_cx_cn"))
        _setValue(customerRec, "email", rec.getValue("custrecord_av_sw_order_cx_email"))
        _setValue(customerRec, "subsidiary", subsidiary)

        var shipAddress = JSON.parse(rec.getValue("custrecord_av_sw_order_ship_address"));
        var billAddress = JSON.parse(rec.getValue("custrecord_av_sw_order_bill_address"));

        if (shipAddress.zipcode !== billAddress.zipcode || shipAddress.street !== billAddress.street) {
          customerRec = _setAddress(customerRec, rec, shipAddress, 'custrecord_av_sw_order_ship_address')
          customerRec = _setAddress(customerRec, rec, billAddress, 'custrecord_av_sw_order_bill_address')
        } else {
          if (shipAddress.country && shipAddress.country.iso) {
            customerRec = _setAddress(customerRec, rec, shipAddress)
          }
        }

        customerRec = _setCurrencies(currenciesIds, customerRec)

        recId = customerRec.save({
          enableSourcing: true,
          ignoreMandatoryFields: true
        })

        return recId

      } catch (ex) {
        log.error({ title: '_createCustomer: ex', details: ex });
        rec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
        avLog.error_message({
          msg: CONSTANTS.SHOP_ERRORS.EX,
          staging: rec.id,
          iface: self.iface,
          logid: self.logId,
          var1: ex.message ? ex.message : JSON.stringify(ex)
        });

        return;

      }
    }

    function _getSubsidiary(name) {
      if (!name) return;
      var subsidiary = '';
      var subsidiarySearch = search.create({
        type: 'customrecord_cseg_msg_sales_chan',
        filters: [
          ['name', search.Operator.IS, name]
        ],
        columns: [
          search.createColumn({ name: 'custrecord_msg_sc_subsidiary', label: 'Subsidiary' })
        ]
      }).run().getRange({
        start: 0,
        end: 1
      });

      if (subsidiarySearch && subsidiarySearch.length) {
        subsidiary = subsidiarySearch[0].getValue('custrecord_msg_sc_subsidiary');
      }

      return subsidiary;
    }

    function _setCurrencies(currenciesIds, customerRec) {

      for (let i = 0; i < currenciesIds.length; i++) {

        customerRec.selectNewLine({
          sublistId: "currency"
        })

        customerRec.setCurrentSublistValue({
          sublistId: "currency",
          fieldId: "currency",
          value: currenciesIds[i]
        })

        customerRec.commitLine({
          sublistId: "currency",
          ignoreRecalc: false
        })

      }

      return customerRec
    }

    function _setAddress(customerRec, rec, address, fieldId) {
      try {
        customerRec.selectNewLine({
          sublistId: 'addressbook'
        });

        if (fieldId == 'custrecord_av_sw_order_bill_address') {
          customerRec.setCurrentSublistValue({
            sublistId: 'addressbook',
            fieldId: 'defaultbilling',
            value: true
          });
        } else if (fieldId == 'custrecord_av_sw_order_ship_address') {
          customerRec.setCurrentSublistValue({
            sublistId: 'addressbook',
            fieldId: 'defaultshipping',
            value: true
          });
        } else {
          customerRec.setCurrentSublistValue({
            sublistId: 'addressbook',
            fieldId: 'defaultbilling',
            value: true
          });

          customerRec.setCurrentSublistValue({
            sublistId: 'addressbook',
            fieldId: 'defaultshipping',
            value: true
          });
        }

        var addressSubrecord = customerRec.getCurrentSublistSubrecord({
          sublistId: 'addressbook',
          fieldId: 'addressbookaddress'
        });

        addressSubrecord.setValue({
          fieldId: 'addressee',
          value: address.firstName + ' ' + address.lastName,
        });

        addressSubrecord.setValue({
          fieldId: 'country',
          value: address.country.iso,
        });

        addressSubrecord.setValue({
          fieldId: 'zip',
          value: address.zipcode,
        });

        addressSubrecord.setValue({
          fieldId: 'addr1',
          value: address.street,
        });

        addressSubrecord.setValue({
          fieldId: 'addr2',
          value: address.additionalAddressLine2,
        });

        addressSubrecord.setValue({
          fieldId: 'city',
          value: address.city,
        });

        customerRec.commitLine({
          sublistId: 'addressbook'
        });

        return customerRec;

      } catch (ex) {

        log.error({ title: '_setAddress: ex', details: ex });
        rec.setValue("custrecord_av_sw_order_state", CONSTANTS.INTERFACE_STATE.ERROR)
        avLog.error_message({
          msg: CONSTANTS.SHOP_ERRORS.EX,
          staging: rec.id,
          iface: self.iface,
          logid: self.logId,
          var1: ex.message ? ex.message : JSON.stringify(ex)
        });

      }

      return customerRec
    }

    function _getAllResults(s) {
      var results = s.run();
      var searchResults = [];
      var searchid = 0;
      do {
        var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
        resultslice.forEach(function (slice) {
          searchResults.push(slice);
          searchid++;
        }
        );
      } while (resultslice.length >= 1000);
      return searchResults;
    }

    function _setValue(rec, fld, val) {

      if (fld && val) {
        rec.setValue({
          fieldId: fld,
          value: val
        })
      }

      return rec
    }

    function _getShipCost(rec) {

      var sCost = rec.getValue("custrecord_av_sw_order_ship_amount");
      var sTax = rec.getValue("custrecord_av_sw_order_taxrate");

      if (!sCost || !sTax) return 0;

      return (sCost / (1 + (sTax / 100)))

    }

    function _lookUpCurrencybyIso(isoCurr) {
      let orderCurrencyId, currenciesIds = [];

      if (!isoCurr) return orderCurrencyId;

      _getAllResults(search.create({
        type: "currency",
        filters:
          [],
        columns: ["name", "symbol"]
      })).every(function (result) {
        if (result.getValue("symbol") == isoCurr) {
          orderCurrencyId = result.getValue("name")
        }
        currenciesIds.push(result.id)
        return true;
      });

      return {
        orderCurrencyId: orderCurrencyId,
        currenciesIds: currenciesIds
      }
    }

    function updateVatId(entitiyId, vatId) {
      var vatSearch = search.lookupFields({
        type: record.Type.CUSTOMER,
        id: entitiyId,
        columns: 'vatregnumber'
      }).vatregnumber;

      if (!vatSearch) {
        record.submitFields({
          type: record.Type.CUSTOMER,
          id: entitiyId,
          values: {
            'vatregnumber': vatId
          }
        })
      }
    }

    return {
      onAction: onAction
    };
  })