/**
 * Copyright (c) 2019 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @version: 2.x
 * @author: [Tuba Mohsin]
 * @dated: [07.02.2023]
 * @Description: Send updated order status to Shopware
 *
 *@NApiVersion 2.1
 *@NScriptType WorkflowActionScript
 */

define(['N/runtime', 'N/record', './libraries/av30_shop_order_tracking_lib.js',],
  function (runtime, record, orderUpdateLib) {

    const onAction = (context) => {
      log.audit('onAction', 'trigerred')
      try {
        let scriptObj = runtime.getCurrentScript();
        const iface = scriptObj.getParameter({ name: "custscript_av30_shop_status_iface" });
        const soId = [context.newRecord.id];
        const shopId = context.newRecord.getValue('custbody_av_sw_order_id');
        if(!shopId) return;
        log.debug('Processing request for ==>', `iface: ${iface}, soID: ${soId}`);

        const statusRef = context.newRecord.getValue('statusRef');
        const isShopOrderClosed = context.newRecord.getValue('custbody_av_sw_order_closed');

        if(statusRef == 'closed') {
          record.submitFields({
            id: context.newRecord.id,
            type: context.newRecord.type,
            values: {
                'custbody_av_sw_order_closed': true,
            }
          })
        } else if(statusRef != 'closed' && isShopOrderClosed) {
          record.submitFields({
            id: context.newRecord.id,
            type: context.newRecord.type,
            values: {
                'custbody_av_sw_order_closed': false,
            }
          })
        }
  
        orderUpdateLib.handleStatusRequest(soId, iface);
      } catch (ex) {
        log.error({ title: "onAction: ex", details: ex });
      }
    };

    return {
      onAction: onAction,
    };
  })