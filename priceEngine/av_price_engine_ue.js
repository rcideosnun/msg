/**
 *@NApiVersion 2.1
 *@NScriptType UserEventScript
 */
define([], function() {

    function beforeLoad(context) {
        context.clientScriptFileId = './av_price_engine_cs.js';
        context.form.getSublist('item').addButton({
            id : 'custpage_applyrebate',
            label : 'Rabatt anwenden',
            functionName:  'applyRebate()'
           
        });

    }

    return {

        beforeLoad: beforeLoad
     
    };
});
