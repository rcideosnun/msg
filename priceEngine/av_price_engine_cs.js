/**
 *@NApiVersion 2.1
 *@NScriptType ClientScript
 */
define(['N/search','N/currentRecord','N/format','N/runtime'], function(search, currentRecord, format, runtime) {
    const discountItemID = 49100;

    function pageInit() {
        window.applyRebate = applyRebate;
    }
    function applyRebate(){
        try {
            debugger;
            let cr = currentRecord.get();
            let vendorId = cr.getValue('entity');
            let itemCount = cr.getLineCount('item');
            let itemsIds = getItemUniqueIds(cr, itemCount);
            let vendorRebate = getVendorRebate(vendorId);
            let vendorPriceList = getVendorPriceList(vendorId, itemsIds);
            cr.setValue('custbody_av_enable_price_engine', true);

            for (let i = 0; i < itemCount; i++){
                let rebate1, rebate2, rebate3, rate = 1;
                let itemId = cr.getSublistValue({sublistId: 'item',fieldId: 'item',line: i});
                let itemCategory = getItemCategory(itemId);
                let purchesePrice = vendorPriceList[itemId] ? vendorPriceList[itemId].ek : false
                if (purchesePrice && Object.keys(vendorPriceList).length > 0)
                {
                    cr.selectLine({sublistId: 'item',line: i});
                    let itemCategoryRebate = itemCategory ? getItemCategoryRebate(vendorId, itemCategory) : null;
                    let vendorPriceRebate1 =  vendorPriceList[itemId].rebate1 || false;
                    let vendorPriceRebate2 = vendorPriceList[itemId].rebate2 || false;
                    // assign rebate 1
                    rebate1 = vendorRebate || itemCategoryRebate || vendorPriceRebate1 || vendorPriceRebate2;
                    // assign rebate 2
                    if (rebate1) {
                        if (vendorRebate && itemCategoryRebate) rebate2 = itemCategoryRebate;
                        else if (vendorRebate || itemCategoryRebate) rebate2 = vendorPriceRebate1 || vendorPriceRebate2;
                        else if ((!vendorRebate && !itemCategoryRebate) && vendorPriceRebate1) rebate2 = vendorPriceRebate2;
                    }
                    //assign rebate 3
                    if (rebate2) {
                        if (vendorRebate && itemCategoryRebate) rebate3 = vendorPriceRebate1 || vendorPriceRebate2;
                        else if (vendorRebate && vendorPriceRebate1 && !itemCategoryRebate) rebate3 = vendorPriceRebate2;
                        else if (!vendorRebate && itemCategoryRebate && vendorPriceRebate1 ) rebate3 = vendorPriceRebate2;
                    
                    }

                    if (rebate1) {
                        cr.setCurrentSublistValue('item','custcol_av_rebatt1',rebate1);
                        rate *= Math.abs((100- rebate1 )/ 100);
                    }
                    if (rebate2) {
                        cr.setCurrentSublistValue('item','custcol_av_rebatt2',rebate2);
                        rate *= Math.abs((100- rebate2) / 100);
                    }
                    if (rebate3) {
                        cr.setCurrentSublistValue('item','custcol_av_rebatt3',rebate3);
                        rate *= Math.abs((100- rebate3) / 100);
                    }
                    if (purchesePrice) {
                        rate = parseFloat(purchesePrice) * (rate);
                        cr.setCurrentSublistValue('item','rate', rate);
                        cr.setCurrentSublistValue('item','custcol_av_purchase_price', parseFloat(purchesePrice));
                    }
                    cr.commitLine({sublistId: 'item'});
                }
            }

            let subtotal = cr.findSublistLineWithValue({sublistId: 'item',fieldId: 'item',value: -2});
            if (subtotal > 0) {
                cr.removeLine({sublistId: 'item',line: subtotal,ignoreRecalc: true});
            }
            let discount = cr.findSublistLineWithValue({sublistId: 'item',fieldId: 'item',value: discountItemID});
            if (discount > 0) {
                cr.removeLine({sublistId: 'item',line: discount, ignoreRecalc: true});
            }

            console.log('finish:' + runtime.getCurrentScript().getRemainingUsage());
        } catch (error) {
            alert('Apply rebate failed: ' + error.message );
        }
    }
    function getItemUniqueIds(cr,itemCount){
        let itemIds = [];
        for (let i = 0; i < itemCount; i++){
            itemIds.push(cr.getSublistValue({sublistId: 'item',fieldId: 'item',line: i}));
        }
        return itemIds;
    }
    function getVendorRebate(vendorId){
        var vendorRebate = search.lookupFields({type: search.Type.VENDOR,id: vendorId,columns: ['custentity_av_rebate_vendor']}).custentity_av_rebate_vendor;
        let value = parseFloat(vendorRebate);
        if (isNaN(value)){
            return false;
        }
        else return value;
    }
    function getItemCategoryRebate(vendorId,itemCat){
        let itemCatRebate;
        var customrecord_av_item_category_rabattSearchObj = search.create({
            type: 'customrecord_av_item_category_rabatt',
            filters:
            [
                ['custrecord_av_vendor_parent','anyof',vendorId], 
                'AND', 
                ['custrecord_av_item_category','anyof',itemCat]
            ],
            columns:
            [
                search.createColumn({
                    name: 'scriptid',
                    sort: search.Sort.ASC
                }),
                'custrecord_av_item_category_rebatt',
            ]
        });
        var searchResultCount = customrecord_av_item_category_rabattSearchObj.runPaged().count;
        // TODO: what if more than 1 resault ?         
        customrecord_av_item_category_rabattSearchObj.run().each(function(result){
            itemCatRebate = result.getValue('custrecord_av_item_category_rebatt');
            return false;
        });

        if (itemCatRebate) return parseFloat(itemCatRebate);
        else return false;
        
    }
    function getVendorPriceList(vendorId, itemList){ 
        let itemListObj = {};
        let customrecord_av_vendor_price_listSearchObj = search.create({
            type: 'customrecord_av_vendor_price_list',
            filters:
            [
               [
                ["custrecord_av_item.isinactive","is","F"],
                "AND",
                ["custrecord_av_item.internalid","anyof",itemList],
                "AND",
                ["custrecord_av_vendor.internalid","anyof",vendorId],
                "AND",
                ["custrecord_av_valid_from","onorbefore","today"]
               ], 
               "AND", 
               [
                [
                    ["custrecord_av_valid_to","onorafter","today"]
                ],
                "OR",
                [
                    ["custrecord_av_valid_to","isempty",""]
                ]
                ]
            ],
            columns:
            [
                "custrecord_av_item",
                search.createColumn({
                   name: "custrecord_av_valid_from",
                   sort: search.Sort.DESC
                }),
                "custrecord_av_valid_to",
                "custrecord_av_rabatt1",
                "custrecord_av_rabatt2",
                "custrecord_av_ek",
                search.createColumn({
                   name: "custentity_av_rebate_vendor",
                   join: "CUSTRECORD_AV_VENDOR"
                })
             ]
        });
        var itemListSearch = getAllResults(customrecord_av_vendor_price_listSearchObj);
        itemListSearch.forEach(function(result) {
            let item = result.getValue('custrecord_av_item');
            if(!itemListObj[item]){
                itemListObj[item] = {
                    item : result.getValue('custrecord_av_item'),
                    ek : parseFloat(result.getValue('custrecord_av_ek')) || 0,
                    rebate1 : parseFloat(result.getValue('custrecord_av_rabatt1')) || 0,
                    rebate2 : parseFloat(result.getValue('custrecord_av_rabatt2')) || 0,
                }

            }
            return true;
        });
        return itemListObj;
    }
    function getItemCategory(itemId){ 
        if (itemId){
            let itemCategory = search.lookupFields({type: 'inventoryitem',id: itemId,columns: ['cseg_msg_igt']}).cseg_msg_igt; 
            if (itemCategory && itemCategory.length > 0) {
                return itemCategory[0].value;    
            }
            else return false;
        }
        else return false;
    }
    function getAllResults(s) {
        var results = s.run();
        var searchResults = [];
        var searchid = 0;
        do {
            var resultslice = results.getRange({start:searchid,end:searchid+1000});
            resultslice.forEach(function(slice) {
                searchResults.push(slice);
                searchid++;
            }
            );
        } while (resultslice.length >=1000);
        return searchResults;
    }
    return {
        pageInit: pageInit,
        applyRebate: applyRebate
    };
});
