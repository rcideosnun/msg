/**
 *@NApiVersion 2.1
 *@NScriptType WorkflowActionScript
 */
define(['N/search','N/format'], function(search, format) {
    const discountItemID = 49100;
    function onAction(scriptContext) {
        try {
            let cr = scriptContext.newRecord;
            let priceEngineEnabled = cr.getValue('custbody_av_enable_price_engine');
            let vendorId = cr.getValue('entity');
            let itemCount = cr.getLineCount('item');
            let itemsIds = getItemUniqueIds(cr, itemCount);
            let vendorRebate = getVendorRebate(vendorId);
            let vendorPriceList = getVendorPriceList(vendorId, itemsIds);
            if (priceEngineEnabled){
                for (let i = 0; i < itemCount; i++){
                    let rebate1, rebate2, rebate3, rate = 1;
                    let itemId = scriptContext.newRecord.getSublistValue({sublistId: 'item',fieldId: 'item',line: i});
                    let itemCategory = getItemCategory(itemId);
                    let purchesePrice = vendorPriceList[itemId] ? vendorPriceList[itemId].ek : false
    
                    if (purchesePrice && Object.keys(vendorPriceList).length > 0)
                    {
                        scriptContext.newRecord.selectLine({sublistId: 'item',line: i});
                        let itemCategoryRebate = itemCategory ? getItemCategoryRebate(vendorId, itemCategory) : null;
                        let vendorPriceRebate1 = vendorPriceList[itemId].rebate1 || false;
                        let vendorPriceRebate2 = vendorPriceList[itemId].rebate2 || false;
                        // assign rebate 1
                        rebate1 = vendorRebate || itemCategoryRebate || vendorPriceRebate1 || vendorPriceRebate2;
                        // assign rebate 2
                        if (rebate1) {
                            if (vendorRebate && itemCategoryRebate) rebate2 = itemCategoryRebate;
                            else if (vendorRebate || itemCategoryRebate) rebate2 = vendorPriceRebate1 || vendorPriceRebate2;
                            else if ((!vendorRebate && !itemCategoryRebate) && vendorPriceRebate1) rebate2 = vendorPriceRebate2;
                        }
                        //assign rebate 3
                        if (rebate2) {
                            if (vendorRebate && itemCategoryRebate) rebate3 = vendorPriceRebate1 || vendorPriceRebate2;
                            else if (vendorRebate && vendorPriceRebate1 && !itemCategoryRebate) rebate3 = vendorPriceRebate2;
                            else if (!vendorRebate && itemCategoryRebate && vendorPriceRebate1 ) rebate3 = vendorPriceRebate2;
                        
                        }
                        if (rebate1) {
                            scriptContext.newRecord.setCurrentSublistValue('item','custcol_av_rebatt1',rebate1);
      
                            rate *= Math.abs((100- rebate1 )/ 100);
                        }
                        if (rebate2) {
                            scriptContext.newRecord.setCurrentSublistValue('item','custcol_av_rebatt2',rebate2);
                            rate *= Math.abs((100- rebate2) / 100);
                        }
                        if (rebate3) {
                            scriptContext.newRecord.setCurrentSublistValue('item','custcol_av_rebatt3',rebate3);
                            rate *= Math.abs((100- rebate3) / 100);
                        }
                        if (purchesePrice) {
                            rate = parseFloat(purchesePrice) * (rate);
                            scriptContext.newRecord.setCurrentSublistValue('item','rate', rate);
                            scriptContext.newRecord.setCurrentSublistValue('item','custcol_av_purchase_price', parseFloat(purchesePrice));
                        }
                        scriptContext.newRecord.commitLine({sublistId: 'item'});              
                    }
                }
            }
            let subtotalLineId = scriptContext.newRecord.findSublistLineWithValue({sublistId: 'item',fieldId: 'item',value: -2});
            if (subtotalLineId > 0){
                scriptContext.newRecord.removeLine({sublistId: 'item',line: subtotalLineId,ignoreRecalc: true});
            }

            scriptContext.newRecord.selectNewLine({sublistId: 'item'});
            scriptContext.newRecord.setCurrentSublistValue('item','item',-2);
            scriptContext.newRecord.commitLine({sublistId: 'item'});
         
            let discountLineId = scriptContext.newRecord.findSublistLineWithValue({sublistId: 'item',fieldId: 'item',value: discountItemID});
            if (discountLineId > 0){
                scriptContext.newRecord.removeLine({sublistId: 'item',line: discountLineId,ignoreRecalc: true});
            }
            let invoiceRebate = scriptContext.newRecord.getValue('custbody_av_invoice_rebatt');
            if (invoiceRebate){
                scriptContext.newRecord.selectNewLine({sublistId: 'item'});
                scriptContext.newRecord.setCurrentSublistValue('item','item',discountItemID);
                var discountPercent = format.format({
                    value: invoiceRebate,
                    type: format.Type.PERCENT
                });
                scriptContext.newRecord.setCurrentSublistText('item','rate', '-' + discountPercent);
                scriptContext.newRecord.commitLine({sublistId: 'item'});
            }
            cr.setValue('custbody_av_enable_price_engine', false);
            
        } catch (error) {
            log.debug('price engine failed', error);
        }
    }
    function getPurchesePrice(itemId, vendorPriceList){
        if (itemId && vendorPriceList){
            let purchesePrice = vendorPriceList.find(a => a.item == itemId);
            if (purchesePrice){
                return purchesePrice.ek;
            }
            else {
                return false;
            }
        }
        else return false;
    }
    function getItemUniqueIds(cr,itemCount){
        let itemIds = [];
        for (let i = 0; i < itemCount; i++){
            itemIds.push(cr.getSublistValue({sublistId: 'item',fieldId: 'item',line: i}));
        }
        return itemIds;
    }
    function getItemCategory(itemId){
        if (itemId){
            let itemCategory = search.lookupFields({type: 'inventoryitem',id: itemId,columns: ['cseg_msg_igt']}).cseg_msg_igt; 
            if (itemCategory && itemCategory.length > 0) {
                return itemCategory[0].value;    
            }
            else return false;
        }
        else return false;
    }
    function getVendorPriceList(vendorId, itemList){ 
        let itemListObj = {};
        let customrecord_av_vendor_price_listSearchObj = search.create({
            type: 'customrecord_av_vendor_price_list',
            filters:
            [
               [
                ["custrecord_av_item.isinactive","is","F"],
                "AND",
                ["custrecord_av_item.internalid","anyof",itemList],
                "AND",
                ["custrecord_av_vendor.internalid","anyof",vendorId],
                "AND",
                ["custrecord_av_valid_from","onorbefore","today"]
               ], 
               "AND", 
               [
                [
                    ["custrecord_av_valid_to","onorafter","today"]
                ],
                "OR",
                [
                    ["custrecord_av_valid_to","isempty",""]
                ]
                ]
            ],
            columns:
            [
                "custrecord_av_item",
                search.createColumn({
                   name: "custrecord_av_valid_from",
                   sort: search.Sort.DESC
                }),
                "custrecord_av_valid_to",
                "custrecord_av_rabatt1",
                "custrecord_av_rabatt2",
                "custrecord_av_ek",
                search.createColumn({
                   name: "custentity_av_rebate_vendor",
                   join: "CUSTRECORD_AV_VENDOR"
                })
             ]
        });
        var itemListSearch = getAllResults(customrecord_av_vendor_price_listSearchObj);
        itemListSearch.forEach(function(result) {
            let item = result.getValue('custrecord_av_item');
            if(!itemListObj[item]){
                itemListObj[item] = {
                    item : result.getValue('custrecord_av_item'),
                    ek : parseFloat(result.getValue('custrecord_av_ek')) || 0,
                    rebate1 : parseFloat(result.getValue('custrecord_av_rabatt1')) || 0,
                    rebate2 : parseFloat(result.getValue('custrecord_av_rabatt2')) || 0,
                }

            }
            return true;
        });
        return itemListObj;
    }
    function getVendorRebate(vendorId){
        if (vendorId){
            var vendorRebate = search.lookupFields({type: search.Type.VENDOR,id: vendorId,columns: ['custentity_av_rebate_vendor']}).custentity_av_rebate_vendor;
            let value = parseFloat(vendorRebate);
            if (isNaN(value)){
                return false;
            }
            else return value;
        }
        else return false;
    }
    function getItemCategoryRebate(vendorId,itemCat){
        if (vendorId && itemCat){
            let rabat1;
            // todo: confirm rabate 1/2 add condtional to fetch the correct one.
            var customrecord_av_item_category_rabattSearchObj = search.create({
                type: 'customrecord_av_item_category_rabatt',
                filters:
                [
                    ['custrecord_av_vendor_parent','anyof',vendorId], 
                    'AND', 
                    ['custrecord_av_item_category','anyof',itemCat]
                ],
                columns:
                [
                    search.createColumn({
                        name: 'scriptid',
                        sort: search.Sort.ASC
                    }),
                    'custrecord_av_item_category_rebatt',
                ]
            });
            var searchResultCount = customrecord_av_item_category_rabattSearchObj.runPaged().count;
            // TODO: what if more than 1 resault ?         
            customrecord_av_item_category_rabattSearchObj.run().each(function(result){
                rabat1 = result.getValue('custrecord_av_item_category_rebatt');
                return false;
            });
    
            if (rabat1) return parseFloat(rabat1);
            else return false;
        }
        else return false;
        
        
    }
    function getVendorPriceRebate(itemId, vendorPriceList, rebate){
        if (itemId){
            let purchesePrice = vendorPriceList.find(a => a.item == itemId);
            if (purchesePrice && rebate == 1){
                return purchesePrice.rebate1 ? parseFloat(purchesePrice.rebate1) : false;
            }
            else if (purchesePrice && rebate == 2){
                return purchesePrice.rebate2 ? parseFloat(purchesePrice.rebate2) : false;
            }
            else {
                return false;
            }
        }
        else return false;
    }
    function getAllResults(s) {
        var results = s.run();
        var searchResults = [];
        var searchid = 0;
        do {
            var resultslice = results.getRange({start:searchid,end:searchid+1000});
            resultslice.forEach(function(slice) {
                searchResults.push(slice);
                searchid++;
            }
            );
        } while (resultslice.length >=1000);
        return searchResults;
    }
    return {
        onAction: onAction
    };
});
