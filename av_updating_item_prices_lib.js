/**
 ** Copyright (c) 2023 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 ** @author: Polina Melnykova
 ** @Description: This library for Updating Item Prices from custom record
 * *@NApiVersion 2.1
 * Spec: Updating Item Prices from custom record
 */
define(['N/search', 'N/record'],

    function (search, record) {


        function searchTheNewestPrice(itemId, taxSch) {

            var pricesS = search.create({

                type: "customrecord_msg_item_prices",
                filters:
                    [
                        ["custrecord_msg_ip_item", "anyof", itemId],
                        "AND",
                        ["isinactive", "is", "F"]
                    ],
                columns:
                    [
                        search.createColumn({
                            name: "custrecord_msg_ip_date_from",
                            sort: search.Sort.DESC,
                            label: "Date from"
                        }),
                        search.createColumn({ name: "custrecord_msg_ip_date_to", label: "Date to" }),
                        search.createColumn({ name: "custrecord_msg_ip_base", label: "Base Price" }),
                        search.createColumn({ name: "custrecord_msg_ip_b2b", label: "B2B Price Net" }),
                        search.createColumn({ name: "custrecord_msg_ip_item", label: "Item" })
                    ]
            });
            var pricesBaseArr = [];
            var pricesB2BArr = [];
            pricesS.run().each(function (result) {
                pricesBaseArr.push(result.getValue('custrecord_msg_ip_base'));
                pricesB2BArr.push(result.getValue('custrecord_msg_ip_b2b'));
                return true;
            });
            var prices = [pricesBaseArr[0], pricesB2BArr[0]];

            if (taxSch == 1) {
                prices[0] = (prices[0] / 1.2).toFixed(3);
                prices[1] = (prices[1] / 1.2).toFixed(3);
            };
            if (taxSch == 3) {
                prices[0] = (prices[0] / 1.1).toFixed(3);
                prices[1] = (prices[1] / 1.1).toFixed(3);
            };

            return prices;
        }

        function comparase(prices, base, B2B) {

            if (prices[0] == base && prices[1] == B2B) {
                return false;
            } else { return true; }
        };

        function update(rec) {
            var itemId = rec.id;
            var taxschedule = rec.getValue('taxschedule');
            var countLine = rec.getLineCount('price1');
            for (var i = 0; i < countLine; i++) {
                var priceLvl = rec.getSublistValue({
                    sublistId: 'price1',
                    fieldId: 'pricelevel',
                    line: i
                });
                if (priceLvl == 1) {
                    var lineBase = i;
                    var oldBase = rec.getSublistValue({
                        sublistId: 'price1',
                        fieldId: 'price_1_',
                        line: i
                    });
                };
                if (priceLvl == 6) {
                    var lineB2B = i;
                    var oldB2B = rec.getSublistValue({
                        sublistId: 'price1',
                        fieldId: 'price_1_',
                        line: i
                    });
                };
            }

            var newPrices = searchTheNewestPrice(itemId, taxschedule);

            if (comparase(newPrices, oldBase, oldB2B)) {

                rec.setSublistValue({
                    sublistId: 'price1',
                    fieldId: 'price_1_',
                    line: lineBase,
                    value: newPrices[0]
                });
                rec.setSublistValue({
                    sublistId: 'price1',
                    fieldId: 'price_1_',
                    line: lineB2B,
                    value: newPrices[1]
                });

            }
        }

        return {
            update: update
        }
    }
);