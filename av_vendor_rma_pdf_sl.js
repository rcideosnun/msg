/** 
 * Copyright (c) 2019 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @author: [Polina Melnykova]
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @description : 
 * Spec: RMA PDF for vendor returns
 */


define(['N/render', 'N/record'], function(render, record) 
{

function onRequest(context) {

 var custom_id = context.request.parameters.custscript_record_id;
 var pdfFileName = "returnauthorization";
 var renderer = render.create();
 //var xmlTmplFile = file.load('Templates/PDF Templates/custtmpl_209_7021634_sb1_964.xml');
 //renderer.templateContent = xmlTmplFile.getContents();
 renderer.setTemplateByScriptId("CUSTTMPL_209_7021634_SB1_964");
 var content = renderer.addRecord({
   templateName: 'record',
   record: record.load({
     type: record.Type.RETURN_AUTHORIZATION,
     id: custom_id
   })
 });
 
 context.response.setHeader({
   name: 'content-disposition',
   value: 'inline; filename="' + pdfFileName + '_' + custom_id + '.pdf"'
 });

 var pdf = renderer.renderAsPdf();
 context.response.writeFile(pdf, true);

}
return {
 onRequest: onRequest
}
})