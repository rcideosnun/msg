/**
 ** Copyright (c) 2015 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.0
 ** @author: Roberto Cideos
 ** @dated: 27.07.2022
 ** @Description: This library is used to load configuration object
 * *@NApiVersion 2.1
 */

define(["exports", "N/search", "../pimCoreIntegration/av_int_log.js"],
    function (exports, search, avLog) {

        exports._getConfigObj = function (configId) {
            try {
                if (!configId) return null

                var configFields = ["custrecord_av_integration_base_url",
                    "custrecord_av_integration_username",
                    "custrecord_av_integration_password",
                    "custrecord_av_integration_sftp_guid",
                    "custrecord_av_integration_sftp_hkey",
                    "custrecord_av_integration_stock_patch",
                    "custrecord_av_integration_auth_token",
                    "custrecord_av_integration_auth_req",
                    "custrecord_av_integration_sftp_out"]

                var configObj = search.lookupFields({
                    type: "customrecord_av_integration_config",
                    id: configId,
                    columns: configFields
                })

                return configObj

            } catch (e) {
                avLog.create({
                    receive: "Credentials are invalid"
                })
                throw new Error(JSON.stringify({
                    errorDetails: JSON.stringify(e)
                }))
            }
        }

    })