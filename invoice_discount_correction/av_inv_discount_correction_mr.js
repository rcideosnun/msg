/**
 ** Copyright (c) 2022 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 *
 * Author: Tuba Mohsin
 * Date: 24.08.2024
 * transfer body level discount to line level
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */

define([
    "N/search",
    "N/runtime",
    './av_inv_discount_correction_lib'
], function (search, runtime, lib) {
    const RECORD_TYPE = {
        'CustInvc': 'invoice',
    }

    const getInputData = () => {
        log.audit("getInputData", `-Script Started on ${new Date()}-`);
        try {
            return getInvoices()
        } catch (ex) {
            log.error({ title: "getInputData : ex", details: ex });
        }
    };

    const map = (context) => {
        log.audit({ title: "map", details: 'triggered' });
        const data = JSON.parse(context.value);
        log.audit('map data', data);
        try {
            lib.handleDiscountCorrectionRequest(data);
        } catch (ex) {
            log.audit('error', ex);
        }
    };


    const summarize = () => {
        log.audit("summarize", `-Script Finished on ${new Date()}-`);
    };

    const getInvoices = () => {
        let invoiceList = [];
        const currentRuntime = runtime.getCurrentScript();

        let tranSearch = search.load({
            id: currentRuntime.getParameter({ name: "custscript_av_invoice_search" }),
        });

        let myPagedData = tranSearch.runPaged({ pageSize: 1000 });

        myPagedData.pageRanges.forEach(function (pageRange) {
            let myPage = myPagedData.fetch({ index: pageRange.index });
            myPage.data.forEach(function (result) {
                let internalId = result.id || result.getValue('internalid') || result.getValue({ name: "internalid", summary: "GROUP" });
                let recordType = result.recordType || RECORD_TYPE[result.getValue('type')] || RECORD_TYPE[result.getValue({ name: "type", summary: "GROUP" })]

                invoiceList.push({ id: internalId, type: recordType })
            });
        });

        let resultGroups = [];
        const size = 10;

        for (let i = 0; i < invoiceList.length; i += size) {
            resultGroups.push(invoiceList.slice(i, i + size));
        }
        log.audit('resultGroups', resultGroups);
        return resultGroups;
    };

    return {
        getInputData: getInputData,
        map: map,
        summarize: summarize,
    };
});
