/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.1
 ** @author: [Tuba Mohsin]
 ** @Description: transfer body level discount to line level
 *
 *  @NApiVersion 2.1
 *  @NScriptType WorkflowActionScript
 */
define(['../pimCoreIntegration/av_int_log.js', './av_inv_discount_correction_lib'],
    function (avLog, lib) {

        const onAction = (context) => {
            const newRecord = context.newRecord;
            try {
                log.audit('newRecord', newRecord)
                const discountItem = newRecord.getValue('discountitem');
                if (!discountItem) return;
                const data = { id: newRecord.id, type: newRecord.type };
                lib.handleDiscountCorrectionRequest([data]);
            } catch (ex) {
                log.debug("Error", ex);
                avLog.error_message({
                    transaction: newRecord.id,
                    var1: `Error occurred while correcting TO: InternalID ${newRecord.id}.`,
                    var2: JSON.stringify(ex)
                });
            }
        };

        return {
            onAction: onAction,
        };
    });
