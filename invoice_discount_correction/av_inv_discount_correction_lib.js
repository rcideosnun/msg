/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 ** @version: 2.x
 ** @author: [Tuba Mohsin]
 ** @Description: 
 */
define(['N/record', 'N/search', 'N/task', 'N/format', '../pimCoreIntegration/av_int_log.js'], function (record, search, task, format, avLog) {
    /**
     *
     * @param {Object} context
     * @returns
     */
    const STATUS_MAP = {
        'picked': 'A',
        'packed': 'B',
        'shipped': 'C'
    }

    const handleDiscountCorrectionRequest = (invoiceList) => {
        invoiceList.forEach(invoice => {
            moveDiscountLineLevel(invoice)
        });
    }

    const moveDiscountLineLevel = (invoice) => {
        try {
            const nsRecordObj = record.load({
                type: invoice.type,
                id: invoice.id,
                isDynamic: true
            });

            const discountItem = nsRecordObj.getValue('discountitem');
            const discountRate = nsRecordObj.getText('discountrate');
            const discountValue = nsRecordObj.getValue('discountrate');
            const discounttotal = nsRecordObj.getValue('discounttotal');
            log.audit('discountRate', discountRate);
            log.audit('discountValue', discountValue);
            let isPercent = false;
            if (discountRate.indexOf('%') != -1) {
                isPercent = true;
            }
            if (!discountItem) return;

            const itemCount = nsRecordObj.getLineCount({ sublistId: 'item' });
            if (itemCount == 0) return;

            // nsRecordObj.selectLine({
            //     sublistId: 'item',
            //     line: 0
            // });

            // const taxCode = nsRecordObj.getCurrentSublistValue({
            //     sublistId: 'item',
            //     fieldId: 'taxcode',
            //     line: 0
            // });

            let cartDiscountItem = nsRecordObj.findSublistLineWithValue({ sublistId: 'item', fieldId: 'item_display', value: 90008 });
            let discountDescription = null;
            if (cartDiscountItem > 0) {

                nsRecordObj.selectLine({
                    sublistId: 'item',
                    line: cartDiscountItem
                });

                discountDescription = nsRecordObj.getCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'description',
                    line: cartDiscountItem
                });

                nsRecordObj.removeLine({ sublistId: 'item', line: cartDiscountItem, ignoreRecalc: true });
            }

            // if (itemCount > 1) {
            log.audit('adding subtotal')
            nsRecordObj.selectNewLine({
                sublistId: 'item'
            });

            nsRecordObj.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'item',
                value: '-2'
            });

            nsRecordObj.commitLine('item');
            // }

            nsRecordObj.selectNewLine({
                sublistId: 'item'
            });
            nsRecordObj.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'item',
                value: discountItem
            });
            nsRecordObj.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'price',
                value: -1
            });

            if (discountDescription) {
                nsRecordObj.setCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'description',
                    value: discountDescription
                });
            }

            // nsRecordObj.setCurrentSublistValue({
            //     sublistId: 'item',
            //     fieldId: 'taxcode',
            //     value: taxCode
            // });
            log.audit('discountRate', discountRate);
            if (isPercent) {
                nsRecordObj.setCurrentSublistText({
                    sublistId: 'item',
                    fieldId: 'rate',
                    text: discountRate,
                    forceSyncSourcing: true
                });
            } else {
                nsRecordObj.setCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'amount',
                    value: discountValue
                });
            }
            nsRecordObj.commitLine("item");

            nsRecordObj.setValue('discountitem', '');
            nsRecordObj.save();
        } catch (ex) {
            log.error('ex', ex)
        }
    }



    return {
        handleDiscountCorrectionRequest,
    };
});
