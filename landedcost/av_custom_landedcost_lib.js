/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tehseen Ahmed
* Date: 11/8/2022
* Adds landed cost on the item receipts
*/

define(['N/log', 'N/record', 'N/search'], function (log, record, search) {

  function getItemReceipts(purchaseOrder) {
    var itemReceipts = [];
    var itemReceiptSearch = search.create({
      type: search.Type.ITEM_RECEIPT,
      filters:
        [
          ['createdfrom', 'anyof', purchaseOrder],
          'AND',
          ['mainline', 'is', 'T']
        ],
      columns: []
    });
    itemReceiptSearch.run().each(function (result) {
      itemReceipts.push(result.id);
      return true;
    });

    return itemReceipts;
  }

  function getLandedCostRecords(purchaseOrder) {
    var landedCostRecords = [];
    var landedCostSearch = search.create({
      type: 'customrecord_av_lc_main',
      filters: [['custrecord_av_lc_parent_po', 'anyof', purchaseOrder]],
      columns: ['custrecord_av_lc_category', 'custrecord_av_lc_alloc_method', 'custrecord_av_lc_amt']
    });
    landedCostSearch.run().each(function (result) {
      landedCostRecords.push({
        category: result.getValue('custrecord_av_lc_category'),
        method: result.getValue('custrecord_av_lc_alloc_method'),
        amount: parseFloat(result.getValue('custrecord_av_lc_amt'))
      });
      return true;
    });
    return landedCostRecords;
  }

  function getPurchaseOrderTotals(purchaseOrderId) {
    var totals = {
      totalWeight: 0,
      totalValue: 0,
      totalQuantity: 0
    };
    var purchaseOrder = record.load({
      type: record.Type.PURCHASE_ORDER,
      id: purchaseOrderId,
      isDynamic: true
    })
    var itemLineCount = purchaseOrder.getLineCount({
      sublistId: 'item'
    });
    for (var i = 0; i < itemLineCount; i++) {

      var item = purchaseOrder.getSublistValue({
        sublistId: 'item',
        fieldId: 'item',
        line: i
      })

      if (item !== '-2' && item !== '52418') {
        var quantity = purchaseOrder.getSublistValue({
          sublistId: 'item',
          fieldId: 'quantity',
          line: i
        })

        var weight = purchaseOrder.getSublistValue({
          sublistId: 'item',
          fieldId: 'custcol_av_item_weight',
          line: i
        })

        var rate = purchaseOrder.getSublistValue({
          sublistId: 'item',
          fieldId: 'rate',
          line: i
        })

        totals.totalWeight += (parseFloat(quantity) * parseFloat(weight));
        totals.totalValue += (parseFloat(quantity) * parseFloat(rate));
        totals.totalQuantity += parseFloat(quantity);
      }
    }

    return totals;
  }

  function updateItemReceipt(itemReceiptId, landedCosts, orderTotals) {
    var totalLandedCost = [];
    var itemReceiptRecord = record.load({
      type: record.Type.ITEM_RECEIPT,
      id: itemReceiptId,
      isDynamic: true
    })

    var itemLineCount = itemReceiptRecord.getLineCount({
      sublistId: 'item'
    });

    for (var i = 0; i < itemLineCount; i++) {
      var itemLandedCost = getItemLandedCost(itemReceiptRecord, landedCosts, orderTotals, i);
      itemReceiptRecord.selectLine({
        sublistId: 'item',
        line: i
      });

      var landedCostSubRec = itemReceiptRecord.getCurrentSublistSubrecord({
        sublistId: 'item',
        fieldId: 'landedcost'
      });

      var landedCostCount = landedCostSubRec.getLineCount({
        sublistId: 'landedcostdata'
      });

      for (var j = landedCostCount - 1; j >= 0; j--) {
        landedCostSubRec.removeLine({
          sublistId: 'landedcostdata',
          line: j
        });
      }

      itemLandedCost.forEach(function (landedCost) {
        if (landedCost.itemLandedCost > 0) {

          landedCostSubRec.selectNewLine({
            sublistId: 'landedcostdata'
          });

          landedCostSubRec.setCurrentSublistValue({
            sublistId: 'landedcostdata',
            fieldId: 'amount',
            value: landedCost.itemLandedCost
          });

          landedCostSubRec.setCurrentSublistValue({
            sublistId: 'landedcostdata',
            fieldId: 'costcategory',
            value: landedCost.category
          });

          landedCostSubRec.commitLine({
            sublistId: 'landedcostdata'
          })
        }
      });

      totalLandedCost.push({
        item: itemReceiptRecord.getSublistValue({
          sublistId: 'item',
          fieldId: 'item',
          line: i
        }),
        landedcost: landedCostSubRec.getValue({
          fieldId: 'total'
        })
      })

      itemReceiptRecord.commitLine({
        sublistId: 'item'
      })
    }

    itemReceiptRecord.setValue({
      fieldId: 'custbody_av_landedcost_added',
      value: true
    })

    itemReceiptRecord.save({
      enableSourcing: true,
      ignoreMandatoryFields: true
    });

    return totalLandedCost;
  }

  function updatePurchaseOrder(purchaseOrderId, totalLandedCosts, landedCosts, orderTotals) {
    var itemsLandedCost = [];
    totalLandedCosts.forEach(function (itemReceiptLandedCosts) {
      itemReceiptLandedCosts.forEach(function (itemLandedCost) {
        var addLandedCost = true;
        itemsLandedCost.map(function (landedcost) {
          if (landedcost.item == itemLandedCost.item) {
            landedcost.landedcost += itemLandedCost.landedcost;
            addLandedCost = false;
          }
        })

        if (addLandedCost) {
          itemsLandedCost.push(itemLandedCost)
        }
      })
    })

    var purchaseOrder = record.load({
      type: record.Type.PURCHASE_ORDER,
      id: purchaseOrderId,
      isDynamic: true
    })

    var itemLineCount = purchaseOrder.getLineCount({
      sublistId: 'item'
    });

    for (var i = 0; i < itemLineCount; i++) {
      var item = purchaseOrder.getSublistValue({
        sublistId: 'item',
        fieldId: 'item',
        line: i
      })

      if (item !== '-2' && item !== '52418') {
        purchaseOrder.selectLine({
          sublistId: 'item',
          line: i
        });

        var itemLandedCost = getItemLandedCost(purchaseOrder, landedCosts, orderTotals, i);
        var totalCost = 0;
        itemLandedCost.forEach(function (landedCost) {
          if (landedCost.itemLandedCost > 0) {
            totalCost += parseFloat(landedCost.itemLandedCost);
            switch (landedCost.category) {
              case '1': // Bezugskosten
                purchaseOrder.setCurrentSublistValue({
                  sublistId: 'item',
                  fieldId: 'custcol_av_total_lc_bez',
                  value: landedCost.itemLandedCost
                });
                break;
              case '2': // Lagerkosten
                purchaseOrder.setCurrentSublistValue({
                  sublistId: 'item',
                  fieldId: 'custcol_av_total_lc_lag',
                  value: landedCost.itemLandedCost
                });
                break;
              case '3': // Zoll
                purchaseOrder.setCurrentSublistValue({
                  sublistId: 'item',
                  fieldId: 'custcol_av_total_lc_zoll',
                  value: landedCost.itemLandedCost
                });
                break;
            }
          }
        });

        purchaseOrder.setCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'custcol_av_total_lc_cost',
          value: totalCost
        });

        var amount = parseFloat(purchaseOrder.getSublistValue({
          sublistId: 'item',
          fieldId: 'amount',
          line: i
        }))

        purchaseOrder.setCurrentSublistValue({
          sublistId: 'item',
          fieldId: 'custcol_av_lc_plus_purchase',
          value: totalCost + amount
        });

        purchaseOrder.commitLine({
          sublistId: 'item'
        })
      }
    }

    purchaseOrder.setValue({
      fieldId: 'custbody_av_landedcost_added',
      value: true
    })

    return purchaseOrder.save({
      enableSourcing: true,
      ignoreMandatoryFields: true
    });
  }

  function getItemLandedCost(transactionRecord, landedCosts, orderTotals, i) {
    var updatedCosts = landedCosts;
    var rate = transactionRecord.getSublistValue({
      sublistId: 'item',
      fieldId: 'rate',
      line: i
    })

    var quantity = parseFloat(transactionRecord.getSublistValue({
      sublistId: 'item',
      fieldId: 'quantity',
      line: i
    }))

    var weight = transactionRecord.getSublistValue({
      sublistId: 'item',
      fieldId: 'custcol_av_item_weight',
      line: i
    })

    updatedCosts.map(function (landedCost) {
      var costPerUnit = 0;
      switch (landedCost.method) {
        case '1': // Quantity
          costPerUnit = landedCost.amount / orderTotals.totalQuantity;
          break;
        case '2': // Value
          costPerUnit = ((landedCost.amount * (rate * quantity)) / orderTotals.totalValue) / quantity;
          break;
        case '3': // Weight
          costPerUnit = ((landedCost.amount * (parseFloat(weight) * quantity)) / orderTotals.totalWeight) / quantity;
          break;
      }

      landedCost.itemLandedCost = (costPerUnit * quantity).toFixed(2);
    });

    return updatedCosts;
  }

  function updateStatus(purchaseOrderId) {
    record.submitFields({
      type: record.Type.PURCHASE_ORDER,
      id: purchaseOrderId,
      values: {
        custbody_av_landedcost_added: true
      }
    })
  }

  return {
    getItemReceipts: getItemReceipts,
    getLandedCostRecords: getLandedCostRecords,
    getPurchaseOrderTotals: getPurchaseOrderTotals,
    updateItemReceipt: updateItemReceipt,
    updatePurchaseOrder: updatePurchaseOrder,
    updateStatus: updateStatus
  }
})