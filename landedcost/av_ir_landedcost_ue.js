/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tehseen Ahmed
* Date: 15/8/2022
* Updates landedcostperline checkbox
* @NApiVersion 2.1
* @NScriptType UserEventScript
*/

define(['N/record'], function (record) {
  function beforeSubmit(context) {
    try {
      if (context.type === context.UserEventType.CREATE) {
        var newRecord = context.newRecord;
        newRecord.setValue({
          fieldId: 'landedcostperline',
          value: true
        });
      }
    } catch (ex) {
      log.error({ title: 'beforeSubmit : ex', details: ex });
    }
  }

  return {
    beforeSubmit: beforeSubmit
  }
})