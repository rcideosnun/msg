/**
** Copyright (c) 2022 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tehseen Ahmed
* Date: 11/8/2022
* Adds landed cost on the item receipts
* @NApiVersion 2.1
* @NScriptType WorkflowActionScript
*/

define(['N/runtime', 'SuiteScripts/landedcost/av_custom_landedcost_lib.js'],
  function (runtime, landedCostHelper) {
    function onAction(context) {
      log.audit({ title: 'onAction', details: 'triggered' });
      try {
        var transactionRecord = context.newRecord;
        var recordId = transactionRecord.type == 'itemreceipt' ? transactionRecord.getValue('createdfrom') : transactionRecord.id;
        var createdfrom = transactionRecord.getText('createdfrom');
        if (transactionRecord.type == 'purchaseorder'
          || (transactionRecord.type == 'itemreceipt' && createdfrom.indexOf('Purchase') > -1)) {
          var landedCosts = landedCostHelper.getLandedCostRecords(recordId);
          if (landedCosts && landedCosts.length > 0) {
            var itemReceipts = landedCostHelper.getItemReceipts(recordId);
            var orderTotals = landedCostHelper.getPurchaseOrderTotals(recordId);

            var totalLandedCosts = [];
            itemReceipts.forEach(function (itemReceipt) {
              var totalLandedCost = landedCostHelper.updateItemReceipt(itemReceipt, landedCosts, orderTotals);
              totalLandedCosts.push(totalLandedCost);
            });

            var purchaseOrderId = landedCostHelper.updatePurchaseOrder(recordId, totalLandedCosts, landedCosts, orderTotals);
            log.audit({ title: 'onAction purchaseOrderId', details: purchaseOrderId });
          } else {
            landedCostHelper.updateStatus(recordId);
          }
        }

      } catch (ex) {
        log.error({ title: 'onAction : ex', details: ex });
      }
    }

    return {
      onAction: onAction
    }
  }
)