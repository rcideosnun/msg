
/**
** Copyright (c) 2020 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Tuba Mohsin
* Date: 24.08.2023
* Create invoice for orders that are shipped or has digital delivery
* Spec: https://docs.google.com/document/d/1EAQaRMjDpnSaRvmoScU7YwkyVPd7_kDxZKQocb-l8uA/edit
* @NApiVersion 2.1
* @NScriptType MapReduceScript
*/

define(['N/search', 'N/record', 'N/runtime', "../pimCoreIntegration/av_int_log.js"],
    function (search, record, runtime, avLog) {

        const getInputData = () => {
            try {
                log.debug("DEBUG", `-Script Starting on ${new Date()}-`);
                return _getOrdersToBill();
            } catch (e) {
                log.error('Error in GetInputData', e);
                avLog.error_message({
                    msg: 'IE01',
                    var1: `Error occurred while creating Invoice` + (ex.message ? ex.message : ''),
                    var2: JSON.stringify(ex)
                })
            }
        }

        const map = (context) => {
            const toBillObj = JSON.parse(context.value);
            try {
                if (!toBillObj.soId) return;
                log.debug("Processing element", JSON.stringify(toBillObj));

                const invRec = record.transform({
                    fromType: "salesorder",
                    fromId: toBillObj.soId,
                    toType: "invoice"
                });

                if (toBillObj.isFulfillment) {
                    invRec.setValue("entryformquerystring", `id=${toBillObj.soId}&memdoc=0&transform=salesord&e=T&itemship=${toBillObj.ifId}&xml=T`);
                }

                const invId = invRec.save({
                    enableSourcing: false,
                    ignoreMandatoryFields: true
                });

                log.debug("Invoice created with id: ", invId)

                if (toBillObj.isFulfillment) {
                    record.submitFields({
                        type: "itemfulfillment",
                        id: toBillObj.ifId,
                        values: {
                            custbody_av_msg_if_invoice: invId,
                        }
                    });
                }

            } catch (e) {
                log.error('Error in Map stage', e);
                avLog.error_message({
                    msg: 'IE01',
                    var1: `Error occurred while creating Invoice for SO: ${toBillObj.soId}.` + (ex.message ? ex.message : ''),
                    var2: JSON.stringify(ex)
                })
            }
        }

        const summarize = () => {
            log.debug("DEBUG", `-Script Finished on ${new Date()}`)
        }

        const _getOrdersToBill = () => {
            const currentRuntime = runtime.getCurrentScript();
            const ordersToBill = [], uniqueIds = [];
            let isFulfillment = false;

            const ifSearchObj = search.load({
                id: currentRuntime.getParameter({ name: "custscript_av_search_id" }),
            });

            let myPagedData = ifSearchObj.runPaged({ pageSize: 1000 });

            myPagedData.pageRanges.forEach(function (pageRange) {
                let myPage = myPagedData.fetch({ index: pageRange.index });

                myPage.data.forEach(function (result) {
                    if (result.recordType == "itemfulfillment") isFulfillment = true;

                    if (uniqueIds.indexOf(result.id) == -1) {
                        ordersToBill.push({
                            soId: isFulfillment ? result.getValue("createdfrom") : result.id,
                            ifId: result.id,
                            type: result.recordType,
                            isFulfillment
                        });
                        uniqueIds.push(result.id)
                    }
                    return true;
                });

                log.debug("Orders to Bill", JSON.stringify(ordersToBill))

            });
            return ordersToBill;
        }

        return {
            getInputData: getInputData,
            map: map,
            summarize: summarize
        };

    })