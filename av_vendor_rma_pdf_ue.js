/** 
 * Copyright (c) 2019 Alta Via Consulting GmbH
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 * @author: [Polina Melnykova]
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @description : 
 * Spec: RMA PDF for vendor returns
 */

define(['N/runtime', 'N/search'], function (runtime, search) {
  function beforeLoad(context) {
    var user = runtime.getCurrentUser();
    var language = user.getPreference('language');
    var buttonLable = 'Vendor RMA';
    if (language.includes('de')) {
      buttonLable = 'Lieferanten RMA'
    }

    context.form.addButton({
      id: "custpage_vcpb",
      label: buttonLable,
      functionName: "onButtonClick"
    });
    context.form.clientScriptModulePath = "SuiteScripts/av_vendor_rma_pdf_cl.js";
  }

  function beforeSubmit(context) {
    if (context.type != context.UserEventType.CREATE) return;

    try {
      var newRecord = context.newRecord;
      var itemId = newRecord.getSublistValue({
        sublistId: 'item',
        fieldId: 'item',
        line: 0
      });
      var manufacturerDetails = getManufacturerDetails(itemId);

      if (manufacturerDetails) {
        newRecord.setValue({
          fieldId: 'custbody_av6_rma_item_name_manufac',
          value: manufacturerDetails.name
        });
        newRecord.setValue({
          fieldId: 'custbodycustbody_msg_hersteller',
          value: manufacturerDetails.address
        });
      }
    } catch (ex) {
      log.error({ title: 'beforeSubmit: ex', details: ex });
    }

  }

  function getManufacturerDetails(itemId) {
    var itemLookup = search.lookupFields({
      type: search.Type.ITEM,
      id: itemId,
      columns: ['custitem_av_manufacturer', 'vendorname']
    });
    if (itemLookup) {
      var details = {
        name: itemLookup.vendorname
      }
      var manufacturer = itemLookup.custitem_av_manufacturer[0].value;
      if (manufacturer) {
        details.address = search.lookupFields({
          type: search.Type.VENDOR,
          id: manufacturer,
          columns: ['address']
        }).address;
      }
      return details;
    } else {
      return false;
    }
  }

  return {
    beforeLoad: beforeLoad,
    beforeSubmit: beforeSubmit
  };
});