/**
 *@NApiVersion 2.1
 *@NScriptType Suitelet
 */
define(['N/search','./av_b2b_price_engine_lib'], function(search, lib) {

    function onRequest(context) {
        let ItemList = lib.getItemList();

        ItemList.forEach(element => {
            lib.calulatePrice(element);
        });
    }

    return {
        onRequest: onRequest
    };
});
