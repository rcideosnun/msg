/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
** @version: 2.1
** @author: [Adrian Reszka]
** @dated: [24.04.2023]
** @Description: Lib for b2b price engine
*
*  @NApiVersion 2.1
*/
define(['N/record', 'N/search', 'N/runtime'], function (record, search, runtime) {
    function getItemList(itemPriceId) {
        let itelListObj = {};
        var currentSc = runtime.getCurrentScript();

        var itemSearch = search.load({
            id: currentSc.getParameter({ name: "custscript_av_update_b2bprices_search" }),
        });
        log.debug('itemSearch',itemSearch)

        let searchResult = getAllResults(itemSearch);
        log.debug('searchResult',searchResult)
        searchResult.forEach(function (result) {
            log.debug('result',result)
            //let item = result.getValue({ name: 'internalid', join: 'CUSTRECORD_MSG_IP_ITEM' });
            let item = result.id;


            // log.debug('result.id',result.id)
            // log.debug('vendor',result.getValue({ name: 'custitem_av_manufacturer', join: 'CUSTRECORD_MSG_IP_ITEM' }) )
            // log.debug('basePriceFromItem',result.getValue({ name: 'baseprice', join: 'CUSTRECORD_MSG_IP_ITEM' }))
            // log.debug('itemCategoryId',result.getValue({ name: 'cseg_msg_igt', join: 'CUSTRECORD_MSG_IP_ITEM' }))
            // log.debug('basePrice', result.getValue({ name: 'custrecord_msg_ip_base' }))


            if (!itelListObj[item]) {
                itelListObj[item] = {
                    itemPriceInternalId: result.id,
                    itemIdNumber: result.getValue({ name: 'custrecord_msg_ip_item' }),
                    basePrice: parseFloat(result.getValue({ name: 'custrecord_msg_ip_base' })) || 0,
                    currentB2BPrice: parseFloat(result.getValue({ name: 'custrecord_msg_ip_b2b' })) || 0,
                    srpPrice: parseFloat(result.getValue({ name: 'custrecord_msg_ip_srp' })) || 0,
                    avarageCost: parseFloat(result.getValue({ name: 'averagecost', join: 'CUSTRECORD_MSG_IP_ITEM' })) || 0,
                    itemCategoryId: result.getValue({ name: 'cseg_msg_igt', join: 'CUSTRECORD_MSG_IP_ITEM' }),
                    basePriceFromItem: parseFloat(result.getValue({ name: 'baseprice', join: 'CUSTRECORD_MSG_IP_ITEM' })) || 0,
                    manufacturerVendor: result.getValue({ name: 'custitem_av_manufacturer', join: 'CUSTRECORD_MSG_IP_ITEM' })
                };
            }
        });
        return itelListObj;
    }
    function getAllResults(s) {
        var results = s.run();
        var searchResults = [];
        var searchid = 0;
        do {
            var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
            resultslice.forEach(function (slice) {
                searchResults.push(slice);
                searchid++;
            }
            );
        } while (resultslice.length >= 1000);
        return searchResults;
    }
    function percentToDecimal(percent) {
        return parseFloat(percent) / 100;
    }
    function calulatePrice(itemPrice) {
        try {
            findItemCategoryReabte(itemPrice);
            let replacmentPrice = calulateReplacmentPrice(itemPrice);
            log.debug('replacmentPrice',replacmentPrice)
            let price = replacmentPrice > itemPrice.avarageCost ? replacmentPrice : itemPrice.avarageCost;
            log.debug('price',price)
            let surcharge = getSurcharge(price);
            let calulatedB2BPrice = price * (1 + surcharge);
            log.debug('calulatedB2BPrice',calulatedB2BPrice)
            let VKNet = itemPrice.srpPrice * 0.65;
            log.debug('VKNet',VKNet)

            if (calulatedB2BPrice > itemPrice.basePrice) {
                itemPrice.b2bPrice = itemPrice.basePrice;
            }
            else if (calulatedB2BPrice <= itemPrice.basePrice) {
                if (calulatedB2BPrice > VKNet) {
                    itemPrice.b2bPrice = calulatedB2BPrice;
                }
                else if (calulatedB2BPrice <= VKNet) {
                    itemPrice.b2bPrice = VKNet;
                }
            }

            log.debug('final B2BPrice', itemPrice.b2bPrice);
            log.debug('itemPrice', itemPrice);

            itemPrice.b2bPrice = Math.round((itemPrice.b2bPrice + Number.EPSILON) * 100) / 100;
            if (itemPrice.b2bPrice > 0) {
                if (itemPrice.currentB2BPrice != itemPrice.b2bPrice) {
                    log.debug('update B2B Price with :', itemPrice.b2bPrice);
                    record.submitFields({
                        type: 'customrecord_msg_item_prices',
                        id: itemPrice.itemPriceInternalId,
                        values: {
                            custrecord_msg_ip_b2b: itemPrice.b2bPrice
                        }
                    });

                }
            }
            return 0;
        } catch (e) {
            log.error('failed to update b2b price: ' + itemPrice.item, e.message);
        }

        // calc replacement price
        // compare replacement with location avg cost
        // multiple by surcharge
    }
    function findItemCategoryReabte(itemPrice) {
        try {
            if (itemPrice && itemPrice.itemCategory && itemPrice.manufacturerVendor) {
                var itemCategorySearchObj = search.create({
                    type: 'customrecord_av_item_category_rabatt',
                    filters:
                        [
                            ['custrecord_av_vendor_parent', 'anyof', itemPrice.manufacturerVendor],
                            'AND',
                            ['custrecord_av_item_category', 'anyof', itemPrice.itemCategory],
                            'AND',
                            ['custrecord_av_item_category_rebatt', 'isnotempty', '']
                        ],
                    columns:
                        [
                            'custrecord_av_item_category_rebatt',
                            search.createColumn({ name: 'internalid', sort: search.Sort.DESC, label: 'Internal ID' })
                        ]
                });
                let searchResultCount = itemCategorySearchObj.runPaged().count;
                // toCheck : is it possible to have more than one matches ? for now, script will pick last created.
                if (searchResultCount > 0) {
                    itemCategorySearchObj.run().each(function (result) {
                        itemPrice.categoryRebate = result.getValue('custrecord_av_item_category_rebatt');
                        return false;
                    });
                }
                else {
                    itemPrice.categoryRebate = 0;
                }
            }
            else {
                itemPrice.categoryRebate = 0;
            }
            return itemPrice;
        } catch (er) {
            log.error('Failed to get category rebate', er);
        }
    }
    function calulateReplacmentPrice(item) {
        let replacmentPrice = 0;
        if (item.ek && item.ek > 0) {
            replacmentPrice = item.ek;
            if (item.reb1 > 0) replacmentPrice *= (1 - item.reb1);
            if (item.reb2 > 0) replacmentPrice *= (1 - item.reb2);
            if (item.venReb > 0) replacmentPrice *= (1 - item.venReb);
            if (item.categoryRebate > 0) replacmentPrice *= (1 - item.categoryRebate);
            if (item.freightCost > 0) replacmentPrice *= (1 + item.freightCost);
        }
        return replacmentPrice;
    }
    function getSurcharge(price) {
        let surcharge = 0;
        var customrecord_av_cost_price_scaleSearchObj = search.create({
            type: 'customrecord_av_cost_price_scale',
            filters:
                [
                    ['custrecord_av_cost_price_from', 'lessthanorequalto', price],
                    'AND',
                    ['custrecord_av_cost_price_to', 'greaterthan', price]
                ],
            columns:
                [
                    'custrecord_av_surcharge'
                ]
        });
        customrecord_av_cost_price_scaleSearchObj.run().each(function (result) {
            surcharge = percentToDecimal(result.getValue('custrecord_av_surcharge'));
            return true;
        });
        return surcharge;
    }
    function getVendorPriceList(uniqueItemList) {
        let vendorPriceListObj = {};
        if (uniqueItemList.length) {
            let customrecord_av_vendor_price_listSearchObj = search.create({
                type: 'customrecord_av_vendor_price_list',
                filters:
                    [
                        [
                            ['custrecord_av_item.isinactive', 'is', 'F'],
                            'AND',
                            ['custrecord_av_item.internalid', 'noneof', '@NONE@'],
                            'AND',
                            ['custrecord_av_item', 'anyof', uniqueItemList],
                            'AND',
                            ['custrecord_av_vendor', 'noneof', '@NONE@'],
                            'AND',
                            ['custrecord_av_valid_from', 'onorbefore', 'today']
                        ],
                        'AND',
                        [
                            [
                                ['custrecord_av_valid_to', 'onorafter', 'today']
                            ],
                            'OR',
                            [
                                ['custrecord_av_valid_to', 'isempty', '']
                            ]
                        ]
                    ],
                columns:
                    [
                        'custrecord_av_item',
                        search.createColumn({ name: 'internalid', join: 'CUSTRECORD_AV_ITEM' }),
                        'custrecord_av_vendor',
                        search.createColumn({ name: 'custrecord_av_valid_from', sort: search.Sort.DESC }),
                        'custrecord_av_valid_to',
                        'custrecord_av_rabatt1',
                        'custrecord_av_rabatt2',
                        'custrecord_av_ek',
                        search.createColumn({ name: 'custentity_av_vendor_freight_cost', join: 'CUSTRECORD_AV_VENDOR' }),
                        search.createColumn({ name: 'cseg_msg_igt', join: 'CUSTRECORD_AV_ITEM' }),
                        search.createColumn({ name: 'custentity_av_vendor_freight_cost', join: 'CUSTRECORD_AV_VENDOR' })
                    ]
            });
            let searchResult = getAllResults(customrecord_av_vendor_price_listSearchObj);
            searchResult.forEach(function (result) {
                let item = result.getValue({ name: 'internalid', join: 'CUSTRECORD_AV_ITEM' });
                if (!vendorPriceListObj[item]) {
                    vendorPriceListObj[item] = {
                        itemIdNumber: result.getValue({ name: 'custrecord_av_item' }),
                        ek: parseFloat(result.getValue({ name: 'custrecord_av_ek' })) || 0,
                        reb1: percentToDecimal(result.getValue({ name: 'custrecord_av_rabatt1' })) || 0,
                        reb2: percentToDecimal(result.getValue({ name: 'custrecord_av_rabatt2' })) || 0,
                        itemId: result.getValue({ name: 'internalid', join: 'CUSTRECORD_AV_ITEM' }),
                        Artikelnummer: result.getValue({ name: 'itemid', join: 'CUSTRECORD_AV_ITEM' }),
                        itemType: result.getValue({ name: 'type', join: 'CUSTRECORD_AV_ITEM' }),
                        itemCategory: result.getValue({ name: 'cseg_msg_igt', join: 'CUSTRECORD_AV_ITEM' }),
                        venReb: percentToDecimal(result.getValue({ name: 'custentity_av_rebate_vendor', join: 'CUSTRECORD_AV_VENDOR' })) || 0,
                        freightCost: percentToDecimal(result.getValue({ name: 'custentity_av_vendor_freight_cost', join: 'CUSTRECORD_AV_VENDOR' })) || 0,
                        vendor: result.getValue({ name: 'custrecord_av_vendor' }),
                    };
                }
            });

        }

        return vendorPriceListObj;
    }
    return {
        getItemList: getItemList,
        getVendorPriceList: getVendorPriceList,
        calulatePrice: calulatePrice

    };
});
