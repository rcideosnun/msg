/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 */
define(['N/runtime','./av_b2b_price_engine_lib_new'], function (runtime, b2bLib) {

    function getInputData() {
        try {
            let scriptObj = runtime.getCurrentScript();
            let itemPricesObjList = b2bLib.getItemList();
            let finalItemList = {};
            // insted of uvp use custrecord_msg_ip_srp on the item prices record.

            log.debug('item price count', Object.keys(itemPricesObjList).length);
            log.debug('Remaining governance units: ' + scriptObj.getRemainingUsage());

            let uniqueItemIds = Object.keys(itemPricesObjList);
            log.debug('uniqueItemIds', uniqueItemIds);
            let VendorPriceList = b2bLib.getVendorPriceList(uniqueItemIds);
            log.debug('vpl count', Object.keys(VendorPriceList).length);
            log.debug('Remaining governance post vendor units: ' + scriptObj.getRemainingUsage());

            for (const item in itemPricesObjList) {
                for (const priceList in VendorPriceList) {
                    if (item == priceList && itemPricesObjList[item].manufacturerVendor == VendorPriceList[priceList].vendor) {
                        finalItemList[item] = itemPricesObjList[item];
                        finalItemList[item].ek = VendorPriceList[priceList].ek;
                        finalItemList[item].reb1 = VendorPriceList[priceList].reb1;
                        finalItemList[item].reb2 = VendorPriceList[priceList].reb2;
                        finalItemList[item].venReb = VendorPriceList[priceList].venReb;
                        finalItemList[item].itemCategory = VendorPriceList[priceList].itemCategory;
                        finalItemList[item].freightCost = VendorPriceList[priceList].freightCost;
                    }

                }
            }

            log.debug('postGroup', Object.keys(finalItemList).length);
            let itemPricesDataList = Object.entries(finalItemList).map(entry => entry[1]);
            log.debug('postGroup List', itemPricesDataList);
            return itemPricesDataList;
        } catch (er) {
            log.error('get Input data failes', er);
        }
    }

    function map(context) {
        try {
            let itemPriceObj = JSON.parse(context.value);
            log.debug('MAP itemPriceObj', itemPriceObj);
            // b2bLib.calulatePrice(itemPriceObj);
        } catch (er) {
            log.error(er);
        }
    }
    function summarize(context) {
        var totalItemsProcessed = 0;
        context.output.iterator().each(function (key, value) {
            totalItemsProcessed++;
        });
        var summaryMessage = 'Usage: ' + context.usage + ' Concurrency: ' + context.concurrency + '  Number of yields: ' + context.yields;
        log.audit({ title: 'Summary of usase', details: summaryMessage });

    }

    return {
        getInputData: getInputData,
        map: map,
        summarize: summarize
    };
});
