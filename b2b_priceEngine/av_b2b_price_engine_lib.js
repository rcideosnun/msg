/**
 * I23 lib
 * @NApiVersion 2.1
 */
define(['N/https', 'N/record', 'N/search', 'N/runtime'], function(https, record, search, runtime) {

    function getItemList()
    {
        let ItemVendorList = [];
        var customrecord_av_vendor_price_listSearchObj = search.create({
            type: 'customrecord_av_vendor_price_list',
            filters:
            [
                ['custrecord_av_vendor.internalid','noneof','@NONE@'], 
                'AND', 
                ['custrecord_av_item','noneof','@NONE@'],
                'AND', 
                ['custrecord_av_item.custitem_av_b2b_calculation_no','is','F']
            ],
            columns:
            [
                'custrecord_av_vendor',
                'custrecord_av_valid_from',
                'custrecord_av_valid_to',
                'custrecord_av_item',
                search.createColumn({
                    name: 'cseg_msg_igt',
                    join: 'CUSTRECORD_AV_ITEM'
                }),
                'custrecord_av_ean',
                'custrecord_av_uvp',
                'custrecord_av_ek',
                'custrecord_av_rabatt1',
                'custrecord_av_rabatt2',
                search.createColumn({
                    name: 'custentity_av_rebate_vendor',
                    join: 'CUSTRECORD_AV_VENDOR'
                }),
                search.createColumn({
                    name: 'custentity_av_vendor_freight_cost',
                    join: 'CUSTRECORD_AV_VENDOR'
                }),
                search.createColumn({
                    name: 'averagecost',
                    join: 'CUSTRECORD_AV_ITEM'
                }),
                search.createColumn({
                    name: 'custitem_av_b2b_price',
                    join: 'CUSTRECORD_AV_ITEM'
                }),
                search.createColumn({
                    name: 'baseprice',
                    join: 'CUSTRECORD_AV_ITEM'
                }),
                search.createColumn({
                    name: 'type',
                    join: 'CUSTRECORD_AV_ITEM'
                })

            ]
        });
        var itemListSearch = getAllResults(customrecord_av_vendor_price_listSearchObj);
        itemListSearch.forEach(function(result) {
            let record = {};
            record.vendor = result.getValue('custrecord_av_vendor');
            record.validFrom = result.getValue('custrecord_av_valid_from');
            record.validTo = result.getValue('custrecord_av_valid_to');
            record.item = result.getValue('custrecord_av_item');
            record.category = result.getValue({name: 'cseg_msg_igt',join: 'CUSTRECORD_AV_ITEM'});
            record.ean = result.getValue('custrecord_av_ean');
            record.uvp = parseFloat(result.getValue('custrecord_av_uvp')) || 0;
            record.ek = parseFloat(result.getValue('custrecord_av_ek')) || 0;
            record.rebate1 = toDecimal(result.getValue('custrecord_av_rabatt1')) || 0;
            record.rebate2 = toDecimal(result.getValue('custrecord_av_rabatt2')) || 0;
            record.vendorRebate = toDecimal(result.getValue({name: 'custentity_av_rebate_vendor',join: 'CUSTRECORD_AV_VENDOR'})) || 0;
            record.freightCost = toDecimal(result.getValue({name: 'custentity_av_vendor_freight_cost',join: 'CUSTRECORD_AV_VENDOR'})) || 0;
            record.avarageCost = parseFloat(result.getValue({ name: 'averagecost',join: 'CUSTRECORD_AV_ITEM'})) || 0;
            // record.b2bprice = result.getValue({ name: 'custitem_av_b2b_price',join: 'CUSTRECORD_AV_ITEM'});
            record.basePrice = parseFloat(result.getValue({ name: 'baseprice',join: 'CUSTRECORD_AV_ITEM'})) || 0;     
            record.itemType = result.getValue({name: 'type',join: 'CUSTRECORD_AV_ITEM'});         
           
            ItemVendorList.push(record);

            // .run().each has a limit of 4,000 results
            return true;
        });

        let categoryList = [];
        var customrecord_av_item_category_rabattSearchObj = search.create({
            type: 'customrecord_av_item_category_rabatt',
            filters:
            [
                ['custrecord_av_vendor_parent','noneof','@NONE@'], 
                'AND', 
                ['custrecord_av_item_category','noneof','@NONE@'], 
                'AND', 
                ['custrecord_av_item_category_rebatt','isnotempty','']
            ],
            columns:
            [
                'custrecord_av_vendor_parent',
                'custrecord_av_item_category',
                'custrecord_av_item_category_rebatt'
            ]
        });
        var itemRebateSearch = getAllResults(customrecord_av_item_category_rabattSearchObj);
        itemRebateSearch.forEach(function(result) {
            let record = {};
            record.vendor = result.getValue('custrecord_av_vendor_parent');
            record.itemCategory = result.getValue('custrecord_av_item_category');
            record.categoryRebate = toDecimal(result.getValue('custrecord_av_item_category_rebatt')) || 0;
            categoryList.push(record);
            return true;
        });
        // ADD FILTERING OUT RECORDS THAT DO NOT HAVE ENOUGH DATA
        ItemVendorList.forEach(ivl => {
            let itemCategory = categoryList.find(a=> a.vendor == ivl.vendor &&  a.itemCategory == ivl.category);
            if (itemCategory){
                ivl.categoryRebate = itemCategory.categoryRebate;
            }
        });
        return ItemVendorList;
        
    }
    function calulatePrice(item){
        try {
            // get 'item prices' list and run for each
            var objRecord = record.load({type: 'inventoryitem',id: item.item,isDynamic: true,});
            let priceListCount = objRecord.getLineCount('recmachcustrecord_msg_ip_item');
            let priceList = [];
            for (let i = 0; i < priceListCount; i++){
                let record = {};
                record.currentB2BPrice = objRecord.getSublistValue('recmachcustrecord_msg_ip_item','custrecord_msg_ip_b2b',i);
                record.basePrice = objRecord.getSublistValue('recmachcustrecord_msg_ip_item','custrecord_msg_ip_base',i);
                record.dateFrom = objRecord.getSublistValue('recmachcustrecord_msg_ip_item','custrecord_msg_ip_date_from',i);
                record.itemPricesId = objRecord.getSublistValue('recmachcustrecord_msg_ip_item','id',i);
                record.b2bPrice = 0;
                priceList.push(record);
            }

            let replacmentPrice = calulateReplacmentPrice(item);
            let price =  replacmentPrice > item.avarageCost ? replacmentPrice : item.avarageCost;
            let surcharge = getSurcharge(price);
            let calulatedB2BPrice = price * (1 + surcharge);
            let VKNet = item.uvp * 0.65;


            priceList.forEach(priceObj => {
                if (calulatedB2BPrice > priceObj.basePrice)
                {
                    priceObj.b2bPrice = priceObj.basePrice;
               
                } 
                else if (calulatedB2BPrice <=  priceObj.basePrice){
                    if (calulatedB2BPrice > VKNet){
                        priceObj.b2bPrice = calulatedB2BPrice;
                    }
                    else if (calulatedB2BPrice <= VKNet)
                    {
                        priceObj.b2bPrice = VKNet;
                    }
                }
                log.debug('final B2BPrice', priceObj.b2bPrice);
                priceObj.b2bPrice = Math.round((priceObj.b2bPrice + Number.EPSILON) * 100) / 100;
                if (priceObj.b2bPrice > 0){  

                    let b2bLine = objRecord.findSublistLineWithValue('recmachcustrecord_msg_ip_item','id', priceObj.itemPricesId);
                    objRecord.selectLine({sublistId: 'recmachcustrecord_msg_ip_item', line: b2bLine});
                    let currentValue = objRecord.getCurrentSublistValue('recmachcustrecord_msg_ip_item','custrecord_msg_ip_b2b');
                    if (currentValue != priceObj.b2bPrice){

                        log.debug('update item', priceObj.itemPricesId);
                        objRecord.setCurrentSublistValue({sublistId: 'recmachcustrecord_msg_ip_item',fieldId: 'custrecord_msg_ip_b2b',value: priceObj.b2bPrice, ignoreFieldChange: true});
                        objRecord.commitLine({sublistId: 'recmachcustrecord_msg_ip_item'});
            
                    }
                }



            
            });
            objRecord.save();
       
            return calulatedB2BPrice;
        } catch (e) {
            log.error('failed to update b2b price: ' + item.item, e.message)
        }

        // calc replacement price
        // compare replacement with location avg cost
        // multiple by surcharge
    }
    function calulateReplacmentPrice(item){
        //TODO : Only get the valid prices during that time by 
        let replacmentPrice = 0;
        //log.debug('rebates',item.rebate1 + ',' + item.rebate2  + ',' + item.vendorRebate  + ',' + item.categoryRebate  + ',' + item.freightCost   );
        if (item.ek && item.ek > 0){
            replacmentPrice = item.ek;
            if (item.rebate1 > 0) replacmentPrice *= (1 - item.rebate1);
            if (item.rebate2 > 0) replacmentPrice *= (1 - item.rebate2);
            if (item.vendorRebate > 0) replacmentPrice *= (1 - item.vendorRebate);
            if (item.categoryRebate > 0) replacmentPrice *= ( 1 - item.categoryRebate);
            if (item.freightCost > 0) replacmentPrice *= ( 1 + item.freightCost);
        }
        return replacmentPrice;
    }
    function getSurcharge(price){
        let surcharge = 0;
        var customrecord_av_cost_price_scaleSearchObj = search.create({
            type: 'customrecord_av_cost_price_scale',
            filters:
            [
                ['custrecord_av_cost_price_from','lessthanorequalto',price], 
                'AND', 
                ['custrecord_av_cost_price_to','greaterthan',price]
            ],
            columns:
            [
                'custrecord_av_surcharge'
            ]
        });
        customrecord_av_cost_price_scaleSearchObj.run().each(function(result){
            surcharge = toDecimal(result.getValue('custrecord_av_surcharge'));
            return true;
        });
        return surcharge;
    }
    function getAllResults(s) {
        var results = s.run();
        var searchResults = [];
        var searchid = 0;
        do {
            var resultslice = results.getRange({start:searchid,end:searchid+1000});
            resultslice.forEach(function(slice) {
                searchResults.push(slice);
                searchid++;
            }
            );
        } while (resultslice.length >=1000);
        return searchResults;
    }
    function toDecimal(percent) {
        return parseFloat(percent) / 100;
    } 

    return {
        getItemList : getItemList,
        calulatePrice: calulatePrice

    };
});
