/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 */
 define(['./av_b2b_price_engine_lib'], function(lib) {

    function getInputData() {
        let ItemList = lib.getItemList();
        log.debug('ItemList count',ItemList.length);
        return ItemList;
    }

    function map(context) {
        log.debug('context value',context.value);
        let obj = JSON.parse(context.value);

        lib.calulatePrice(obj);
    }

    function summarize(context) {
        var totalItemsProcessed = 0;
        context.output.iterator().each(function (key, value) {
            totalItemsProcessed++;
        });
        var summaryMessage = 'Usage: ' + context.usage + ' Concurrency: ' + context.concurrency + '  Number of yields: ' + context.yields;
        log.audit({ title: 'Summary of usase', details: summaryMessage });

    }

    return {
        getInputData: getInputData,
        map: map,
        summarize: summarize
    };
});
