/**
 ** Copyright (c) 2019 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
** @version: 2.1
** @author: [Roberto Cideos]
** @dated: [19/08/2022]
** @Description: Processing of Bank Statement Payment Validation for Vorauskasse
*
*  @NApiVersion 2.1
*  @NScriptType WorkflowActionScript
*/

define(['N/runtime',
    'N/search',
    'N/record'],
    function (runtime,
        search,
        record) {

        var self = this;

        var CONSTANTS = {
            AMOUNT_TOLERANCE: 1,
            ORDER_STATUS: {
                PENDING_FULFILLMENT: "B"
            },
            ERROR_MSGS: {
                ORDER_NOT_FOUND: "No order was found with the given data",
                AMT_NOT_FOUND: "No amount was found in the processed file",
                TRESHOLD_SURPASS: "The amount is above the permitted treshold of 1 euro"
            },
            REC_STATE: {
                ERROR: 2,
                DONE: 3
            },
            PROCESS_RECORD: _updateOrderStatus
        }

        /**
         * 
         * @param {Object} context 
         * @returns 
         */

        function onAction(context) {

            var currentRecord = context.newRecord

            if (runtime.envType == "SANDBOX") {
                //setup sandbox global variables
                self.currentRecord = currentRecord

            } else if (runtime.envType == "PRODUCTION") {
                //setup production global variables
                self.currentRecord = currentRecord
            }

            return _processStagingRecord(context)

        }

        /**
         * 
         * @param {Object} context 
         * @returns 
         */

        function _processStagingRecord(context) {

            let currentRec = context.newRecord

            try {

                let amt = currentRec.getValue("custrecord_av_amount_paid");
                let soId = _getSalesOrderId();

                if (!amt) {
                    _createError(currentRec.id, CONSTANTS.ERROR_MSGS.AMT_NOT_FOUND);
                    return;
                }

                if (!soId) {
                    _createError(currentRec.id, CONSTANTS.ERROR_MSGS.ORDER_NOT_FOUND);
                    return;
                }

                return CONSTANTS.PROCESS_RECORD(Number(amt), soId)


            } catch (e) {
                self.currentRecord.setValue("custrecord_av_process_status", CONSTANTS.REC_STATE.ERROR)
            }

        }

        /**
         * 
         * @param {String} amt 
         * @param {String} soId 
         */

        function _updateOrderStatus(amt, soId) {
            let totalSo = _getSoTotal(soId);
            let paymentsS = _searchForExistingPayments(soId);
            self.currentRecord.setValue("custrecord_av_linked_transaction", soId);

            for (let i = 0; i < paymentsS.length; i++) {
                amt += Number(paymentsS[i].amt)
            }
            log.error("Amts", `totalSo: ${totalSo}, amt ${amt}`)
            if (Math.abs(Math.abs(totalSo) - Math.abs(amt)) <= CONSTANTS.AMOUNT_TOLERANCE) {

                record.submitFields({
                    type: "salesorder",
                    id: soId,
                    values: {
                        orderstatus: CONSTANTS.ORDER_STATUS.PENDING_FULFILLMENT
                    }

                })

                self.currentRecord.setValue("custrecord_av_process_status", CONSTANTS.REC_STATE.DONE)

            } else {


                self.currentRecord.setValue("custrecord_av_process_status", CONSTANTS.REC_STATE.ERROR)
                _createError(null, CONSTANTS.ERROR_MSGS.TRESHOLD_SURPASS)
                return;
            }

            for (let i = 0; i < paymentsS.length; i++) {
                record.submitFields({
                    type: "customrecord_av_payment_adv_vorauskasse",
                    id: paymentsS[i].recId,
                    values: {
                        custrecord_av_process_status: CONSTANTS.REC_STATE.DONE
                    }

                })
            }

        }

        /**
         * 
         * @param {String} soId 
         */

        function _getSoTotal(soId) {

            return search.lookupFields({
                type: "salesorder",
                id: soId,
                columns: "total"
            }).total

        }

        /**
         * 
         * @returns 
         */

        function _searchForExistingPayments(soId) {
            var paymentRes = [];
            let filters = [["isinactive", "is", "F"],
                "AND",
            ["custrecord_av_linked_transaction", "noneof", "@NONE@"]];

            if (self.currentRecord.id) {
                filters.push("AND");
                filters.push(["internalid", "noneof", self.currentRecord.id]);
            }

            if (soId) {
                filters.push("AND");
                filters.push(["custrecord_av_linked_transaction", "anyof", soId]);
            }else{
                return [];
            }

            _getAllResults(search.create({
                type: "customrecord_av_payment_adv_vorauskasse",
                filters: filters,
                columns:
                    ["custrecord_av_linked_transaction", "custrecord_av_amount_paid"]
            })).every(function (result) {

                paymentRes.push({
                    soId: result.getValue("custrecord_av_linked_transaction"),
                    amt: result.getValue("custrecord_av_amount_paid"),
                    recId: result.id
                })

                return true;
            });

            return paymentRes
        }

        function _getSalesOrderId() {

            let str = self.currentRecord.getValue("custrecord_av_order_number");

            if (!str) return null;

            let matches = str.match(/\d+/g);

            if (!matches.length) return null

            let shopOrdersS = _getShopwareOrders();

            let orderId = _getTranId(matches, shopOrdersS)

            return orderId
        }

        /**
         * 
         * @param {Array} matches 
         * @param {Object} shopOrdersS 
         * @returns 
         */

        function _getTranId(matches, shopOrdersS) {
            let soId = null;

            for (let i = 0; i < matches.length; i++) {

                if (shopOrdersS[matches[i]]) {
                    soId = shopOrdersS[matches[i]];
                    break;
                }

            }

            return soId
        }

        function _getShopwareOrders() {
            let shopOrdersByNumber = {};

            _getAllResults(search.create({
                type: "transaction",
                filters:
                    [
                        ["custbody_av_sw_order_number", "isnotempty", ""],
                        "AND",
                        ["mainline", "is", "T"],
                        "AND",
                        ["status", "anyof", "SalesOrd:A"],
                        "AND",
                        ["custbody_av_payment_method", "startswith", "Vorkasse"]
                    ],
                columns:
                    ["custbody_av_sw_order_number"]
            })).every(function (result) {

                if (!shopOrdersByNumber[result.getValue("custbody_av_sw_order_number")]) {
                    shopOrdersByNumber[result.getValue("custbody_av_sw_order_number")] = result.id;
                }

                return true;
            });

            return shopOrdersByNumber

        }

        /**
         * 
         * @param {SuiteScript Search Object} s 
         * @returns 
         */

        function _getAllResults(s) {
            var results = s.run();
            var searchResults = [];
            var searchid = 0;
            do {
                var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
                resultslice.forEach(function (slice) {
                    searchResults.push(slice);
                    searchid++;
                }
                );
            } while (resultslice.length >= 1000);
            return searchResults;
        }

        /**
         * 
         * @param {String} recId 
         * @param {String} msg 
         */

        function _createError(recId, msg) {

            try {

                self.currentRecord.selectNewLine({
                    sublistId: "recmachcustrecord_av_file_error_rec"
                })

                self.currentRecord.setCurrentSublistValue({
                    sublistId: "recmachcustrecord_av_file_error_rec",
                    fieldId: "custrecord_av_file_error_msg",
                    value: msg
                })

                if (recId) {
                    self.currentRecord.setCurrentSublistValue({
                        sublistId: "recmachcustrecord_av_file_error_rec",
                        fieldId: "custrecord_av_file_error_rec",
                        value: recId
                    })
                }

                self.currentRecord.commitLine({
                    sublistId: "recmachcustrecord_av_file_error_rec"
                })

                self.currentRecord.setValue("custrecord_av_process_status", CONSTANTS.REC_STATE.ERROR);

            } catch (e) {
                log.debug("Debug", JSON.stringify(e))
            }

        }

        return {
            onAction: onAction
        };

    })