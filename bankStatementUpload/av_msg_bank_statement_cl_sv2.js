/**
** Copyright (c) 2020 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Roberto Cideos
* Date: 18.08.2022
* @Description Bank Statement Payment Validation for Vorauskasse ClientScript
* @NApiVersion 2.1
* @NScriptType ClientScript
*/

define(["N/currentRecord"],

    function (currentRecord) {

        /**
         * Function to be executed after page is initialized.
         *
         * @param {Object} scriptContext
         *
         */

        function uploadXmlFile(scriptContext) {

            return true
        }

        return {
            uploadXmlFile: uploadXmlFile,
            saveRecord: function(){},
            pageInit: function(){}
        };

    });