/**
** Copyright (c) 2020 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Roberto Cideos
* Date: 18.08.2022
* @Description Bank Statement Payment Validation for Vorauskasse
* @NApiVersion 2.1
* @NScriptType Suitelet
* @NModuleScope SameAccount
*/

define(['N/ui/serverWidget',
    'N/record',
    //'./av_fam_invoice_mapping_cfg_sv2.js',
    'N/runtime',
    'N/search',
    'N/file',
    'N/xml',
    'N/redirect'],

    function (serverWidget,
        record,
        //invoiceFieldCfg,
        runtime,
        search,
        file,
        xml,
        redirect) {

        const CONSTANTS = {
            CLIENT_SCRIPT_MODULE_PATH: "SuiteScripts/bankStatementUpload/av_msg_bank_statement_cl_sv2.js",
            UPLOAD_FOLDER: 337,
            FORM_SETUP: {
                TITLE: "Bank Statement Payment Upload"
            },
            PAYMENT_REC_TYPE: "735"
        }

        /**
         * Definition of the Suitelet script trigger point.
         *
         * @param {Object} context
         * @param {ServerRequest} context.request - Encapsulation of the incoming request
         * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
         * @Since 2015.2
         */
        function onRequest(context) {

            var scriptObj = runtime.getCurrentScript();

            let form = serverWidget.createForm({
                title: CONSTANTS.FORM_SETUP.TITLE
            })

            form.addField({
                id: "custpage_file",
                type: serverWidget.FieldType.FILE,
                label: "File",
                source: "file"
            })

            /*form.addField({
                id: "custpage_subsidiary",
                label: "Subsidiary",
                type: serverWidget.FieldType.SELECT,
                source: "subsidiary"
            })*/

            form.clientScriptModulePath = CONSTANTS.CLIENT_SCRIPT_MODULE_PATH

            if (context.request.method == 'GET') {

                form.addSubmitButton({
                    label: "Load XML File"
                })

                context.response.writePage(form)

            } else {

                log.debug("context.request.files", JSON.stringify(context.request.files))

                let processId = _saveAndProcessXml(context);

                log.debug("Record Id", processId)

                form.addSubmitButton({
                    label: "Load XML File"
                })

                redirect.redirect({
                    url: `/app/common/custom/custrecordentrylist.nl?rectype=${CONSTANTS.PAYMENT_REC_TYPE}`
                });


                context.response.writePage(form)

            }

            log.debug("RGU", JSON.stringify(scriptObj.getRemainingUsage()))
        }

        /**
         * 
         * @param {Object} context 
         */

        function _saveAndProcessXml(context) {

            let fileId = _saveXmlFile(context);

            let processId = _processXmlFile(fileId);

            return processId
        }

        /**
         * 
         * @param {Object} context 
         * @returns 
         */

        function _saveXmlFile(context) {

            let fileObj = context.request.files.custpage_file;
            fileObj.folder = CONSTANTS.UPLOAD_FOLDER;
            fileObj.Encoding = file.Encoding.UTF_8;
            return fileObj.save();

        }

        /**
         * 
         * @param {String} fileId 
         */

        function _processXmlFile(fileId) {
            let fileIds = [];
            let fileObj = file.load({
                id: fileId
            })

            let fileContent = fileObj.getContents()

            let fileContentsXml = xml.Parser.fromString({
                text: fileContent
            });

            let creditLines = fileContentsXml.getElementsByTagName("Ntry").length

            for (let j = 0; j < creditLines; j++) {

                let currentCreditLine = fileContentsXml.getElementsByTagName("Ntry")[j];

                let isCreditLine = currentCreditLine.getElementsByTagName("CdtDbtInd")[0].textContent == "CRDT";

                if (!isCreditLine) continue;

                var paymentRec = record.create({
                    type: "customrecord_av_payment_adv_vorauskasse",
                    isDynamic: true
                })

                if (currentCreditLine.getElementsByTagName("BookgDt").length) {
                    let bookLines = currentCreditLine.getElementsByTagName("BookgDt");
                    let bookdDate = bookLines[0].getElementsByTagName("Dt")[0].textContent;

                    if (bookdDate) {
                        paymentRec.setValue({
                            fieldId: "custrecord_av_payment_date",
                            value: new Date(bookdDate)
                        })
                    }
                }

                if (currentCreditLine.getElementsByTagName("NtryDtls").length) {

                    let ntryDetLine = currentCreditLine.getElementsByTagName("NtryDtls");

                    let txDtlsLine = ntryDetLine[0].getElementsByTagName("TxDtls");//TxDtls

                    let amtDtlsLine = txDtlsLine[0].getElementsByTagName("AmtDtls");//AmtDtls

                    let rmtIngLine = txDtlsLine[0].getElementsByTagName("RmtInf"); //RmtInf

                    if (!amtDtlsLine.length) continue;

                    if (!amtDtlsLine[0].getElementsByTagName("InstdAmt")) continue;

                    let amt = amtDtlsLine[0].getElementsByTagName("InstdAmt")[0].getElementsByTagName("Amt")[0].textContent;

                    if (amt) {
                        paymentRec.setValue({
                            fieldId: "custrecord_av_amount_paid",
                            value: amt
                        })
                    }

                    if (!rmtIngLine.length) continue;

                    if (!rmtIngLine[0].getElementsByTagName("Ustrd").length) continue;

                    let ustdr = rmtIngLine[0].getElementsByTagName("Ustrd")[0].textContent;

                    if (ustdr) {

                        paymentRec.setValue({
                            fieldId: "custrecord_av_order_number",
                            value: ustdr
                        });

                        paymentRec.setValue({
                            fieldId: "name",
                            value: ustdr
                        })
                    }
                }

                fileIds.push(paymentRec.save())

            }

        }

        return {
            onRequest: onRequest
        };

    });