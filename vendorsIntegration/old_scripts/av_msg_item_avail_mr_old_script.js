/**
** Copyright (c) 2020 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Roberto Cideos
* Date: 05/07/2022
* Collects files from vendors
* @NApiVersion 2.1
* @NScriptType MapReduceScript
* @NAmdConfig  ./JsLibraryConfig.json
*/

define(['N/search',
    'N/runtime',
    'N/sftp',
    '../libs/av_sftp_lib_sv2',
    'N/file',
    'N/https',
    'N/encode',
    './av_int_log_2_1',
    'N/record',
    'xlsx'],
    function (search,
        runtime,
        sftp,
        settings,
        file,
        https,
        encode,
        avLog,
        record,
        XLSX) {

        var self = this;

        const CONSTANTS = {
            INTERFACE_STATE: {
                NEW: 1,
                ERROR: 2,
                DONE: 3
            },
            HTTP_CODE: {
                SUCCESS: 200,
                ERROR: 400
            },
            VENDOR_DATA_PROCESS: {
                "2": {
                    VENDOR_NAME: "Horizon Hobby",
                    PROCESSING_FUNCTION: _processHorizonFile,
                    SKU_CSV_KEY: "Artikel-Nr",
                    AVAILABILITY_MAP: {
                        AVAIL_CSV_KEY: "Verfügbarkeit",
                        VALS_MAP: {
                            DELIVERY_WITHIN_DAYS: {
                                NS_ID: 3,
                                VENDOR_FILE_VAL: [1, 2]
                            },
                            NOT_ONLINE: {
                                NS_ID: 4,
                                VENDOR_FILE_VAL: [0]
                            }
                        }
                    }
                },
                "4": {
                    VENDOR_NAME: "Globe/DJI",
                    PROCESSING_FUNCTION: _processGlobeFile,
                    SKU_CSV_KEY: "Artikel-Nr",
                    AVAILABILITY_MAP: {
                        AVAIL_CSV_KEY: "Menge Verfügbar",
                        VALS_MAP: {
                            DELIVERY_WITHIN_DAYS: {
                                NS_ID: 3,
                                VENDOR_FILE_VAL: [0]
                            },
                            NOT_ONLINE: {
                                NS_ID: 4,
                                VENDOR_FILE_VAL: []
                            }
                        }
                    }
                },
                "5": {
                    VENDOR_NAME: "Hoeco/Traxxas",
                    PROCESSING_FUNCTION: _processHoecoFile,
                    SKU_CSV_KEY: "Artikel-Nr",
                    AVAILABILITY_MAP: {
                        AVAIL_CSV_KEY: null,
                        VALS_MAP: {
                            DELIVERY_WITHIN_DAYS: {
                                NS_ID: 3,
                                VENDOR_FILE_VAL: [1]
                            },
                            NOT_ONLINE: {
                                NS_ID: 4,
                                VENDOR_FILE_VAL: [0]
                            }
                        }
                    }
                },
                "6": {
                    VENDOR_NAME: "Carson/Tamiya",
                    PROCESSING_FUNCTION: _processCarsonFile,
                    SKU_CSV_KEY: "Art.-Nr.",
                    AVAILABILITY_MAP: {
                        AVAIL_CSV_KEY: "Lagerstatus werkseitig",
                        VALS_MAP: {
                            DELIVERY_WITHIN_DAYS: {
                                NS_ID: 3,
                                VENDOR_FILE_VAL: ["J"]
                            },
                            NOT_ONLINE: {
                                NS_ID: 4,
                                VENDOR_FILE_VAL: ["A", "B", "N"]
                            }
                        }
                    }
                },
                "7": {
                    VENDOR_NAME: "Amewi",
                    PROCESSING_FUNCTION: _processAmewiFile,
                    SKU_CSV_KEY: "Artikel-Nr",
                    AVAILABILITY_MAP: {
                        AVAIL_CSV_KEY: null,
                        VALS_MAP: {
                            DELIVERY_WITHIN_DAYS: {
                                NS_ID: 3,
                                VENDOR_FILE_VAL: [100]
                            },
                            NOT_ONLINE: {
                                NS_ID: 4,
                                VENDOR_FILE_VAL: [0]
                            }
                        }
                    }
                }
            }
        }

        function getInputData() {
            try {
                log.debug("DEBUG", '-Script Started-' + JSON.stringify(new Date()))
                var scriptObj = runtime.getCurrentScript();

                if (runtime.envType == "SANDBOX") {
                    //setup sandbox global variables
                    self.processId = scriptObj.getParameter({ name: 'custscript_av_vendor_configid' });
                    self.fileId = scriptObj.getParameter({ name: 'custscript_av_vendor_file_id' });
                    self.vendorId = scriptObj.getParameter({ name: 'custscript_av_vendor_netsuite_id' });
                    self.iface = scriptObj.getParameter({ name: 'custscript_av_vendor_interface' });
                } else if (runtime.envType == "PRODUCTION") {
                    //setup production global variables
                    self.processId = scriptObj.getParameter({ name: 'custscript_av_vendor_configid' });
                    self.fileId = scriptObj.getParameter({ name: 'custscript_av_vendor_file_id' });
                    self.vendorId = scriptObj.getParameter({ name: 'custscript_av_vendor_netsuite_id' });
                    self.iface = scriptObj.getParameter({ name: 'custscript_av_vendor_interface' });
                }
                self.producerLogId = avLog.create({
                    iface: self.iface,
                    state: CONSTANTS.INTERFACE_STATE.NEW
                })
                record.attach({
                    record: {
                        type: 'file',
                        id: fileId
                    },
                    to: {
                        type: 'customrecord_av_integration_log',
                        id: self.producerLogId
                    }
                });
                // let itemsS = _getItems();
                log.audit('before processing function', '-----');
                let data = CONSTANTS.VENDOR_DATA_PROCESS[self.processId].PROCESSING_FUNCTION(self);
                log.audit('after processing function', `data length: ${data.length}`);
                return data;

            } catch (e) {
                log.debug("Error in connection", JSON.stringify(e))
                return []
            }
        }

        /**
         * 
         * @param {Object} context 
         */

        function map(context) {
            try {
                let { data, producerLogId, vendorId }  = JSON.parse(context.value);
                const itemId = _getItemByVendorItemName(data.vendorName);
                if(itemId) {
                    data.item = itemId;
                    _upsertProducerStock(data, vendorId, producerLogId);
                }
                // record.submitFields({
                //     type: currItem.itemType,
                //     id: currItem.itemId,
                //     values: {
                //         custitem_av_delivery_note: currItem.nsStatusId
                //     }
                // })

                context.write({
                    key: context.key,
                    value: context.value
                })

            } catch (e) {
                log.debug('Error in map', JSON.stringify(e))
            }
        }

        /**
         * 
         * @param {Object} summary 
         */

        function summarize(summary) {
            // var logId;
            var producerLogId;
            var vendorId
            summary.output.iterator().each(function (key, value) {
                // log.debug("Values", JSON.stringify(value.self))
                let params = JSON.parse(value);
                producerLogId  = params.producerLogId;
                vendorId = params.vendorId;
                // logId = JSON.parse(value).self.logId
                return true;
            });
            
            // delete out of date item stock which are not updated by CSV.
            log.debug('vendorId', vendorId);
            if(vendorId) _deleteOutofDateItemStock(vendorId);

            if(producerLogId) {
                avLog.update({
                    id: producerLogId,
                    state: CONSTANTS.INTERFACE_STATE.DONE
                })
            }
            // if (logId) {
            //     avLog.update({
            //         id: logId,
            //         state: CONSTANTS.INTERFACE_STATE.DONE
            //     })
            // }
            log.debug("DEBUG", '-Script Finished-' + JSON.stringify(new Date()))
        }

        /**
         * 
         * @param {SuiteScript Object Search} s 
         * @returns 
         */

        // function _getAllResults(s) {
        //     var results = s.run();
        //     var searchResults = [];
        //     var searchid = 0;
        //     do {
        //         var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
        //         resultslice.forEach(function (slice) {
        //             searchResults.push(slice);
        //             searchid++;
        //         }
        //         );
        //     } while (resultslice.length >= 1000);
        //     return searchResults;
        // }


        // function _getItems() {
        //     let itemsBySku = {};
        //     _getAllResults(search.create({
        //         type: "item",
        //         filters:
        //             [
        //                 ["isinactive", "is", "F"]
        //             ],
        //         columns: ["itemid", "vendorname"]
        //     })).every(function (result) {

        //         itemsBySku[result.getValue("vendorname")] = {
        //             id: result.id,
        //             itemType: result.recordType
        //         }

        //         return true
        //     });

        //     return itemsBySku
        // }

        function _getItemByVendorItemName(vendor) {
            let itemId = null;
            search.create({
                type: "item",
                filters:
                    [
                        ["isinactive", "is", "F"],
                        'AND',
                        ["vendorname", "is", vendor]
                    ],
                columns: ["itemid", "vendorname"]
            }).run().each(function (result) {
                itemId =  result.id;
                return;
            })
            return itemId;
        }

        function _getAllProducerStock(vendorId) {
          let producerStockMap = {};
          const stockSearchObj = search.create({
            type: "customrecord_msg_producer_stock",
            filters: [["custrecord_msg_vsa_vendor", "anyof", vendorId]],
            columns: [
              "custrecord_msg_vsa_item",
              "custrecord_msg_vsa_vendor",
              "custrecord_msg_vsa_availability",
              "custrecord_msg_vsa_quantity",
              "custrecord_msg_vsa_is_available",
              search.createColumn({
                name: "vendorname",
                join: "CUSTRECORD_MSG_VSA_ITEM"
             })
            ],
          });

          let myPagedData = stockSearchObj.runPaged({ pageSize: 1000 });
          myPagedData.pageRanges.forEach(function (pageRange) {
            let myPage = myPagedData.fetch({ index: pageRange.index });
            myPage.data.forEach(function (result) {
              const values = result.toJSON().values;
              const vendorItemName = result.getValue({name:'CUSTRECORD_MSG_VSA_ITEM.vendorname'});
              log.audit('vendorItemName', vendorItemName);
              producerStockMap[vendorItemName] = values
  
            });
          });

          return producerStockMap;
        }


        /**
         * 
         * @param {Object} jsonFromCsv 
         */

        function _upsertProducerStock(productFromCSV, vendorId, producerLogId) {
            try {
                log.debug('_upsertProducerStock', `started for vendor item: ${productFromCSV.vendorName}`);
                // search producer stock
                let producerStockId = _getProducerStock(vendorId, productFromCSV.item);
                if(producerStockId) {
                    let prodId = record.submitFields({
                        type: "customrecord_msg_producer_stock",
                        id: producerStockId,
                        values: {
                            custrecord_msg_vsa_availability:  productFromCSV.avail,
                            custrecord_msg_vsa_quantity: (productFromCSV.quantity || null),
                            custrecord_msg_vsa_is_available:productFromCSV.isAvail
                        }
                    })
                    return prodId ? true : false;
                 } 

           
            const productStock = record.create({
                type: 'customrecord_msg_producer_stock',
            });

              productStock.setValue('custrecord_msg_vsa_item', productFromCSV.item)
              productStock.setValue('custrecord_msg_vsa_vendor', vendorId)
              productStock.setValue('custrecord_msg_vsa_availability', productFromCSV.avail)
              productStock.setValue('custrecord_msg_vsa_quantity', (productFromCSV.quantity || null))
              productStock.setValue('custrecord_msg_vsa_is_available', productFromCSV.isAvail)
 
              let newProdId = productStock.save({
                enableSourcing: false,
                ignoreMandatoryFields: true
              });
             
              return newProdId ? true : false;
             } catch(ex) {
                log.debug('error in _upsertProducerStock', JSON.stringify(ex));
                avLog.update({
                    id: producerLogId,
                    responseCode: CONSTANTS.HTTP_CODE.ERROR,
                    state: CONSTANTS.INTERFACE_STATE.ERROR,
                    receive: JSON.stringify(ex),
                })
                avLog.error_message({
                    logid: producerLogId,
                    msg: CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR,
                    var1: JSON.stringify(ex),
                })
             }
        }

        function _deleteOutofDateItemStock(vendorId) {
            try {
                log.debug('_deleteOutofDateItemStock', 'Deleting outofdate producer stock, that are no longer sent in the file.');
                let count = 0;
                search.create({
                    type: "customrecord_msg_producer_stock",
                    filters:
                    [   
                        ["custrecord_msg_vsa_vendor", "anyof", vendorId],
                        "AND",
                        ["lastmodified","noton","today"]
                    ],
                    columns: []
                }).run().each(function (result) {
                    log.debug('result', result);
                    if(result && result.id) {
                        count++;
                        record.delete({
                            type: "customrecord_msg_producer_stock",
                            id: result.id
                        });
                    }
                })
                log.debug('_deleteOutofDateItemStock', `${count} records deleted.`);
             } catch(ex) {
                log.debug('error in _deleteItemStock', JSON.stringify(ex));
             }
        }


        function _processHorizonFile() {
            log.debug("_processHorizonFile")
            // let csvSkus = [];
            let producerStocks = [];
            let fileObj = file.load({
                id: self.fileId
            });
            let configObj = CONSTANTS.VENDOR_DATA_PROCESS[self.processId];

            /*Starts: parse through XLSX lib*/
            let workbook = XLSX.read(fileObj.getContents(), { type: "base64" });
            let worksheet = workbook.Sheets[workbook.SheetNames[0]];
            let jsonFromCsv = XLSX.utils.sheet_to_json(worksheet);
            /*Ends: parse through XLSX lib*/

            log.debug('start of loop ---');
            for (let i = 0; i < jsonFromCsv.length; i++) { //we skip headers

                // if (!itemsBySku[jsonFromCsv[i][configObj.SKU_CSV_KEY]]) continue;

                const producerStockObj = {
                    vendorName: jsonFromCsv[i][configObj.SKU_CSV_KEY],
                    avail: jsonFromCsv[i][configObj.AVAILABILITY_MAP.AVAIL_CSV_KEY],
                    isAvail: configObj.AVAILABILITY_MAP.VALS_MAP.DELIVERY_WITHIN_DAYS.VENDOR_FILE_VAL.includes(jsonFromCsv[i][configObj.AVAILABILITY_MAP.AVAIL_CSV_KEY])
                }
                producerStocks.push({data: producerStockObj, producerLogId: self.producerLogId, vendorId: self.vendorId });
                // producerStocks.push({data: producerStockObj, self});
             
                // const producerStockResult = _upsertProducerStock(producerStockObj);
                // anyErrorsProducerStock = !producerStockResult ? true : anyErrorsProducerStock;

                // commenting update item master
                // let skuAvail = {
                //     sku: jsonFromCsv[i][configObj.SKU_CSV_KEY]
                // };

                // let lineAvailId = Number(jsonFromCsv[i][configObj.AVAILABILITY_MAP.AVAIL_CSV_KEY]); // availability status of vendor [value]

                // for (availStatus in configObj.AVAILABILITY_MAP.VALS_MAP) {
                //     if (configObj.AVAILABILITY_MAP.VALS_MAP[availStatus].VENDOR_FILE_VAL.indexOf(lineAvailId) != -1) {
                //         skuAvail.nsStatusId = configObj.AVAILABILITY_MAP.VALS_MAP[availStatus].NS_ID;
                //         skuAvail.itemId = itemsBySku[jsonFromCsv[i][configObj.SKU_CSV_KEY]].id;
                //         skuAvail.itemType = itemsBySku[jsonFromCsv[i][configObj.SKU_CSV_KEY]].itemType;
                //         csvSkus.push(skuAvail);
                //     }
                // }
            }
            log.debug('end of loop ---');
            // log.debug('total stock lines', producerStocks.length);
            // log.debug("Stock lines in CSV", JSON.stringify(jsonFromCsv))
            // log.debug("Skus to update", JSON.stringify(csvSkus))
            // return csvSkus
            return producerStocks;
        }

        function _processGlobeFile() {
            log.debug("_processGlobeFile")
            let csvSkusLines = [], producerStocks = [], csvLines = [];
            // let csvSkus = [];
            let fileObj = file.load({
                id: self.fileId
            });
            let configObj = CONSTANTS.VENDOR_DATA_PROCESS[self.processId];

            fileObj.lines.iterator().each(function (line) {
                csvLines.push(line);
                line.value.replace(/","/g, ".");
                let lineVal = line.value.split(";");
                csvSkusLines.push(lineVal);
                return true;
            });

            for (let i = 1; i < csvSkusLines.length; i++) {
                let csvLine = csvSkusLines[i][0].split(";")
                let lineSku = csvLine[0];
                let lineAvailId = csvSkusLines[i][4];
                let skuAvail = {
                    sku: lineSku
                };

                const producerStockObj = {
                    vendorName: lineSku,
                    avail: lineAvailId,
                    isAvail: Number(lineAvailId) ? true : false
                }

                producerStocks.push({data: producerStockObj, producerLogId: self.producerLogId, vendorId: self.vendorId });


                // if (!itemsBySku[lineSku]) continue;

                // if (!Number(lineAvailId)) {
                //     skuAvail.nsStatusId = configObj.AVAILABILITY_MAP.VALS_MAP.NOT_ONLINE.NS_ID;
                //     skuAvail.itemId = itemsBySku[lineSku].id;
                //     skuAvail.itemType = itemsBySku[lineSku].itemType;
                // } else {
                //     skuAvail.nsStatusId = configObj.AVAILABILITY_MAP.VALS_MAP.DELIVERY_WITHIN_DAYS.NS_ID;
                //     skuAvail.itemId = itemsBySku[lineSku].id;
                //     skuAvail.itemType = itemsBySku[lineSku].itemType;
                //     csvSkus.push(skuAvail);
                // }
            }

            log.debug("Stock lines in CSV", JSON.stringify(csvSkusLines))
            // log.debug("Skus to update", JSON.stringify(csvSkus))

            return producerStocks

        }

        function _processHoecoFile() {

            log.debug("_processHoecoFile");
            let csvSkusLines = [], csvSkus = [], csvLines = [];
            let producerStocks = [];
            let fileObj = file.load({
                id: self.fileId
            });
            let configObj = CONSTANTS.VENDOR_DATA_PROCESS[self.processId];

            fileObj.lines.iterator().each(function (line) {
                csvLines.push(line);
                line.value.replace(/","/g, ".");
                let lineVal = line.value.split(";");
                csvSkusLines.push(lineVal);
                return true;
            });

            for (let i = 0; i < csvSkusLines.length; i++) {
                let csvLine = csvSkusLines[i][0].split(";")
                let lineSku = csvLine[0];
                let lineAvailId = Number(csvSkusLines[i][2]);
                let skuAvail = {
                    sku: lineSku
                };

                const producerStockObj = {
                    vendorName: lineSku,
                    avail: lineAvailId,
                    isAvail: Number(lineAvailId) ? true : false
                }
                producerStocks.push({data: producerStockObj, producerLogId: self.producerLogId, vendorId: self.vendorId });


                // if (!itemsBySku[lineSku]) continue;

                // log.debug("Debug", JSON.stringify({
                //     sku: lineSku,
                //     status: lineAvailId
                // }))

                // for (availStatus in configObj.AVAILABILITY_MAP.VALS_MAP) {
                //     if (configObj.AVAILABILITY_MAP.VALS_MAP[availStatus].VENDOR_FILE_VAL.indexOf(lineAvailId) != -1) {
                //         skuAvail.nsStatusId = configObj.AVAILABILITY_MAP.VALS_MAP[availStatus].NS_ID;
                //         skuAvail.itemId = itemsBySku[lineSku].id;
                //         skuAvail.itemType = itemsBySku[lineSku].itemType;
                //         csvSkus.push(skuAvail);
                //     }
                // }
            }

            // log.debug("Stock lines in CSV", JSON.stringify(csvSkusLines))
            // log.debug("Skus to update", JSON.stringify(csvSkus))
            log.debug('total stock lines', producerStocks.length);
            return producerStocks;

        }

        function _processCarsonFile() {
            log.debug("_processCarsonFile")
            let csvSkusLines = [], csvSkus = [], csvLines = [];
            let producerStocks = [];
            let fileObj = file.load({
                id: self.fileId
            });
            let configObj = CONSTANTS.VENDOR_DATA_PROCESS[self.processId];

            fileObj.lines.iterator().each(function (line) {
                csvLines.push(line);
                line.value.replace(/","/g, ".");
                let lineVal = line.value.split(";");
                csvSkusLines.push(lineVal);
                return true;
            });

            for (let i = 1; i < csvSkusLines.length; i++) {
                let csvLine = csvSkusLines[i][0].split(";")
                let lineSku = csvLine[0];
                let lineAvailId = csvSkusLines[i][5];
                let skuAvail = {
                    sku: lineSku
                };

                const producerStockObj = {
                    vendorName: lineSku,
                    avail: lineAvailId,
                    isAvail: configObj.AVAILABILITY_MAP.VALS_MAP.DELIVERY_WITHIN_DAYS.VENDOR_FILE_VAL.includes(lineAvailId) ? true : false
                }
                producerStocks.push({data: producerStockObj, producerLogId: self.producerLogId, vendorId: self.vendorId });

                // if (!itemsBySku[lineSku]) continue;

                // for (availStatus in configObj.AVAILABILITY_MAP.VALS_MAP) {
                //     if (configObj.AVAILABILITY_MAP.VALS_MAP[availStatus].VENDOR_FILE_VAL.indexOf(lineAvailId) != -1) {
                //         skuAvail.nsStatusId = configObj.AVAILABILITY_MAP.VALS_MAP[availStatus].NS_ID;
                //         skuAvail.itemId = itemsBySku[lineSku].id;
                //         skuAvail.itemType = itemsBySku[lineSku].itemType;
                //         csvSkus.push(skuAvail);
                //     }
                // }
            }

            log.debug("Stock lines in CSV", JSON.stringify(csvSkusLines))
            // log.debug("Skus to update", JSON.stringify(csvSkus))

            return producerStocks
        }
        
        function _processAmewiFile() {

            log.debug("_processAmewiFile")
            let csvSkusLines = [], csvSkus = [], csvLines = [];
            let producerStocks = [];
            let fileObj = file.load({
                id: self.fileId
            });
            let configObj = CONSTANTS.VENDOR_DATA_PROCESS[self.processId];

            fileObj.lines.iterator().each(function (line) {
                csvLines.push(line);
                line.value.replace(/","/g, ".");
                let lineVal = line.value.split(";");
                csvSkusLines.push(lineVal);
                return true;
            });

            for (let i = 0; i < csvSkusLines.length; i++) {
                let csvLine = csvSkusLines[i][0].split(";")
                let lineSku = csvLine[0];
                let lineAvailId = Number(csvSkusLines[i][1]);
                let skuAvail = {
                    sku: lineSku
                };

                const producerStockObj = {
                    vendorName: lineSku,
                    avail: lineAvailId,
                    isAvail: configObj.AVAILABILITY_MAP.VALS_MAP.DELIVERY_WITHIN_DAYS.VENDOR_FILE_VAL.includes(lineAvailId) ? true : false
                }
                producerStocks.push({data: producerStockObj, producerLogId: self.producerLogId, vendorId: self.vendorId });

                // if (!itemsBySku[lineSku]) continue;

                // for (availStatus in configObj.AVAILABILITY_MAP.VALS_MAP) {
                //     if (configObj.AVAILABILITY_MAP.VALS_MAP[availStatus].VENDOR_FILE_VAL.indexOf(lineAvailId) != -1) {
                //         skuAvail.nsStatusId = configObj.AVAILABILITY_MAP.VALS_MAP[availStatus].NS_ID;
                //         skuAvail.itemId = itemsBySku[lineSku].id;
                //         skuAvail.itemType = itemsBySku[lineSku].itemType;
                //         csvSkus.push(skuAvail);
                //     }
                // }

                // return csvSkus
            }

            log.debug("Stock lines in CSV", JSON.stringify(csvSkusLines))
            // log.debug("Skus to update", JSON.stringify(csvSkus))

            return producerStocks;

        }


        return {
            getInputData: getInputData,
            map: map,
            summarize: summarize
        };

    })