/**
** Copyright (c) 2020 Alta Via Consulting GmbH
** All Rights Reserved.
**
** This software is the confidential and proprietary information of
** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
** such Confidential Information and shall use it only in accordance
** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
*
* Author: Roberto Cideos
* Date: 05/07/2022
* Collects files from vendors
* @NApiVersion 2.1
* @NScriptType MapReduceScript
*/

define(['N/search',
    'N/runtime',
    'N/sftp',
    '../libs/av_sftp_lib_sv2',
    'N/file',
    'N/https',
    'N/encode',
    './av_int_log_2_1',
    'N/record',
    'N/task'],
    function (search,
        runtime,
        sftp,
        settings,
        file,
        https,
        encode,
        avLog,
        record,
        task) {

        var self = this;

        const CONSTANTS = {
            INTERFACE_STATE: {
                NEW: 1,
                ERROR: 2,
                DONE: 3
            },
            FILE_PROCESSING_SCRIPT_ID: {
                ID: "customscript_av_item_avail_mr"
            }
        }

        function getInputData() {
            try {
                log.debug("DEBUG", '-Script Started-' + JSON.stringify(new Date()))
                var scriptObj = runtime.getCurrentScript();

                var configId = self.configId = scriptObj.getParameter({ name: 'custscript_av_vendor_config_id' });

                if (runtime.envType == "SANDBOX") {
                    //setup sandbox global variables
                    self.config = settings._getConfigObj(configId, null, false);
                    self.inboundFolderId = scriptObj.getParameter({ name: 'custscript_av_vendor_folder_id' });
                    self.iface = scriptObj.getParameter({ name: 'custscript_av_vendor_iface' });
                    self.isFtp = scriptObj.getParameter({ name: 'custscript_av_vendor_is_ftp' });
                    self.filePrefix = scriptObj.getParameter({ name: 'custscript_av_vendor_prefix' });
                    self.inputEncode = scriptObj.getParameter({ name: 'custscript_av_vendor_encode_format' });
                    self.vendorId = scriptObj.getParameter({ name: 'custscript_av_vendor_id' });

                } else if (runtime.envType == "PRODUCTION") {
                    //setup production global variables
                    self.config = settings._getConfigObj(configId, null, false);
                    self.inboundFolderId = scriptObj.getParameter({ name: 'custscript_av_vendor_folder_id' });
                    self.iface = scriptObj.getParameter({ name: 'custscript_av_vendor_iface' });
                    self.isFtp = scriptObj.getParameter({ name: 'custscript_av_vendor_is_ftp' });
                    self.filePrefix = scriptObj.getParameter({ name: 'custscript_av_vendor_prefix' });
                    self.inputEncode = scriptObj.getParameter({ name: 'custscript_av_vendor_encode_format' })
                    self.vendorId = scriptObj.getParameter({ name: 'custscript_av_vendor_id' });

                }

                self.logId = avLog.create({
                    iface: self.iface,
                    url: `${self.config.custrecord_av_integration_base_url}${self.config.custrecord_av_integration_stock_patch}`,
                    state: CONSTANTS.INTERFACE_STATE.NEW
                })

                return self.isFtp ? _getFilesFromServer(self) : _getFilesFromEndpoint(self)
            } catch (e) {
                log.debug("Error in connection", JSON.stringify(e))
                return []
            }
        }

        function map(context) {
            try {
                let fileContent = JSON.parse(context.value), encodedStr;

                let encodingObj = {
                    "encode.Encoding.BASE_64": encode.Encoding.BASE_64,
                    "encode.Encoding.UTF_8": encode.Encoding.UTF_8
                }

                if (fileContent.fileType != file.Type.EXCEL) {
                    if (encodingObj[fileContent.self.inputEncode] != encode.Encoding.UTF_8) {
                        encodedStr = encode.convert({
                            string: fileContent.fileCtn,
                            inputEncoding: encodingObj[fileContent.self.inputEncode],
                            outputEncoding: encode.Encoding.UTF_8
                        });
                    } else {
                        encodedStr = fileContent.fileCtn
                    }
                } else {
                    encodedStr = fileContent.fileCtn
                }

                let today = new Date();
                let params = {
                    name: `${fileContent.self.filePrefix}_Stock_${today.getDate()}_${today.getMonth() + 1}_${today.getFullYear()}`,
                    fileType: fileContent.fileType || file.Type.CSV,
                    contents: encodedStr,
                    folder: fileContent.self.inboundFolderId
                };

                log.debug("Params", JSON.stringify(params))

                let fileId = file.create(params).save();

                if (fileId) {

                    record.attach({
                        record: {
                            type: 'file',
                            id: fileId
                        },
                        to: {
                            type: 'customrecord_av_integration_log',
                            id: fileContent.self.logId
                        }
                    });

                    let params = {
                        "custscript_av_vendor_configid": fileContent.self.configId,
                        "custscript_av_vendor_file_id": fileId,
                        "custscript_av_vendor_netsuite_id": fileContent.self.vendorId,
                        "custscript_av_vendor_interface": fileContent.self.iface
                    };

                    let scriptTask = task.create({
                        taskType: task.TaskType.MAP_REDUCE,
                        scriptId: CONSTANTS.FILE_PROCESSING_SCRIPT_ID.ID,
                        params: params
                    });

                    let taskId = scriptTask.submit();
                    log.debug(`Processing File Script Triggered for ${fileContent.self.filePrefix}`, JSON.stringify({
                        taskId: taskId,
                        params: params
                    }))
                }

                context.write({
                    key: context.key,
                    value: context.value
                })

            } catch (e) {
                log.debug('Error saving file', JSON.stringify(e))
            }


        }

        function summarize(summary) {
            var logId;

            summary.output.iterator().each(function (key, value) {
                log.debug("Values", JSON.stringify(value))
                logId = JSON.parse(value).self.logId
                return true;
            });

            if (logId) {
                avLog.update({
                    id: logId,
                    state: CONSTANTS.INTERFACE_STATE.DONE
                })
            }
            log.debug("DEBUG", '-Script Finished-' + JSON.stringify(new Date()))
        }

        /**
         * 
         * @param {Object} self 
         */

        function _getFilesFromServer(self) {
            log.debug("self", JSON.stringify(self))
            var connectionObj = sftp.createConnection({
                username: self.config.custrecord_av_integration_username,
                passwordGuid: self.config.custrecord_av_integration_sftp_guid,
                url: self.config.custrecord_av_integration_base_url,
                directory: self.config.custrecord_av_integration_sftp_out,
                hostKey: self.config.custrecord_av_integration_sftp_hkey,
                port: 2222,
                hostKeyType: 'rsa'
            });

            self.fileNamesFromServer = [];
            var filesFromServer = [];
            var files = [];
            var i;
            self.counter = 0;

            var listOfFiles = connectionObj.list();

            log.debug("Files", JSON.stringify(listOfFiles))

            for (i = 0; i < listOfFiles.length; i++) {
                files.push(listOfFiles[i]);
            }

            var documentsArray = [], downloadedFileObj;

            var documentsSearch = _getAllResults(search.create({
                type: 'file',
                columns: ['name'],
                filters: ['folder', 'is', self.inboundFolderId]
            }))


            documentsSearch.every(function (result) {

                documentsArray.push(result.getValue('name'));

                return true

            })

            files.forEach(function (fileId) {
                if (fileId.size > 0 && documentsArray.indexOf(fileId.name) == -1) {
                    var downloadFile = connectionObj.download({
                        filename: fileId.name
                    });

                    downloadedFileObj = {
                        fileName: fileId.name,
                        fileContents: downloadFile.getContents(),
                        folderId: self.inboundFolderId,
                        logId: self.logId,
                        iface: self.iface,
                        credentials: self.credentials
                    }

                }

            })

            return [{
                fileCtn: downloadedFileObj.fileContents,
                self: self,
                fileType: file.Type.EXCEL
            }]
        }

        /**
         * 
         * @param {Object} self 
         */

        function _getFilesFromEndpoint(self) {
            var headers;
            if (self.config.custrecord_av_integration_auth_req) {
                headers = {
                    "Authorization": self.config.custrecord_av_integration_auth_token
                }
            }

            var req = https.request({
                method: https.Method.GET,
                url: `${self.config.custrecord_av_integration_base_url}${self.config.custrecord_av_integration_stock_patch}`,
                headers: headers
            })

            return [{
                fileCtn: req.body,
                self: self
            }]
        }

        /**
         * 
         * @param {SuiteScript Object Search} s 
         * @returns 
         */

        function _getAllResults(s) {
            var results = s.run();
            var searchResults = [];
            var searchid = 0;
            do {
                var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
                resultslice.forEach(function (slice) {
                    searchResults.push(slice);
                    searchid++;
                }
                );
            } while (resultslice.length >= 1000);
            return searchResults;
        }

        return {
            getInputData: getInputData,
            map: map,
            summarize: summarize
        };

    })