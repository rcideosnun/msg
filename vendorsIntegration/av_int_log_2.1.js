/**
 * av_int_log.js
 * @NApiVersion 2.1
 *
 * ** Copyright (c) 2018 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ("Confidential Information"). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 **
 ** @version: 1.0
 ** @author: Peter von Zimmermann
 ** @dated: 14/08/2018
 *
 *  2.0 Intergration Log
 *
 *  *  Functions:
 *  1.  create
 *  creates a new log record
 *
 *  2. update
 *  updates existing log provided with id with all fields provided
 *
 *  3. error message
 *  creates an error message for the log record, error has to be created in custom record
 *  customrecord_av15_error_messages with the name provided under msg
 *
 *  Save log and error record
 *
 *  Log Parameter Object example
 *  {send: <senddata>,
 *   receive : <receivedata>,
 *   id : internal id of log record in case of update
 *   responsecode : 200,
 *   state : <1 = new, 2 = error, 3 = done>,
 *   entity : <internal id of releated entity (customer, vendor...)>,
 *   transaction : <internal id of releated transaction>,
 *   item : <internal id of releated item>,
 *   url : <url of call>
 *  }
 *
 *  Error Parameter Object example
 *  { msg: <name of the error from customrecord_av15_error_messages (has to exist)>,
 *   logid : <internal id of releated log record>,
 *   var1 : 'varibale 1 of error, free text',
 *   var2 : 'varibale 2 of error, free text',
 *   transaction : <internal id of releated transaction>
 *  }
 *
 */

 define(['N/record', 'N/search'], function (record, search) {

    function create(log) {
        if (log) {
            var rec = record.create({
                type: 'customrecord_av_integration_log',
                isDynamic: true
            });
            return modify(rec, log);
        }
    }

    function update(log) {
        if (log && log.id) {
            var rec = record.load({
                type: 'customrecord_av_integration_log',
                id: log.id,
                isDynamic: true
            });
            return modify(rec, log);
        }
    }

    function log_error(error) {

        var answer = error_message(error);

        if (!error.logid) {
            create({
                transaction: error.transaction,
                send: answer,
                state: 3
            })
        } else {
            update({
                id: error.logid,
                transaction: error.transaction,
                send: answer,
                state: 3
            })
        }

        return answer;

    }

    function modify(rec, log) {

        rec = setText(rec, 'custrecord_av_integration_type', log.iface);
        rec = setLongtext(rec, 'custrecord_av_integration_send', log.send);
        rec = setLongtext(rec, 'custrecord_av_integration_received', log.receive);
        rec = setValue(rec, 'custrecord_av_integration_code', log.responseCode);
        rec = setValue(rec, 'custrecord_av_integration_staging', log.staging);
        rec = setValue(rec, 'custrecord_av_integration_state', log.state);
        rec = setValue(rec, 'custrecord_av_integration_entity', log.entity);
        rec = setValue(rec, 'custrecord_av_integration_transaction', log.transaction);
        rec = setValue(rec, 'custrecord_av_integration_item', log.item);
        rec = setValue(rec, 'custrecord_av_integration_url', log.url);
        rec = setValue(rec, 'custrecord_av_integration_brand', log.brand);
        rec = setValue(rec, 'custrecord_av_integration_gc', log.gc);


        return rec.save();

    }

    function setValue(rec, fieldId, value) {
        if (rec && fieldId && value) {
            rec.setValue({
                fieldId: fieldId,
                value: value
            });
        }
        return rec;
    }

    function setLongtext(rec, fieldId, value) {
        if (value) {
            if (typeof value == 'object') {
                value = JSON.stringify(value);
            }
            value = value.substr(0, 100000);
            setValue(rec, fieldId, value);
        }
        return rec;
    }

    function setText(rec, fieldId, value) {
        if (rec && fieldId && value) {
            rec.setText({
                fieldId: fieldId,
                text: value
            });
        }
        return rec;
    }

    function error_message(error) {
        log.debug("Error", JSON.stringify(error))
        if (error && error.var1) {
            var em = record.create({
                type: 'customrecord_av_interface_errormessage',
                isDynamic: true
            });
            if (error.logid) {
                em.setValue('custrecord_av_if_er_log', error.logid);
            }
            if (error.iface) {
                em.setValue('custrecord_av_if_er_type', error.iface);
            }
            if (error.msg) {
                em.setText('custrecord_av_if_er_msg', error.msg.toString().substr(0, 300));
            }
            if (error.var1) {
                em.setValue('custrecord_av_if_er_var1', error.var1.toString().substr(0, 300));
            }
            if (error.var2) {
                em.setValue('custrecord_av_if_er_var2', error.var2.toString().substr(0, 300));
            }
            if (error.staging) {
                em.setValue('custrecord_av_if_er_sw', error.staging);
            }
            var eid = em.save();
        }

        return {
            error: error.msg,
            msg: error.msg,
            var2: error.var2
        };

    }



    return {
        create: create,
        update: update,
        error_message: error_message
    };
});