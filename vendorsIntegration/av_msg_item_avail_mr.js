/**
 ** Copyright (c) 2020 Alta Via Consulting GmbH
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of
 ** Alta Via Consulting GmbH ('Confidential Information'). You shall not disclose
 ** such Confidential Information and shall use it only in accordance
 ** with the terms of the license agreement you entered into with Alta Via Consulting GmbH.
 *
 * Author: Roberto Cideos
 * Date: 05/07/2022
 * Collects files from vendors
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 * @NAmdConfig  ./JsLibraryConfig.json
 */
define([
  "N/search",
  "N/runtime",
  "N/file",
  "./av_int_log_2_1",
  "N/record",
  "xlsx",
], function (
  search,
  runtime,
  file,
  avLog,
  record,
  XLSX
) {
  var self = this;

  const CONSTANTS = {
    INTERFACE_STATE: {
      NEW: 1,
      ERROR: 2,
      DONE: 3,
    },
    HTTP_CODE: {
      SUCCESS: 200,
      ERROR: 400,
    },
    VENDOR_DATA_PROCESS: {
      2: {
        VENDOR_NAME: "Horizon Hobby",
        PROCESSING_FUNCTION: _processHorizonFile,
        SKU_CSV_KEY: "Artikel-Nr",
        AVAILABILITY_MAP: {
          AVAIL_CSV_KEY: "Verfügbarkeit",
          ONLINE: [1, 2],
          NOT_ONLINE: [0],
        },
      },
      4: {
        VENDOR_NAME: "Globe/DJI",
        PROCESSING_FUNCTION: _processGlobeFile,
        SKU_CSV_KEY: "Artikel-Nr",
        QTY_KEY: "Menge Verfuegbar",
        AVAILABILITY_MAP: {
          AVAIL_CSV_KEY: "Menge Verfuegbar",
          ONLINE: [],
          NOT_ONLINE: [0],
        },
      },
      5: {
        VENDOR_NAME: "Hoeco/Traxxas",
        PROCESSING_FUNCTION: _processHoecoFile,
        SKU_CSV_KEY: "Artikel-Nr",
        QTY_KEY: null,
        AVAILABILITY_MAP: {
          AVAIL_CSV_KEY: null,
          ONLINE: [],
          NOT_ONLINE: [0],
        },
      },
      6: {
        VENDOR_NAME: "Carson/Tamiya",
        PROCESSING_FUNCTION: _processCarsonFile,
        SKU_CSV_KEY: "Art.-Nr.",
        AVAILABILITY_MAP: {
          AVAIL_CSV_KEY: "Lagerstatus werkseitig",
          ONLINE: ["J"],
          NOT_ONLINE: ["A", "B", "N"],
        },
      },
      7: {
        VENDOR_NAME: "Amewi",
        PROCESSING_FUNCTION: _processAmewiFile,
        SKU_CSV_KEY: "Artikel-Nr",
        AVAILABILITY_MAP: {
          AVAIL_CSV_KEY: null,
          ONLINE: [100],
          NOT_ONLINE: [0],
        },
      },
    },
  };

  function getInputData() {
    try {
      log.debug("DEBUG", "-Script Started-" + JSON.stringify(new Date()));
      const scriptObj = runtime.getCurrentScript();
      self.processId = scriptObj.getParameter({ name: "custscript_av_vendor_configid" });
      self.fileId = scriptObj.getParameter({ name: "custscript_av_vendor_file_id" });
      self.vendorId = scriptObj.getParameter({  name: "custscript_av_vendor_netsuite_id"});
      self.iface = scriptObj.getParameter({ name: "custscript_av_vendor_interface" });

      self.producerLogId = avLog.create({
        iface: self.iface,
        state: CONSTANTS.INTERFACE_STATE.NEW,
      });
      record.attach({
        record: {
          type: "file",
          id: fileId,
        },
        to: {
          type: "customrecord_av_integration_log",
          id: self.producerLogId,
        },
      });

      log.debug(`Processing Script Triggered for ${CONSTANTS.VENDOR_DATA_PROCESS[self.processId].VENDOR_NAME}`, JSON.stringify({
        vendorId: self.vendorId,
        fileId: self.fileId
      }))

      let data = CONSTANTS.VENDOR_DATA_PROCESS[self.processId].PROCESSING_FUNCTION(self);
      return data;
    } catch (e) {
      log.error("Error in connection", e);
      return [];
    }
  }

  /**
   *
   * @param {Object} context
   */
  function map(context) {
    try {
      const { data, producerLogId, vendorId, toDelete } = JSON.parse( context.value );
      if(!toDelete) {
        let itemId = null;
        const prodStockInternalId = data.producerStockId;
        if(!prodStockInternalId) {
          itemId = _getItemByVendorItemName(data.vendorName);
          data.item  = itemId;
         }

       if(prodStockInternalId || itemId) {
         _upsertProducerStock(data, vendorId, producerLogId, prodStockInternalId);
       }
      } else {
       _deleteOutofDateItemStock([data.id]);
      }
     context.write({
      key: producerLogId,
     })
    } catch (e) {
      log.error("Error in map", e);
    }
  }

  /**
   *
   * @param {Object} summary
   */

  function summarize(summary) {
    let producerLogId;

    summary.output.iterator().each(function (key, value) {
        producerLogId = key;
        return false;
    });

    if (producerLogId) {
      avLog.update({
        id: producerLogId,
        state: CONSTANTS.INTERFACE_STATE.DONE,
      });
    }

    log.debug("DEBUG", "-Script Finished-" + JSON.stringify(new Date()));
  }

  function _getItemByVendorItemName(vendor) {
    let itemId = null;
    search.create({
        type: "item",
        filters: [
          ["isinactive", "is", "F"],
          "AND",
          [
            ["vendorname","is",vendor], 
              "OR", 
            ["custitem_av_vendor_item_number","is", vendor]
          ],
        ],
        columns: ["itemid", "vendorname", "custitem_av_vendor_item_number"],
      }).run().each(function (result) {
        itemId = result.id;
        return;
      });

    return itemId;
  }

  function _getAllProducerStock(vendorId) {
    let producerStockMap = {};
    const stockSearchObj = search.create({
      type: "customrecord_msg_producer_stock",
      filters: [["custrecord_msg_vsa_vendor", "anyof", vendorId]],
      columns: [
        "custrecord_msg_vsa_item",
        "custrecord_msg_vsa_vendor",
        "custrecord_msg_vsa_availability",
        "custrecord_msg_vsa_quantity",
        "custrecord_msg_vsa_is_available",
        search.createColumn({
          name: "vendorname",
          join: "CUSTRECORD_MSG_VSA_ITEM",
        }),
        search.createColumn({
          name: "custitem_av_vendor_item_number",
          join: "CUSTRECORD_MSG_VSA_ITEM",
       })
      ],
    });

    let myPagedData = stockSearchObj.runPaged({ pageSize: 1000 });
    myPagedData.pageRanges.forEach(function (pageRange) {
      let myPage = myPagedData.fetch({ index: pageRange.index });
      myPage.data.forEach(function (result) {
          const values = result.toJSON().values;
          const custVendorItemName = values["CUSTRECORD_MSG_VSA_ITEM.custitem_av_vendor_item_number"];
          const DefaultVendorItemName = values["CUSTRECORD_MSG_VSA_ITEM.vendorname"];
          const vendorItemName = DefaultVendorItemName || custVendorItemName;
          producerStockMap[vendorItemName] = {
            data: values,
            id: result.id,
          };
      });
    });

    return producerStockMap;
  }

  /**
   *
   * @param {Object} jsonFromCsv
   */

  function _upsertProducerStock( productFromCSV, vendorId, producerLogId, producerStockId) {
    try {
      if (producerStockId) {
        record.submitFields({
          type: "customrecord_msg_producer_stock",
          id: producerStockId,
          values: {
            custrecord_msg_vsa_availability: productFromCSV.avail,
            custrecord_msg_vsa_quantity: productFromCSV.qty,
            custrecord_msg_vsa_is_available: productFromCSV.isAvail,
          },
        });
        return;
      }
      const productStock = record.create({
        type: "customrecord_msg_producer_stock",
      });

      productStock.setValue("custrecord_msg_vsa_item", productFromCSV.item);
      productStock.setValue("custrecord_msg_vsa_vendor", vendorId);
      productStock.setValue("custrecord_msg_vsa_availability", productFromCSV.avail);
      productStock.setValue("custrecord_msg_vsa_quantity", productFromCSV.qty);
      productStock.setValue("custrecord_msg_vsa_is_available", productFromCSV.isAvail);

      productStock.save({
        enableSourcing: false,
        ignoreMandatoryFields: true,
      });

      return;
    } catch (ex) {
      log.error("error in _upsertProducerStock", ex);
      avLog.update({
        id: producerLogId,
        responseCode: CONSTANTS.HTTP_CODE.ERROR,
        state: CONSTANTS.INTERFACE_STATE.ERROR,
        receive: JSON.stringify(ex),
      });
      avLog.error_message({
        logid: producerLogId,
        msg: CONSTANTS.ERROR_MESSAGES.INTERNAL_ERROR,
        var1: JSON.stringify(ex),
      });
    }
  }

  function _deleteOutofDateItemStock(producerStockIds) {
    try {
      producerStockIds.forEach(id => {
        record.delete({
            type: "customrecord_msg_producer_stock",
            id: id,
          });
      });
      // log.debug("_deleteOutofDateItemStock", `${producerStockIds.length} records deleted.`);
    } catch (ex) {
      log.debug("error in _deleteItemStock", ex);
    }
  }

  function checkSavedProducerStock(producerStockObj, savedVals) {
    if (
      producerStockObj.isAvail != savedVals["custrecord_msg_vsa_is_available"]
    ) {
      return true;
    } else if (
      producerStockObj.avail != savedVals["custrecord_msg_vsa_availability"]
    ) {
      return true;
    } else if (
      (producerStockObj.qty || savedVals["custrecord_msg_vsa_quantity"]) &&
      producerStockObj.qty != savedVals["custrecord_msg_vsa_quantity"]
    ) {
      return true;
    }
    return false;
  }

  function _processHorizonFile() {
    // log.debug("_processHorizonFile", "Triggered");
    let producerStocks = [];
    const fileObj = file.load({
      id: self.fileId,
    });
    const configObj = CONSTANTS.VENDOR_DATA_PROCESS[self.processId];
    const producerStockMap = _getAllProducerStock(self.vendorId);
    /*Starts: parse through XLSX lib*/
    let workbook = XLSX.read(fileObj.getContents(), { type: "base64" });
    let worksheet = workbook.Sheets[workbook.SheetNames[0]];
    let jsonFromCsv = XLSX.utils.sheet_to_json(worksheet);
    /*Ends: parse through XLSX lib*/

    for (let i = 0; i < jsonFromCsv.length; i++) {
      //we skip headers
      const producerStockObj = {
        vendorName: jsonFromCsv[i][configObj.SKU_CSV_KEY],
        avail: jsonFromCsv[i][configObj.AVAILABILITY_MAP.AVAIL_CSV_KEY],
        isAvail: configObj.AVAILABILITY_MAP.ONLINE.includes(jsonFromCsv[i][configObj.AVAILABILITY_MAP.AVAIL_CSV_KEY]),
        qty: null, //qty is not sent by this vendor
        producerStockId: null
      };

      if (producerStockMap[producerStockObj.vendorName]) {
        // CHECK IF DATA IS CHANGED OR NOT
        producerStockMap[producerStockObj.vendorName]["inCSV"] = true;
        const savedVals = producerStockMap[producerStockObj.vendorName].data;
        const isChanged = checkSavedProducerStock(producerStockObj, savedVals);
        producerStockObj.producerStockId = producerStockMap[producerStockObj.vendorName].id;
        if (!isChanged) continue; // don't add the csv row in array. No need to update as it is not changed.
      } 

      producerStocks.push({
        data: producerStockObj,
        toDelete: false,
        producerLogId: self.producerLogId,
        vendorId: self.vendorId,
      });
    }

    Object.keys(producerStockMap).forEach(stock => {
      if(producerStockMap[stock]['inCSV']) return;
      producerStocks.push({
          data: producerStockMap[stock],
          toDelete: true,
          producerLogId: self.producerLogId,
          vendorId: self.vendorId,
        });
    });

    return producerStocks;
  }

  function _processGlobeFile() {
    // log.debug("_processGlobeFile", "Triggered");
    const csvSkusLines = [],
      csvLines = [];
    let producerStocks = [];
    let lineSkuIndex = 0, lineAvailIndex = 4;

    const fileObj = file.load({
      id: self.fileId,
    });
    const producerStockMap = _getAllProducerStock(self.vendorId);
    const configObj = CONSTANTS.VENDOR_DATA_PROCESS[self.processId];

    fileObj.lines.iterator().each(function (line) {
      csvLines.push(line);
      line.value.replace(/","/g, ".");
      let lineVal = line.value.split(";");
      csvSkusLines.push(lineVal);
      return true;
    });
    
    for(let x = 0; x < csvSkusLines[0].length; x++) {
      if(csvSkusLines[0][x] == configObj.AVAILABILITY_MAP.AVAIL_CSV_KEY) {
        lineAvailIndex = x;
      }
    }
    

    for (let i = 1; i < csvSkusLines.length; i++) {
      const csvLine = csvSkusLines[i][lineSkuIndex].split(";");
      const lineSku = csvLine[0];
      const lineAvailId = csvSkusLines[i][lineAvailIndex];

      const producerStockObj = {
        vendorName: lineSku,
        avail: lineAvailId,
        isAvail: Number(lineAvailId) ? true : false, // if 0 not available (false), otherwise yes.
        qty: Number(lineAvailId),
        producerStockId: null
      };

      if (producerStockMap[producerStockObj.vendorName]) {
        // CHECK IF DATA IS CHANGED OR NOT
        producerStockMap[producerStockObj.vendorName]["inCSV"] = true;
        producerStockObj.producerStockId = producerStockMap[producerStockObj.vendorName].id;
        const savedVals = producerStockMap[producerStockObj.vendorName].data;
        const isChanged = checkSavedProducerStock(producerStockObj, savedVals);
        if (!isChanged) continue; // don't add the csv row in array. No need to update as it is not changed.
      }

      producerStocks.push({
        data: producerStockObj,
        producerLogId: self.producerLogId,
        vendorId: self.vendorId,
        toDelete: false,
      });
    }

    Object.keys(producerStockMap).forEach(stock => {
      if(producerStockMap[stock]['inCSV']) return;
      producerStocks.push({
          data: producerStockMap[stock],
          toDelete: true,
          producerLogId: self.producerLogId,
          vendorId: self.vendorId,
        });
    });

    return producerStocks;
  }

  function _processHoecoFile() {
    // log.debug("_processHoecoFile", "Triggered");
    const csvSkusLines = [],
      csvLines = [];
    let producerStocks = [];
    const fileObj = file.load({
      id: self.fileId,
    });
    const producerStockMap = _getAllProducerStock(self.vendorId);
    // let configObj = CONSTANTS.VENDOR_DATA_PROCESS[self.processId];

    fileObj.lines.iterator().each(function (line) {
      csvLines.push(line);
      line.value.replace(/","/g, ".");
      let lineVal = line.value.split(";");
      csvSkusLines.push(lineVal);
      return true;
    });

    for (let i = 0; i < csvSkusLines.length; i++) {
      const csvLine = csvSkusLines[i][0].split(";");
      const lineSku = csvLine[0];
      const lineAvailId = Number(csvSkusLines[i][2]);

      const producerStockObj = {
        vendorName: lineSku,
        avail: lineAvailId,
        isAvail: Number(lineAvailId) ? true : false,
        qty: lineAvailId,
        producerStockId: null
      };

      if (producerStockMap[producerStockObj.vendorName]) {
        // CHECK IF DATA IS CHANGED OR NOT
        producerStockMap[producerStockObj.vendorName]["inCSV"] = true;
        const savedVals = producerStockMap[producerStockObj.vendorName].data;
        producerStockObj.producerStockId = producerStockMap[producerStockObj.vendorName].id;
        const isChanged = checkSavedProducerStock(producerStockObj, savedVals);
        if (!isChanged) continue; // don't add the csv row in array. No need to update as it is not changed.
      }

      producerStocks.push({
        data: producerStockObj,
        producerLogId: self.producerLogId,
        vendorId: self.vendorId,
        toDelete: false,
      });
    }

    Object.keys(producerStockMap).forEach(stock => {
      if(producerStockMap[stock]['inCSV']) return;
      producerStocks.push({
          data: producerStockMap[stock],
          toDelete: true,
          producerLogId: self.producerLogId,
          vendorId: self.vendorId,
        });
    });

    return producerStocks;
  }

  function _processCarsonFile() {
    // log.debug("_processCarsonFile", "Triggered");
    const csvSkusLines = [],
      csvLines = [];
    let producerStocks = [];
    const fileObj = file.load({
      id: self.fileId,
    });
    const producerStockMap = _getAllProducerStock(self.vendorId);
    const configObj = CONSTANTS.VENDOR_DATA_PROCESS[self.processId];

    fileObj.lines.iterator().each(function (line) {
      csvLines.push(line);
      line.value.replace(/","/g, ".");
      let lineVal = line.value.split(";");
      csvSkusLines.push(lineVal);
      return true;
    });

    for (let i = 1; i < csvSkusLines.length; i++) {
      const csvLine = csvSkusLines[i][0].split(";");
      const lineSku = csvLine[0];
      const lineAvailId = csvSkusLines[i][5];

      const producerStockObj = {
        vendorName: lineSku,
        avail: lineAvailId,
        isAvail: configObj.AVAILABILITY_MAP.ONLINE.includes(lineAvailId)
          ? true
          : false,
        qty: null,
        producerStockId: null
      };

      if (producerStockMap[producerStockObj.vendorName]) {
        // CHECK IF DATA IS CHANGED OR NOT
        producerStockMap[producerStockObj.vendorName]["inCSV"] = true;
        producerStockObj.producerStockId = producerStockMap[producerStockObj.vendorName].id;
        const savedVals = producerStockMap[producerStockObj.vendorName].data;
        const isChanged = checkSavedProducerStock(producerStockObj, savedVals);
        if (!isChanged) continue; // don't add the csv row in array. No need to update as it is not changed.
      }

      producerStocks.push({
        data: producerStockObj,
        producerLogId: self.producerLogId,
        vendorId: self.vendorId,
        toDelete: false,
      });
    }

    Object.keys(producerStockMap).forEach(stock => {
      if(producerStockMap[stock]['inCSV']) return;
      producerStocks.push({
          data: producerStockMap[stock],
          toDelete: true,
          producerLogId: self.producerLogId,
          vendorId: self.vendorId,
        });
    });

    return producerStocks;
  }

  function _processAmewiFile() {
    // log.debug("_processAmewiFile", "Triggered");
    const csvSkusLines = [],
      csvLines = [];
    let producerStocks = [];
    const fileObj = file.load({
      id: self.fileId,
    });
    const configObj = CONSTANTS.VENDOR_DATA_PROCESS[self.processId];
    const producerStockMap = _getAllProducerStock(self.vendorId);

    fileObj.lines.iterator().each(function (line) {
      csvLines.push(line);
      line.value.replace(/","/g, ".");
      let lineVal = line.value.split(";");
      csvSkusLines.push(lineVal);
      return true;
    });

    for (let i = 0; i < csvSkusLines.length; i++) {
      const csvLine = csvSkusLines[i][0].split(";");
      const lineSku = csvLine[0];
      const lineAvailId = Number(csvSkusLines[i][1]);

      const producerStockObj = {
        vendorName: lineSku,
        avail: lineAvailId,
        isAvail: configObj.AVAILABILITY_MAP.ONLINE.includes(lineAvailId)
          ? true
          : false,
        qty: null,
        producerStockId: null
      };

      if (producerStockMap[producerStockObj.vendorName]) {
        // CHECK IF DATA IS CHANGED OR NOT
        producerStockMap[producerStockObj.vendorName]["inCSV"] = true;
        producerStockObj.producerStockId = producerStockMap[producerStockObj.vendorName].id;
        const savedVals = producerStockMap[producerStockObj.vendorName].data;
        const isChanged = checkSavedProducerStock(producerStockObj, savedVals);
        if (!isChanged) continue; // don't add the csv row in array. No need to update as it is not changed.
      }

      producerStocks.push({
        data: producerStockObj,
        producerLogId: self.producerLogId,
        vendorId: self.vendorId,
        toDelete: false,
      });
    }
    Object.keys(producerStockMap).forEach(stock => {
      if(producerStockMap[stock]['inCSV']) return;
      producerStocks.push({
          data: producerStockMap[stock],
          toDelete: true,
          producerLogId: self.producerLogId,
          vendorId: self.vendorId,
        });
    });
    
    return producerStocks;
  }

  return {
    getInputData: getInputData,
    map: map,
    summarize: summarize,
  };
});
